//
//  AppDelegate.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import Locksmith
import RealmSwift
import VK_ios_sdk
import GoogleMaps
import GooglePlaces
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var authManager: AuthorizationManager!
    var pushManager: PushNotificationManager!
    let gcmMessageIDKey = "gcm.message_id"
    
    private lazy var configurators: [ConfiguratorProtocol] = {
        return [
            AppearanceConfigurator()
        ]
    }()

    private lazy var linkManager = OpenAppByLinkManager()
    
    private lazy var realm = try! Realm()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        pushManager = PushNotificationManager()
        pushManager.registerForPushNotifications()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        authManager = AuthorizationManager()
        for configurator in configurators {
            configurator.configure()
        }
        if UserDefaults.standard.bool(forKey: "isAuthorized") {
            TabBarModule.create().present()
        } else {
            if UserDefaults.standard.bool(forKey: "isShowedCarousel") {
                presentWelcomeVC()
            } else {
                let viewController = UIStoryboard(name: "WelcomeCarouselViewController", bundle: nil).instantiateViewController(withIdentifier: "WelcomeCarouselViewController") as! WelcomeCarouselViewController
                window?.rootViewController = viewController
            }
        }

        
        GMSServices.provideAPIKey("AIzaSyDu9B2Rt2vRmG7_5UHeiDWWPaT0NHg7DKs")
        GMSPlacesClient.provideAPIKey("AIzaSyDu9B2Rt2vRmG7_5UHeiDWWPaT0NHg7DKs")
        return true
    }
    
    func logOut() {
        authManager.forceLogout()
        UserDefaults.standard.set(false, forKey: "isAuthorized")
        DispatchQueue.main.async {
            UIApplication.shared.unregisterForRemoteNotifications()
        }
        do {
            try Locksmith.deleteDataForUserAccount(userAccount: "myconvy")
        } catch {
            print(error)
        }
        pushManager.registerForPushNotifications()
        if let result = realm.objects(User.self).first {
            try! realm.write {
                realm.delete(result)
            }
        }
        presentWelcomeVC()
    }
    
    func presentWelcomeVC() {
        WelcomeModule.create().present()
    }

    // вызывается при клике через браузер(вконтакте)
    // выгружено/в фоне
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
        ) -> Bool {
        VKSdk.processOpen(url, fromApplication: UIApplication.OpenURLOptionsKey.sourceApplication.rawValue)
        if !UserDefaults.standard.bool(forKey: "isAuthorized") {
            return false
        }
        
        guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true),
            let params = components.queryItems,
            let value = params.last?.value else {
                print("Invalid URL or album path missing")
                return false
        }

        guard let externalLinkType = getExternalLinkTypeFromWebView(url: url) else { return false }
        linkManager.presentViewControllerIfNeeded(typeLink: externalLinkType, value: value)
        return true
    }

    private func getExternalLinkTypeFromWebView(url: URL) -> ExternalLink? {
        guard
            let componentsParameter = url.relativeString.split(separator: "/").last,
            let parseComponentsParameter = componentsParameter.split(separator: "?").first
        else { return nil }
        return ExternalLink(rawValue: String(parseComponentsParameter))
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            guard granted else { return }
            self.getNotificationSettings()
        }
    }

    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }

        let token = tokenParts.joined()
        UserDefaults.standard.set(token, forKey: "push_token")
        print("Device Token: \(token)")
    }

    // вызывается при клике на ссылку в ватсапе, телеге (приложение выгружено/в фоне)
    // таббар уже загружен
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {

        if !UserDefaults.standard.bool(forKey: "isAuthorized") {
            return false
        }

        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let incomingURL = userActivity.webpageURL,
            let components = NSURLComponents(url: incomingURL, resolvingAgainstBaseURL: true),
            let params = components.queryItems,
            let externalLinkType = params.first?.name,
            let value = params.first?.value
            else { return false }
        
        linkManager.presentViewControllerIfNeeded(typeLink: ExternalLink(rawValue: externalLinkType)!, value: value)
        return true
    }

}

extension AppDelegate {
    
    static var currentDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    static var currentWindow: UIWindow {
        return currentDelegate.window!
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
}
