//
//  ApplicationConfigurator.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


class ApplicationConfigurator: ConfiguratorProtocol {
    
    func configure() {
        TabBarModule.create().present()
    }
    
}
