//
//  AppearanceConfigurator.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 12/03/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class AppearanceConfigurator: ConfiguratorProtocol {
    func configure() {
        UINavigationBar.appearance().barTintColor = OrangeTheme.myconvyGray
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: OrangeTheme.myconvyGray,
                                                            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 18)]
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().tintColor = OrangeTheme.myconvyGray
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: OrangeTheme.myconvyGray], for: .normal)
        
        
    }
    
    
}
