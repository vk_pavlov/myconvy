//
//  PrivacySettingsViewInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 03/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol PrivacySettingsViewInput: class, Presentable {
    func setupInitialState()
    func setPrivacySettings(_ settings: [[Bool]])
}
