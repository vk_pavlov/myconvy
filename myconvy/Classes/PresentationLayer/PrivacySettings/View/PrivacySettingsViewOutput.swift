//
//  PrivacySettingsViewOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 03/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


protocol PrivacySettingsViewOutput {
    func viewIsReady()
    func didChangePrivacySettings(_ socPrivacy: Int, phonePrivacy: Int, locPrivacy: Int)
}
