//
//  PrivacySettingsViewController.swift
//  myconvy
//
//  Created by Евгений Каштанов on 03/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class PrivacySettingsViewController: UIViewController {
    
    @IBOutlet weak var privacySettingsTableView: UITableView!
    var output: PrivacySettingsViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    private let items = [["Никому", "Только подпискам", "Всем"], ["Никому", "Только подпискам", "Всем"]]
    private let titlesOfSections = ["Кому видны ссылки на мои соцсети:", "Кому виден мой номер телефона:"]
    private var privacyViewModels = [[Bool]]()
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            output.didChangePrivacySettings(selectedSocialPrivacy, phonePrivacy: selectedPhonePrivacy, locPrivacy: 0)
        }
    }
    
    private var selectedSocialPrivacy: Int {
        return privacyViewModels[0].firstIndex(of: true)!
    }
    
    private var selectedPhonePrivacy: Int {
        return privacyViewModels[1].firstIndex(of: true)!
    }
}

extension PrivacySettingsViewController: PrivacySettingsViewInput {
    func setPrivacySettings(_ settings: [[Bool]]) {
        privacyViewModels = settings
        privacySettingsTableView.reloadData()
    }
    
    func setupInitialState() {
        privacySettingsTableView.register(ChoiceSettingsCell.nib, forCellReuseIdentifier: ChoiceSettingsCell.identifier)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension PrivacySettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return titlesOfSections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return titlesOfSections[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChoiceSettingsCell.identifier, for: indexPath) as! ChoiceSettingsCell
        cell.isChoosen = privacyViewModels[indexPath.section][indexPath.row]
        cell.cellText = items[indexPath.section][indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        privacyViewModels[indexPath.section] = Array.init(repeating: false, count: 3)
        privacyViewModels[indexPath.section][indexPath.row] = !privacyViewModels[indexPath.section][indexPath.row]
        privacySettingsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}
