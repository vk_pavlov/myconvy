//
//  PrivacySettingsPresenter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 03/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class PrivacySettingsPresenter {
    weak var view: PrivacySettingsViewInput!
    var interactor: PrivacySettingsInteractorInput!
    var router: PrivacySettingsRouterInput!
    private let socPrivacy = CommonData.selfUser().socPrivacy
    private let phonePrivacy = CommonData.selfUser().phonePrivacy
    
    private var privacyViewModel: [[Bool]] {
        var result = [Array.init(repeating: false, count: 3), Array.init(repeating: false, count: 3)]
        for (index, _) in result[0].enumerated() {
            if index == socPrivacy {
                result[0][index] = true
            }
        }
        for (index, _) in result[1].enumerated() {
            if index == phonePrivacy {
                result[1][index] = true
            }
        }
        return result
    }
}

extension PrivacySettingsPresenter: PrivacySettingsViewOutput {
    func didChangePrivacySettings(_ socPrivacy: Int, phonePrivacy: Int, locPrivacy: Int) {
        interactor.editUserPrivacy(socPrivacy, phonePrivacy: phonePrivacy, locPrivacy: locPrivacy)
    }
    
    func viewIsReady() {
        view.setupInitialState()
        view.setPrivacySettings(privacyViewModel)
    }
}

extension PrivacySettingsPresenter: PrivacySettingsInteractorOutput {
    
    
    
}

extension PrivacySettingsPresenter: PrivacySettingsModuleInput {
    
    var viewController: UIViewController {
        return view.viewController
    }
    
    func present(from: UIViewController) {
        view.present(fromViewController: from)
    }
}
