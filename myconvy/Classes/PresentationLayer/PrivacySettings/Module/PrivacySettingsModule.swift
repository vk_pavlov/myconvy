//
//  PrivacySettingsModule.swift
//  myconvy
//
//  Created by Евгений Каштанов on 03/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class PrivacySettingsModule {
    class func create() -> PrivacySettingsModuleInput {
        let storyboard = UIStoryboard(name: "PrivacySettings", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PrivacySettingsViewController") as! PrivacySettingsViewController
        let presenter = PrivacySettingsPresenter()
        let interactor = PrivacySettingsInteractor()
        let router = PrivacySettingsRouter()
        let networkService = NetworkService()
        
        
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        interactor.output = presenter
        interactor.userDataFetcher = NetworkUserDataFetcher()
        return presenter
    }
}
