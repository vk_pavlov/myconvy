//
//  PrivacySettingsIneractor.swift
//  myconvy
//
//  Created by Евгений Каштанов on 03/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import RealmSwift

class PrivacySettingsInteractor {
    weak var output: PrivacySettingsInteractorOutput!
    var userDataFetcher: UserDataFetcher!
}

extension PrivacySettingsInteractor: PrivacySettingsInteractorInput {
    func editUserPrivacy(_ socPrivacy: Int, phonePrivacy: Int, locPrivacy: Int) {
        userDataFetcher.editUserPrivacy(phonePrivacy: phonePrivacy, socPrivacy: socPrivacy, locPrivacy: locPrivacy) { (response, error) in
            guard response != nil else { return }
            let selfUser = CommonData.selfUser()
            let realm = try! Realm()
            try! realm.write {
                selfUser.socPrivacy = socPrivacy
                selfUser.phonePrivacy = phonePrivacy
            }
        }
    }
}
