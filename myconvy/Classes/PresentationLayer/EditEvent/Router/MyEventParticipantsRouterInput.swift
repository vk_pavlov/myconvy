//
//  MyEventParticipantsRouterInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol MyEventParticipantsRouterInput {
    func presentUserProfile(from: UIViewController, user: User)
}
