//
//  MyEventParticipantsModuleInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import RealmSwift

protocol MyEventParticipantsModuleInput {
    var viewController: UIViewController { get }
    func present(from viewController: UIViewController, listParticipants: List<User>, listRespondedUsers: List<User>)
}
