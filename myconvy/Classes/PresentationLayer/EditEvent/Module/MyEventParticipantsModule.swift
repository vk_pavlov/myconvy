//
//  MyEventParticipantsModule.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class MyEventParticipantsModule {
    class func create() -> MyEventParticipantsModuleInput {
        let storyboard = UIStoryboard(name: "MyEventParticipants", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MyEventParticipantsViewController") as! MyEventParticipantsViewController
        let presenter = MyEventParticipantsPresenter()
        let interactor = MyEventParticipantsInteractor()
        let router = MyEventParticipantsRouter()
        
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        
        interactor.output = presenter
        return presenter
    }
}
