//
//  MyEventInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import RealmSwift

protocol MyEventParticipantsViewInput: class, Presentable {
    func setUsers(listParticipants: List<User>, listRespondedUsers: List<User>)
    func setupInitialState()
}
