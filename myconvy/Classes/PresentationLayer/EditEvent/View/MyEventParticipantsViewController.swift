//
//  MyEventViewController.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import RealmSwift

class MyEventParticipantsViewController: UIViewController {
    
    
    @IBOutlet weak var usersTable: UITableView!
    
    
    var output: MyEventParticipantsViewOutput!
    var listParticipants: List<User>!
    var listRespondedUsers: List<User>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    
    
}

extension MyEventParticipantsViewController: MyEventParticipantsViewInput {
    
    func setUsers(listParticipants: List<User>, listRespondedUsers: List<User>) {
        self.listParticipants = listParticipants
        self.listRespondedUsers = listRespondedUsers
    }
    
    func setupInitialState() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

// Mark: tableView configuration

extension MyEventParticipantsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if listRespondedUsers.count != 0 {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return listParticipants.count
        case 1:
            return listRespondedUsers.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParticipantCell", for: indexPath) as! ParticipantCell
            cell.configure(user: listParticipants[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RespondedUserCell", for: indexPath) as! RespondedUserCell
            cell.configure(user: listRespondedUsers[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sections = ["Участники", "Отклики"]
        return sections[section]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 84
        } else {
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return true
        } else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if indexPath.section == 0 {
            let deleteAction = UITableViewRowAction(style: .destructive, title: "Удалить") {
                _, indexPath in
                self.listParticipants.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            return [deleteAction]
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let user = listParticipants[indexPath.row]
            tableView.deselectRow(at: indexPath, animated: true)
            output.didUserCellPressed(user)
        } else {
            let user = listRespondedUsers[indexPath.row]
            tableView.deselectRow(at: indexPath, animated: true)
            output.didUserCellPressed(user)
        }
    }
}
