//
//  MyEventParticipantsOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


protocol MyEventParticipantsViewOutput {
    func viewIsReady()
    func didUserCellPressed(_ user: User)
}
