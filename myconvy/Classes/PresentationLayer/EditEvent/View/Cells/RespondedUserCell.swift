//
//  RespondedUserCell.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class RespondedUserCell: UITableViewCell {
    
    @IBOutlet weak var photoUserImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var agePlusCityLabel: UILabel!
    @IBOutlet weak var acceptRespondButton: UIButton!
    @IBOutlet weak var rejectRespondButton: UIButton!
    
    func configure(user: User) {
        photoUserImageView.layer.cornerRadius = photoUserImageView.frame.height / 2
        photoUserImageView.image = UIImage(named: user.photoProfile)
        userNameLabel.text = user.name + " " + user.lastName
        if !user.birthDate.isEmpty {
            agePlusCityLabel.text = user.city + ", " + user.birthDate
            
        } else {
            agePlusCityLabel.text = user.city
        }
        acceptRespondButton.layer.cornerRadius = acceptRespondButton.frame.height / 2
        acceptRespondButton.backgroundColor = UIColor.myconvyYellowTranslucent
        acceptRespondButton.layer.borderWidth = 1
        acceptRespondButton.layer.borderColor = UIColor.myconvyOrange.cgColor
        rejectRespondButton.layer.cornerRadius = rejectRespondButton.frame.height / 2
        rejectRespondButton.backgroundColor = UIColor.white
        rejectRespondButton.layer.borderColor = UIColor.gray.cgColor
        rejectRespondButton.layer.borderWidth = 1
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
