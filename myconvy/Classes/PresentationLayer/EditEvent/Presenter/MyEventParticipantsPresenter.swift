//
//  MyEventParticipantsPresenter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import RealmSwift

class MyEventParticipantsPresenter {
    weak var view: MyEventParticipantsViewInput!
    var interactor: MyEventParticipantsInteractorInput!
    var router: MyEventParticipantsRouterInput!
    
    var listParticipants: List<User>!
    var listRespondedUsers: List<User>!
    
}

extension MyEventParticipantsPresenter: MyEventParticipantsViewOutput {
    func viewIsReady() {
        view.setUsers(listParticipants: listParticipants, listRespondedUsers: listRespondedUsers)
        view.setupInitialState()
    }
    
    func didUserCellPressed(_ user: User) {
        router.presentUserProfile(from: view.viewController, user: user)
    }
    
    
}

extension MyEventParticipantsPresenter: MyEventParticipantsInteractorOutput {
    
    
    
}

extension MyEventParticipantsPresenter: MyEventParticipantsModuleInput {

    func present(from: UIViewController, listParticipants: List<User>, listRespondedUsers: List<User>) {
        self.listParticipants = listParticipants
        self.listRespondedUsers = listRespondedUsers
        view.present(fromViewController: from)
    }
    
    var viewController: UIViewController {
        return view.viewController
    }
    
    
}
