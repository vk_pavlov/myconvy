//
//  SelectedEventSelectedEventConfigurator.swift
//  myconvy
//
//  Created by jumper17 on 12/11/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

class SelectedEventModule {
    class func create() -> SelectedEventModuleInput {
        let storyboard = UIStoryboard(name: "SelectedEvent", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectedEventViewController") as! SelectedEventViewController
        let presenter = SelectedEventPresenter()
        let interactor = SelectedEventInteractor()
        let router = SelectedEventRouter()
        
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        interactor.output = presenter
        
        interactor.eventDataFetcher = NetworkEventDataFetcher()
        
        return presenter
    }

}
