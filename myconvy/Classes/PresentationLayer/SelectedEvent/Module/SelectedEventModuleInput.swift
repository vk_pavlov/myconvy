//
//  SelectedEventSelectedEventInitializer.swift
//  myconvy
//
//  Created by jumper17 on 12/11/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit


protocol SelectedEventModuleInput {
    var viewController: UIViewController { get }
    func present(from: UIViewController, eventId: Int)
}
