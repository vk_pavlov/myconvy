//
//  SelectedEventSelectedEventRouterInput.swift
//  myconvy
//
//  Created by jumper17 on 12/11/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

protocol SelectedEventRouterInput {
    func presentParticipiantsEvent(from: UIViewController, participiantsId: [Int], ordersId: [Int]?, eventId: Int)
    func presentInfoAboutAuthorEvent(from: UIViewController, userId: Int)
    func presentEditEvent(from: UIViewController, eventViewModel: NewEventViewModel, eventId: Int)
    func presentPlace(from: UIViewController, coordinate: Place, isParticipant: Bool)
}
