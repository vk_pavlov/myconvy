//
//  SelectedEventSelectedEventRouter.swift
//  myconvy
//
//  Created by jumper17 on 12/11/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

class SelectedEventRouter: SelectedEventRouterInput {
    func presentPlace(from: UIViewController, coordinate: Place, isParticipant: Bool) {
        MapModule.create().present(from: from, mapPresentationMode: .selectedMarker, initialCoordinate: coordinate, output: nil, isParticipant: isParticipant)
    }
    
    func presentParticipiantsEvent(from: UIViewController, participiantsId: [Int], ordersId: [Int]?, eventId: Int) {
        EditEventParticipantsModule.create().present(from: from, users: participiantsId, otherUsers: ordersId, eventId: eventId)
    }

    func presentEditEvent(from: UIViewController, eventViewModel: NewEventViewModel, eventId: Int) {
        NewEventModule.create().present(from: from, viewModel: eventViewModel, eventId: eventId)
    }
    
    func presentInfoAboutAuthorEvent(from: UIViewController, userId: Int) {
        if CommonData.selfUser().id == userId {
            ProfileModule.create().present(from:from)
        } else {
            UserProfileModule.create().present(from: from, userId: userId)
        }
    }
    
}
