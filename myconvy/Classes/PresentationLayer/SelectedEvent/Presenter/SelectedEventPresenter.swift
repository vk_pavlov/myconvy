//
//  SelectedEventSelectedEventPresenter.swift
//  myconvy
//
//  Created by jumper17 on 12/11/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit
import GoogleMaps

class SelectedEventPresenter: NSObject {

    weak var view: SelectedEventViewInput!
    var interactor: SelectedEventInteractorInput!
    var router: SelectedEventRouterInput!
    private var id: Int!
    private var selectedEvent: EventTwo!
    private let selfUserId = CommonData.selfUser().id
    private var relationState: RelationButtonState = .none
    private let locationManager = CLLocationManager()
    private var place: Place!
    private let geocoder = GMSGeocoder()
    
    private func selectedEventViewModel(from: EventTwo) -> SelectedEventViewModel {
        place = from.place
        let infoAboutCreator = CreatorInfo(userId: from.creator.id, name: from.creator.name, moreInfo: from.creator.universityPlusAge, avatar: from.creator.avatar, online: from.creator.online)
        let headerInfo = HeaderSelectedEventViewModel(category: from.category, title: from.title, date: Date(timeIntervalSince1970: from.startDate))
        let conditionsEventViewViewModel = ConditionsEventViewViewModel(participantsCount: from.partCount,
                                                                        participantsGender: from.partSex,
                                                                        requestToEventCount: from.orders?.count)
        return SelectedEventViewModel(headerViewModel: headerInfo,
                                      description: from.description,
                                      participantsCount: from.partCount,
                                      gender: from.partSex,
                                      place: from.place,
                                      distance: 0,
                                      descriptionPlace: from.plDesc ?? "",
                                      creatorInfo: infoAboutCreator,
                                      conditionsEventViewViewModel: conditionsEventViewViewModel)
        
    }
    
    private func updateRelationState() {
        if selectedEvent.status == 3 {
            relationState = .eventFinished
            return
        }
        
        switch selectedEvent.callerStatus {
        case 1:
            if selfUserId == selectedEvent.creator.id {
                relationState = .iAmCreator
                break
            }
            relationState = .iAmParticipant
        case 2: relationState = .waitingApprove
        default: relationState = .none
        }
    }
    
    private var editEventViewModel: NewEventViewModel {
        return NewEventViewModel(title: selectedEvent.title,
            place: place,
            date: selectedEvent.startDate,
            category: selectedEvent.category,
            descriptionEvent: selectedEvent.description,
            descriptionPlace: selectedEvent.plDesc,
            sex: selectedEvent.partSex,
            partLimit: selectedEvent.partLimit,
            privacy: selectedEvent.privacy)
    }
    
    private func updateRelationStateAfterFinishedOperation() {
        switch relationState {
        case .none: relationState = .waitingApprove
        case .iAmParticipant, .waitingApprove: relationState = .none
        default: break
        }
    }
    
    private var isParticipant: Bool {
        return selectedEvent.callerStatus == 1
    }
    
}

extension SelectedEventPresenter: SelectedEventModuleInput {
    var viewController: UIViewController {
        return view.viewController
    }
    
    func present(from: UIViewController, eventId: Int) {
        self.id = eventId
        view.present(fromViewController: from)
    }
}

extension SelectedEventPresenter: SelectedEventViewOutput {
    func didShowAddressButtonPressed() {
        router.presentPlace(from: view.viewController, coordinate: place, isParticipant: isParticipant)
    }
    
    var titleEvent: String {
        return selectedEvent.title
    }
    
    var eventId: Int {
        return id
    }
    
    var relationButtonState: RelationButtonState {
        return relationState
    }
    
    func didCallerStatusButtonPressed() {
        switch relationState {
        case .none: interactor.joinMeetById(id)
        case .iAmParticipant, .waitingApprove: interactor.leaveMeet(id)
        case .iAmCreator: router.presentEditEvent(from: view.viewController, eventViewModel: editEventViewModel, eventId: id)
        default: break
        }
    }
    
    func didParticipantsButtonPressed() {
        guard let participants = selectedEvent.participants else { return }
        router.presentParticipiantsEvent(from: view.viewController, participiantsId: participants, ordersId: selectedEvent.orders, eventId: id)
    }
    
    func didSendReport(message: ReportAction) {
        interactor.sendReport(eventId: id, message: message)
    }

    func didDeleteEvent() {
        interactor.deleteMeetByID(id)
    }

    func didUpdateInfoAboutEvent() {
        interactor.getEventById(id)
    }

    func didInfoAboutAuthorEventPressed(_ userId: Int) {
        router.presentInfoAboutAuthorEvent(from: view.viewController, userId: userId)
    }
    
    func viewIsReady() {
        locationManager.delegate = self
        view.setupInitialState()
    }

}

extension SelectedEventPresenter: SelectedEventInteractorOutput {
    func didFailureGetEvent(error: String) {
        view.showErrorAlert(error)
    }
    
    func didReciveLeaveFromEvent() {
        updateRelationStateAfterFinishedOperation()
        view.updateStateButtonAfterRespond()
    }
    
    func didReciveDeleteEvent() {
        view.dissmiss()
    }
    
    func didReciveEvent(_ event: EventTwo) {
        locationManager.startUpdatingLocation()
        selectedEvent = event
        updateRelationState()
        let selectedEventViewModel = self.selectedEventViewModel(from: event)
        view.setSelectedEventViewModel(selectedEventViewModel)
        if isParticipant {
            let coordinate = CLLocationCoordinate2D(latitude: place.lat, longitude: place.lng)
            geocoder.reverseGeocodeCoordinate(coordinate) { [weak self] (response, error) in
                if let thoroughfare = response?.firstResult()?.thoroughfare {
                    self?.view.setAddress(thoroughfare)
                }
            }
        }
    }

    func didFailedSendReport() {
        view.showErrorAlert(error: nil)
    }

    func didReciveSendReport() {
        view.showSuccessAlert()
    }

    func didFailureJoinMeet(error: Error) {
        view.showErrorAlert(error: error)
    }

    func didReciveJoinMeetById() {
        updateRelationStateAfterFinishedOperation()
        view.updateStateButtonAfterRespond()
    }
}

extension SelectedEventPresenter: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard place != nil else { return }
        locationManager.stopUpdatingLocation()
        let toPoint = CLLocation(latitude: place.lat, longitude: place.lng)
        guard let lastMyLocation = locations.last else { return }
        view.setDistance(lastMyLocation.distance(from: toPoint))
    }
}
