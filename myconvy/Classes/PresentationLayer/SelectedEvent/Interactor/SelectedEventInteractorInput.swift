//
//  SelectedEventSelectedEventInteractorInput.swift
//  myconvy
//
//  Created by jumper17 on 12/11/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import Foundation

protocol SelectedEventInteractorInput {
    func getEventById(_ id: Int)
    func joinMeetById(_ eventId: Int)
    func leaveMeet(_ eventId: Int)
    func deleteMeetByID(_ eventId: Int)
    func sendReport(eventId: Int, message: ReportAction)
}
