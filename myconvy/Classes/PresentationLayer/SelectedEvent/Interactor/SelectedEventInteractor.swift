//
//  SelectedEventSelectedEventInteractor.swift
//  myconvy
//
//  Created by jumper17 on 12/11/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

class SelectedEventInteractor {
    weak var output: SelectedEventInteractorOutput!
    var eventDataFetcher: EventDataFetcher!
}

extension SelectedEventInteractor: SelectedEventInteractorInput {
    func sendReport(eventId: Int, message: ReportAction) {
        eventDataFetcher.sendReport(eventId: eventId, action: message) { (response, error) in
            if response != nil {
                self.output.didReciveSendReport()
            }
        }
    }

    func deleteMeetByID(_ eventId: Int) {
        eventDataFetcher.deleteMeetByID(eventId: eventId) { (response, error) in
            if response != nil {
                self.output.didReciveDeleteEvent()
            }
        }
    }

    func leaveMeet(_ eventId: Int) {
        eventDataFetcher.leaveMeet(eventId: eventId) { (response, error) in
            if response != nil {
                self.output.didReciveLeaveFromEvent()
            }
        }
    }
    
    func joinMeetById(_ eventId: Int) {
        eventDataFetcher.joinMeetByID(eventId: eventId) { (response, error) in
            if response != nil {
                self.output.didReciveJoinMeetById()
            }
        }
    }
    
    func getEventById(_ id: Int) {
        eventDataFetcher.getMeetByID(eventId: id) {  [weak self] (selectedEventResponse, error) in
            guard let self = self else { return }
            if let selectedEventResponse = selectedEventResponse {
                self.output.didReciveEvent(selectedEventResponse.meetInfo)
            }
            if let problem = error?.problem {
                self.output.didFailureGetEvent(error: problem)
            }
        }
    }
    
}
