//
//  SelectedEventSelectedEventInteractorOutput.swift
//  myconvy
//
//  Created by jumper17 on 12/11/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import Foundation

protocol SelectedEventInteractorOutput: class {
    func didReciveEvent(_ event: EventTwo)
    func didReciveJoinMeetById()
    func didReciveLeaveFromEvent()
    func didFailureJoinMeet(error: Error)
    func didFailureGetEvent(error: String)
    func didReciveDeleteEvent()
    
    func didReciveSendReport()
    func didFailedSendReport()
}
