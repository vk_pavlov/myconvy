//
//  SelectedEventSelectedEventViewInput.swift
//  myconvy
//
//  Created by jumper17 on 12/11/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

protocol SelectedEventViewInput: class, Presentable {
    func setupInitialState()
    func setSelectedEventViewModel(_ model: SelectedEventViewModel)
    func updateStateButtonAfterRespond()
    func showErrorAlert(error: Error?)
    func showSuccessAlert()
    func setDistance(_ distance: Double)
    func setAddress(_ address: String)
    func showErrorAlert(_ error: String)
}
