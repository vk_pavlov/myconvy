//
//  SelectedEventSelectedEventViewOutput.swift
//  myconvy
//
//  Created by jumper17 on 12/11/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

enum ReportAction: String {
    case insult = "Оскорбления"
    case obsceneLanguage = "Ненормативная лексика"
    case spam = "Спам"
}

protocol SelectedEventViewOutput {
    func viewIsReady()
    func didParticipantsButtonPressed()
    func didInfoAboutAuthorEventPressed(_ userId: Int)
    func didCallerStatusButtonPressed()
    func didUpdateInfoAboutEvent()
    func didDeleteEvent()
    func didSendReport(message: ReportAction)
    func didShowAddressButtonPressed()
    
    var relationButtonState: RelationButtonState { get }
    var titleEvent: String { get }
    var eventId: Int { get }
}
