//
//  SelectedEventSelectedEventViewController.swift
//  myconvy
//
//  Created by jumper17 on 12/11/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

class SelectedEventViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    
    var output: SelectedEventViewOutput!
    
    private let spinner = Spinner(style: .gray)
    private let selectedEventHeaderView = SelectedEventHeaderView()
    private let descriptionEventView = DescriptionView(title: "Описание", placeholder: "")
    private let conditionsEventView = ConditionsEventView()
    private let mapsPositionView = MapsPositionView(buttonTitle: "Показать на карте")
    
    private let addressLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.contentMode = .left
        label.numberOfLines = 0
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        return label
    }()
    
    private let addressLabelView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return view
    }()
    
    private let descriptionPlaceView = DescriptionView(title: "Описание места", placeholder: "")
    private let creatorView = CreatorView()
    
    private var callerStatusButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 24)
        button.addTarget(self, action: #selector(callerStatusButtonPressed), for: .touchUpInside)
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return button
    }()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        creatorView.delegate = self
        descriptionEventView.isUserInteractionEnabled = false
        descriptionEventView.isClearBackgroundTextView = true
        descriptionPlaceView.isUserInteractionEnabled = false
        descriptionPlaceView.isClearBackgroundTextView = true
        
        conditionsEventView.delegate = self
        mapsPositionView.delegate = self
        
        stackView.addArrangedSubview(selectedEventHeaderView)
        stackView.addArrangedSubview(descriptionEventView)
        stackView.addArrangedSubview(conditionsEventView)
        stackView.addArrangedSubview(mapsPositionView)
        
        addressLabelView.addSubview(addressLabel)
        addressLabel.leadingAnchor.constraint(equalTo: addressLabelView.leadingAnchor, constant: 16).isActive = true
        addressLabel.trailingAnchor.constraint(equalTo: addressLabelView.trailingAnchor, constant: -16).isActive = true
        addressLabel.topAnchor.constraint(equalTo: addressLabelView.topAnchor).isActive = true
        addressLabel.bottomAnchor.constraint(equalTo: addressLabelView.bottomAnchor).isActive = true
        stackView.addArrangedSubview(addressLabelView)
        
        stackView.addArrangedSubview(descriptionPlaceView)
        stackView.addArrangedSubview(creatorView)
        
        let viewWithButton = UIView()
        viewWithButton.translatesAutoresizingMaskIntoConstraints = false
        viewWithButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
        viewWithButton.addSubview(callerStatusButton)
        
        callerStatusButton.centerYAnchor.constraint(equalTo: viewWithButton.centerYAnchor).isActive = true
        callerStatusButton.leadingAnchor.constraint(equalTo: viewWithButton.leadingAnchor, constant: 16).isActive = true
        callerStatusButton.trailingAnchor.constraint(equalTo: viewWithButton.trailingAnchor, constant: -16).isActive = true
        
        stackView.addArrangedSubview(viewWithButton)
        
        scrollView.contentInset.bottom = 60
        scrollView.alwaysBounceVertical = true
        //spinner.setStateSpinner(enable: true, view: self.view)
        addressLabelView.isHidden = true
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.didUpdateInfoAboutEvent()
    }

    // MARK: Private
    
    @objc private func callerStatusButtonPressed() {
        if output.relationButtonState == .iAmParticipant {
            let actionSheet = UIAlertController(title: "Вы действительно хотите покинуть событие?", message: nil, preferredStyle: .actionSheet)
            let cancel = UIAlertAction(title: "Отмена", style: .cancel)
            let insult = UIAlertAction(title: "Покинуть", style: .destructive) { (UIAlertAction) in
                //self.spinner.setStateSpinner(enable: false, view: self.view)
                self.callerStatusButton.isEnabled = false
                self.output.didCallerStatusButtonPressed()
            }
            actionSheet.addAction(cancel)
            actionSheet.addAction(insult)
            present(actionSheet, animated: true, completion: nil)
        } else {
            if output.relationButtonState != .iAmCreator {
                callerStatusButton.isEnabled = false
            }
            output.didCallerStatusButtonPressed()
        }
    }

    private func showReportAlertController() {
        let reportAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel)
        
        let insult = UIAlertAction(title: "Оскорбления", style: .destructive) { (UIAlertAction) in
            self.output.didSendReport(message: .insult)
        }
        
        let obsceneLanguage = UIAlertAction(title: "Нецензурная лексика", style: .destructive) { (UIAlertAction) in
            self.output.didSendReport(message: .obsceneLanguage)
        }
        
        let spam = UIAlertAction(title: "Это спам", style: .destructive) { (UIAlertAction) in
            self.output.didSendReport(message: .spam)
        }
        
        reportAlertController.addAction(insult)
        reportAlertController.addAction(obsceneLanguage)
        reportAlertController.addAction(spam)
        reportAlertController.addAction(cancel)
        present(reportAlertController, animated: true, completion: nil)
    }

    private func configureBackgroundRelationButton() {
        switch output.relationButtonState {
        case .none:
            callerStatusButton.backgroundColor = OrangeTheme.myconvyOrange
            callerStatusButton.setTitleColor(.white, for: .normal)
            callerStatusButton.layer.borderWidth = 0
        case .eventFinished:
            callerStatusButton.backgroundColor = .white
            callerStatusButton.layer.borderWidth = 2
            callerStatusButton.layer.borderColor = OrangeTheme.myconvyOrange.cgColor
            callerStatusButton.setTitleColor(OrangeTheme.myconvyOrange, for: .normal)
        default:
            callerStatusButton.backgroundColor = .white
            callerStatusButton.layer.borderWidth = 2
            callerStatusButton.layer.borderColor = OrangeTheme.myconvyOrange.cgColor
            callerStatusButton.setTitleColor(OrangeTheme.myconvyOrange, for: .normal)
        }
    }
    
    private func confugureTitleRelationButton() {
        switch output.relationButtonState {
        case .none: callerStatusButton.setTitle("Откликнуться", for: .normal)
        case .eventFinished: callerStatusButton.setTitle("Событие завершено", for: .normal)
        case .iAmParticipant: callerStatusButton.setTitle("Вы участник!", for: .normal)
        case .waitingApprove: callerStatusButton.setTitle("Отклик отправлен", for: .normal)
        case .iAmCreator: callerStatusButton.setTitle("Редактировать", for: .normal)
        }
    }
    
    private func deleteEventIfNeeded() {
        let alertViewController = UIAlertController(title: "Внимание", message: "Вы точно хотите удалить событие?", preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: "Удалить", style: .destructive) { (UIAlertAction) in
            self.output.didDeleteEvent()
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
        alertViewController.addAction(deleteAction)
        alertViewController.addAction(cancelAction)
        present(alertViewController, animated: true, completion: nil)
    }

    // MARK: Actions

    @IBAction func rightBarButtonPressed(_ sender: UIBarButtonItem) {
        let mainAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel)
        let sharedAction = UIAlertAction(title: "Поделиться", style: .default) { (UIAlertAction) in
            self.sharedButtonPressed()
        }
        mainAlert.addAction(sharedAction)
        mainAlert.addAction(cancel)
        if output.relationButtonState == .iAmCreator {
            let deleteEventAction = UIAlertAction(title: "Удалить событие", style: .destructive) { (UIAlertAction) in
                self.deleteEventIfNeeded()
            }
            mainAlert.addAction(deleteEventAction)
        } else {
            let reportAction = UIAlertAction(title: "Пожаловаться", style: .destructive) { (UIAlertAction) in
                self.showReportAlertController()
            }
            mainAlert.addAction(reportAction)
        }
        
        present(mainAlert, animated: true, completion: nil)
    }

    private func sharedButtonPressed() {
        let link = DefaultLinksShared.event + String(output.eventId)
        let vc = UIActivityViewController(activityItems: [link], applicationActivities: [])
        present(vc, animated: true)
    }
  
}

extension SelectedEventViewController: SelectedEventViewInput {
    func showErrorAlert(_ error: String) {
        spinner.setStateSpinner(enable: false, view: self.view)
        let alert = UIAlertController(title: "Ошибка", message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ок", style: .cancel, handler: { _ in
            self.dissmiss()
        })
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func setAddress(_ address: String) {
        UIView.animate(withDuration: 0.3, animations: {
            self.addressLabelView.isHidden = false
            self.addressLabel.text = address
            self.view.layoutIfNeeded()
        })
    }
    
    func setDistance(_ distance: Double) {
        mapsPositionView.distance = distance
    }
    
    func setSelectedEventViewModel(_ model: SelectedEventViewModel) {
        selectedEventHeaderView.set(model: model.headerViewModel)
        descriptionEventView.text = model.description
        if model.description!.isEmpty {
            descriptionEventView.isHidden = true
        }
        if model.descriptionPlace.isEmpty {
            descriptionPlaceView.isHidden = true
        }
        conditionsEventView.set(model: model.conditionsEventViewViewModel)
        descriptionPlaceView.text = model.descriptionPlace
        creatorView.set(model: model.creatorInfo)
        configureBackgroundRelationButton()
        confugureTitleRelationButton()
    }
    

    func showSuccessAlert() {
        spinner.setStateSpinner(enable: false, view: self.view)
        let alert = UIAlertController(title: "Жалоба на событие \(output.titleEvent) отправлена!", message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ок", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }

    func showErrorAlert(error: Error?) {
        let alertError = UIAlertController(title: "Ошибка", message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ок", style: .default, handler: nil)
        alertError.addAction(cancelAction)

        if let error = error as? NetworkError.JoinMeetById {
            alertError.message = error.description
        } else {
            alertError.message = "Что-то не так с интернет соединением"
        }
        present(alertError, animated: true, completion: nil)
        //respondButton.isEnabled = true
    }

    
    func updateStateButtonAfterRespond() {
        configureBackgroundRelationButton()
        confugureTitleRelationButton()
        callerStatusButton.isEnabled = true
    }
    
    func setupInitialState() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

}

extension SelectedEventViewController: CreatorViewDelegate {
    func creatorViewCreatorDidPressed(_ view: CreatorView, creatorId: Int) {
        output.didInfoAboutAuthorEventPressed(creatorId)
    }
}

extension SelectedEventViewController: ConditionsEventViewDelegate {
    func conditionsEventViewParticipantsButtonPressed(_ view: ConditionsEventView) {
        output.didParticipantsButtonPressed()
    }
    
}

extension SelectedEventViewController: MapsPositionViewDelegate {
    var isNoSelectedAddress: Bool {
        return addressLabel.text!.isEmpty
    }
    
    func mapsPositionViewSelectedAddressPressed(_ view: MapsPositionView) {
        output.didShowAddressButtonPressed()
    }
}
