//
//  EventRibbonViewInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol FeedViewInput: class, Presentable {
    func setEvents(events: [EventCellViewModel])
    func setEventsOnlySubscribtions(events: [EventCellViewModel])
    func setupInitialState()
    func showViewOfNoInternetConnection()
}
