//
//  EventRibbonViewOutput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol FeedViewOutput {
    func viewIsReady()
    func didSelectedEventPressed(_ eventId: Int)
    func updateListEvents(start: Int, place: Int, followingOnly: Bool, search: String)
    func didFilterPressed()
    func didSelectedCategory(id: Int)
}
