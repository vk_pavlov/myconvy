//
//  NavigationItemFeedTitleView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 29/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

protocol NavigationItemFeedTitleViewDelegate: class {
    func navigationItemFeedTitleView(_ view: NavigationItemFeedTitleView, textfieldShouldReturn textField: UITextField)
    func navigationItemFeedTitleViewDidFilterPressed(_ view: NavigationItemFeedTitleView)
    func navigationItemFeedTitleView(_ view: NavigationItemFeedTitleView, didChangeSubscriptionsActivateState activate: Bool)
    func navigationItemFeedTitleViewEnableSubscriptions(_ view: NavigationItemFeedTitleView)
    func navigationItemFeedTitleViewDisableSubscriptions(_ view: NavigationItemFeedTitleView)
}

class NavigationItemFeedTitleView: UIView {

    weak var delegate: NavigationItemFeedTitleViewDelegate?

    private var subscriptionsActivateButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "subscriptions_icon")!, for: .normal)
        button.addTarget(self, action: #selector(subscriptionsActivateButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private var textField = InsetableTextField()
    
    private var filtersButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "settingsFeed")!, for: .normal)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(filtersButtonPressed), for: .touchUpInside)
        return button
    }()
    
    var isActivatedSubscriptions: Bool = false {
        didSet {
            subscriptionsActivateButton.setImage(isActivatedSubscriptions ? UIImage(named: "subscription_icon_active")! : UIImage(named: "subscriptions_icon")!, for: .normal)
        }
    }
    
    var textFieldText: String? {
        get {
            return textField.text
        }
        set {
            textField.text = newValue
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(subscriptionsActivateButton)
        addSubview(textField)
        //addSubview(filtersButton)
        textField.delegate = self
        translatesAutoresizingMaskIntoConstraints = false
        makeConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func makeConstraints() {
        
        subscriptionsActivateButton.anchor(top: topAnchor,
                             leading: leadingAnchor,
                             bottom: bottomAnchor,
                             trailing: nil,
                             padding: UIEdgeInsets(top: 18, left: 16, bottom: 10, right: 777))
        
        subscriptionsActivateButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        subscriptionsActivateButton.widthAnchor.constraint(equalToConstant: 50).isActive = true

        textField.anchor(top: topAnchor,
                         leading: subscriptionsActivateButton.trailingAnchor,
                         bottom: nil,
                         trailing: trailingAnchor,
                         padding: UIEdgeInsets(top: 28, left: 10, bottom: 777, right: 60))
        
        textField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        

//        filtersButton.anchor(top: topAnchor,
//                              leading: textField.trailingAnchor,
//                              bottom: nil,
//                              trailing: trailingAnchor,
//                              padding: UIEdgeInsets(top: 18, left: 10, bottom: 777, right: 12))
//
//        filtersButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        filtersButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        subscriptionsActivateButton.layer.cornerRadius = subscriptionsActivateButton.frame.width / 2
    }
    
    @objc private func subscriptionsActivateButtonPressed() {
        isActivatedSubscriptions ? delegate?.navigationItemFeedTitleViewDisableSubscriptions(self) : delegate?.navigationItemFeedTitleViewEnableSubscriptions(self)
    }
    
    @objc private func filtersButtonPressed() {
        delegate?.navigationItemFeedTitleViewDidFilterPressed(self)
    }

}

extension NavigationItemFeedTitleView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        delegate?.navigationItemFeedTitleView(self, textfieldShouldReturn: textField)
        return true
    }
}
