//
//  InsetableTextField.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 29/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

class InsetableTextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = #colorLiteral(red: 0.9882352941, green: 0.6470588235, blue: 0.2941176471, alpha: 0.08)
        textColor = #colorLiteral(red: 0.9921568627, green: 0.7725490196, blue: 0.537254902, alpha: 1)
        attributedPlaceholder = NSAttributedString(string: "Событие", attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.9921568627, green: 0.7725490196, blue: 0.537254902, alpha: 1)])
        
        font = UIFont.systemFont(ofSize: 15)
        clearButtonMode = .whileEditing
        borderStyle = .none
        layer.cornerRadius = 10
        layer.masksToBounds = true
        let image = UIImage(named: "Shape")
        leftView = UIImageView(image: image)
        leftView?.frame = CGRect(x: 0, y: 0, width: 17, height: 17)
        leftViewMode = .always
        returnKeyType = .search
    }
    
    func clearText() {
        text = ""
    }

    // сдвинуть иконку поиска вправо
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.leftViewRect(forBounds: bounds)
        rect.origin.x += 6
        return rect
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.placeholderRect(forBounds: bounds)
        rect.origin.x += 6
        return rect
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.editingRect(forBounds: bounds)
        rect.origin.x += 6
        return rect
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
