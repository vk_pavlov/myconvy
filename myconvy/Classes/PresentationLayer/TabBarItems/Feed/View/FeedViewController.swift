//
//  EventRibbonViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {

    @IBOutlet weak var eventsTableView: UITableView!
    
    var output: FeedViewOutput!
    private var events = [EventCellViewModel]()
    private var eventsOnlySubscribtions = [EventCellViewModel]()
    private var eventsToShow = [EventCellViewModel]()
    private var start = 0
    private let spinner = Spinner(style: .gray)

    private var noInternetConnectionView: NoInternetConnectionView!
    private lazy var noEventStubView = StubsFactory.makeStub(.noEvents(self))
    
    private let titleView = NavigationItemFeedTitleView()
    private let feedCategoriesCollectionView = CategoriesCollectionView()
    private var followingOnly = false
    
    private lazy var selectedCategories = Array.init(repeating: false, count: categories.count)

    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        return refreshControl
    }()

    // MARK: Lifecycle

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        output.updateListEvents(start: 0, place: 0, followingOnly: true, search: "")
        output.updateListEvents(start: 0, place: 0, followingOnly: false, search: "")
        spinner.setStateSpinner(enable: true, view: self.view)
        output.viewIsReady()
        titleView.delegate = self
        feedCategoriesCollectionView.dataSource = self
        feedCategoriesCollectionView.delegate = self
        
        view.addSubview(titleView)
        view.addSubview(feedCategoriesCollectionView)
        
        feedCategoriesCollectionView.anchor(top: titleView.bottomAnchor,
                                            leading: view.leadingAnchor,
                                            bottom: nil,
                                            trailing: view.trailingAnchor)
        
        eventsTableView.topAnchor.constraint(equalTo: feedCategoriesCollectionView.bottomAnchor).isActive = true
        self.navigationItem.titleView?.isUserInteractionEnabled = true
        eventsTableView.backgroundColor = .white
        
        titleView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func configureStub() {
        noEventStubView.removeFromSuperview()
        if eventsToShow.isEmpty {
            titleView.isActivatedSubscriptions ? (noEventStubView = StubsFactory.makeStub(.noEventsSubscribtions(self))) : (noEventStubView = StubsFactory.makeStub(.noEvents(self)))
            noEventStubView.translatesAutoresizingMaskIntoConstraints = true
            eventsTableView.addSubview(noEventStubView)
            noEventStubView.frame = view.frame
        }
    }

    // MARK: Action
    
    @IBAction func newEventPressed(_ sender: UIBarButtonItem) {
        
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        spinner.setStateSpinner(enable: true, view: view)
        titleView.isActivatedSubscriptions ? output.updateListEvents(start: 0, place: 0, followingOnly: true, search: "") : output.updateListEvents(start: 0, place: 0, followingOnly: false, search: "")
        titleView.isActivatedSubscriptions ? (eventsOnlySubscribtions = []) : (events = [])
        titleView.textFieldText = ""
    }

    @objc func repeatRequest() {
        spinner.setStateSpinner(enable: true, view: self.view)
    }
    
}

extension FeedViewController: FeedViewInput {
    func setEventsOnlySubscribtions(events: [EventCellViewModel]) {
        eventsOnlySubscribtions += events
        if titleView.isActivatedSubscriptions {
            eventsToShow = eventsOnlySubscribtions
            configureStub()
            eventsTableView.reloadData()
        }
        refreshControl.endRefreshing()
        spinner.setStateSpinner(enable: false, view: self.view)
        
    }
    
    func showViewOfNoInternetConnection() {
        spinner.setStateSpinner(enable: false, view: self.view)
        refreshControl.endRefreshing()
        noInternetConnectionView.addToView(view: eventsTableView, result: nil)
    }

    func setupInitialState() {
        eventsTableView.register(EventCell.nib, forCellReuseIdentifier: EventCell.identifier)
        noInternetConnectionView = NoInternetConnectionView(frame: view.bounds)
        noInternetConnectionView.repeatRequestButton.addTarget(self, action: #selector(repeatRequest), for: .touchUpInside)
        eventsTableView.insertSubview(refreshControl, at: 0)
    }
    
    func setEvents(events: [EventCellViewModel]) {
        self.events += events
        eventsToShow = events
        start = events.count
        spinner.setStateSpinner(enable: false, view: self.view)
        configureStub()
        refreshControl.endRefreshing()
        eventsTableView.reloadData()
    }
}

extension FeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventsToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EventCell.identifier, for: indexPath) as! EventCell
        if !eventsToShow.isEmpty {
            let viewModel = eventsToShow[indexPath.row]
            cell.set(viewModel: viewModel)
        }
        let view = UIView()
        view.backgroundColor = .clear
        cell.selectedBackgroundView = view
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event = eventsToShow[indexPath.row]
        output.didSelectedEventPressed(event.id)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == start - 3 && start % 100 == 0 {
            output.updateListEvents(start: start, place: 0, followingOnly: followingOnly, search: "")
        }
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

}

extension FeedViewController: NavigationItemFeedTitleViewDelegate {
    func navigationItemFeedTitleView(_ view: NavigationItemFeedTitleView, textfieldShouldReturn textField: UITextField) {
        guard let text = textField.text else { return }
        output.updateListEvents(start: 0, place: 0, followingOnly: followingOnly, search: text)
    }
    
    func navigationItemFeedTitleViewDidFilterPressed(_ view: NavigationItemFeedTitleView) {
        
    }
    
    func navigationItemFeedTitleView(_ view: NavigationItemFeedTitleView, didChangeSubscriptionsActivateState activate: Bool) {
        
    }
    
    func navigationItemFeedTitleViewEnableSubscriptions(_ view: NavigationItemFeedTitleView) {
        start = eventsOnlySubscribtions.count
        eventsToShow = eventsOnlySubscribtions
        titleView.isActivatedSubscriptions = true
        followingOnly = true
        configureStub()
        eventsTableView.reloadData()
    }
    
    func navigationItemFeedTitleViewDisableSubscriptions(_ view: NavigationItemFeedTitleView) {
        start = events.count
        eventsToShow = events
        titleView.isActivatedSubscriptions = false
        followingOnly = false
        configureStub()
        eventsTableView.reloadData()
    }
}

extension FeedViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return desires.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryWithShadowCollectionViewCell.reuseId, for: indexPath) as! CategoryWithShadowCollectionViewCell
        cell.title = desires[indexPath.row]
        //cell.isSelectedCategory = selectedCategories[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output.didSelectedCategory(id: indexPath.row)
        //let cell = collectionView.cellForItem(at: indexPath) as! CategoryWithShadowCollectionViewCell
        //selectedCategories[indexPath.row] = !selectedCategories[indexPath.row]
        //cell.isSelectedCategory = selectedCategories[indexPath.row]
    }
}
