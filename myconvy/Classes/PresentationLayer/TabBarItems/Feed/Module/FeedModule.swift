//
//  EventRibbonModule.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class FeedModule {
    class func create() -> FeedModuleInput {
        let storyboard = UIStoryboard(name: "Feed", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FeedViewController") as! FeedViewController
        let presenter = FeedPresenter()
        let interactor = FeedInteractor()
        let router = FeedRouter()
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        interactor.output = presenter
        interactor.eventDataFetcher = NetworkEventDataFetcher()
        return presenter
    }
}
