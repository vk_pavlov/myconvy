//
//  EventRibbonPresenter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import CoreLocation

class FeedPresenter: NSObject {
    weak var view: FeedViewInput!
    var interactor: FeedInteractorInput!
    var router: FeedRouterInput!
    private let locationManager = CLLocationManager()
    private var lastMyLocation: CLLocation?
    private var pushManager: PushNotificationManager!
}

extension FeedPresenter: FeedViewOutput {
    func didSelectedCategory(id: Int) {
        router.presentDefaultNewEvent(from: view.viewController, defaultCategoryIndex: id)
    }
    
    func didFilterPressed() {
        let model = FilterViewModel(sortedBy: 0, categories: [], gender: 0, minAges: 0, maxAges: 0)
        router.presentFilter(from: view.viewController, viewModel: model, output: self)
    }
    
    func updateListEvents(start: Int, place: Int, followingOnly: Bool, search: String) {
        locationManager.startUpdatingLocation()
        if followingOnly {
            interactor.getMeetsOnlySubscribtions(start: start, place: place, search: search)
        } else {
            interactor.getMeets(start: start, place: place, search: search)
        }
    }
    
    func didSelectedEventPressed(_ eventId: Int) {
        router.presentSelectedEvent(from: view.viewController, eventId: eventId)
    }
    
    func viewIsReady() {
        locationManager.delegate = self
        view.setupInitialState()
        pushManager = AppDelegate.currentDelegate.pushManager
        pushManager.requestForPushNotification()
    }
    
    private func cellViewModel(from: EventTwo) -> EventCellViewModel {
        return EventCellViewModel(id: from.id,
                                  authorId: from.creator.id,
                                  title: from.title,
                                  cagegory: from.category,
                                  author: from.creator.namePlusAge,
                                  distance: distanceToEvent(eventPlace: from.place),
                                  startDate: Date(timeIntervalSince1970: from.startDate),
                                  callerStatus: CallerStatus(rawValue: from.callerStatus)!,
                                  eventStatus: EventStatus(rawValue: from.status)!,
                                  isOrders: from.isOrders)
    }
    
    private func distanceToEvent(eventPlace: Place) -> Double {
        let toPoint = CLLocation(latitude: eventPlace.lat, longitude: eventPlace.lng)
        guard lastMyLocation != nil else { return 0 }
        return lastMyLocation!.distance(from: toPoint)
    }
}

extension FeedPresenter: FeedInteractorOutput {
    
    func didReciveEventsOnlySubscribtions(_ events: [EventTwo]) {
        let cells = events.map { (event) in
            cellViewModel(from: event)
        }
        locationManager.stopUpdatingLocation()
        view.setEventsOnlySubscribtions(events: cells)
    }

    func didReciveEvents(_ events: [EventTwo]) {
        var cells = events.map { (event) in
            cellViewModel(from: event)
        }
        interactor.getMeetsGone(success: { (oldEvents) in
            let oldEventCells = oldEvents.map { (event) in
                self.cellViewModel(from: event)
            }
            cells += oldEventCells
            self.view.setEvents(events: cells)
            self.locationManager.stopUpdatingLocation()
        }) { (error) in
            self.view.setEvents(events: cells)
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func didReciveError() {
        view.showViewOfNoInternetConnection()
    }
    
}

extension FeedPresenter: FeedModuleInput {
    var viewController: UIViewController {
        return view.viewController
    }
}


extension FeedPresenter: FilterEventModuleOutput {
    
}

extension FeedPresenter: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        lastMyLocation = locations.last
    }
}
