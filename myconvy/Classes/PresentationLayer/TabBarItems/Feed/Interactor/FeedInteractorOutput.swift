//
//  EventRibbonInteractorOutput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol FeedInteractorOutput: class {
    func didReciveError()
    func didReciveEvents(_ events: [EventTwo])
    func didReciveEventsOnlySubscribtions(_ events: [EventTwo])
}
