//
//  EventRibbonInteractorInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol FeedInteractorInput {
    func getMeets(start: Int, place: Int, search: String)
    func getMeetsGone(success: @escaping ([EventTwo]) -> (), error: @escaping (ResultError) -> ())
    func getMeetsOnlySubscribtions(start: Int, place: Int, search: String)
}
