//
//  EventRibbonInteractor.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import RealmSwift


class FeedInteractor {
    weak var output: FeedInteractorOutput!
    var eventDataFetcher: EventDataFetcher!
}

extension FeedInteractor: FeedInteractorInput {
    func getMeetsGone(success: @escaping ([EventTwo]) -> (), error: @escaping (ResultError) -> ()) {
        eventDataFetcher.getMeetsGone(start: 0, place: 0, search: "", followingOnly: false) { (feedResponse, errorResponse) in
            if let feedResponse = feedResponse {
                success(feedResponse.meetsInfo)
            }
            if let errorResponse = errorResponse {
                error(errorResponse)
            }
        }
    }

    func getMeetsOnlySubscribtions(start: Int, place: Int, search: String) {
        eventDataFetcher.getMeets(start: start, place: place, search: search, followingOnly: true) { [weak self] (feedResponse, errorResponse) in
            if let feedResponse = feedResponse {
                self?.output.didReciveEventsOnlySubscribtions(feedResponse.meetsInfo)
            }
            
        }
    }
    
    func getMeets(start: Int, place: Int, search: String) {
        eventDataFetcher.getMeets(start: start, place: place, search: search, followingOnly: false) { [weak self] (feedResponse, errorResponse) in
            if let feedResponse = feedResponse {
                self?.output.didReciveEvents(feedResponse.meetsInfo)
            }
          
        }
    }
    
    
}
