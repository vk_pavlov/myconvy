//
//  EventRibbonRouterInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol FeedRouterInput {
    func presentSelectedEvent(from: UIViewController, eventId: Int)
    func presentFilter(from: UIViewController, viewModel: FilterViewModel, output: FilterEventModuleOutput)
    func presentDefaultNewEvent(from: UIViewController, defaultCategoryIndex: Int)
}
