//
//  EventRibbonRouter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class FeedRouter {
    
}

extension FeedRouter: FeedRouterInput {
    func presentFilter(from: UIViewController, viewModel: FilterViewModel, output: FilterEventModuleOutput) {
        FilterEventModule.create().present(from: from, viewModel: viewModel, output: output)
    }
    
    func presentSelectedEvent(from: UIViewController, eventId: Int) {
        SelectedEventModule.create().present(from: from, eventId: eventId)
    }
    
    func presentDefaultNewEvent(from: UIViewController, defaultCategoryIndex: Int) {
        NewEventModule.create().present(from: from, defaultCategoryIndex: defaultCategoryIndex)
    }
}
