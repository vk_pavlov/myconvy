//
//  DoingsModule.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class MapModule {
    class func create() -> MapModuleInput {
        let storyboard = UIStoryboard(name: "Map", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        let presenter = MapPresenter()
        let interactor = MapInteractor()
        let router = MapRouter()
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        interactor.output = presenter
        interactor.eventDataFetcher = NetworkEventDataFetcher()
        return presenter
    }
}
