//
//  MapModuleOutput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 23/01/2020.
//  Copyright © 2020 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol MapModuleOutput {
    func setSelectedAddress(streetName: String, coordinate: Place)
}
