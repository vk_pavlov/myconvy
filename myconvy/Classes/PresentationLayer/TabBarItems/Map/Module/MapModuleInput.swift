//
//  DoingsModuleInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol MapModuleInput {
    var viewController: UIViewController { get }
    func present(from viewController: UIViewController, mapPresentationMode: MapPresentationMode, initialCoordinate: Place?, output: MapModuleOutput?, isParticipant: Bool)
}
