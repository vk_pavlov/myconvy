//
//  PlaceMarker.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 20/01/2020.
//  Copyright © 2020 Vyacheslav Pavlov. All rights reserved.
//
import GoogleMaps


class PlaceMarker: GMSMarker {
    
    var eventId: Int
    var category: Int
    var titleEvent: String
    var date: Date
    
    init(eventId: Int, category: Int, titleEvent: String, date: Date) {
        self.eventId = eventId
        self.category = category
        self.titleEvent = titleEvent
        self.date = date
        super.init()
        icon = UIImage(named: "marker_icon")!.imageWithColor(OrangeTheme.myconvyOrange, newSize: CGSize(width: 35, height: 35))
        groundAnchor = CGPoint(x: 0.5, y: 1)
        appearAnimation = .pop
    }
}
