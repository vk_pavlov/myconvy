//
//  DoingsViewControllerInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

protocol MapViewInput: class, Presentable {
    func setupInitialState()
    func setMarkers(_ markers: [PlaceMarker])
    func setSubscriptionsMarkers(_ markers: [PlaceMarker])
}
