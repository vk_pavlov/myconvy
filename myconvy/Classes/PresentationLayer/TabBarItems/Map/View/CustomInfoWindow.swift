//
//  CustomInfoWindow.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 20/01/2020.
//  Copyright © 2020 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol CustomInfoWindowDelegate: class {
    func customInfoWindow(_ view: CustomInfoWindow, didShowMoreInfoAboutEvent eventId: Int)
}

final class CustomInfoWindow: UIView {
    
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var moreInfoButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateStartView: DateStartView!
    
    private var eventId: Int!
    
    weak var delegate: CustomInfoWindowDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadView() -> CustomInfoWindow {
        let customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?[0] as! CustomInfoWindow
        return customInfoWindow
    }
    
    func set(eventId: Int, category: Int, title: String, date: Date) {
        self.eventId = eventId
        categoryImageView.image = UIImage(named: categoriesImageName[category])
        categoryNameLabel.text = categories[category]
        titleLabel.text = title
        categoryNameLabel.sizeToFit()
        dateStartView.date = date
    }
    
    override func awakeFromNib() {
        
        dateStartView.dateOtherFormat = "dd MMMM в HH:mm"
        dateStartView.dateTodayFormat = "Сегодня в HH:mm"
        dateStartView.dateTomorrowFormat = "Завтра в HH:mm"
        
        categoryImageView.layer.cornerRadius = 17
        categoryImageView.clipsToBounds = true
        categoryImageView.image = UIImage(named: "category_art")!
        
        categoryNameLabel.textColor = OrangeTheme.myconvyOrange
        categoryNameLabel.backgroundColor = UIColor.hexStringToUIColor(hex: "#FFEDDC")
        categoryNameLabel.layer.cornerRadius = 5
        categoryNameLabel.clipsToBounds = true
        
        titleLabel.textColor = OrangeTheme.myconvyOrange
        titleLabel.text = "Сегодня бухаем всю ночь!"
        
        dateStartView.date = Date()
        
        moreInfoButton.layer.cornerRadius = 8
        moreInfoButton.backgroundColor = OrangeTheme.myconvyOrange
        
        contentImageView.layer.shadowColor = UIColor.black.cgColor
        contentImageView.layer.shadowOpacity = 0.2
        contentImageView.layer.shadowOffset = .zero
        contentImageView.layer.shadowRadius = 4
    }
    
    @IBAction func moreInfoButtonPressed(_ sender: UIButton) {
        delegate?.customInfoWindow(self, didShowMoreInfoAboutEvent: eventId)
    }
    
}
