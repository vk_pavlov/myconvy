//
//  DoingsViewControllerOutput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol MapViewOutput {
    func viewIsReady()
    func didFilterEventPressed()
    func didSelectedAddressButtonPressed(streetName: String, coordinate: Place)
    func didShowMoreButtonPressed(eventId: Int)
    func didUpdateMarkers()
    func didSelectedCategory(id: Int)
    
    var mapMode: MapPresentationMode { get }
    var previousCoordinate: Place? { get }
    var iAmParticipant: Bool { get }
}
