//
//  DoingsViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController {
    
    var output: MapViewOutput!
    
    var locationManager: CLLocationManager!
    var mapBearing: Double = 0
    var googleMap: GMSMapView!
    private var initialZoom: Float = 10
    
    private var selectionAddressMarker: UIImageView!
    
    private var customInfoWindow : CustomInfoWindow!
    private var tappedMarker : PlaceMarker!
    
    private var safeAddressButton: UIButton!
    private var addressLabel: UILabel!
    private var markers = [PlaceMarker]()
    private var subscribtionsMarkers = [PlaceMarker]()
    private var isActiveOnlySubscribtions = false {
        didSet {
            isActiveOnlySubscribtions ? activateSubscribtionsButton.setImage(UIImage(named: "subscription_icon_active"), for: .normal) : activateSubscribtionsButton.setImage(UIImage(named: "subscriptions_icon"), for: .normal)
        }
    }
    
    private let myLocationButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 46).isActive = true
        button.widthAnchor.constraint(equalToConstant: 46).isActive = true
        button.setImage(UIImage(named: "location_icon"), for: .normal)
        button.addTarget(self, action: #selector(myLocationButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private let increaseButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 46).isActive = true
        button.widthAnchor.constraint(equalToConstant: 46).isActive = true
        button.setImage(UIImage(named: "increase_icon"), for: .normal)
        button.addTarget(self, action: #selector(increaseButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private let reduceButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 46).isActive = true
        button.widthAnchor.constraint(equalToConstant: 46).isActive = true
        button.setImage(UIImage(named: "reduce_icon"), for: .normal)
        button.addTarget(self, action: #selector(reduceButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private let activateSubscribtionsButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.widthAnchor.constraint(equalToConstant: 50).isActive = true
        button.setImage(UIImage(named: "subscriptions_icon"), for: .normal)
        button.addTarget(self, action: #selector(subscriptionsButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private let categoriesCollectionView = CategoriesCollectionView()
    
    private var cameraPosition: CLLocationCoordinate2D {
        return googleMap.camera.target
    }
    
//    private var onlySubscribtionsEvent: [PlaceMarker] {
//        return markers.filter { (marker) -> Bool in
//            return marker.
//        }
//    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customInfoWindow = CustomInfoWindow().loadView()
        customInfoWindow.delegate = self
        let camera: GMSCameraPosition!
        if let initialCoordinate = output.previousCoordinate {
            camera = GMSCameraPosition.camera(withLatitude: initialCoordinate.lat, longitude: initialCoordinate.lng, zoom: 17.0)
            initialZoom = 17
        } else {
            if output.mapMode == .selectionAddress {
                initialZoom = 17
            }
             camera = GMSCameraPosition.camera(withLatitude: 55.7, longitude: 37.6, zoom: 10.0)
        }
        
        googleMap = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = googleMap
        locationManager = CLLocationManager()
        locationManager.delegate = self
        googleMap.settings.indoorPicker = false
        googleMap.addSubview(myLocationButton)
        googleMap.addSubview(categoriesCollectionView)
        googleMap.addSubview(increaseButton)
        googleMap.addSubview(reduceButton)
        googleMap.addSubview(activateSubscribtionsButton)
        googleMap.isMyLocationEnabled = true
        googleMap.delegate = self
        categoriesCollectionView.backgroundColor = .clear
        
        categoriesCollectionView.dataSource = self
        categoriesCollectionView.delegate = self
        
        output.viewIsReady()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let tabBatTopAnchor = tabBarController?.tabBar.topAnchor {
            categoriesCollectionView.bottomAnchor.constraint(equalTo: tabBatTopAnchor, constant: -25).isActive = true
        } else {
            categoriesCollectionView.bottomAnchor.constraint(equalTo: googleMap.bottomAnchor, constant: -25).isActive = true
        }
        
        categoriesCollectionView.leadingAnchor.constraint(equalTo: googleMap.leadingAnchor).isActive = true
        categoriesCollectionView.trailingAnchor.constraint(equalTo: googleMap.trailingAnchor).isActive = true
        
        myLocationButton.trailingAnchor.constraint(equalTo: googleMap.trailingAnchor, constant: -16).isActive = true
        myLocationButton.bottomAnchor.constraint(equalTo: categoriesCollectionView.topAnchor, constant: -17).isActive = true
        
        reduceButton.bottomAnchor.constraint(equalTo: myLocationButton.topAnchor, constant: -150).isActive = true
        reduceButton.trailingAnchor.constraint(equalTo: googleMap.trailingAnchor, constant: -16).isActive = true
        
        increaseButton.bottomAnchor.constraint(equalTo: reduceButton.topAnchor, constant: -13).isActive = true
        increaseButton.trailingAnchor.constraint(equalTo: googleMap.trailingAnchor, constant: -16).isActive = true
        
        activateSubscribtionsButton.leadingAnchor.constraint(equalTo: googleMap.leadingAnchor, constant: 16).isActive = true
        activateSubscribtionsButton.topAnchor.constraint(equalTo: view!.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        
        if output.mapMode == .selectionAddress {
            addSelectionAddressMarkerIfNeeded()
        } else if output.mapMode == .selectedMarker {
            addSelectedEventMarkerWithCircle()
        }
    }
    
    private func addSelectedEventMarkerWithCircle() {
        if !output.iAmParticipant {
            let circleCenter : CLLocationCoordinate2D  = CLLocationCoordinate2DMake(output.previousCoordinate!.lat, output.previousCoordinate!.lng);
            let circ = GMSCircle(position: circleCenter, radius: 100)
            circ.fillColor = UIColor(red: 0.0, green: 0.7, blue: 0, alpha: 0.1)
            circ.strokeColor = UIColor(red: 255/255, green: 153/255, blue: 51/255, alpha: 0.5)
            circ.strokeWidth = 2.5;
            circ.map = googleMap
        }
        if tappedMarker == nil {
            tappedMarker = PlaceMarker(eventId: 1, category: 1, titleEvent: "", date: Date())
            tappedMarker.map = googleMap
            tappedMarker.position = CLLocationCoordinate2D(latitude: output.previousCoordinate!.lat, longitude: output.previousCoordinate!.lng)
        }
    }
    
    private func addSelectionAddressMarkerIfNeeded() {
        if selectionAddressMarker == nil {
            selectionAddressMarker = UIImageView()
            selectionAddressMarker.frame.size = CGSize(width: 35, height: 35)
            selectionAddressMarker.image = UIImage(named: "marker_icon")!.imageWithColor(OrangeTheme.myconvyOrange, newSize: CGSize(width: 35, height: 35))
            
            let point = googleMap.projection.point(for: cameraPosition)
            selectionAddressMarker.center = point
            googleMap.addSubview(selectionAddressMarker)
        }
        if addressLabel == nil {
            addressLabel = UILabel()
            addressLabel.textAlignment = .center
            addressLabel.font = UIFont.boldSystemFont(ofSize: 18)
            addressLabel.numberOfLines = 0
            addressLabel.translatesAutoresizingMaskIntoConstraints = false
            googleMap.addSubview(addressLabel)
            addressLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 40).isActive = true
            addressLabel.leadingAnchor.constraint(equalTo: googleMap.leadingAnchor, constant: 16).isActive = true
            addressLabel.trailingAnchor.constraint(equalTo: googleMap.trailingAnchor, constant: -16).isActive = true
        }
        if safeAddressButton == nil {
            safeAddressButton = UIButton()
            safeAddressButton.translatesAutoresizingMaskIntoConstraints = false
            safeAddressButton.widthAnchor.constraint(equalToConstant: 120).isActive = true
            safeAddressButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            googleMap.addSubview(safeAddressButton)
            safeAddressButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30).isActive = true
            safeAddressButton.centerXAnchor.constraint(equalTo: googleMap.centerXAnchor).isActive = true
            safeAddressButton.setTitle("Выбрать", for: .normal)
            
            safeAddressButton.layer.shadowColor = UIColor.black.cgColor
            safeAddressButton.layer.shadowOpacity = 0.2
            safeAddressButton.layer.shadowOffset = .zero
            safeAddressButton.layer.shadowRadius = 4
            
            safeAddressButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
            safeAddressButton.backgroundColor = OrangeTheme.myconvyOrange
            safeAddressButton.setTitleColor(.white, for: .normal)
            safeAddressButton.layer.cornerRadius = 20
            safeAddressButton.addTarget(self, action: #selector(selectionAddressButtonPressed), for: .touchUpInside)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if output.mapMode == .allMarkers {
            navigationController?.setNavigationBarHidden(true, animated: animated)
        }
        output.didUpdateMarkers()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: - Private
    
    @objc private func selectionAddressButtonPressed() {
        let place = Place(lat: cameraPosition.latitude, lng: cameraPosition.longitude)
        guard let text = addressLabel.text else { return }
        output.didSelectedAddressButtonPressed(streetName: text, coordinate: place)
    }
    
    @objc private func myLocationButtonPressed() {
        locationManager.requestWhenInUseAuthorization()
        guard let lat = self.googleMap.myLocation?.coordinate.latitude,
              let lng = self.googleMap.myLocation?.coordinate.longitude else { return }

        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 17)
        self.googleMap.animate(to: camera)
        initialZoom = 17
    }
    
    @objc private func increaseButtonPressed() {
        initialZoom += 1
        googleMap.animate(toZoom: initialZoom)
    }
    
    @objc private func reduceButtonPressed() {
        initialZoom -= 1
        googleMap.animate(toZoom: initialZoom)
    }
    
    @objc private func subscriptionsButtonPressed() {
        if !isActiveOnlySubscribtions {
            removeMarkers(markers)
            addMarkers(subscribtionsMarkers)
        } else {
            removeMarkers(subscribtionsMarkers)
            addMarkers(markers)
        }
        isActiveOnlySubscribtions = !isActiveOnlySubscribtions
    }

    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                self.addressLabel.text = address.thoroughfare
                UIView.animate(withDuration: 0.25) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    private func removeMarkers(_ markers: [PlaceMarker]) {
        for marker in markers {
            marker.map = nil
        }
    }
    
    private func addMarkers(_ markers: [PlaceMarker]) {
        for marker in markers {
            marker.map = googleMap
        }
    }
}

extension MapViewController: MapViewInput {
    func setSubscriptionsMarkers(_ markers: [PlaceMarker]) {
        if subscribtionsMarkers == markers {
            return
        }
        if !subscribtionsMarkers.isEmpty {
            removeMarkers(subscribtionsMarkers)
        }
        subscribtionsMarkers = markers
        guard isActiveOnlySubscribtions else { return }
        addMarkers(subscribtionsMarkers)
    }
    
    func setMarkers(_ markers: [PlaceMarker]) {
        if markers == self.markers {
            return
        }
        if !self.markers.isEmpty {
            removeMarkers(self.markers)
        }
        self.markers = markers
        guard !isActiveOnlySubscribtions else { return }
        addMarkers(self.markers)
    }

    func setupInitialState() {
        if output.mapMode != .allMarkers {
             categoriesCollectionView.isHidden = true
             activateSubscribtionsButton.isHidden = true
        }
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            if output.mapMode != .allMarkers {
                if let initialCoordinate = output.previousCoordinate {
                    let camera = GMSCameraPosition.camera(withLatitude: initialCoordinate.lat, longitude: initialCoordinate.lng , zoom: 17)
                    self.googleMap.animate(to: camera)
                    return
                }
                
                locationManager.startUpdatingLocation()
                googleMap.isMyLocationEnabled = true
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            googleMap.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
}

extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if output.mapMode == .selectionAddress {
            reverseGeocodeCoordinate(coordinate: position.target)
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if output.mapMode != .allMarkers {
            return false
        }
        tappedMarker = (marker as! PlaceMarker)
        let position = marker.position
        googleMap.animate(toLocation: position)
        let point = googleMap.projection.point(for: position)
        let newPoint = googleMap.projection.coordinate(for: point)
        let camera = GMSCameraUpdate.setTarget(newPoint)
        googleMap.animate(with: camera)
        customInfoWindow?.set(eventId: tappedMarker.eventId,
                              category: tappedMarker.category,
                              title: tappedMarker.titleEvent,
                              date: tappedMarker.date)
        customInfoWindow?.center = googleMap.projection.point(for: position)
        customInfoWindow?.center.y -= 105
        googleMap.insertSubview(customInfoWindow!, at: 1)
        return false
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return UIView()
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        customInfoWindow?.removeFromSuperview()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if output.mapMode == .allMarkers && tappedMarker != nil {
            let position = tappedMarker?.position
            customInfoWindow?.center = mapView.projection.point(for: position!)
            customInfoWindow?.center.y -= 105
        }
    }
    
}

extension MapViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return desires.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryWithShadowCollectionViewCell.reuseId, for: indexPath) as! CategoryWithShadowCollectionViewCell
        cell.title = desires[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output.didSelectedCategory(id: indexPath.row)
    }
}

extension MapViewController: CustomInfoWindowDelegate {
    func customInfoWindow(_ view: CustomInfoWindow, didShowMoreInfoAboutEvent eventId: Int) {
        output.didShowMoreButtonPressed(eventId: eventId)
    }
}
