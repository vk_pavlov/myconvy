//
//  DoingsPresenter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import GoogleMaps

enum MapPresentationMode {
    case allMarkers
    case selectedMarker
    case selectionAddress
}

class MapPresenter {
    weak var view: MapViewInput!
    var interactor: MapInteractorInput!
    var router: MapRouterInput!
    var actualEvents: [Event]!
    private var isSelectionAddress: Bool!
    var output: MapModuleOutput!
    private var coordinate: Place!
    private var mapPresentationMode: MapPresentationMode = .allMarkers
    private var isParticipant: Bool!
    
    private func placeMarker(from: EventTwo) -> PlaceMarker {
        let marker = PlaceMarker(eventId: from.id,
                                 category: from.category,
                                 titleEvent: from.title,
                                 date: Date(timeIntervalSince1970: from.startDate))
        marker.position = CLLocationCoordinate2D(latitude: from.place.lat, longitude: from.place.lng)
        return marker
    }
}

extension MapPresenter: MapViewOutput {
    func didSelectedCategory(id: Int) {
        router.presentDefaultNewEvent(from: view.viewController, defaultCategoryIndex: id)
    }
    
    func didUpdateMarkers() {
        if mapMode == .allMarkers {
            interactor.getEvents()
            interactor.getEventsSubscriptions()
        }
    }
    
    func didShowMoreButtonPressed(eventId: Int) {
        router.presentSelectedEvent(from: view.viewController, eventId: eventId)
    }
    
    var iAmParticipant: Bool {
        return isParticipant
    }
    
    var previousCoordinate: Place? {
        return coordinate
    }
    
    func didSelectedAddressButtonPressed(streetName: String, coordinate: Place) {
        output.setSelectedAddress(streetName: streetName, coordinate: coordinate)
        view.dissmiss()
    }
    
    var mapMode: MapPresentationMode {
        return mapPresentationMode
    }
    
    func didFilterEventPressed() {
        router.presentFilterEvent(from: view.viewController, output: self)
    }
    
    func viewIsReady() {
        view.setupInitialState()
    }
}

extension MapPresenter: MapInteractorOutput {
    func didReciveSubscribtionsEvents(_ events: [EventTwo]) {
        let markers = events.map { (event) in
            placeMarker(from: event)
        }
        view.setSubscriptionsMarkers(markers)
    }
    
    func didReciveEvents(_ events: [EventTwo]) {
        let markers = events.map { (event) in
            placeMarker(from: event)
        }
        view.setMarkers(markers)
    }

    func didReciveError() {
    }

}

extension MapPresenter: MapModuleInput {
    func present(from viewController: UIViewController, mapPresentationMode: MapPresentationMode, initialCoordinate: Place?, output: MapModuleOutput?, isParticipant: Bool) {
        self.isParticipant = isParticipant
        self.output = output
        self.mapPresentationMode = mapPresentationMode
        coordinate = initialCoordinate
        view.present(fromViewController: viewController)
    }
    
    var viewController: UIViewController {
        return view.viewController
    }
}

extension MapPresenter: FilterEventModuleOutput {
    
}
