//
//  DoingsRouterInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 08/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol MapRouterInput {
    func presentFilterEvent(from: UIViewController, output: FilterEventModuleOutput)
    func presentSelectedEvent(from: UIViewController, eventId: Int)
    func presentDefaultNewEvent(from: UIViewController, defaultCategoryIndex: Int)
}
