//
//  DoingsRouter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 08/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class MapRouter {
    
}

extension MapRouter: MapRouterInput {
   
    func presentFilterEvent(from: UIViewController, output: FilterEventModuleOutput) {
        
    }
    
    func presentSelectedEvent(from: UIViewController, eventId: Int) {
        SelectedEventModule.create().present(from: from, eventId: eventId)
    }
    
    func presentDefaultNewEvent(from: UIViewController, defaultCategoryIndex: Int) {
        NewEventModule.create().present(from: from, defaultCategoryIndex: defaultCategoryIndex)
    }
}
