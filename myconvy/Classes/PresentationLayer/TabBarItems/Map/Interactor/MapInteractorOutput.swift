//
//  DoingsInteractorOutput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol MapInteractorOutput: class {
    func didReciveEvents(_ events: [EventTwo])
    func didReciveSubscribtionsEvents(_ events: [EventTwo])
    func didReciveError()
}
