//
//  DoingsInteractor.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


class MapInteractor {
    weak var output: MapInteractorOutput!
    var eventDataFetcher: EventDataFetcher!
}

extension MapInteractor: MapInteractorInput {
    func getEventsSubscriptions() {
        eventDataFetcher.getMeets(start: 0, place: 0, search: "", followingOnly: true) { [weak self] (feedResponse, errorResponse) in
            if let feedResponse = feedResponse {
                self?.output.didReciveSubscribtionsEvents(feedResponse.meetsInfo)
            }
        }
    }
    
    func getEvents() {
        eventDataFetcher.getMeets(start: 0, place: 0, search: "", followingOnly: false) { [weak self] (feedResponse, errorResponse) in
            if let feedResponse = feedResponse {
                self?.output.didReciveEvents(feedResponse.meetsInfo)
            }
        }
    }
}
