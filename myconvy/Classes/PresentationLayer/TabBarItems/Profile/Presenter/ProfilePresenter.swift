//
//  ProfilePresenter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import GoogleMaps

class ProfilePresenter: NSObject {
    weak var view: ProfileViewInput!
    var interactor: ProfileInteractorInput!
    var router: ProfileRouterInput!
    private var user: UserTwo!
    private let locationManager = CLLocationManager()
    private var lastMyLocation: CLLocation?
    
    private func profileViewModel() -> ProfileViewModel {
        let headerUserInfoModel = HeaderUserInfoViewModel(fullName: user.fullName,
                                                          otherInfo: user.cityPlusAge,
                                                          avatarUrl: user.avatar,
                                                          online: user.online)
        
        let counterStackViewViewModel = CountersStackViewViewModel(eventsCount: user.meetsCreated!.count,
                                                                   subscribersCount: user.usersFollowers!.count,
                                                                   subscriptionsCount: user.usersFollowing!.count)
        
        let profileTableTitleViewViewModel = ProfileTableTitleViewViewModel(headerUserInfoViewModel: headerUserInfoModel, counterStackViewViewModel: counterStackViewViewModel)
        return ProfileViewModel(profileTitleViewViewModel: profileTableTitleViewViewModel)
    }
    
    private func editProfileViewModel() -> EditProfileViewModel {
        let contactInfo: [ContactInfoViewModel] = [
            ContactInfoViewModel(title: "Телефон", placeholder: "Введите Ваш телефон", value: user.contPhone),
            ContactInfoViewModel(title: "ВКонтакте", placeholder: "Введите Ваш ID", value: user.contVk),
            ContactInfoViewModel(title: "Facebook", placeholder: "Введите Ваш ID", value: user.contFb),
            ContactInfoViewModel(title: "Instagram", placeholder: "Введите Ваш ID", value: user.contInst),
            ContactInfoViewModel(title: "Email", placeholder: "Введите Ваш email", value: user.contEmail)
        ]
        
        return EditProfileViewModel(avatar: user.avatar,
                                    name: user.name,
                                    lastName: user.lastname,
                                    gender: user.sex ?? 0,
                                    birthdate: user.birthdate ?? 0,
                                    interest: user.categories ?? [],
                                    city: user.city,
                                    university: user.univ,
                                    relations: user.relations,
                                    contactInfo: contactInfo,
                                    aboutSelf: user.status)
    }
    
    private func cellViewModel(from: EventTwo) -> EventCellViewModel {
        return EventCellViewModel(id: from.id,
                                  authorId: from.creator.id,
                                  title: from.title,
                                  cagegory: from.category,
                                  author: from.creator.namePlusAge,
                                  distance: distanceToEvent(eventPlace: from.place),
                                  startDate: Date(timeIntervalSince1970: from.startDate),
                                  callerStatus: CallerStatus(rawValue: from.callerStatus)!,
                                  eventStatus: EventStatus(rawValue: from.status)!,
                                  isOrders: from.isOrders)
    }
    
    private func distanceToEvent(eventPlace: Place) -> Double {
        let toPoint = CLLocation(latitude: eventPlace.lat, longitude: eventPlace.lng)
        guard lastMyLocation != nil else { return 0 }
        return lastMyLocation!.distance(from: toPoint)
    }
}


extension ProfilePresenter: ProfileViewOutput {
    var userId: Int {
        return CommonData.selfUser().id
    }
    
    func didPhotoProfilePressed(imageView: UIImageView) {
        router.presentPhotoBrowser(from: view.viewController, imageView: imageView)
    }

    func willUpdateUserInfo() {
        locationManager.startUpdatingLocation()
        interactor.getUserById(userId: userId)
    }
    
    func didSelectedEventPressed(_ eventId: Int) {
        router.presentSelectedEvent(from: view.viewController, eventId: eventId)
    }

    func viewIsReady() {
        locationManager.delegate = self
        view.setupInitialState()
    }
    
    func didSettingsProfileButtonPressed(){
        router.presentSettingsProfile(from: view.viewController)
    }
    
    func didEditProfileButtonPressed() {
        guard user != nil else { return }
        let editUserViewModel = self.editProfileViewModel()
        router.presentEditProfile(from: view.viewController, editUserViewModel: editUserViewModel)
    }
    
    func didSubscribersButtonPressed() {
        guard let usersFollowers = user?.usersFollowers else { return }
        router.presentSubscribers(from: view.viewController, users: usersFollowers)
    }
    
    func didSubscriptionsButtonPressed() {
        guard let usersFollowing = user?.usersFollowing else { return }
        router.presentSubscriptions(from: view.viewController, users: usersFollowing)
    }
}

extension ProfilePresenter: ProfileInteractorOutput {
    func didReceiveUser(_ user: UserTwo) {
        locationManager.stopUpdatingLocation()
        self.user = user
        let userViewModel = profileViewModel()
        view.setProfileViewModel(userViewModel)
        guard let meetsCreated = user.meetsCreated else { return }
        interactor.getMeetsMade(meetsCreated)
    }
    
    func didReciveError() {
        view.showViewOfNoInternetConnection()
    }

    func didReciveMeetsOrders(_ events: [EventTwo]) {
        let cellsViewModel = events.map { (event) in
            cellViewModel(from: event)
        }
        guard let meetsWithSelf = user.meets else { return }
        interactor.getMeetsWithSelf(meetsWithSelf) { [weak self] (meets) in
            guard let self = self else { return }
            let cellsWithSelfViewModel = meets.map { (event) in
                self.cellViewModel(from: event)
            }
            let filteredViewModels = cellsWithSelfViewModel.filter { (viewModel) -> Bool in
                return viewModel.authorId != self.userId
            }
            self.view.setOtherEvents(cellsViewModel + filteredViewModels)
        }
    }
    
    func didReciveMeetsMade(_ events: [EventTwo]) {
        interactor.getMeetsOrders(user.meetsRequests!)
        let activeEvents = events.filter { $0.status != 3 }
        let archiveEvents = events.filter { $0.status == 3 }
        let resultEvents = activeEvents + archiveEvents.reversed()
        let cellsViewModel = resultEvents.map { (event) in
            cellViewModel(from: event)
        }
        view.setMyEvents(cellsViewModel)
    }
    
}

extension ProfilePresenter: ProfileModuleInput {

    func present(from: UIViewController) {
        view.present(fromViewController: from)
    }
    
    var viewController: UIViewController {
        return view.viewController
    }
}

extension ProfilePresenter: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        lastMyLocation = locations.last
    }
}
