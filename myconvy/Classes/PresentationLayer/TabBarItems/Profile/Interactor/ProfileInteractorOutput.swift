//
//  ProfileInteractorOutput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


protocol ProfileInteractorOutput: class {
    func didReceiveUser(_ user: UserTwo)
    func didReciveMeetsOrders(_ events: [EventTwo])
    func didReciveMeetsMade(_ events: [EventTwo])
    func didReciveError()
}
