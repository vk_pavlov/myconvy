//
//  ProfileInteractorInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol ProfileInteractorInput {
    func getUserById(userId: Int)
    func getMeetsMade(_ meetsMade: [Int])
    func getMeetsOrders(_ meetsOrders: [Int])
    func getMeetsWithSelf(_ meetsId: [Int], meets: @escaping ([EventTwo]) -> Void)
}
