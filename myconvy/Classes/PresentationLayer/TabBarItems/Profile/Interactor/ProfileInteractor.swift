//
//  ProfileInteractor.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class ProfileInteractor {
    weak var output: ProfileInteractorOutput!
    var userDataFetcher: UserDataFetcher!
    var eventDataFetcher: EventDataFetcher!
}


extension ProfileInteractor: ProfileInteractorInput {
    func getMeetsWithSelf(_ meetsId: [Int], meets: @escaping ([EventTwo]) -> Void) {
        eventDataFetcher.getMeetsByID(eventIds: meetsId) { (eventsResponse, error) in
            if let eventsResponse = eventsResponse {
                meets(eventsResponse.meetsInfo)
            }
        }
    }
    

    func getUserById(userId: Int) {
        userDataFetcher.getUserById(userId: userId) { [weak self] (userResponse, errorResponce) in
            if let userResponse = userResponse {
                self?.output.didReceiveUser(userResponse.userInfo)
            }
            
        }
    }
    
    func getMeetsMade(_ meetsMade: [Int]) {
        eventDataFetcher.getMeetsByID(eventIds: meetsMade) { [weak self] (eventsResponse, error) in
            guard let self = self else { return }
            if let eventsResponse = eventsResponse {
                self.output.didReciveMeetsMade(eventsResponse.meetsInfo)
            }
        }
    }
    
    func getMeetsOrders(_ meetsOrders: [Int]) {
        eventDataFetcher.getMeetsByID(eventIds: meetsOrders) { [weak self] (eventsResponse, error) in
            guard let self = self else { return }
            if let eventsResponse = eventsResponse {
                self.output.didReciveMeetsOrders(eventsResponse.meetsInfo)
            }
        }
    }
}
