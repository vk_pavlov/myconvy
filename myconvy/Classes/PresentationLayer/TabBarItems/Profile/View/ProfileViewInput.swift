//
//  ProfileViewInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol ProfileViewInput: class, Presentable {
    func setProfileViewModel(_ model: ProfileViewModel)
    func setupInitialState()
    func setMyEvents(_ eventCellViewModels: [EventCellViewModel])
    func setOtherEvents(_ eventCellViewModels: [EventCellViewModel])
    func showViewOfNoInternetConnection()
    func showSpinner()
}
