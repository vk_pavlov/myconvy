//
//  ProfileViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileViewController: UIViewController {

    @IBOutlet weak var eventsTableView: UITableView!
    
    var output: ProfileViewOutput!
    
    private let spinner = Spinner(style: .gray)
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        return refreshControl
    }()
    
    private let profileTableHeaderView = ProfileTableHeaderView()
    private var myEventsViewModels = [EventCellViewModel]()
    private var otherEventsViewModels = [EventCellViewModel]()
    private var eventsForShow = [EventCellViewModel]()
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        profileTableHeaderView.isSelf = true
        eventsTableView.backgroundColor = .white
        profileTableHeaderView.delegate = self

        eventsTableView.tableHeaderView = profileTableHeaderView

        profileTableHeaderView.centerXAnchor.constraint(equalTo: eventsTableView.centerXAnchor).isActive = true
        profileTableHeaderView.widthAnchor.constraint(equalTo: eventsTableView.widthAnchor).isActive = true
        profileTableHeaderView.topAnchor.constraint(equalTo: eventsTableView.topAnchor).isActive = true

        eventsTableView.tableHeaderView?.layoutIfNeeded()
        settingsLeftBarButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        spinner.setStateSpinner(enable: true, view: view)
        output.willUpdateUserInfo()
    }

    // MARK: Private

    private func settingsLeftBarButtonItem() {
        let i = navigationController!.viewControllers.index(of: self)!
        if i > 1 {
            self.navigationItem.leftBarButtonItem = nil
        }
    }

    private func createAnActionWhenYouClickOnAnAvatar() -> UIAlertController {
        let actionWhenYouClickOnAnAvatar = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel)
        let showPhotoImage = UIAlertAction(title: "Открыть фотографию", style: .default) { (UIAlertAction) in

        }
        let changePhotoImage = UIAlertAction(title: "Изменить фотографию", style: .default) { (UIAlertAction) in
            
        }
        let deletePhotoImage = UIAlertAction(title: "Удалить фотографию", style: .destructive){ (UIAlertAction) in
            
        }
        actionWhenYouClickOnAnAvatar.addAction(showPhotoImage)
        actionWhenYouClickOnAnAvatar.addAction(changePhotoImage)
        actionWhenYouClickOnAnAvatar.addAction(deletePhotoImage)
        actionWhenYouClickOnAnAvatar.addAction(cancel)
        return actionWhenYouClickOnAnAvatar
    }

    // MARK: Actions

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
//        let commonStubHeight = commonStubOne.bounds.height
//        commonStubTwo.removeFromView(view: eventsTableView, result: { () in
//            self.eventsTableView.contentInset.bottom -= commonStubHeight
//        })
//        commonStubTwo.removeFromView(view: eventsTableView, result: { () in
//            self.eventsTableView.contentInset.bottom -= commonStubHeight
//        })
        //output.didUpdateUserInfo(user.id)
    }
    
    @IBAction func sharesButtonPressed(_ sender: UIBarButtonItem) {
        let link = DefaultLinksShared.user + String(output.userId)
        let vc = UIActivityViewController(activityItems: [link], applicationActivities: [])
        present(vc, animated: true)
    }
    
    @IBAction func settingsButtonPressed(_ sender: UIBarButtonItem) {
        output.didSettingsProfileButtonPressed()
    }
    
}

extension ProfileViewController: ProfileViewInput {
    
    
    func showSpinner() {
        spinner.setStateSpinner(enable: true, view: self.view)
    }

    func showViewOfNoInternetConnection() {
        spinner.setStateSpinner(enable: false, view: self.view)
        refreshControl.endRefreshing()
        //noInternetConnectionView.addToView(view: view, result: nil)
    }

    func setOtherEvents(_ eventCellViewModels: [EventCellViewModel]) {
        otherEventsViewModels = eventCellViewModels
        spinner.setStateSpinner(enable: false, view: self.view)
        if profileTableHeaderView.selectedSegmentIndex == 1 {
            eventsForShow = otherEventsViewModels
        }
        //stateOfStub()
        //noInternetConnectionView.removeFromView(view: view, result: nil)
        eventsTableView.reloadData()
    }
    
    func setMyEvents(_ eventCellViewModels: [EventCellViewModel]) {
        spinner.setStateSpinner(enable: false, view: self.view)
        myEventsViewModels = eventCellViewModels
        if profileTableHeaderView.selectedSegmentIndex == 0 {
            eventsForShow = myEventsViewModels
        }
        eventsTableView.reloadData()
    }
    
    func setProfileViewModel(_ model: ProfileViewModel) {
        profileTableHeaderView.set(model: model.profileTitleViewViewModel)
        eventsTableView.updateHeaderViewHeight()
        refreshControl.endRefreshing()
        spinner.setStateSpinner(enable: false, view: self.view)
    }
    
    func setupInitialState() {
        eventsTableView.register(EventCell.nib, forCellReuseIdentifier: EventCell.identifier)
    }
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventsForShow.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = eventsTableView.dequeueReusableCell(withIdentifier: EventCell.identifier, for: indexPath) as! EventCell
        cell.set(viewModel: eventsForShow[indexPath.row])
        let view = UIView()
        view.backgroundColor = .clear
        cell.selectedBackgroundView = view
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedEventId = eventsForShow[indexPath.row].id
        output.didSelectedEventPressed(selectedEventId)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

}

extension ProfileViewController: ProfileTableHeaderViewDelegate {
    func profileTableHeaderViewDidChangeSegmentItem(_ view: ProfileTableHeaderView, at index: Int) {
        index == 0 ? (eventsForShow = myEventsViewModels) : (eventsForShow = otherEventsViewModels)
        eventsTableView.reloadData()
    }
    
    func profileTableHeaderViewMoreInfoButtonPressed(_ view: ProfileTableHeaderView) {
        output.didEditProfileButtonPressed()
    }
    
    func profileTableHeaderViewAvatarImageViewPressed(_ view: ProfileTableHeaderView, imageView: UIImageView) {
        output.didPhotoProfilePressed(imageView: imageView)
    }
    
    func profileTableHeaderViewDidEventsPressed(_ view: ProfileTableHeaderView) {
        let indexPath = IndexPath(row: 0, section: 0)
        if !eventsTableView.indexPathsForVisibleRows!.isEmpty {
            eventsTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    func profileTableHeaderViewDidSubscribersPressed(_ view: ProfileTableHeaderView) {
        output.didSubscribersButtonPressed()
    }
    
    func profileTableHeaderViewDidSubscriptionsPressed(_ view: ProfileTableHeaderView) {
        output.didSubscriptionsButtonPressed()
    }
}

