//
//  ProfileViewOutput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol ProfileViewOutput {
    func viewIsReady()
    func didSettingsProfileButtonPressed()
    func didEditProfileButtonPressed()
    func didSubscribersButtonPressed()
    func didSubscriptionsButtonPressed()
    func didSelectedEventPressed(_ eventId: Int)
    func willUpdateUserInfo()
    func didPhotoProfilePressed(imageView: UIImageView)
    
    var userId: Int { get }
}
