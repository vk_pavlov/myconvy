//
//  ProfileRouter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 20/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//
import UIKit

class ProfileRouter {
}

extension ProfileRouter: ProfileRouterInput {
    func presentPhotoBrowser(from: UIViewController, imageView: UIImageView) {
        guard let image = imageView.image else { return }
        PhotoBrowserModule.create().present(from: from, image: image, initialPosition: imageView.center)
    }

    func presentCreateEvent(from: UIViewController) {
       // NewEventModule.create().present(from: from)
    }

    func presentSelectedEvent(from: UIViewController, eventId: Int) {
        SelectedEventModule.create().present(from: from, eventId: eventId)
    }
    
    func presentRespondedEvent(from: UIViewController, eventId: String) {
       // SelectedEventModule.create().present(from: from, eventId: eventId)
    }
    
    func presentSettingsProfile(from: UIViewController) {
        SettingsProfileModule.create().present(from: from)
    }
    
    func presentEditProfile(from: UIViewController, editUserViewModel: EditProfileViewModel) {
        EditProfileModule.create().present(from: from, viewModel: editUserViewModel)
    }
    
    func presentSubscribers(from: UIViewController, users: [Int]) {
        ListUsersModule.create().present(from: from, users: users, navItemTitle: "Подписчики")
    }
    
    func presentSubscriptions(from: UIViewController, users: [Int]) {
        ListUsersModule.create().present(from: from, users: users, navItemTitle: "Подписки")
    }
}
