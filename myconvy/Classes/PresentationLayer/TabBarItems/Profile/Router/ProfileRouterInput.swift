//
//  ProfileRouterInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 20/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol ProfileRouterInput {
    func presentRespondedEvent(from: UIViewController, eventId: String)
    func presentSettingsProfile(from: UIViewController)
    func presentEditProfile(from: UIViewController, editUserViewModel: EditProfileViewModel)
    func presentSubscribers(from: UIViewController, users: [Int])
    func presentSubscriptions(from: UIViewController, users: [Int])
    func presentSelectedEvent(from: UIViewController, eventId: Int)
    func presentCreateEvent(from: UIViewController)
    func presentPhotoBrowser(from: UIViewController, imageView: UIImageView)
}
