//
//  ProfileModuleInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol ProfileModuleInput {
    var viewController: UIViewController { get }
    func present(from:UIViewController)
}
