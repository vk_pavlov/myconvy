//
//  File.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import Starscream
import SwiftyJSON

enum TypeAnswerSocket: String {
    case chat
    case chats
    case message
    case messages
}

class ChatsPresenter {
    weak var view: ChatsViewInput!
    var interactor: ChatsInteractorInput!
    var router: ChatsRouterInput!
    private var socket: WebSocket!
    private var ourselfId = CommonData.selfUser().id

    private func chatsParsing(data: JSON) -> [Chat] {
        guard
            let result = data["result"].dictionary,
            let chatsInfo = result["chats_info"]?.array
        else {
            return [Chat]()
        }
        var chats = [Chat]()
        for chatJSON in chatsInfo {
            let chat = Chat(data: chatJSON, ourselfId: String(ourselfId))
            chats.append(chat)
        }
        return chats
    }

    private func getChats() {
        socket.write(string: "method=getChats")
    }

}

extension ChatsPresenter: ChatsViewOutput {
    
    func didSelectedChat(chat: Chat) {
        socket.delegate = nil
        socket.pongDelegate = nil
        router.presentSelectedChat(from: view.viewController, chat: chat, socket: socket)
    }

    func disconnectSocket() {
        guard socket != nil else { return }
        socket.disconnect()
    }

    func updateSocketState() {
        if (socket.delegate == nil) {
            self.socket.delegate = self
        }
        if socket.isConnected {
            getChats()
        } else {
            socket.connect()
        }
    }

    func viewIsReady() {
        socket = WebSocket(url: URL(string: fullPathToSocket)!)
        socket.delegate = self
        view.setupInitialState()
    }
    
}

extension ChatsPresenter: ChatsInteractorOutput {
   
}

extension ChatsPresenter: ChatsModuleInput {
    var viewController: UIViewController {
        return view.viewController
    }
}


extension ChatsPresenter: WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        print("websocket is connected")
        getChats()
    }

    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("websocket is disconnected: \(error?.localizedDescription)")
    }

    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        //print("got some text: \(text)")
        do {
            guard let data = text.data(using: .utf16) else { return }
            let jsonData = try JSON(data: data)
            guard let typeAnswer = jsonData["type"].string else { return }
            let type = TypeAnswerSocket(rawValue: typeAnswer)!
            if type == .message {
                getChats()
                return
            } else {
                let chats = chatsParsing(data: jsonData)
                view.setChats(chats: chats)
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }

    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("got some data: \(data.count)")
    }

}

