//
//  MessagesViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu

class ChatsViewController: UIViewController {

    var output: ChatsViewOutput!
    private var chats = [Chat]()
    private lazy var noMessagesStubView = StubsFactory.makeStub(.noMessages)
    
    @IBOutlet weak var messagesTableView: UITableView!

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.updateSocketState()
    }

    deinit {
        output.disconnectSocket()
    }


    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        output.updateSocketState()
    }
    
    private func configureStub() {
        noMessagesStubView.removeFromSuperview()
        if chats.isEmpty {
            noMessagesStubView.translatesAutoresizingMaskIntoConstraints = true
            messagesTableView.addSubview(noMessagesStubView)
            noMessagesStubView.frame = view.frame
        }
    }
}

extension ChatsViewController: ChatsViewInput {
    func setChats(chats: [Chat]) {
        self.chats = chats
        configureStub()
        refreshControl.endRefreshing()
        messagesTableView.reloadData()
    }

    
    func setupInitialState() {
        messagesTableView.register(ChatCell.nib, forCellReuseIdentifier: ChatCell.identifier)
        messagesTableView.insertSubview(refreshControl, at: 0)
    }
    
    
}


extension ChatsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChatCell.identifier, for: indexPath) as! ChatCell
        cell.configure(chat: chats[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedChat = chats[indexPath.row]
        output.didSelectedChat(chat: selectedChat)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
