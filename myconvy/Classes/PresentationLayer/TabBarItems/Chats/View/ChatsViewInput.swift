//
//  MessagesViewInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol ChatsViewInput: class, Presentable {
    func setupInitialState()
    func setChats(chats: [Chat])
}
