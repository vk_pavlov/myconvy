//
//  MessagesViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu

class ChatsViewController: UIViewController {

    var output: ChatsViewOutput!
    private var searchController: UISearchController!
    private let items = ["Все сообщения", "Чаты по событиям", "Диалоги"]
    private var chats = [Chat]()
    private var searchResultChats = [Chat]()
    
    @IBOutlet weak var messagesTableView: UITableView!

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.updateSocketState()
    }

    deinit {
        output.disconnectSocket()
    }

    private func initialStateSlideMenu() {
//        let menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: BTTitle.title("Все сообщения"), items: items)
//        menuView.cellHeight = 44
//        self.navigationItem.titleView = menuView
//        menuView.didSelectItemAtIndexHandler = {[weak self] (indexPath: Int) -> () in
//            switch indexPath {
//            case 1:
//                self!.filterCategoryDialogs(for: .groupChat)
//            case 2:
//                self!.filterCategoryDialogs(for: .oneToOne)
//            default:
//                self!.filterCategoryDialogs(for: .all)
//            }
//        }
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        output.updateSocketState()
    }
    
    private func setupStateSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        messagesTableView.tableHeaderView = searchController.searchBar
        searchController.searchResultsUpdater = self
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск"
        searchController.searchBar.setValue("Отмена", forKey: "_cancelButtonText")
    }
    
    func filterContent(for searchText: String) {
        searchResultChats = chats.filter({ (chat) -> Bool in
            let isMatch = chat.chatName.localizedCaseInsensitiveContains(searchText)
            return isMatch
        })
    }

//    private func filterCategoryDialogs(for category: TypeChat) {
//        if category == .all {
//            searchResultChats = chats
//        } else {
//            searchResultChats = chats.filter({ (dialog) -> Bool in
//                let isMatch = (dialog.typeChat == category)
//                return isMatch
//            })
//        }
//        messagesTableView.reloadData()
//    }
}

extension ChatsViewController: ChatsViewInput {
    func setChats(chats: [Chat]) {
        self.chats = chats
        searchResultChats = chats
        refreshControl.endRefreshing()
        messagesTableView.reloadData()
    }

    
    func setupInitialState() {
        messagesTableView.register(ChatCell.nib, forCellReuseIdentifier: ChatCell.identifier)
        messagesTableView.insertSubview(refreshControl, at: 0)
//        initialStateSlideMenu()
        setupStateSearchController()
    }
    
    
}


extension ChatsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResultChats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChatCell.identifier, for: indexPath) as! ChatCell
        cell.configure(chat: searchResultChats[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedChat = searchResultChats[indexPath.row]
        output.didSelectedChat(chatId: selectedChat.chatId, chatName: selectedChat.chatName)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}


extension ChatsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchText = searchController.searchBar.text
        if searchText!.isEmpty {
            searchResultChats = chats
        } else {
            filterContent(for: searchText!)
        }
        messagesTableView.reloadData()
    }
    
    
}
