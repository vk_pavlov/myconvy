//
//  MessagesRouter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import Starscream


class ChatsRouter {
    
}

extension ChatsRouter: ChatsRouterInput {
    func presentSelectedChat(from: UIViewController, chat: Chat, socket: WebSocket) {
        SelectedChatModule.create().present(from: from, chat: chat, socket: socket)
    }

  
    
}
