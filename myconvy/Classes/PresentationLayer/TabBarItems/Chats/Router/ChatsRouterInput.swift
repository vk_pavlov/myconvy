//
//  MessagesRouterInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import Starscream

protocol ChatsRouterInput {
    func presentSelectedChat(from: UIViewController, chat: Chat, socket: WebSocket)
}
