//
//  MessagesModule.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class ChatsModule {
    class func create() -> ChatsModuleInput {
        let storyboard = UIStoryboard(name: "Chats", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ChatsViewController") as! ChatsViewController
        let presenter = ChatsPresenter()
        let interactor = ChatsInteractor()
        let router = ChatsRouter()
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        interactor.output = presenter
        return presenter
    }
}
