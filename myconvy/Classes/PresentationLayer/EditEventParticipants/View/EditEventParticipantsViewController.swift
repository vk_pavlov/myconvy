//
//  MyEventViewController.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class EditEventParticipantsViewController: UIViewController {
    
    var output: EditEventParticipantsViewOutput!
    private var сellViewModels = [[UserCellViewModel](), [UserCellViewModel]()]
    let spinner = Spinner(style: .gray)
    var commonStub: CommonStubView!
    private var currentModelForOperation: UserCellViewModel!
        
    @IBOutlet weak var listUsersTableView: UITableView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        //spinner.setStateSpinner(enable: true, view: self.view)
        output.viewIsReady()
    }
        
    private func setCommonStub() {
        let prevNavController = navigationController!.previousViewController()
        if let _ = prevNavController as? ProfileViewController {
            if navigationItem.title == "Подписчики" {
                //commonStub = CommonStubView(frame: view.bounds, stub: .mySubscribers, vc: self, targetOnButton: nil)
            } else {
                //commonStub = CommonStubView(frame: view.bounds, stub: .mySubscriptions, vc: self, targetOnButton: nil)
            }
        } else {
            if navigationItem.title == "Подписчики" {
                //commonStub = CommonStubView(frame: view.bounds, stub: .subscribers, vc: self, targetOnButton: nil)
            } else if navigationItem.title == "Подписки" {
                //commonStub = CommonStubView(frame: view.bounds, stub: .subscriptions, vc: self, targetOnButton: nil)
            }
        }
    }
        
    //    private func stateOfStub() {
    //        if commonStub != nil {
    //            if users.isEmpty {
    //                commonStub.addToView(view: listUsersTableView, result: nil)
    //            } else {
    //                commonStub.removeFromView(view: listUsersTableView, result: nil)
    //            }
    //        }
    //    }
}

extension EditEventParticipantsViewController: EditEventParticipantsViewInput {
    func deleteUser() {
        if let index = сellViewModels[0].index(of: currentModelForOperation) {
            сellViewModels[0].remove(at: index)
        }
        listUsersTableView.reloadData()
    }
    
    func addUser() {
        сellViewModels[0].append(currentModelForOperation)
        if let index = сellViewModels[1].index(of: currentModelForOperation) {
            сellViewModels[1].remove(at: index)
        }
        listUsersTableView.reloadData()
    }
    
    func rejectUser() {
        if let index = сellViewModels[1].index(of: currentModelForOperation) {
            сellViewModels[1].remove(at: index)
        }
        listUsersTableView.reloadData()
    }
    
    func setOrdersViewModels(_ userCellViewModels: [UserCellViewModel]) {
        сellViewModels[1] = userCellViewModels
        listUsersTableView.reloadData()
    }
    
    func setParticipantsViewModels(_ userCellViewModels: [UserCellViewModel]) {
//        spinner.setStateSpinner(enable: false, view: self.view)
        сellViewModels[0] = userCellViewModels
//        stateOfStub()
        listUsersTableView.reloadData()
    }
    
    func setupInitialState() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        listUsersTableView.register(UserCell.nib, forCellReuseIdentifier: UserCell.identifier)
        setCommonStub()
    }
}

extension EditEventParticipantsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = OrangeTheme.myconvyGray
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return сellViewModels[1].isEmpty ? nil : "Заявки"
        }
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return сellViewModels[1].isEmpty ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? сellViewModels[0].count : сellViewModels[1].count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UserCell.identifier) as! UserCell
        cell.delegate = self
        cell.set(viewModel: сellViewModels[indexPath.section][indexPath.row])
        indexPath.section == 0 ? (cell.isHiddenButtons = true) : (cell.isHiddenButtons = false)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userCellViewModel = сellViewModels[indexPath.section][indexPath.row]
        output.didSelectedUserPressed(userCellViewModel.id)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let viewModel = сellViewModels[0][indexPath.row]
            currentModelForOperation = viewModel
            output.deleteUserFromEvent(userId: viewModel.id)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
         return "Удалить"
    }
}

extension EditEventParticipantsViewController: UserCellDelegate {
    func userCellAddUserToEvent(_ cell: UserCell) {
        if let indexPathCell = listUsersTableView.indexPath(for: cell) {
            let viewModel = сellViewModels[1][indexPathCell.row]
            currentModelForOperation = viewModel
            output.didEditMeetPartRelation(action: .confirm, userId: viewModel.id)
        }
        
    }
    
    func userCellRejectUser(_ cell: UserCell) {
        if let indexPathCell = listUsersTableView.indexPath(for: cell) {
            let viewModel = сellViewModels[1][indexPathCell.row]
            currentModelForOperation = viewModel
            output.didEditMeetPartRelation(action: .denite, userId: viewModel.id)
        }
    }
}
