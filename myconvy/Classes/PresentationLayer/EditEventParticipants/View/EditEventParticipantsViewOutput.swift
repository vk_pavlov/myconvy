//
//  MyEventParticipantsOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

enum EditMeetRelAction: String {
    case confirm = "confirm"
    case denite = "denite"
}

protocol EditEventParticipantsViewOutput {
    func viewIsReady()
    func didEditMeetPartRelation(action: EditMeetRelAction, userId: Int)
    func didSelectedUserPressed(_ userId: Int)
    func deleteUserFromEvent(userId: Int)
}
