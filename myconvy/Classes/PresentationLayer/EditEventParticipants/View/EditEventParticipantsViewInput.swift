//
//  MyEventInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol EditEventParticipantsViewInput: class, Presentable {
    func setupInitialState()
    func setParticipantsViewModels(_ userCellViewModels: [UserCellViewModel])
    func setOrdersViewModels(_ userCellViewModels: [UserCellViewModel])
    func addUser()
    func rejectUser()
    func deleteUser()
}
