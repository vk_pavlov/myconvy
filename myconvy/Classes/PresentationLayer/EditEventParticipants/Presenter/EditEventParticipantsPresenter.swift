//
//  MyEventParticipantsPresenter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class EditEventParticipantsPresenter {
    weak var view: EditEventParticipantsViewInput!
    var interactor: EditEventParticipantsInteractorInput!
    var router: EditEventParticipantsRouterInput!
    
    private var participantsIds: [Int]!
    private var ordersIds: [Int]?
    private var eventId: Int!
    private var selfId = CommonData.selfUser().id
    
    private func cellViewModel(from: UserTwo) -> UserCellViewModel {
        return UserCellViewModel(id: from.id,
                                 imageUrl: from.avatar,
                                 online: from.online,
                                 name: from.name,
                                 agePlusCity: from.cityPlusAge)
    }
}

extension EditEventParticipantsPresenter: EditEventParticipantsViewOutput {
    func deleteUserFromEvent(userId: Int) {
        if selfId == userId {
            return
        }
        interactor.deleteUserFromEvent(eventId, userId: userId)
    }
    
    func didEditMeetPartRelation(action: EditMeetRelAction, userId: Int) {
        if action == .confirm {
            interactor.confirmUser(userId, eventId: eventId)
        } else {
            interactor.deniteUser(userId, eventId: eventId)
        }
    }
    
    func viewIsReady() {
        view.setupInitialState()
        
        if !participantsIds.isEmpty {
            interactor.getParticipants(usersIds: participantsIds)
        }
        
        if let ordersIds = ordersIds {
            if !ordersIds.isEmpty {
                interactor.getOrders(usersIds: ordersIds)
            }
        }
    }
    
    func didSelectedUserPressed(_ userId: Int) {
        router.presentUserProfile(from: view.viewController, userId: userId)
    }
}

extension EditEventParticipantsPresenter: EditEventParticipantsInteractorOutput {
    func didReciveDeleteUser() {
        view.deleteUser()
    }
    
    func didReciveAddUserToEvent() {
        view.addUser()
    }
    
    func didReciveDeniteUser() {
        view.rejectUser()
    }
    
    func didReciveParticipants(_ users: [UserTwo]) {
        let userCellViewModels = users.map { (user) in
            cellViewModel(from: user)
        }
        view.setParticipantsViewModels(userCellViewModels)
    }
    
    func didReciveOrders(_ users: [UserTwo]) {
        let userCellViewModels = users.map { (user) in
            cellViewModel(from: user)
        }
        view.setOrdersViewModels(userCellViewModels)
    }
}

extension EditEventParticipantsPresenter: EditEventParticipantsModuleInput {

    func present(from: UIViewController, users: [Int], otherUsers: [Int]?, eventId: Int) {
        self.participantsIds = users
        self.ordersIds = otherUsers
        self.eventId = eventId
        view.present(fromViewController: from)
    }
    
    var viewController: UIViewController {
        return view.viewController
    }
    
}
