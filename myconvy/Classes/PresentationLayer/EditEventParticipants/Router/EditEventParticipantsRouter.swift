//
//  MyEventParticipantsRouter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class EditEventParticipantsRouter {

}

extension EditEventParticipantsRouter: EditEventParticipantsRouterInput {
    func presentUserProfile(from: UIViewController, userId: Int) {
        if CommonData.selfUser().id == userId {
            ProfileModule.create().present(from:from)
        } else {
            UserProfileModule.create().present(from: from, userId: userId)
        }
    }
}
