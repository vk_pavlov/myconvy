//
//  MyEventParticipantsRouterInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol EditEventParticipantsRouterInput {
    func presentUserProfile(from: UIViewController, userId: Int)
}
