//
//  MyEventParticipantsInteractor.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

class EditEventParticipantsInteractor {
    weak var output: EditEventParticipantsInteractorOutput!
    var userDataFetcher: UserDataFetcher!
    var eventDataFetcher: EventDataFetcher!
}

extension EditEventParticipantsInteractor: EditEventParticipantsInteractorInput {
    func deleteUserFromEvent(_ eventId: Int, userId: Int) {
        eventDataFetcher.editMeetPartRelation(eventId: eventId, userId: userId, action: .denite) { (response, error) in
            if response != nil {
                self.output.didReciveDeleteUser()
            }
        }
    }
    
    func confirmUser(_ userId: Int, eventId: Int) {
        eventDataFetcher.editMeetPartRelation(eventId: eventId, userId: userId, action: .confirm) { (response, error) in
            if response != nil {
                self.output.didReciveAddUserToEvent()
            }
        }
    }
    
    func deniteUser(_ userId: Int, eventId: Int) {
        eventDataFetcher.editMeetPartRelation(eventId: eventId, userId: userId, action: .denite) { (response, error) in
            if response != nil {
                self.output.didReciveDeniteUser()
            }
        }
    }
    
    func getParticipants(usersIds: [Int]) {
        userDataFetcher.getUsersById(usersIds: usersIds) { (usersResponse, errorResponse) in
            if let usersResponse = usersResponse {
                self.output.didReciveParticipants(usersResponse.usersInfo)
            }
        }
    }
    
    func getOrders(usersIds: [Int]) {
        userDataFetcher.getUsersById(usersIds: usersIds) { (usersResponse, errorResponse) in
            if let usersResponse = usersResponse {
                self.output.didReciveOrders(usersResponse.usersInfo)
            }
        }
    }
}

