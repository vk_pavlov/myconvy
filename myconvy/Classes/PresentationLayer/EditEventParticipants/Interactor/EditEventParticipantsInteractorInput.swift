//
//  MyEventParticipantsInteractorInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol EditEventParticipantsInteractorInput {
    func getParticipants(usersIds: [Int])
    func getOrders(usersIds: [Int])
    func confirmUser(_ userId: Int, eventId: Int)
    func deniteUser(_ userId: Int, eventId: Int)
    func deleteUserFromEvent(_ eventId: Int, userId: Int)
}
