//
//  MyEventParticipantsInteractorOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol EditEventParticipantsInteractorOutput: class {
    func didReciveParticipants(_ users: [UserTwo])
    func didReciveOrders(_ users: [UserTwo])
    func didReciveAddUserToEvent()
    func didReciveDeniteUser()
    func didReciveDeleteUser()
}
