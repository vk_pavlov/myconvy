//
//  MyEventParticipantsModuleInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol EditEventParticipantsModuleInput {
    var viewController: UIViewController { get }
    func present(from: UIViewController, users: [Int], otherUsers: [Int]?, eventId: Int)
}
