//
//  MyEventParticipantsModule.swift
//  myconvy
//
//  Created by Евгений Каштанов on 16/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class EditEventParticipantsModule {
    class func create() -> EditEventParticipantsModuleInput {
        let storyboard = UIStoryboard(name: "EditEventParticipants", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EditEventParticipantsViewController") as! EditEventParticipantsViewController
        let presenter = EditEventParticipantsPresenter()
        let interactor = EditEventParticipantsInteractor()
        let router = EditEventParticipantsRouter()
        
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        
        interactor.output = presenter
        interactor.userDataFetcher = NetworkUserDataFetcher()
        interactor.eventDataFetcher = NetworkEventDataFetcher()
        return presenter
    }
}
