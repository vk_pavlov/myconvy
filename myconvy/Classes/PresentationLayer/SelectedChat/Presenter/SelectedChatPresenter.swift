//
//  SelectedDialogSelectedDialogPresenter.swift
//  myconvy
//
//  Created by jumper17 on 02/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit
import Starscream
import SwiftyJSON

class SelectedChatPresenter {

    weak var view: SelectedChatViewInput!
    var interactor: SelectedChatInteractorInput!
    var router: SelectedChatRouterInput!

    private var selectedChat: Chat!

    private var socket: WebSocket!

    private var isMessageSending = false
    private var lastMessageSending: MyMessage!

    private var ourselfId = CommonData.selfUser().id
    internal var start = 0

    private func getMessages() {
        socket.write(string: "method=getMessages&chatid=\(selectedChat.chatId)&start=\(start)")
    }

    private func parsedMessage(data: JSON) -> [MyMessage] {
        guard
            let result = data["result"].dictionary,
            let messagesInfo = result["messinfo"]?.array
            else {
                return [MyMessage]()
        }
        var messages = [MyMessage]()
        for messageJSON in messagesInfo {
            let message = MockMessage(data: messageJSON, ourselfId: String(ourselfId))
            messages.append(message)
        }
        messages.sort { (m1, m2) -> Bool in
            return m1.sentDate < m2.sentDate
        }
        return messages
    }

    private func parseMessage(data: JSON) -> MyMessage? {
        guard
            let newmessage = data["newmessage"].dictionary,
            let messData = newmessage["messdata"]
            else {
                return nil
        }
        let message = MockMessage(data: messData, ourselfId: String(ourselfId))
        return message
    }

    private func configBottomTitle() -> String {
        let participantsCount = selectedChat.participants.idS.count
        switch participantsCount {
        case 1:
            return "1 участник"
        case 2,3,4:
            return "\(participantsCount) участника"
        default:
            return "\(participantsCount) участников"
        }
    }
}

extension SelectedChatPresenter: SelectedChatModuleInput {

    func present(from: UIViewController, chat: Chat, socket: WebSocket) {
        self.selectedChat = chat
        self.socket = socket
        self.socket.delegate = self
        view.present(fromViewController: from)
    }

    var viewController: UIViewController {
        return view.viewController
    }
}

extension SelectedChatPresenter: SelectedChatViewOutput {
    func didParticipantsPressed() {
        let participants = selectedChat.participants.idS.map { (user) -> Int in
            return Int(user)!
        }
        router.presentListUsers(from: view.viewController, users: participants)
    }

    func didRightBarButtonItemPressed() {
        router.presentSelectedEvent(from: view.viewController, eventId: Int(selectedChat.eventId)!)
    }

    var currentChatId: String {
        return selectedChat.chatId
    }

    func sendMessage(message: MyMessage) {
        isMessageSending = true
        lastMessageSending = message
        socket.write(string: "method=sendMessage&chatid=\(selectedChat.chatId)&text=\(message.kind.get())")
    }


    func deleteDelegate() {
        self.socket.delegate = nil
    }

    func updateSocketState() {
        if socket.isConnected {
            getMessages()
        } else {
            socket.connect()
        }
    }

    func didAvatarUserPressed(userId: Int) {
        router.presentUserProfile(from: view.viewController, userId: userId)
    }
    
    func viewIsReady() {
        view.configTitleView(title: selectedChat.chatName, bottomTitle: configBottomTitle())
        view.setupInitialState()
    }
    
}

extension SelectedChatPresenter: SelectedChatInteractorOutput {
    
}

extension SelectedChatPresenter: WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        print("websocket is connected")
        getMessages()
    }

    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("websocket is disconnected: \(error?.localizedDescription)")
    }

    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        //print("got some text: \(text)")
        do {
            guard let data = text.data(using: .utf16) else { return }
            let jsonData = try JSON(data: data)
            guard let typeAnswer = jsonData["type"].string else {
                if isMessageSending {
                    guard let result = jsonData["result"].dictionary,
                        let _ = result["success"] else { return }
                    isMessageSending = false
                    view.insertNewMessage(message: lastMessageSending)
                }
                return
            }
            let type = TypeAnswerSocket(rawValue: typeAnswer)!
            if type == .messages {
                let messages = parsedMessage(data: jsonData)
                start += messages.count
                view.setMessages(messages: messages)
            } else if type == .message {
                guard let message = parseMessage(data: jsonData) else { return }
                view.insertNewMessage(message: message)
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }

    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        //print("got some data: \(data.count)")
    }

}
