//
//  SelectedDialogSelectedDialogRouterInput.swift
//  myconvy
//
//  Created by jumper17 on 02/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

protocol SelectedChatRouterInput {
    func presentUserProfile(from: UIViewController, userId: Int)
    func presentSelectedEvent(from: UIViewController, eventId: Int)
    func presentListUsers(from: UIViewController, users: [Int])
}
