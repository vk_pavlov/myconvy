//
//  SelectedDialogSelectedDialogRouter.swift
//  myconvy
//
//  Created by jumper17 on 02/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

class SelectedChatRouter {

}

extension SelectedChatRouter: SelectedChatRouterInput {
    func presentListUsers(from: UIViewController, users: [Int]) {
        ListUsersModule.create().present(from: from, users: users, navItemTitle: "Участники")
    }

    func presentSelectedEvent(from: UIViewController, eventId: Int) {
        SelectedEventModule.create().present(from: from, eventId: eventId)
    }

    func presentUserProfile(from: UIViewController, userId: Int) {
        if CommonData.selfUser().id == userId {
            ProfileModule.create().present(from:from)
        } else {
            UserProfileModule.create().present(from: from, userId: userId)
        }
    }
    
    
}
