//
//  SelectedDialogSelectedDialogViewInput.swift
//  myconvy
//
//  Created by jumper17 on 02/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

protocol SelectedChatViewInput: class, Presentable {
    func setupInitialState()
    func configTitleView(title: String, bottomTitle: String)
    func setMessages(messages: [MyMessage])
    func insertNewMessage(message: MyMessage)
}
