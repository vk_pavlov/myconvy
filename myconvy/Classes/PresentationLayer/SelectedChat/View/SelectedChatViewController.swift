//
//  SelectedDialogSelectedDialogViewController.swift
//  myconvy
//
//  Created by jumper17 on 02/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit
import MessageKit
import InputBarAccessoryView

class SelectedChatViewController: MessagesViewController {


    @IBOutlet weak var titleSelectedChat: UILabel!
    @IBOutlet weak var participantsButton: UIButton!

    var output: SelectedChatViewOutput!
    private var firstShow = true

    lazy var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()

    private var messageList = [MyMessage]()
    let ourself = CommonData.selfUser()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectedChatIsShow"), object: nil)
        output.updateSocketState()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectedChatIsHidden"), object: nil)
        output.deleteDelegate()
    }

    @IBAction func rightBarButtonItemPressed(_ sender: UIBarButtonItem) {
        output.didRightBarButtonItemPressed()
    }

    @objc func participantsButtonPressed() {
        output.didParticipantsPressed()
    }

    private func configInputTextView() {
        messageInputBar.inputTextView.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 0.4)
        messageInputBar.inputTextView.layer.borderWidth = 1
        messageInputBar.inputTextView.layer.borderColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 0.4).cgColor
        messageInputBar.inputTextView.layer.cornerRadius = 16
        messageInputBar.inputTextView.placeholder = "Сообщение"
        messageInputBar.inputTextView.textContainerInset = UIEdgeInsets(top: 7, left: 8, bottom: 8, right: 4)
        messageInputBar.inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 7, left: 12, bottom: 8, right: 12)
        messageInputBar.inputTextView.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)

       // messagesCollectionView.transform = CGAffineTransform.init(rotationAngle: (-(CGFloat)(Double.pi)))
        
    }

   //func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        if indexPath.row == start - 3 && start % 20 == 0 {
        //            output.updateListEvents(start: start)
        //        }
        //
        //    }


    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if indexPath.section == 5 && output.start % 100 == 0 && !firstShow {
//            output.updateSocketState()
//        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell  {
        if indexPath.section < 4 {
            print("")
            // Are we within 3 rows from the top?
//            if !isLoadingList && !allRecordsLoaded {
//                loadMessages(messageList.count + AdditionalRecordsToLoad)
//            }
        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }


}

extension SelectedChatViewController: SelectedChatViewInput {
    func insertNewMessage(message: MyMessage) {
        messageList.append(message)
        messagesCollectionView.insertSections([messageList.count - 1])
        messagesCollectionView.scrollToBottom()
    }


    func setMessages(messages: [MyMessage]) {
        //let newMessage = messages + messageList
        messageList = messages
        messagesCollectionView.reloadData()
        if firstShow {
            messagesCollectionView.scrollToBottom()
        }
        firstShow = false
    }

    func configTitleView(title: String, bottomTitle: String) {
        let titleView = TitleView()
        titleView.topTitleString = title
        titleView.bottomTitleString = bottomTitle
        titleView.bottomTitle.addTarget(self, action: #selector(participantsButtonPressed), for: .touchUpInside)
        navigationItem.titleView = titleView
    }

    func setupInitialState() {

        self.navigationController!.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .white
        configInputTextView()

        messageInputBar.padding = UIEdgeInsets(top: 10, left: 12, bottom: 8, right: 15)
        messageInputBar.separatorLine.backgroundColor = UIColor(red: 187/255, green: 187/255, blue: 187/255, alpha: 0.3)
        messageInputBar.sendButton.title = ""
        messageInputBar.sendButton.activityViewColor = OrangeTheme.myconvyOrange
        messageInputBar.sendButton.image = UIImage(named: "send-letter")
        messageInputBar.sendButton.setSize(CGSize(width: 32, height: 32), animated: false)
        messageInputBar.setRightStackViewWidthConstant(to: 30, animated: false)
        
        messageInputBar.delegate = self
        messageInputBar.sendButton.tintColor = OrangeTheme.myconvyOrange


        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messageCellDelegate = self
        messagesCollectionView.prefetchDataSource = self

        scrollsToBottomOnKeyboardBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true

        //navigationController?.navigationBar.tintColor = OrangeTheme.myconvyOrange
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
            layout.setMessageIncomingAvatarPosition(.init(vertical: .messageBottom))
            layout.setMessageOutgoingAvatarSize(.zero)
        }
    }


}

extension SelectedChatViewController: MessageInputDelegate {
    func sendWasTapped(message: String) {

    }


}

extension SelectedChatViewController: MessagesDataSource {

    func currentSender() -> SenderType {
        return Sender(senderId: String(ourself.id), displayName: ourself.fullName)
    }

    //    func otherSender() -> Sender {
    //        return Sender(senderId: "456", displayName: "知らない人")
    //    }

    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messageList.count
    }

    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messageList[indexPath.section]
    }

    // Дата над яяейками (посередине экрана)
        func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
//            if Calendar.current.isDateInToday(message.sentDate) && Calendar.current.isDateInYesterday(messageList[indexPath.section - 1].sentDate) {
//                return NSAttributedString(
//                    string: MessageKitDateFormatter.shared.string(from: message.sentDate),
//                    attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10),
//                                 NSAttributedString.Key.foregroundColor: UIColor.darkGray]
//                )
//            }
            return nil
        }

    // Над ячейкой
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if !isFromCurrentSender(message: message) {
            let name = message.sender.displayName
            return NSAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
        }
        return nil
    }

    // под ячейкой
//        func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
//            let dateString = formatter.string(from: message.sentDate)
//            return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
//        }

}

extension SelectedChatViewController: MessagesDisplayDelegate {

    func configureAccessoryView(_ accessoryView: UIView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {

        let maxYPosition = accessoryView.center.y
        var frameLabel = CGRect(x: -35, y: maxYPosition - 20, width: 40, height: 20)
        if !isFromCurrentSender(message: message) {
            frameLabel.origin = CGPoint(x: 6, y: maxYPosition - 40)
        }

        let label = UILabel(frame: frameLabel)
        let attrString = NSAttributedString(
                                string: message.sentDate.getDateAsStringWithFormat(dateFormat: "HH:mm"),
                                attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10),
                                             NSAttributedString.Key.foregroundColor: UIColor.darkGray]
                            )

        label.attributedText = attrString
        if !accessoryView.subviews.isEmpty {
            accessoryView.subviews[0].removeFromSuperview()
        }
        accessoryView.addSubview(label)
    }

    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .white : .darkText
    }

    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ?
            UIColor(red: 223/255, green: 114/255, blue: 0/255, alpha: 1) :
            UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
    }

//    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
//        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
//        return .bubbleTail(corner, .curved)
//    }

    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        guard let url = URL(string: messageList[indexPath.section].author.photoProfile) else {
            avatarView.set(avatar: Avatar(initials: "?"))
            return
        }
        avatarView.kf.setImage(with: url)
    }

}


extension SelectedChatViewController: MessagesLayoutDelegate {

    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        //if indexPath.section % 3 == 0 { return 10 }
        return 0
    }

    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        if isFromCurrentSender(message: message) {
            return 0
        }
        return 16
    }

    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 10
    }

}

extension SelectedChatViewController: MessageCellDelegate {

//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if indexPath.row == start - 3 && start % 20 == 0 {
//            output.updateListEvents(start: start)
//        }
//
//    }


    func didTapMessage(in cell: MessageCollectionViewCell) {
        print("Message tapped")
    }

    func didTapAvatar(in cell: MessageCollectionViewCell) {
        guard let indexPath = messagesCollectionView.indexPath(for: cell) else { return }
        let selectedSender = messageList[indexPath.section].author
        output.didAvatarUserPressed(userId: Int(selectedSender.id))
    }
}

extension SelectedChatViewController: MessageInputBarDelegate {

    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        for component in inputBar.inputTextView.components {
            if let image = component as? UIImage {

                //                    let imageMessage = MockMessage(image: image, sender: currentSender() as! Sender, messageId: UUID().uuidString, date: Date())
                //                    messageList.append(imageMessage)
                //                    messagesCollectionView.insertSections([messageList.count - 1])

            } else if let text = component as? String {

                let attributedText = NSAttributedString(string: text, attributes: [.font: UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular),
                                                                                   .foregroundColor: UIColor.white])
                let message = MockMessage(attributedText: attributedText,
                                          sender: currentSender(),
                                          messageId: UUID().uuidString,
                                          date: Date(),
                                          ourself: ourself,
                                          chatId: output.currentChatId)
                output.sendMessage(message: message)
            }
        }
        inputBar.inputTextView.text = String()
    }
}


fileprivate extension UIEdgeInsets {
    init(top: CGFloat = 0, bottom: CGFloat = 0, left: CGFloat = 0, right: CGFloat = 0) {
        self.init(top: top, left: left, bottom: bottom, right: right)
    }
}

extension SelectedChatViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {

    }


}
