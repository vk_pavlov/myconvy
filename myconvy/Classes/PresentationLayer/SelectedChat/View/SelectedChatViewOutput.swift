//
//  SelectedDialogSelectedDialogViewOutput.swift
//  myconvy
//
//  Created by jumper17 on 02/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

protocol SelectedChatViewOutput {
    func viewIsReady()
    func didAvatarUserPressed(userId: Int)
    func updateSocketState()
    func deleteDelegate()
    func sendMessage(message: MyMessage)
    func didRightBarButtonItemPressed()
    func didParticipantsPressed()

    var currentChatId: String { get }
    var start: Int { get set }
}
