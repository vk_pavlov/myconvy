//
//  TitleView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 10/08/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class TitleView: UIView {

    @IBOutlet weak var topTitle: UILabel!
    @IBOutlet weak var bottomTitle: UIButton!
    @IBOutlet var contentView: UIView!

    var topTitleString: String {
        set {
            topTitle.text = newValue
        }
        get {
            return topTitle.text!
        }
    }

    var bottomTitleString: String {
        set {
            bottomTitle.setTitle(newValue, for: .normal)
        }
        get {
            return bottomTitle.currentTitle!
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configTitleView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configTitleView()
    }

    private func configTitleView() {
        Bundle.main.loadNibNamed("TitleView", owner: self, options: nil)
        contentView.fixInView(self)
    }

    // костыль для того чтобы тап по кнопке распознавался (изначально задается размер всех subView в 0)
    // https://stackoverflow.com/questions/45996888/uitapgesturerecognizer-on-navigationitem-titleview-not-working-on-ios-11/46229619

    override var intrinsicContentSize: CGSize {
        return CGSize(width: 40, height: 40)//UIView.layoutFittingExpandedSize
    }

}
