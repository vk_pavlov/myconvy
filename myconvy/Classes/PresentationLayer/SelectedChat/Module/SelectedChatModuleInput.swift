//
//  SelectedDialogSelectedDialogModuleInput.swift
//  myconvy
//
//  Created by jumper17 on 02/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit
import Starscream

protocol SelectedChatModuleInput: class {
    var viewController: UIViewController { get }
    func present(from: UIViewController, chat: Chat, socket: WebSocket)
}
