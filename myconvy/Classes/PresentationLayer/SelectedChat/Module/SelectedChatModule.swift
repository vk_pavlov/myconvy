//
//  SelectedDialogSelectedDialogInitializer.swift
//  myconvy
//
//  Created by jumper17 on 02/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

class SelectedChatModule {
    class func create() -> SelectedChatModuleInput {
        let storyboard = UIStoryboard(name: "SelectedChat", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectedChatViewController") as! SelectedChatViewController
        let presenter = SelectedChatPresenter()
        let interactor = SelectedChatInteractor()
        let router = SelectedChatRouter()
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        interactor.output = presenter
        return presenter
    }
 

}
