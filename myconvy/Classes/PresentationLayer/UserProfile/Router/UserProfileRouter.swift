//
//  UserProfileRouter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 17/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class UserProfileRouter {
}

extension UserProfileRouter: UserProfileRouterInput {
    func presentPhotoBrowser(from: UIViewController, imageView: UIImageView) {
        guard let image = imageView.image else { return }
        PhotoBrowserModule.create().present(from: from, image: image, initialPosition: imageView.center)
    }

    func presentSelectedEvent(from: UIViewController, eventId: Int) {
        SelectedEventModule.create().present(from: from, eventId: eventId)
    }
    
    func presentInfoAboutUser(from: UIViewController, userId: Int) {
        InfoAboutUserModule.create().present(from: from, userId: userId)
    }
    
    func presentSubscribers(from: UIViewController, users: [Int]) {
        ListUsersModule.create().present(from: from, users: users, navItemTitle: "Подписчики")
    }
    
    func presentSubscriptions(from: UIViewController, users: [Int]) {
        ListUsersModule.create().present(from: from, users: users, navItemTitle: "Подписки")
    }
}
