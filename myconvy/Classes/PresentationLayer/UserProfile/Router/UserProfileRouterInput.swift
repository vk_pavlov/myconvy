//
//  UserProfileRouterInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 17/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol UserProfileRouterInput {
    func presentSelectedEvent(from: UIViewController, eventId: Int)
    func presentInfoAboutUser(from: UIViewController, userId: Int)
    func presentSubscribers(from: UIViewController, users: [Int])
    func presentSubscriptions(from: UIViewController, users: [Int])
    func presentPhotoBrowser(from: UIViewController, imageView: UIImageView)
}
