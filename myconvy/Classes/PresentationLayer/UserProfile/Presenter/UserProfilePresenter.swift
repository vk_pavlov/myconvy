//
//  UserProfilePresenter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 17/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import GoogleMaps

enum RelationAction: String {
    case subscribe
    case unsubscribe
}

class UserProfilePresenter: NSObject {
    weak var view: UserProfileViewInput!
    var interactor: UserProfileInteractorInput!
    var router: UserProfileRouterInput!
    var user: UserTwo!
    var userId: Int!
    var selfUserId = CommonData.selfUser().id
    private let locationManager = CLLocationManager()
    private var lastMyLocation: CLLocation?
    
    private func profileViewModel() -> ProfileViewModel {
        let headerUserInfoModel = HeaderUserInfoViewModel(fullName: user.fullName,
                                                          otherInfo: user.cityPlusAge,
                                                          avatarUrl: user.avatar,
                                                          online: user.online)
        
        let counterStackViewViewModel = CountersStackViewViewModel(eventsCount: user.meetsCreated!.count,
                                                                   subscribersCount: user.usersFollowers!.count,
                                                                   subscriptionsCount: user.usersFollowing!.count)
        
        let profileTableTitleViewViewModel = ProfileTableTitleViewViewModel(headerUserInfoViewModel: headerUserInfoModel,
                                                                            counterStackViewViewModel: counterStackViewViewModel,
                                                                            isSubscribtion: self.isSubscribtion)
        return ProfileViewModel(profileTitleViewViewModel: profileTableTitleViewViewModel)
    }
    
    private var isSubscribtion: Bool {
        return user.usersFollowers!.contains(selfUserId) ? true : false
    }
    
    private func cellViewModel(from: EventTwo) -> EventCellViewModel {
        return EventCellViewModel(id: from.id,
                                  authorId: from.creator.id,
                                  title: from.title,
                                  cagegory: from.category,
                                  author: from.creator.namePlusAge,
                                  distance: distanceToEvent(eventPlace: from.place),
                                  startDate: Date(timeIntervalSince1970: from.startDate),
                                  callerStatus: CallerStatus(rawValue: from.callerStatus)!,
                                  eventStatus: EventStatus(rawValue: from.status)!,
                                  isOrders: from.isOrders)
    }
    
    private func distanceToEvent(eventPlace: Place) -> Double {
        let toPoint = CLLocation(latitude: eventPlace.lat, longitude: eventPlace.lng)
        guard lastMyLocation != nil else { return 0 }
        return lastMyLocation!.distance(from: toPoint)
    }
}

extension UserProfilePresenter: UserProfileViewOutput {
    func didSubscriptionsButtonPressed() {
        guard let usersFollowing = user.usersFollowing else { return }
        router.presentSubscriptions(from: view.viewController, users: usersFollowing)
    }
    
    func didSubscribersButtonPressed() {
        guard let usersFollowers = user.usersFollowers else { return }
        router.presentSubscribers(from: view.viewController, users: usersFollowers)
    }
    
    func changeRelation(action: RelationAction) {
        interactor.editUserRel(action: action, userId: userId)
    }
    
    var userID: Int {
        return userId
    }
    
    func didPhotoProfilePressed(imageView: UIImageView) {
        router.presentPhotoBrowser(from: view.viewController, imageView: imageView)
    }
    
    func didSelectedEventPressed(_ eventId: Int) {
        router.presentSelectedEvent(from: view.viewController, eventId: eventId)
    }
    
    func viewIsReady() {
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        interactor.getUserByID(userId: userId)
        view.setupInitialState()
    }
    
    func didInfoAboutUserPressed() {
        router.presentInfoAboutUser(from: view.viewController, userId: userId)
    }
    
}

extension UserProfilePresenter: UserProfileInteractorOutput {
    func didFailureUser(error: String) {
        view.showErrorAlert(error: error)
    }
    
    func didFailureOpenUser() {
        view.dissmiss()
    }

    func didReciveError(error: Error) {
        //view.showErrorAlert(error: error)
    }

    func didReciveEditUserRel() {
        view.updateRelationsBetweenUsers()
    }
    
    func didReciveMeetsMade(_ events: [EventTwo]) {
        let activeEvents = events.filter {$0.status != 3}
        let archiveEvents = events.filter {$0.status == 3}
        
        let meetsMade = activeEvents + archiveEvents.reversed()
        let cellsViewModel = meetsMade.map { (event) in
            cellViewModel(from: event)
        }
        view.setEventsViewModel(cellsViewModel)
    }
    
    func didReciveUser(_ user: UserTwo) {
        locationManager.stopUpdatingLocation()
        self.user = user
        let userViewModel = profileViewModel()
        view.set(userViewModel)
        guard let meetsCreated = user.meetsCreated else { return }
        interactor.getMeetsMade(meetsCreated)
    }
    
}

extension UserProfilePresenter: UserProfileModuleInput {
    func presentModal(feom viewController: UIViewController, userId: Int) {
        view.presentModal(fromViewController: viewController)
    }

    func present(from viewController: UIViewController, userId: Int) {
        self.userId = userId
        view.present(fromViewController: viewController)
    }
    
    var viewController: UIViewController {
        return view.viewController
    }
    
}

extension UserProfilePresenter: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        lastMyLocation = locations.last
    }
}
