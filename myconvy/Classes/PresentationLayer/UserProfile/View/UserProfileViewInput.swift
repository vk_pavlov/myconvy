//
//  UserProfileViewInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 17/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

protocol UserProfileViewInput: class, Presentable {
    func set(_ viewModel: ProfileViewModel)
    func setupInitialState()
    func setEventsViewModel(_ viewModels: [EventCellViewModel])
    func updateRelationsBetweenUsers()
    func showErrorAlert(error: String)
}
