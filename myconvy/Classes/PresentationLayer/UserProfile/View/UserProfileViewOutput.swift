//
//  UserProfileViewOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 17/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol UserProfileViewOutput {
    func viewIsReady()
    func didInfoAboutUserPressed()
    func changeRelation(action: RelationAction)
    func didSubscriptionsButtonPressed()
    func didSubscribersButtonPressed()
    func didSelectedEventPressed(_ eventId: Int)
    func didPhotoProfilePressed(imageView: UIImageView)
    var userID: Int { get }
}
