//
//  UserProfileViewController.swift
//  myconvy
//
//  Created by Евгений Каштанов on 17/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController {
    
    @IBOutlet weak var userProfileTableView: UITableView!
    
    var output: UserProfileViewOutput!
    private var userEventsViewModel = [EventCellViewModel]()
    private let spinner = Spinner(style: .gray)

    private var commonStub: CommonStubView!
    
    private let profileTableHeaderView = ProfileTableHeaderView()
    private var currentRelationAction: RelationAction!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileTableHeaderView.isSelf = false
        profileTableHeaderView.delegate = self

        userProfileTableView.tableHeaderView = profileTableHeaderView

        profileTableHeaderView.centerXAnchor.constraint(equalTo: userProfileTableView.centerXAnchor).isActive = true
        profileTableHeaderView.widthAnchor.constraint(equalTo: userProfileTableView.widthAnchor).isActive = true
        profileTableHeaderView.topAnchor.constraint(equalTo: userProfileTableView.topAnchor).isActive = true
        
        userProfileTableView.tableHeaderView?.layoutIfNeeded()
        output.viewIsReady()
    }
    
    // MARK: Actions
    
    @IBAction func rightBarButtonPressed(_ sender: UIBarButtonItem) {
        showActionSheetUserProfile()
    }
    
    private func showActionSheetUserProfile() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel)
        let sharedAction = UIAlertAction(title: "Поделиться", style: .default) { (UIAlertAction) in
            self.sharedButtonPressed()
        }
//        let blockUser = UIAlertAction(title: "Заблокировать", style: .destructive)
//            { (UIAlertAction) in
//        }
//        let complainAction = UIAlertAction(title: "Пожаловаться", style: .destructive)
//        { (UIAlertAction) in
//        }
        actionSheet.addAction(sharedAction)
        //actionSheet.addAction(blockUser)
        //actionSheet.addAction(complainAction)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    private func stateOfStub() {
//        if meetsMade.isEmpty {
//            let commonStubHeight = commonStub.bounds.height
//            userProfileTableView.contentInset.bottom += commonStubHeight
//            commonStub.addToView(view: userProfileTableView, result: nil)
//        } else {
//            commonStub.removeFromView(view: userProfileTableView, result: nil)
//        }
    }

    private func sharedButtonPressed() {
        let link = DefaultLinksShared.user + String(output.userID)
        let vc = UIActivityViewController(activityItems: [link], applicationActivities: [])
        present(vc, animated: true)
    }
    
}

extension UserProfileViewController: UserProfileViewInput {
    func showErrorAlert(error: String) {
        spinner.setStateSpinner(enable: false, view: self.view)
        let alert = UIAlertController(title: "Ошибка", message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ок", style: .cancel, handler: { _ in
            self.dissmiss()
        })
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }

    
    func updateRelationsBetweenUsers() {
        currentRelationAction == .subscribe ? profileTableHeaderView.subscribe() : profileTableHeaderView.unsubscribe()
    }
    
    func setEventsViewModel(_ viewModels: [EventCellViewModel]) {
        spinner.setStateSpinner(enable: false, view: self.view)
        userEventsViewModel = viewModels
//        stateOfStub()
        userProfileTableView.reloadData()
    }
    
    func set(_ viewModel: ProfileViewModel) {
        profileTableHeaderView.set(model: viewModel.profileTitleViewViewModel)
        userProfileTableView.updateHeaderViewHeight()
        spinner.setStateSpinner(enable: false, view: self.view)
    }
    
    func setupInitialState() {
        userProfileTableView.register(EventCell.nib, forCellReuseIdentifier: EventCell.identifier)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}


extension UserProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userEventsViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = userEventsViewModel[indexPath.row]
        let cell = userProfileTableView.dequeueReusableCell(withIdentifier: EventCell.identifier, for: indexPath) as! EventCell
        cell.set(viewModel: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = userEventsViewModel[indexPath.row]
        output.didSelectedEventPressed(viewModel.id)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}

extension UserProfileViewController: ProfileTableHeaderViewDelegate {
    
    func profileTableHeaderViewSubscribe(_ view: ProfileTableHeaderView) {
        currentRelationAction = .subscribe
        output.changeRelation(action: .subscribe)
    }
    
    func profileTableHeaderViewUnsubscribe(_ view: ProfileTableHeaderView) {
        currentRelationAction = .unsubscribe
        output.changeRelation(action: .unsubscribe)
    }
    
    func profileTableHeaderViewMoreInfoButtonPressed(_ view: ProfileTableHeaderView) {
        output.didInfoAboutUserPressed()
    }
    
    func profileTableHeaderViewAvatarImageViewPressed(_ view: ProfileTableHeaderView, imageView: UIImageView) {
        output.didPhotoProfilePressed(imageView: imageView)
    }
    
    func profileTableHeaderViewDidEventsPressed(_ view: ProfileTableHeaderView) {
        let indexPath = IndexPath(row: 0, section: 0)
        if !userProfileTableView.indexPathsForVisibleRows!.isEmpty {
            userProfileTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    func profileTableHeaderViewDidSubscribersPressed(_ view: ProfileTableHeaderView) {
        output.didSubscribersButtonPressed()
    }
    
    func profileTableHeaderViewDidSubscriptionsPressed(_ view: ProfileTableHeaderView) {
        output.didSubscriptionsButtonPressed()
    }
    
}
