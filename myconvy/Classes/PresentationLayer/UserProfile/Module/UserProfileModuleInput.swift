//
//  UserProfileModuleInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 17/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol UserProfileModuleInput {
    var viewController: UIViewController { get }
    func present(from viewController: UIViewController, userId: Int)
    func presentModal(feom viewController: UIViewController, userId: Int)
}
