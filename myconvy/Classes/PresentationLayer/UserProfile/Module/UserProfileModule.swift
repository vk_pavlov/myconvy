//
//  UserProfileModule.swift
//  myconvy
//
//  Created by Евгений Каштанов on 17/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class UserProfileModule {
    class func create() -> UserProfileModuleInput {
        let storyboard = UIStoryboard(name: "UserProfile", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        let presenter = UserProfilePresenter()
        let interactor = UserProfileInteractor()
        let router = UserProfileRouter()
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        
        interactor.output = presenter
        interactor.userDataFetcher = NetworkUserDataFetcher()
        interactor.eventDataFetcher = NetworkEventDataFetcher()
        return presenter
    }
}
