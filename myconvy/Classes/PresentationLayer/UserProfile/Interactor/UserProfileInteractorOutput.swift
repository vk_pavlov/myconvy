//
//  UserProfileInteractorOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 17/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


protocol UserProfileInteractorOutput: class {
    func didReciveUser(_ user: UserTwo)
    func didFailureUser(error: String)
    func didReciveMeetsMade(_ events: [EventTwo])
    func didReciveEditUserRel()
    func didReciveError(error: Error)
}
