//
//  UserProfileInteractorInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 17/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


protocol UserProfileInteractorInput {
    func getUserByID(userId: Int)
    func getMeetsMade(_ meetsMade: [Int])
    func editUserRel(action: RelationAction, userId: Int)
}
