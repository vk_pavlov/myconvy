//
//  UserProfileInteractor.swift
//  myconvy
//
//  Created by Евгений Каштанов on 17/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

class UserProfileInteractor {
    weak var output: UserProfileInteractorOutput!
    var userDataFetcher: UserDataFetcher!
    var eventDataFetcher: EventDataFetcher!
}

extension UserProfileInteractor: UserProfileInteractorInput {
    func editUserRel(action: RelationAction, userId: Int) {
        userDataFetcher.editUserRel(action: action, userId: userId) { (successResponse, error) in
            if successResponse != nil {
                self.output.didReciveEditUserRel()
            }
        }
    }
    
    func getUserByID(userId: Int) {
        userDataFetcher.getUserById(userId: userId) { [weak self] (responseUser, responseError) in
            guard let self = self else { return }
            if let responseUser = responseUser {
                self.output.didReciveUser(responseUser.userInfo)
            }
            if let responseError = responseError {
                self.output.didFailureUser(error: responseError.problem)
            }
        }
    }
    
    func getMeetsMade(_ meetsMade: [Int]) {
        eventDataFetcher.getMeetsByID(eventIds: meetsMade) { (eventsResponse, error) in
            if let eventsResponse = eventsResponse {
                self.output.didReciveMeetsMade(eventsResponse.meetsInfo)
            }
        }
    }
    
}
