//
//  BlacklistSettingsModuleInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 04/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol BlacklistSettingsModuleInput {
    var viewController: UIViewController { get }
    func present(from: UIViewController)
}
