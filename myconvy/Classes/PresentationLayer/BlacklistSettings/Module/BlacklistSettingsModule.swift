//
//  BlacklistSettingsModule.swift
//  myconvy
//
//  Created by Евгений Каштанов on 04/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class BlacklistSettingsModule {
    class func create() -> BlacklistSettingsModuleInput {
        let storyboard = UIStoryboard(name: "BlacklistSettings", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "BlacklistSettingsViewController") as! BlacklistSettingsViewController
        let presenter = BlacklistSettingsPresenter()
        let interactor = BlacklistSettingsInteractor()
        let router = BlacklistSettingsRouter()
        
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        interactor.output = presenter
        return presenter
    }
}
