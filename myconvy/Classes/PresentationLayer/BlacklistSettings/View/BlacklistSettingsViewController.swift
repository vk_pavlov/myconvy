//
//  BlacklistViewController.swift
//  myconvy
//
//  Created by Евгений Каштанов on 04/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class BlacklistSettingsViewController: UIViewController {
    
    var output: BlacklistSettingsViewOutput!
    var chosenCellIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    
    
}

extension BlacklistSettingsViewController: BlacklistSettingsViewInput {
    func setupInitialState() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension BlacklistSettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Заблокированные пользователи"
        case 1:
            cell.textLabel?.text = "От кого скрыты мои события"
        default:
            cell.textLabel?.text = "Чьи события я не хочу видеть"
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
