//
//  BlacklistSettingsPresenter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 04/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class BlacklistSettingsPresenter {
    weak var view: BlacklistSettingsViewInput!
    var interactor: BlacklistSettingsInteractorInput!
    var router: BlacklistSettingsRouterInput!
}

extension BlacklistSettingsPresenter: BlacklistSettingsViewOutput {
    func viewIsReady() {
        view.setupInitialState()
    }
    
    
}

extension BlacklistSettingsPresenter: BlacklistSettingsInteractorOutput {
    
    
    
}

extension BlacklistSettingsPresenter: BlacklistSettingsModuleInput {
    
    var viewController: UIViewController {
        return view.viewController
    }
    
    func present(from: UIViewController) {
        view.present(fromViewController: from)
    }
}
