//
//  ListUsersRouter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/02/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

class ListUsersRouter {
    
}

extension ListUsersRouter: ListUsersRouterInput {
    func presentUserProfile(from: UIViewController, userId: Int) {
        if CommonData.selfUser().id == userId {
            ProfileModule.create().present(from:from)
        } else {
            UserProfileModule.create().present(from: from, userId: userId)
        }
    }
    
}
