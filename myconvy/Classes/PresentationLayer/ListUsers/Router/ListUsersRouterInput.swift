//
//  SubscribersRouterInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 29/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol ListUsersRouterInput {
    func presentUserProfile(from: UIViewController, userId: Int)
}
