//
//  SubscribersViewInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 29/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import RealmSwift

protocol ListUsersViewInput: class, Presentable {
    func setViewModels(_ userCellViewModels: [UserCellViewModel])
    func setupInitialState()
    func setStubView()
}
