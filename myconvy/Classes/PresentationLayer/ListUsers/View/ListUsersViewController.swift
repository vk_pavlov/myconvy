//
//  SubscribersViewController.swift
//  myconvy
//
//  Created by Евгений Каштанов on 29/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class ListUsersViewController: UIViewController {
    
    var output: ListUsersViewOutput!
    private var userCellViewModels = [UserCellViewModel]()
    private let spinner = Spinner(style: .gray)
    private var commonStub: CommonStubView!
    
    @IBOutlet weak var listUsersTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.setStateSpinner(enable: true, view: self.view)
        output.viewIsReady()
    }
    
    private func configureStub() {
        if !userCellViewModels.isEmpty {
            commonStub.removeFromSuperview()
            return
        }
        
        let prevNavController = navigationController!.previousViewController()
        if let _ = prevNavController as? ProfileViewController {
            if output.title == "Подписчики" {
                commonStub = StubsFactory.makeStub(.noSubscribers)
            } else {
                commonStub = StubsFactory.makeStub(.noSubscriptions)
            }
        }
        view.insertSubview(commonStub, at: 1)
        commonStub.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                          leading: view.leadingAnchor,
                          bottom: view.bottomAnchor,
                          trailing: view.trailingAnchor)
    }
    
}

extension ListUsersViewController: ListUsersViewInput {
    func setStubView() {
        spinner.setStateSpinner(enable: false, view: self.view)
        configureStub()
    }
    
    func setViewModels(_ userCellViewModels: [UserCellViewModel]) {
        spinner.setStateSpinner(enable: false, view: self.view)
        self.userCellViewModels = userCellViewModels
        listUsersTableView.reloadData()
    }
    
    func setupInitialState() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        listUsersTableView.register(UserCell.nib, forCellReuseIdentifier: UserCell.identifier)
    }
}

extension ListUsersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userCellViewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UserCell.identifier) as! UserCell
        cell.set(viewModel: userCellViewModels[indexPath.row])
        cell.isHiddenButtons = true
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userCellViewModel = userCellViewModels[indexPath.row]
        output.didSelectedUserPressed(userCellViewModel.id)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
