//
//  SubscribersPresenter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 29/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import RealmSwift


class ListUsersPresenter {
    weak var view: ListUsersViewInput!
    var interactor: ListUsersInteractorInput!
    var router: ListUsersRouterInput!
    private var usersIds: [Int]!
    private var titleNavBar: String!
    
    private func cellViewModel(from: UserTwo) -> UserCellViewModel {
        return UserCellViewModel(id: from.id,
                                 imageUrl: from.avatar,
                                 online: from.online,
                                 name: from.name,
                                 agePlusCity: from.cityPlusAge)
    }
}

extension ListUsersPresenter: ListUsersViewOutput {
    var title: String {
        return titleNavBar
    }
    
    func didSelectedUserPressed(_ userId: Int) {
        router.presentUserProfile(from: view.viewController, userId: userId)
    }
    
    func viewIsReady() {
        view.setupInitialState()
        guard !usersIds.isEmpty else {
            view.setStubView()
            return
        }
        interactor.getUsers(usersIds: usersIds)
    }
}

extension ListUsersPresenter: ListUsersInteractorOutput {
    func didReciveUsers(users: [UserTwo]) {
        let userCellViewModels = users.map { (user) in
            cellViewModel(from: user)
        }
        view.setViewModels(userCellViewModels)
    }
}

extension ListUsersPresenter: ListUsersModuleInput {
    
    var viewController: UIViewController {
        return view.viewController
    }
    
    func present(from: UIViewController, users: [Int], navItemTitle: String) {
        view.viewController.navigationItem.title = navItemTitle
        titleNavBar = navItemTitle
        self.usersIds = users
        view.present(fromViewController: from)
    }
}
