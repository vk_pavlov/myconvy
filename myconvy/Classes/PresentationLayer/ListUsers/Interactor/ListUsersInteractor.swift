//
//  SubscribersInteractor.swift
//  myconvy
//
//  Created by Евгений Каштанов on 29/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

class ListUsersInteractor {
    weak var output: ListUsersInteractorOutput!
    var userDataFetcher: UserDataFetcher!
}

extension ListUsersInteractor: ListUsersInteractorInput {
    func getUsers(usersIds: [Int]) {
        userDataFetcher.getUsersById(usersIds: usersIds) { (usersResponse, errorResponse) in
            if let usersResponse = usersResponse {
                self.output.didReciveUsers(users: usersResponse.usersInfo)
            }
        }
    }
    
    
}
