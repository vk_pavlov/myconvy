//
//  SubscribersInteractorOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 29/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol ListUsersInteractorOutput: class {
    func didReciveUsers(users: [UserTwo])
}
