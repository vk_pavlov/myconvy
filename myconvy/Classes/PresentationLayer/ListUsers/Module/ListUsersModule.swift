//
//  SubscribersModule.swift
//  myconvy
//
//  Created by Евгений Каштанов on 29/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class ListUsersModule {
    class func create() -> ListUsersModuleInput {
        let storyboard = UIStoryboard(name: "ListUsers", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ListUsersViewController") as! ListUsersViewController
        let presenter = ListUsersPresenter()
        let interactor = ListUsersInteractor()
        let router = ListUsersRouter()
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        interactor.output = presenter
        interactor.userDataFetcher = NetworkUserDataFetcher()
        return presenter
    }
}
