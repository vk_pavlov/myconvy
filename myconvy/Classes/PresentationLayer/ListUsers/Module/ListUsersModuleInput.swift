//
//  SubscribersModuleInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 29/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import RealmSwift

protocol ListUsersModuleInput {
    var viewController: UIViewController { get }
    func present(from: UIViewController, users: [Int], navItemTitle: String)
}
