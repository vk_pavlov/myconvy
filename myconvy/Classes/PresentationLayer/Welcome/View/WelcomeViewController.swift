//
//  WelcomeWelcomeViewController.swift
//  myconvy
//
//  Created by jumper17 on 03/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var legalInfoTextView: UITextView!
    
    private let textForlegalInfo = "Регистрируясь и используя сервис, вы соглашаетесь с Правилами пользования и Правилами использования данных."
    var output: WelcomeViewOutput!
    private var items = [AuthorizationModel]()

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var guestButon: UIButton!
    
    private enum Metrics {
        static let minCellsSpacing = 20
        static let widthCollectioinItem = 70
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        collectionView.register(AuthorizationMethodCell.self, forCellWithReuseIdentifier: AuthorizationMethodCell.reuseId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        legalInfoTextView.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(
            withDuration: 0.9,
            delay: 0.4,
            options: [],
            animations: {
                self.legalInfoTextView.alpha = 0.5
            },
            completion: nil
        )
        
    }

}

extension WelcomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AuthorizationMethodCell.reuseId, for: indexPath) as! AuthorizationMethodCell
        cell.set(model: items[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        items[indexPath.row].selection?()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        let totalCellWidth = Metrics.widthCollectioinItem * items.count
        let totalSpacingWidth = Metrics.minCellsSpacing * (items.count - 1)

        let leftInset = (view.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset

        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
    
}

extension WelcomeViewController: WelcomeViewInput {
    
    func set(model: [AuthorizationModel]) {
        self.items = model
        collectionView.reloadData()
    }
    
    func setupInitialState() {
        guestButon.isHidden = true
        legalInfoTextView.backgroundColor = .white
        legalInfoTextView.textColor = UIColor.black.withAlphaComponent(0.5)
        
        let linkedText = NSMutableAttributedString(string: textForlegalInfo)
        
        let hyperlinkedOne = linkedText.setAsLink(textToFind: "Правилами пользования", linkURL: Links.termsOfUse)
        let hyperlinkedTwo = linkedText.setAsLink(textToFind: "Правилами использования данных", linkURL: Links.privacyPolicy)
        
        if hyperlinkedOne && hyperlinkedTwo {
            legalInfoTextView.attributedText = linkedText
            legalInfoTextView.textAlignment = .center
            self.legalInfoTextView.isUserInteractionEnabled = true
            self.legalInfoTextView.isEditable = false
        }
        
//        // Set how links should appear: blue and underlined
        self.legalInfoTextView.linkTextAttributes = [
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        
    }
    
    
}

extension WelcomeViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL, options: [:])
        return false
    }
}
