//
//  WelcomeWelcomeViewInput.swift
//  myconvy
//
//  Created by jumper17 on 03/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

protocol WelcomeViewInput: class, Presentable {
    func setupInitialState()
    func set(model: [AuthorizationModel])
}
