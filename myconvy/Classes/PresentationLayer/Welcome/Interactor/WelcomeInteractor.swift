//
//  WelcomeWelcomeInteractor.swift
//  myconvy
//
//  Created by jumper17 on 03/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit
import CryptoSwift

class WelcomeInteractor {

    weak var output: WelcomeInteractorOutput!
    var networkService: NetworkServiceProtocol!
    
    
}

extension WelcomeInteractor: WelcomeInteractorInput {
    func getMyconvyToken(params: [String: String]?) {
        guard let params = params else { return }
        let token = params["token"]!
        let vkId = params["vkId"]!
        let reghash = params["reghash"]!
        let pushToken = UserDefaults.standard.string(forKey: "push_token") ?? ""

        networkService.getMyconvyToken(token: token, vkId: vkId, reghash: reghash, pushToken: pushToken, result: { (result) in
            switch result {
            case .success:
                self.output.didReciveDataMyconvy()
            case .failure(let error):
                print(error)
            }
        })
    }

}
