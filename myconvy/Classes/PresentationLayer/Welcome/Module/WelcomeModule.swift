//
//  WelcomeWelcomeConfigurator.swift
//  myconvy
//
//  Created by jumper17 on 03/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

class WelcomeModule {
    class func create() -> WelcomeModuleInput {
        let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
        let presenter = WelcomePresenter()
        let interactor = WelcomeInteractor()
        let router = WelcomeRouter()
        let networkService = NetworkService()
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        
        interactor.output = presenter
        interactor.networkService = networkService
        return presenter
    }
}
