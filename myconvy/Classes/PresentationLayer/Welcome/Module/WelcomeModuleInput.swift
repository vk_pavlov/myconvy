//
//  WelcomeWelcomeModuleInput.swift
//  myconvy
//
//  Created by jumper17 on 03/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

protocol WelcomeModuleInput: class {
    var viewController: UIViewController { get }
    func present(from viewController: UIViewController)
    func present()
}
