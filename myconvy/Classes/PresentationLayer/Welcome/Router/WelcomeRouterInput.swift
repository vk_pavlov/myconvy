//
//  WelcomeWelcomeRouterInput.swift
//  myconvy
//
//  Created by jumper17 on 03/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

protocol WelcomeRouterInput {
    func presentTabBar(from: UIViewController)
}
