//
//  WelcomeWelcomePresenter.swift
//  myconvy
//
//  Created by jumper17 on 03/12/2018.
//  Copyright © 2018 myconvy. All rights reserved.
//

import UIKit

class WelcomePresenter {
    
    enum Authorization {
        case vk
        case fb
        case google
    }

    weak var view: WelcomeViewInput!
    var interactor: WelcomeInteractorInput!
    var router: WelcomeRouterInput!
    private var authorizationManager: AuthorizationManager!
    private var authorization: Authorization?
    
    private func generateAuthorizationCells() {
        
        
        let vk = AuthorizeMethodViewModel(image: UIImage(named: "vkLogo")!) {
            self.authorizationManager.authService = VKAuthorization()
            self.authorization = .vk
        }
        
        let google = AuthorizeMethodViewModel(image: UIImage(named: "gLogo")!) {
            self.authorizationManager.authService = GoogleAuthorization()
            self.authorization = .vk
        }
        
        let facebook = AuthorizeMethodViewModel(image: UIImage(named: "fbLogo")!) {
            self.authorizationManager.authService = FBAuthorization()
            self.authorization = .vk
        }
        
        view.set(model: [vk])
    }
}


extension WelcomePresenter: WelcomeModuleInput {
    func present() {
        view.present()
    }
    
    var viewController: UIViewController {
        return view.viewController
    }
    
    func present(from viewController: UIViewController) {
        view.presentModal(fromViewController: viewController)
    }

}

extension WelcomePresenter: WelcomeViewOutput {
    
    func viewIsReady() {
        view.setupInitialState()
        generateAuthorizationCells()
        authorizationManager = AppDelegate.currentDelegate.authManager
        authorizationManager.forceLogout()
        authorizationManager.delegate = self
    }
}

extension WelcomePresenter: WelcomeInteractorOutput {
    func didReciveDataMyconvy() {
        router.presentTabBar(from: view.viewController)
    }
    
}

extension WelcomePresenter: AuthorizationManagerDelegate {
    func shouldPresentAuthorizationController(_ controller: UIViewController) {
        view.presentViewController(viewController: controller)
    }
    
    func accessAuthorizationFinished() {
        if authorization == .vk {
            interactor.getMyconvyToken(params: authorizationManager.paramsForAuthFromMV)
        }
    }

}
 
