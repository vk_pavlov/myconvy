//
//  PhotoBrowserModule.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/07/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class PhotoBrowserModule {
    class func create() -> PhotoBrowserModuleInput {
        let storyboard = UIStoryboard(name: "PhotoBrowser", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PhotoBrowserViewController") as! PhotoBrowserViewController
        let presenter = PhotoBrowserPresenter()
        let interactor = PhotoBrowserInteractor()

        viewController.output = presenter

        presenter.interactor = interactor
        presenter.view = viewController

        interactor.output = presenter
        interactor.networkService = NetworkService()

        return presenter
    }
}
