//
//  PhotoBrowserModuleInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/07/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol PhotoBrowserModuleInput {
    var viewController: UIViewController { get }
    func present(from: UIViewController, image: UIImage, initialPosition: CGPoint)
}
