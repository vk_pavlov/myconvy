//
//  PhotoBrowserViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/07/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class PhotoBrowserViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!

    var output: PhotoBrowserViewOutput!
    
    private var startFrame: CGRect!

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    @IBAction func cancelPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
    }

}

extension PhotoBrowserViewController: PhotoBrowserViewInput {
    func setupInitialState() {
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
       // navigationController?.navigationBar.barTintColor = .black
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(textAttributes, for: .normal)
        UIApplication.shared.statusBarStyle = .lightContent
    }

    func setImage(_ image: UIImage) {
        avatarImageView.image = image
    }

    func setInitialPosition(_ position: CGPoint) {
        startFrame = CGRect(x: position.x, y: position.y, width: 0, height: 0)
    }
}

extension PhotoBrowserViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimatorPresent(startFrame: self.startFrame)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimatorDismiss(endFrame: self.startFrame)
    }
}
