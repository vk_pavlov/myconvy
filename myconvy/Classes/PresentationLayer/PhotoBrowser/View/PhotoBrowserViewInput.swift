//
//  PhotoBrowserViewInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/07/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol PhotoBrowserViewInput: class, Presentable {
    func setImage(_ image: UIImage)
    func setInitialPosition(_ position: CGPoint)
    func setupInitialState()
}
