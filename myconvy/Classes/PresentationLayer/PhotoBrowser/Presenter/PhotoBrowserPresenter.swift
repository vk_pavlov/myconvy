//
//  PhotoBrowserPresenter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/07/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class PhotoBrowserPresenter {
    weak var view: PhotoBrowserViewInput!
    var interactor: PhotoBrowserInteractorInput!
    var image: UIImage!
    var initialPosition: CGPoint!
}

extension PhotoBrowserPresenter: PhotoBrowserViewOutput {

    func viewIsReady() {
        view.setupInitialState()
        view.setInitialPosition(initialPosition)
        view.setImage(image)
    }
}

extension PhotoBrowserPresenter: PhotoBrowserInteractorOutput {

}

extension PhotoBrowserPresenter: PhotoBrowserModuleInput {
    var viewController: UIViewController {
        return view.viewController
    }

    func present(from: UIViewController, image: UIImage, initialPosition: CGPoint) {
        self.image = image
        self.initialPosition = initialPosition
        view.presentNavigationController(fromViewController: from)
    }
}
