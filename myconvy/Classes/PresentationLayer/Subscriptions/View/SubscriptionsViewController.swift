//
//  SubscriptionsViewController.swift
//  myconvy
//
//  Created by Евгений Каштанов on 29/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import RealmSwift


class SubscriptionsViewController: UIViewController {
    
    var output: SubscriptionsViewOutput!
    var listUsers: List<User>!
    var numberOfUsers: Int!
    var isMe: Bool!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    
    
}

extension SubscriptionsViewController: SubscriptionsViewInput {
    func setUserData(_ userData: User) {
        let realm = try! Realm()
        //self.listUsers = userData.usersSubs
        self.numberOfUsers = listUsers.count
        let userMySelf = realm.objects(User.self).first
        if userData.fullName == userMySelf!.fullName {
            isMe = true
        } else {
            isMe = false
        }
    }
    
    func setupInitialState() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension SubscriptionsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if numberOfUsers != 0 {
//            return numberOfUsers
//        } else {
//            return 1
//        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if numberOfUsers != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
            cell.configure(user: listUsers[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoSubscriptionsCell", for: indexPath)
            if isMe {
                cell.textLabel?.text = "Вы пока ни на кого не подписаны! Находите интересных людей и события в разделе \"Поиск\"."
            } else {
                cell.textLabel?.text = "Пользователь пока ни на кого не подписан."
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if numberOfUsers != 0 {
            return 84
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}
