//
//  NewEventPresenter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import GoogleMaps

class NewEventPresenter {
    weak var view: NewEventViewInput!
    var interactor: NewEventInteractorInput!
    var router: NewEventRouterInput!
    private var eventId: Int!
    private var editEventViewModel: NewEventViewModel!
    private var selectedCoordinate: Place!
    private let geocoder = GMSGeocoder()
    private var defaultNewEvent: NewEventViewModel!
    
    private func configureAddressFromCoordinate() {
        let coordinate = CLLocationCoordinate2D(latitude: selectedCoordinate.lat, longitude: selectedCoordinate.lng)
        geocoder.reverseGeocodeCoordinate(coordinate) { [weak self] (response, error) in
            if let thoroughfare = response?.firstResult()?.thoroughfare {
                self?.view.setAddress(thoroughfare)
            }
        }
    }
}

extension NewEventPresenter: NewEventViewOutput {
    
    var isEditMode: Bool {
        return editEventViewModel != nil
    }
    
    func didSelectAddressButtonPressed() {
        router.presentMap(from: view.viewController, mapPresentationMode: .selectionAddress, initialCoordinate: selectedCoordinate, output: self)
    }
    
    func didEditEventPressed(eventViewModel: NewEventViewModel) {
        var viewModel = eventViewModel
        viewModel.place = selectedCoordinate
        viewModel.descriptionEvent = eventViewModel.descriptionEvent ?? ""
        viewModel.descriptionPlace = eventViewModel.descriptionPlace ?? ""
        interactor.editEvent(viewModel, eventId: eventId)
    }
    
    func didCreateEventPressed(eventViewModel: NewEventViewModel) {
        var viewModel = eventViewModel
        viewModel.place = selectedCoordinate
        interactor.createEvent(viewModel)
    }

    func viewIsReady() {
        view.setupInitialState()
        if defaultNewEvent != nil {
            view.setDefaultDataEvent(eventViewModel: defaultNewEvent)
            return
        }
        guard let editEventViewModel = editEventViewModel else { return }
        view.set(eventViewModel: editEventViewModel)
    }
    
}

extension NewEventPresenter: NewEventInteractorOutput {
    func didReciveEditMeet() {
        view.dissmissModal()
    }

    func didReciveCreateMeet(id: Int) {
        view.newEventDidCreated(id: id)
    }

    func didReciveError() {
        view.didReciveError()
    }
}

extension NewEventPresenter: NewEventModuleInput {
    func present(from viewController: UIViewController, defaultCategoryIndex: Int) {
        let randomIndex = Int.random(in: 0 ..< defaultCategories[defaultCategoryIndex].count)
        defaultNewEvent = defaultCategories[defaultCategoryIndex][randomIndex]
        selectedCoordinate = defaultNewEvent.place
        view.presentNavigationController(fromViewController: viewController)
        configureAddressFromCoordinate()
    }
    
    func present(from viewController: UIViewController) {
        view.presentNavigationController(fromViewController: viewController)
    }
    
    func present(from viewController: UIViewController, viewModel: NewEventViewModel, eventId: Int) {
        self.eventId = eventId
        editEventViewModel = viewModel
        selectedCoordinate = viewModel.place
        view.presentNavigationController(fromViewController: viewController)
        configureAddressFromCoordinate()
    }
    
    var viewController: UIViewController {
        return view.viewController
    }
    
}

extension NewEventPresenter: MapModuleOutput {
    func setSelectedAddress(streetName: String, coordinate: Place) {
        selectedCoordinate = coordinate
        view.setAddress(streetName)
    }
}
