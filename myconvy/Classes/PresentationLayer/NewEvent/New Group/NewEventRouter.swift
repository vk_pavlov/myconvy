//
//  NewEventRouter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 12/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class NewEventRouter {
    
}

extension NewEventRouter: NewEventRouterInput {
    func presentMap(from: UIViewController, mapPresentationMode: MapPresentationMode, initialCoordinate: Place?, output: MapModuleOutput) {
        MapModule.create().present(from: from, mapPresentationMode: mapPresentationMode, initialCoordinate: initialCoordinate, output: output, isParticipant: true)
    }
}
