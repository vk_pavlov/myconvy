//
//  NewEventInteractorOutput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol NewEventInteractorOutput: class {
    func didReciveCreateMeet(id: Int)
    func didReciveError()
    func didReciveEditMeet()
}
