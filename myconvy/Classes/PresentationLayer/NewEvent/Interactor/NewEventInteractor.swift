//
//  NewEventInteractor.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

class NewEventInteractor {
    weak var output: NewEventInteractorOutput!
    var eventDataFetcher: EventDataFetcher!
}

extension NewEventInteractor: NewEventInteractorInput {
    func editEvent(_ event: NewEventViewModel, eventId: Int) {
        eventDataFetcher.editMeetByID(eventId: eventId, eventViewModel: event) { (response, error) in
            if response != nil {
                self.output.didReciveEditMeet()
            }
        }
    }

    func createEvent(_ event: NewEventViewModel) {
        eventDataFetcher.createMeet(event: event) { [weak self] (eventResponse, error) in
            guard let self = self else { return }
            guard error == nil else {
                self.output.didReciveError()
                return
            }
            guard let eventResponse = eventResponse else { return }
            self.output.didReciveCreateMeet(id: eventResponse.meetInfo.id)
        }
    }
}
