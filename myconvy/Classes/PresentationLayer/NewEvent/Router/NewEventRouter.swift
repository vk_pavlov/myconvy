//
//  NewEventRouter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 12/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class NewEventRouter {
    
}

extension NewEventRouter: NewEventRouterInput {
    func presentMetroStations(from: UIViewController, output: MetroStationsModuleOutput) {
        MetroStationsModule.create().present(from: from, output: output)
    }
    
    
}
