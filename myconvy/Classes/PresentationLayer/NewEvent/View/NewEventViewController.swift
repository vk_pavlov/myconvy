//
//  NewEventViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

enum AlertForCreateEvent {
    static let title = "Вы создали новое событие!"
    static let message = "Следите за откликами на него в разделе \"Уведомления\""
    enum AlertActionTitle {
        static let shared = "Поделиться"
        static let createEvent = "Готово"
    }
}

class NewEventViewController: UIViewController {

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var createEventButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    
    private let titleInputView = TextInputView(title: "Название")
    private let mapsPositionView = MapsPositionView(buttonTitle: "Выбрать на карте")
    private lazy var unfieldItems: [UnfieldItems] = [titleInputView, mapsPositionView, dateInputView, categoriesCollectionView]
    
    private let addressLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.contentMode = .left
        label.numberOfLines = 0
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        return label
    }()
    
    private let addressLabelView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return view
    }()
    
    private let dateInputView = DatePickerInputView(title: "Дата и время")
    private let moreInfoView = MoreInfoView()
    
    private let moreInfoButtonView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return view
    }()
    
    private let moreInfoButton: ButtonWithImage = {
        let button = ButtonWithImage(iconPosition: .right)
        button.setImage(UIImage(named: "arrow_down"), for: .normal)
        button.heightAnchor.constraint(equalToConstant: 25).isActive = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.black.withAlphaComponent(0.25), for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        button.setTitle("Подробнее", for: .normal)
        button.addTarget(self, action: #selector(moreInfoPressed), for: .touchUpInside)
        return button
    }()
    
    private let categoriesTitleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return view
    }()
    
    private let categoriesTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        label.heightAnchor.constraint(equalToConstant: 22).isActive = true
        label.text = "Категория"
        return label
    }()
    
    private let categoriesCollectionView = CategoriesCollectionView()
    
    private var moreInfoOpened = false {
        didSet {
            moreInfoButton.setImage(moreInfoOpened ? UIImage(named: "arrow_up") : UIImage(named: "arrow_down"), for: .normal)
        }
    }
    
    private var selectedCategory: IndexPath!
    
    var output: NewEventViewOutput!
    let spinner = Spinner(style: .gray)
    
    private var isUnfieldItems: Bool {
        var isUnfield = false
        for item in unfieldItems {
            if item.isEmpty {
                item.highlight()
                isUnfield = true
            }
        }
        return isUnfield
    }
    
    private var editedEventViewModel: NewEventViewModel {
        return NewEventViewModel(title: titleInputView.text!,
            place: Place(lat: 12, lng: 12),
            date: dateInputView.date,
            category: selectedCategory.row,
            descriptionEvent: moreInfoView.descriptionEvent,
            descriptionPlace: moreInfoView.descriptionPlace,
            sex: moreInfoView.selectionGender,
            partLimit: moreInfoView.partLimit,
            privacy: moreInfoView.privacy)
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForKeyboardNotifications()
        scrollView.alwaysBounceVertical = true
        scrollView.contentInset.bottom = 30
        
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.addTarget(self, action: #selector(tapScreenGestureRecognizer))
        view.addGestureRecognizer(tapGestureRecognizer)

        stackView.addArrangedSubview(titleInputView)
        stackView.addArrangedSubview(mapsPositionView)
        mapsPositionView.isHiddenDistanceView = true
        
        addressLabelView.addSubview(addressLabel)
        addressLabel.leadingAnchor.constraint(equalTo: addressLabelView.leadingAnchor, constant: 16).isActive = true
        addressLabel.trailingAnchor.constraint(equalTo: addressLabelView.trailingAnchor, constant: -16).isActive = true
        addressLabel.topAnchor.constraint(equalTo: addressLabelView.topAnchor).isActive = true
        addressLabel.bottomAnchor.constraint(equalTo: addressLabelView.bottomAnchor).isActive = true
        stackView.addArrangedSubview(addressLabelView)
        
        stackView.addArrangedSubview(dateInputView)
        
        categoriesTitleView.addSubview(categoriesTitle)
        categoriesTitle.leadingAnchor.constraint(equalTo: categoriesTitleView.leadingAnchor, constant: 16).isActive = true
        categoriesTitle.topAnchor.constraint(equalTo: categoriesTitleView.topAnchor).isActive = true
        
        stackView.addArrangedSubview(categoriesTitleView)
        stackView.addArrangedSubview(categoriesCollectionView)
        
        
        moreInfoButtonView.addSubview(moreInfoButton)
        moreInfoButton.topAnchor.constraint(equalTo: moreInfoButtonView.topAnchor).isActive = true
        moreInfoButton.leadingAnchor.constraint(equalTo: moreInfoButtonView.leadingAnchor, constant: 16).isActive = true
        
        stackView.addArrangedSubview(moreInfoButtonView)
        stackView.addArrangedSubview(moreInfoView)
        
        moreInfoView.alpha = 0
        moreInfoView.isHidden = true
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
        categoriesCollectionView.someDelegate = self
        mapsPositionView.delegate = self
        
        dateInputView.datePickerMode = .dateAndTime
        
        addressLabelView.isHidden = true
        output.viewIsReady()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
    }
    
    // MARK: Private
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    // MARK: Actions
    
    @objc private func moreInfoPressed() {
        UIView.animate(withDuration: 0.3, animations: {
            if self.moreInfoOpened {
                self.moreInfoView.isHidden = true
                self.moreInfoView.alpha = 0
            } else {
                self.moreInfoView.isHidden = false
                self.moreInfoView.alpha = 1
                self.scrollView.contentOffset = self.moreInfoButtonView.frame.origin
            }
            self.view.layoutIfNeeded()
        }) { _ in
            self.view.endEditing(true)
            self.moreInfoOpened = !self.moreInfoOpened
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset.bottom = 30
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo!
        let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        scrollView.contentInset.bottom = keyboardFrame.height
    }
    
    @objc private func tapScreenGestureRecognizer(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBAction func createEventPressed(_ sender: UIButton) {
        if isUnfieldItems {
            return
        }
        createEventButton.isEnabled = false
        output.didCreateEventPressed(eventViewModel: editedEventViewModel)
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIBarButtonItem) {
        dissmissModal()
    }
    
    @objc private func updateInfoAboutEvent() {
        if isUnfieldItems {
            return
        }
        output.didEditEventPressed(eventViewModel: editedEventViewModel)
    }
    
    private func setDefaultData(eventViewModel: NewEventViewModel) {
        titleInputView.text = eventViewModel.title
        //mapsPositionView =
        dateInputView.date = eventViewModel.date
        selectedCategory = IndexPath(row: eventViewModel.category, section: 0)
        moreInfoView.descriptionEvent = eventViewModel.descriptionEvent
        
        moreInfoView.descriptionPlace = eventViewModel.descriptionPlace
        moreInfoView.selectionGender = eventViewModel.sex
        moreInfoView.partLimit = eventViewModel.partLimit
        moreInfoView.privacy = eventViewModel.privacy
        if !output.isEditMode {
            moreInfoView.hiddenDescriptionEvent()
            moreInfoView.hiddenDescriptionPlace()
        }
    }
    
}

extension NewEventViewController: NewEventViewInput {
    func setDefaultDataEvent(eventViewModel: NewEventViewModel) {
        setDefaultData(eventViewModel: eventViewModel)
        categoriesCollectionView.reloadData()
    }
    
    func setAddress(_ address: String) {
        addressLabelView.isHidden = false
        addressLabel.text = address
    }
    
    func set(eventViewModel: NewEventViewModel) {
        setDefaultData(eventViewModel: eventViewModel)
        createEventButton.setTitle("Сохранить", for: .normal)
        createEventButton.removeTarget(self, action: nil, for: .allEvents)
        createEventButton.addTarget(self, action: #selector(updateInfoAboutEvent), for: .touchUpInside)
        navigationItem.title = "Редактирование"
        categoriesCollectionView.reloadData()
    }
    
    func newEventDidCreated(id: Int) {
        createEventButton.isEnabled = true
        let alert = UIAlertController(title: AlertForCreateEvent.title, message: "", preferredStyle: .alert)
        let sharedAction = UIAlertAction(title: AlertForCreateEvent.AlertActionTitle.shared, style: .default) { (UIAlertAction) in
            let link = DefaultLinksShared.event + String(id)
            let vc = UIActivityViewController(activityItems: [link], applicationActivities: [])
            self.present(vc, animated: true)
            vc.completionHandler = { activity, success in
                self.dissmissModal()
            }
        }

        let finishAction = UIAlertAction(title: AlertForCreateEvent.AlertActionTitle.createEvent, style: .default) { (UIAlertAction) in
            self.dissmissModal()
        }

        alert.addAction(finishAction)
        alert.addAction(sharedAction)
        present(alert, animated: true, completion: nil)
    }

    func didReciveError() {
        //spinner.setStateSpinner(enable: false, view: self.view)
        let alert = AlertsFactory.makeAlertError(error: "Что-то не так с интернет соединением")
        present(alert, animated: true, completion: nil)
    }

    func setupInitialState() {
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .lightContent
        }
        let textAttributes = [NSAttributedString.Key.foregroundColor: OrangeTheme.myconvyOrange,
                              NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 18)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        cancelButton.tintColor = OrangeTheme.myconvyOrange
        cancelButton.setTitleTextAttributes([NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Medium", size: 14)], for: .normal)
        cancelButton.setTitleTextAttributes([NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Medium", size: 14)], for: .highlighted)
        createEventButton.backgroundColor = OrangeTheme.myconvyOrange
    }
    
}

extension NewEventViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.reuseId, for: indexPath) as! CategoryCollectionViewCell
        if selectedCategory == indexPath {
            cell.isSelectedCategory = true
        }
        cell.title = categories[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CategoryCollectionViewCell
        if selectedCategory != nil {
            if collectionView.indexPathsForVisibleItems.contains(selectedCategory) {
                let previousSelectedCell = collectionView.cellForItem(at: selectedCategory) as! CategoryCollectionViewCell
                previousSelectedCell.isSelectedCategory = false
            }
        }
        cell.isSelectedCategory = true
        selectedCategory = indexPath
    }
}

extension NewEventViewController: MapsPositionViewDelegate {
    var isNoSelectedAddress: Bool {
        guard let text = addressLabel.text else { return true }
        return text.isEmpty
    }
    
    func mapsPositionViewSelectedAddressPressed(_ view: MapsPositionView) {
        stackView.endEditing(true)
        output.didSelectAddressButtonPressed()
    }
}

extension NewEventViewController: CategoriesCollectionViewDelegate {
    var isNotSelectedCategory: Bool {
        return selectedCategory == nil
    }
}
