//
//  CategoriesSelectionViewCell.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol CategoriesSelectionViewCellDelegate: class {
    func didSelectedCatefory()
}

class CategoryCollectionViewCell: UICollectionViewCell {
    
    static let reuseId = "CategoryCollectionViewCell"
    
    private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        return label
    }()
    
    var title: String? {
        didSet {
            label.text = title
        }
    }
    
    var isSelectedCategory: Bool = false {
        didSet {
            backgroundColor = isSelectedCategory ? OrangeTheme.myconvyOrange.withAlphaComponent(0.2) : UIColor.black.withAlphaComponent(0.1)
            label.textColor = isSelectedCategory ? UIColor.black.withAlphaComponent(0.5) : UIColor.black.withAlphaComponent(0.25)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(label)
        backgroundColor = UIColor.black.withAlphaComponent(0.1)
        translatesAutoresizingMaskIntoConstraints = false
        
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.cancelsTouchesInView = false
        addGestureRecognizer(tapGestureRecognizer)

        label.anchor(top: topAnchor,
                     leading: leadingAnchor,
                     bottom: bottomAnchor,
                     trailing: trailingAnchor,
                     padding: UIEdgeInsets(top: 9, left: 24, bottom: 9, right: 24))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
        clipsToBounds = false
    }
    
    override func prepareForReuse() {
        isSelectedCategory = false
    }
    
}
