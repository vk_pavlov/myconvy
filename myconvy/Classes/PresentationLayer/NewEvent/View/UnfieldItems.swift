//
//  UnfieldItems.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 06/02/2020.
//  Copyright © 2020 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol UnfieldItems {
    func highlight()
    var isEmpty: Bool { get }
}
