//
//  NewEventModule.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class NewEventModule {
    class func create() -> NewEventModuleInput {
        let storyboard = UIStoryboard(name: "NewEvent", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "NewEventViewController") as! NewEventViewController
        let presenter = NewEventPresenter()
        let interactor = NewEventInteractor()
        let router = NewEventRouter()
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        interactor.output = presenter
        interactor.eventDataFetcher = NetworkEventDataFetcher(networkService: NetworkServiceTwo())
        return presenter
    }
}
