//
//  NewEventModuleInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol NewEventModuleInput {
    var viewController: UIViewController { get }
    func present(from viewController: UIViewController, viewModel: NewEventViewModel, eventId: Int)
    func present(from viewController: UIViewController)
    func present(from viewController: UIViewController, defaultCategoryIndex: Int)
}
