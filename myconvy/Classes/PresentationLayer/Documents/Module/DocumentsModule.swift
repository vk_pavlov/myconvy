//
//  DocumentsModule.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

class DocumentsModule {
    class func create() -> DocumentsModuleInput {
        let storyboard = UIStoryboard(name: "Documents", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DocumentsViewController") as! DocumentsViewController
        let presenter = DocumentsPresenter()
        let interactor = DocumentsInteractor()
        let router = DocumentsRouter()

        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router

        interactor.output = presenter
        return presenter
    }
}
