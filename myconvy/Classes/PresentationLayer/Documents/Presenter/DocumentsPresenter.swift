//
//  DocumentsPresenter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

class DocumentsPresenter {

    weak var view: DocumentsViewInput!
    var interactor: DocumentsInteractorInput!
    var router: DocumentsRouterInput!
}

extension DocumentsPresenter: DocumentsViewOutput {
    func didPrivacyPolicy() {
        router.presentPrivacyPolicy()
    }

    func didTermsOfUse() {
        router.presentTermsOfUse()
    }

    func viewIsReady() {
        view.setupInitialState()
    }


}

extension DocumentsPresenter: DocumentsModuleInput {
    var viewController: UIViewController {
        return view.viewController
    }

    func present(from viewController: UIViewController) {
        view.present(fromViewController: viewController)
    }


}

extension DocumentsPresenter: DocumentsInteractorOutput {

}
