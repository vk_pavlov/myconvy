//
//  DocumentsViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class DocumentsViewController: UIViewController {

    var output: DocumentsViewOutput!

    @IBOutlet weak var documentsTableView: UITableView!
    private let documents = ["Политика конфиденциальности", "Пользовательское соглашение"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DocumentsViewController: DocumentsViewInput {
    func setupInitialState() {
        navigationController?.navigationBar.tintColor = OrangeTheme.myconvyOrange
        navigationItem.title = "Документы"
    }


}

extension DocumentsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documents.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentCell", for: indexPath)
        cell.textLabel?.text = documents[indexPath.row]
        cell.textLabel?.textColor = .black
        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            output.didPrivacyPolicy()
        } else {
            output.didTermsOfUse()
        }
    }
}
