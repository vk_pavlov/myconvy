//
//  DocumentsRouter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

class DocumentsRouter {
    
}

extension DocumentsRouter: DocumentsRouterInput {
    func presentPrivacyPolicy() {
        guard let url = URL(string: Links.privacyPolicy) else { return }
        UIApplication.shared.open(url)
    }

    func presentTermsOfUse() {
        guard let url = URL(string: Links.termsOfUse) else { return }
        UIApplication.shared.open(url)
    }

    
}
