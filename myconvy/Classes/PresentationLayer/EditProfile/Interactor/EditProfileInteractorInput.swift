//
//  EditProfileInteractorInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 28/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol EditProfileInteractorInput {
    func editUser(viewModel: EditProfileViewModel)
    func editUserAvatar(image: UIImage)
}
