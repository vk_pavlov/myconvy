//
//  EditProfileInteractorOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 28/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


protocol EditProfileInteractorOutput: class {
    func didReciveEditUser()
    func didReciveError()
    func didReciveUpdateAvatar()
}
