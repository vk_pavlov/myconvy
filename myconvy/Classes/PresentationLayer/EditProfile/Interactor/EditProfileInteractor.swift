//
//  EditProfileInteractor.swift
//  myconvy
//
//  Created by Евгений Каштанов on 28/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class EditProfileInteractor {
    weak var output: EditProfileInteractorOutput!
    var networkService: NetworkServiceProtocol!
    var userDataFetcher: NetworkUserDataFetcher!
}

extension EditProfileInteractor: EditProfileInteractorInput {
    func editUserAvatar(image: UIImage) {
         networkService.uploadImage(img: image) { (result) in
            switch result {
            case .success(_ ):
                self.output.didReciveUpdateAvatar()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func editUser(viewModel: EditProfileViewModel) {
        userDataFetcher.editUser(profileViewModel: viewModel) { [weak self] (response, error) in
            guard let self = self else { return }
            guard error == nil else {
                self.output.didReciveError()
                return
            }
            guard response != nil else { return }
            self.output.didReciveEditUser()
        }
    }
}
