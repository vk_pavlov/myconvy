//
//  EditProfileViewController.swift
//  myconvy
//
//  Created by Евгений Каштанов on 28/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import CropViewController

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var safeChangesButton: UIButton!
    
    private let editProfileHeaderView = EditProfileHeaderView()
    private let nameView = TextInputView(title: "Имя")
    private let lastNameView = TextInputView(title: "Фамилия")
    private let genderView = SelectionGenderView()
    private let datePickerInputView = DatePickerInputView(title: "Дата рождения")
    private let categoriesCollectionView = CategoriesSelectionView(title: "Интересы")
    private let moreInfoAboutSelfView = MoreInfoAboutSelfView()
    
    var output: EditProfileViewOutput!
    private var editProfileViewModel: EditProfileViewModel!
    
    private let moreInfoButton: ButtonWithImage = {
        let button = ButtonWithImage(iconPosition: .right)
        button.setImage(UIImage(named: "arrow_down"), for: .normal)
        button.heightAnchor.constraint(equalToConstant: 25).isActive = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.black.withAlphaComponent(0.25), for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        button.setTitle("Дополнительная информация", for: .normal)
        button.addTarget(self, action: #selector(moreInfoPressed), for: .touchUpInside)
        button.contentHorizontalAlignment = .left
        return button
    }()
    
    private let moreInfoButtonView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return view
    }()
    
    private var moreInfoOpened = false {
        didSet {
            moreInfoButton.setImage(moreInfoOpened ? UIImage(named: "arrow_up") : UIImage(named: "arrow_down"), for: .normal)
        }
    }
    
    private var isSelectedNewImage = false
    private let spinner = Spinner(style: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentInset.bottom = 50
        registerForKeyboardNotifications()
        output.viewIsReady()
        scrollView.alwaysBounceVertical = true
        
        editProfileHeaderView.delegate = self
        stackView.addArrangedSubview(editProfileHeaderView)
        stackView.addArrangedSubview(nameView)
        stackView.addArrangedSubview(lastNameView)
        stackView.addArrangedSubview(genderView)
        stackView.addArrangedSubview(datePickerInputView)
        stackView.addArrangedSubview(categoriesCollectionView)
        moreInfoButtonView.addSubview(moreInfoButton)
        moreInfoButton.topAnchor.constraint(equalTo: moreInfoButtonView.topAnchor).isActive = true
        moreInfoButton.leadingAnchor.constraint(equalTo: moreInfoButtonView.leadingAnchor, constant: 16).isActive = true
        moreInfoButton.trailingAnchor.constraint(equalTo: moreInfoButtonView.trailingAnchor, constant: -23).isActive = true
        
        stackView.addArrangedSubview(moreInfoButtonView)
        stackView.addArrangedSubview(moreInfoAboutSelfView)
        
        moreInfoButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        
        datePickerInputView.datePickerMode = .date
        categoriesCollectionView.isCellWithShadow = true
        moreInfoAboutSelfView.alpha = 0
        moreInfoAboutSelfView.isHidden = true
        
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.addTarget(self, action: #selector(tapScreenGestureRecognizer))
        view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
    }
    
    // MARK: Private
    
    @objc private func moreInfoPressed() {
        UIView.animate(withDuration: 0.3, animations: {
            if self.moreInfoOpened {
                self.moreInfoAboutSelfView.isHidden = true
                self.moreInfoAboutSelfView.alpha = 0
            } else {
                self.moreInfoAboutSelfView.isHidden = false
                self.moreInfoAboutSelfView.alpha = 1
                self.scrollView.contentOffset = self.moreInfoButtonView.frame.origin
            }
            self.view.layoutIfNeeded()
        }) { _ in
            self.view.endEditing(true)
            self.moreInfoOpened = !self.moreInfoOpened
        }
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    // MARK: Actions
    
    @objc private func keyboardWillHide(_ notification: Notification) {
       scrollView.contentInset.bottom = 50
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo!
        let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let height = keyboardFrame.height
        scrollView.contentInset.bottom = height
        
    }
    
    @IBAction func safeChangeInfo(_ sender: UIButton) {
        if nameView.text!.isEmpty ||
            lastNameView.text!.isEmpty ||
            datePickerInputView.isEmptyDate ||
            categoriesCollectionView.selectedCategories.isEmpty {
            let alert = AlertsFactory.makeAlertError(error: "Обязательные поля не заполнены")
            present(alert, animated: true, completion: nil)
            return
        }
        
        let editedProfileViewModel = EditProfileViewModel(avatar: editProfileHeaderView.avatar,
                                                          name: nameView.text!,
                                                          lastName: lastNameView.text!,
                                                          gender: genderView.selectedGender!,
                                                          birthdate: datePickerInputView.date,
                                                          interest: categoriesCollectionView.selectedCategories,
                                                          city: moreInfoAboutSelfView.city,
                                                          university: moreInfoAboutSelfView.university,
                                                          relations: moreInfoAboutSelfView.relation,
                                                          contactInfo: moreInfoAboutSelfView.contactInformation,
                                                          aboutSelf: moreInfoAboutSelfView.aboutMe)
 
        if editedProfileViewModel == editProfileViewModel {
            if isSelectedNewImage {
                spinner.setStateSpinner(enable: true, view: view)
                output.didUpdatePhotoProfile(image: editProfileHeaderView.image!, isOnlyChangeAvatar: true)
            } else {
                dissmissModal()
                return
            }
        } else {
            if isSelectedNewImage {
                output.didUpdatePhotoProfile(image: editProfileHeaderView.image!, isOnlyChangeAvatar: false)
            }
            spinner.setStateSpinner(enable: true, view: view)
            output.didApplyEditProfilePressed(viewModel: editedProfileViewModel)
        }
    }
    
    
    @IBAction func cancelEditingPressed(_ sender: UIBarButtonItem) {
        dissmissModal()
    }
    
    @objc func tapScreenGestureRecognizer() {
        view.endEditing(true)
    }
    
    private func presentCameraPickerController() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self;
            imagePickerController.sourceType = .camera
            present(imagePickerController, animated: true, completion: nil)
        }
    }

    private func presentPhotoLibraryPickerController() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self;
            imagePickerController.sourceType = .photoLibrary
            present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    private func changeAvatarPressed() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Отмена", style: .cancel)
        let selectFromPhotoLibraryAction = UIAlertAction(title: "Выбрать из библиотеки", style: .default) { (UIAlertAction) in
            self.presentPhotoLibraryPickerController()
        }
        let openCameraAction = UIAlertAction(title: "Использовать камеру", style: .default) { (UIAlertAction) in
            self.presentCameraPickerController()
        }

        alertController.addAction(cancel)
        alertController.addAction(selectFromPhotoLibraryAction)
        alertController.addAction(openCameraAction)
        present(alertController, animated: true, completion: nil)
    }
    
}

extension EditProfileViewController: EditProfileViewInput {
    func successEditUser() {
        spinner.setStateSpinner(enable: false, view: view)
        dissmissModal()
    }
    
    
    func didReciveError() {
        let alert = AlertsFactory.makeAlertError(error: "Что-то не так с интернет соединением")
        present(alert, animated: true, completion: nil)
    }
    
    func set(viewModel: EditProfileViewModel) {
        editProfileViewModel = viewModel
        
        editProfileHeaderView.avatar = viewModel.avatar
        nameView.text = viewModel.name
        lastNameView.text = viewModel.lastName
        genderView.selectedGender = viewModel.gender
        datePickerInputView.date = viewModel.birthdate
        categoriesCollectionView.selectedCategories = viewModel.interest
        
        moreInfoAboutSelfView.city = viewModel.city
        moreInfoAboutSelfView.university = viewModel.university
        moreInfoAboutSelfView.relation = viewModel.relations ?? 0
        moreInfoAboutSelfView.contactInformation = viewModel.contactInfo
        moreInfoAboutSelfView.aboutMe = viewModel.aboutSelf
    }
    
    
    func setupInitialState() {
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .lightContent
        }
        safeChangesButton.backgroundColor = OrangeTheme.myconvyOrange
    }
}

extension EditProfileViewController: EditProfileHeaderViewDelegate {
    func editProfileHeaderViewBackButtonPressed(_ view: EditProfileHeaderView) {
        
    }
    
    func editProfileHeaderViewDidAvatarPressed(_ view: EditProfileHeaderView) {
        changeAvatarPressed()
    }
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
               dissmiss()
               return
           }
           let cropViewController = CropViewController(croppingStyle: .circular, image: image)
           cropViewController.delegate = self
           picker.present(cropViewController, animated: true, completion: nil)
       }

       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           self.dismiss(animated: true, completion: nil)
       }
}

extension EditProfileViewController: CropViewControllerDelegate {
    func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        isSelectedNewImage = true
        editProfileHeaderView.image = image
        dismiss(animated: true, completion: nil)
    }
}
