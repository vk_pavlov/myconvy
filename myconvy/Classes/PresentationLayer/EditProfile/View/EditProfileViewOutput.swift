//
//  EditProfileViewOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 28/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol EditProfileViewOutput {
    func viewIsReady()
    func didApplyEditProfilePressed(viewModel: EditProfileViewModel)
    func didUpdatePhotoProfile(image: UIImage, isOnlyChangeAvatar: Bool)
}
