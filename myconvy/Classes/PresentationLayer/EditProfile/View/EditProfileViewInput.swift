//
//  EditProfileViewInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 28/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol EditProfileViewInput: class, Presentable {
    func set(viewModel: EditProfileViewModel)
    func setupInitialState()
    func didReciveError()
    func successEditUser()
}
