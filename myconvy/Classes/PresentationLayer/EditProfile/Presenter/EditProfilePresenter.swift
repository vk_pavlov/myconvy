//
//  EditProfilePresenter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 28/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class EditProfilePresenter {
    weak var view: EditProfileViewInput!
    var interactor: EditProfileInteractorInput!
    var router: EditProfileRouterInput!
    private var editProfileViewModel: EditProfileViewModel!
    private var shouldUpdateAvatar = false
    private var newAvatar: UIImage!
}

extension EditProfilePresenter: EditProfileViewOutput {
    
    func didUpdatePhotoProfile(image: UIImage, isOnlyChangeAvatar: Bool) {
        shouldUpdateAvatar = true
        newAvatar = image
        if isOnlyChangeAvatar {
            interactor.editUserAvatar(image: image)
        }
    }
    
    func didApplyEditProfilePressed(viewModel: EditProfileViewModel) {
        interactor.editUser(viewModel: viewModel)
    }
    
    func viewIsReady() {
        view.set(viewModel: editProfileViewModel)
        view.setupInitialState()
    }
}

extension EditProfilePresenter: EditProfileInteractorOutput {
    func didReciveUpdateAvatar() {
        shouldUpdateAvatar = false
        view.successEditUser()
    }
    
    func didReciveEditUser() {
        shouldUpdateAvatar ? interactor.editUserAvatar(image: newAvatar) : view.successEditUser()
    }
    
    func didReciveError() {
        view.didReciveError()
    }
}

extension EditProfilePresenter: EditProfileModuleInput {
    var viewController: UIViewController {
        return view.viewController
    }
    
    func present(from viewController: UIViewController, viewModel: EditProfileViewModel) {
        self.editProfileViewModel = viewModel
        view.presentNavigationController(fromViewController: viewController)
    }
    
}
