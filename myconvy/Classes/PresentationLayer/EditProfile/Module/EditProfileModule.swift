//
//  EditProfileModule.swift
//  myconvy
//
//  Created by Евгений Каштанов on 28/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class EditProfileModule {
    class func create() -> EditProfileModuleInput {
        let storyboard = UIStoryboard(name: "EditProfile", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        let presenter = EditProfilePresenter()
        let interactor = EditProfileInteractor()
        let router = EditProfileRouter()
        let networkService = NetworkService()
        
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        
        interactor.output = presenter
        interactor.userDataFetcher = NetworkUserDataFetcher()
        interactor.networkService = networkService
        return presenter
    }
}
