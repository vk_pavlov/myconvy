//
//  EditProfileModuleInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 28/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol EditProfileModuleInput {
    var viewController: UIViewController { get }
    func present(from viewController: UIViewController, viewModel: EditProfileViewModel)

}
