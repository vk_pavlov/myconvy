//
//  AuthorizeMethod.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 28/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

protocol AuthService {
    func signUp()
    func forceLogout()
    var params: [String: String]? { get }
    var delegate: AuthServiceDelegate? { get set }
}

protocol AuthorizationModel {
    var selection: (() -> ())? { get set }
    var image: UIImage { get set }
}

struct AuthorizeMethodViewModel: AuthorizationModel {
    var image: UIImage
    var selection: (() -> ())?
}
