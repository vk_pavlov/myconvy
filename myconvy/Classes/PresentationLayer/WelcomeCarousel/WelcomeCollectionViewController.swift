//
//  WelcomeCollectionViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 28/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

import UIKit

class WelcomeCollectionViewController: HorizontalPeekingPagesCollectionViewController {

    var cells = PageModel.fetchPage()

    override func calculateSectionInset() -> CGFloat {
        return Const.leftDistanceToView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(GalleryCollectionViewCell.self, forCellWithReuseIdentifier: GalleryCollectionViewCell.reuseId)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cells.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryCollectionViewCell.reuseId, for: indexPath) as! GalleryCollectionViewCell
           cell.imageView.image = cells[indexPath.row].image
           cell.label.text = cells[indexPath.row].text
           return cell
    }
}
