//
//  PageModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 28/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit


struct PageModel {
    let text: String
    let image: UIImage
    
    static func fetchPage() -> [PageModel] {
        let item1 = PageModel(text: "Привет! В inJoin ты можешь посмотреть, чем предлагают заняться люди поблизости",
                              image: UIImage(named: "Group")!)
        
        let item2 = PageModel(text: "И присоединиться к ним!",
                              image: UIImage(named: "Groupsecond")!)
        
        let item3 = PageModel(text: "Или создать свое собственное событие…",
                              image: UIImage(named: "Groupthird")!)
        
        let item4 = PageModel(text: "И собрать для него компанию!",
                              image: UIImage(named: "Groupfour")!)
        
        let item5 = PageModel(text: "Присоединяйся прямо сейчас и всегда будь в тренде!",
                              image: UIImage(named: "Groupfive")!)
        
        return [item1, item2, item3, item4, item5]
    }
}

struct Const {
    static let leftDistanceToView: CGFloat = 40
    static let rightDistanceToView: CGFloat = 40
    static let galleryMinimalLineSpacing: CGFloat = 10
    
    static let galleryItemWidth = (UIScreen.main.bounds.width - Const.leftDistanceToView - Const.rightDistanceToView - (Const.galleryMinimalLineSpacing / 2))
}
