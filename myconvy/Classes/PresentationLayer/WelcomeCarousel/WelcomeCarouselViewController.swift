//
//  WelcomeCarouselViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 28/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

class WelcomeCarouselViewController: UIViewController {
    
    @IBOutlet weak var joinButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        joinButton.backgroundColor = OrangeTheme.myconvyOrange
    }
    
    @IBAction func joinButtonPressed(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "isShowedCarousel")
        WelcomeModule.create().present(from: self)
    }
    
    
}
