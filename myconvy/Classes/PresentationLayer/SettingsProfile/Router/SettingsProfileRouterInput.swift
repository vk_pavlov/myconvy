//
//  SettingsProfileRouterInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 26/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol SettingsProfileRouterInput {
    func presentPrivacySettings(from: UIViewController)
    func presentListUsers(from: UIViewController)
    func presentDocuments(from: UIViewController)
    func presentFeedback(from: UIViewController)
}
