//
//  SettingsProfileRouter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 26/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class SettingsProfileRouter {
    
}

extension SettingsProfileRouter: SettingsProfileRouterInput {
    func presentFeedback(from: UIViewController) {
        FeedbackModule.create().present(from: from)
    }

    func presentDocuments(from: UIViewController) {
        DocumentsModule.create().present(from: from)
    }

    func presentListUsers(from: UIViewController) {
        //ListUsersModule.create().present(from: from, users: CommonData.selfUser().usersBanned, navItemTitle: "Заблокированные")
    }
    
    func presentPrivacySettings(from: UIViewController) {
        PrivacySettingsModule.create().present(from: from)
    }
    
}
