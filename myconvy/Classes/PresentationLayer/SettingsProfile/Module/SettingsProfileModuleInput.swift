//
//  SettingsProfileModuleInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 26/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol SettingsProfileModuleInput {
    var viewController: UIViewController { get }
    func present(from: UIViewController)
}
