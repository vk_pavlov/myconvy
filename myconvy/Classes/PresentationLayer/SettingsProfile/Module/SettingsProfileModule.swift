//
//  SettingsProfileModule.swift
//  myconvy
//
//  Created by Евгений Каштанов on 26/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class SettingsProfileModule {
    class func create() -> SettingsProfileModuleInput {
        let storyboard = UIStoryboard(name: "SettingsProfile", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SettingsProfileViewController") as! SettingsProfileViewController
        let presenter = SettingsProfilePresenter()
        let interactor = SettingsProfileInteractor()
        let router = SettingsProfileRouter()
        
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        
        interactor.output = presenter
        interactor.userDataFetcher = NetworkUserDataFetcher()
        return presenter
    }
}
