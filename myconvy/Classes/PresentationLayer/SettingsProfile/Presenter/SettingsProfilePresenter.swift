//
//  SettingsProfilePresenter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 26/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class SettingsProfilePresenter {
    weak var view: SettingsProfileViewInput!
    var interactor: SettingsProfileInteractorInput!
    var router: SettingsProfileRouterInput!
}

extension SettingsProfilePresenter: SettingsProfileViewOutput {
    func didDeleteProfile() {
        interactor.deleteProfile()
    }
    
    func didFeedback() {
        router.presentFeedback(from: view.viewController)
    }

    func didDocumentsCellPressed() {
        router.presentDocuments(from: view.viewController)
    }

    func viewIsReady() {
        view.setupInitialState()
    }
    
    func didPrivacySettingsCellPressed() {
        router.presentPrivacySettings(from: view.viewController)
    }
    
    func didBlacklistSettingsCellPressed() {
        router.presentListUsers(from: view.viewController)
    }
    
}

extension SettingsProfilePresenter: SettingsProfileInteractorOutput {
    func didFailureError() {
        view.showErrorAction()
    }
    
}

extension SettingsProfilePresenter: SettingsProfileModuleInput {
    
    var viewController: UIViewController {
        return view.viewController
    }
    
    func present(from: UIViewController) {
        view.present(fromViewController: from)
    }
}
