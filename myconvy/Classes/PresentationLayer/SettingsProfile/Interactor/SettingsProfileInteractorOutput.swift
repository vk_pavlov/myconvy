//
//  SettingsProfileInteractorOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 26/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol SettingsProfileInteractorOutput: class {
    func didFailureError()
}
