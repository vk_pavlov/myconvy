//
//  SettingsProfileInteractor.swift
//  myconvy
//
//  Created by Евгений Каштанов on 26/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class SettingsProfileInteractor {
    weak var output: SettingsProfileInteractorOutput!
    var userDataFetcher: UserDataFetcher!
}

extension SettingsProfileInteractor: SettingsProfileInteractorInput {
    func deleteProfile() {
        userDataFetcher.deleteUser { (response, error) in
            if response != nil {
                AppDelegate.currentDelegate.logOut()
            }
        }
    }
}
