//
//  SettingsProfileOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 26/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


protocol SettingsProfileViewOutput {
    func viewIsReady()
    func didPrivacySettingsCellPressed()
    func didBlacklistSettingsCellPressed()
    func didDocumentsCellPressed()
    func didFeedback()
    func didDeleteProfile()
}
