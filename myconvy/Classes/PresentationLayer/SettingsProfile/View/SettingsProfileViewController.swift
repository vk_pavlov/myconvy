//
//  SettingsProfileViewController.swift
//  myconvy
//
//  Created by Евгений Каштанов on 26/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class SettingsProfileViewController: UIViewController {
    
    var output: SettingsProfileViewOutput!
    
    private let items = [
        ["Приватность"],
        ["Документы", "Обратная связь"],
        ["Удалить профиль", "Выйти"]
    ]
    
    private var spinner: Spinner!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
}

extension SettingsProfileViewController: SettingsProfileViewInput {
    func showErrorAction() {
        self.spinner.setStateSpinner(enable: false, view: self.view)
        let alert = Helpers.createAlertWithError(error: "Что-то пошло не так")
        present(alert, animated: true, completion: nil)
    }
    
    func setupInitialState() {
        spinner = Spinner()
        //navigationController?.navigationBar.tintColor = OrangeTheme.myconvyOrange
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension SettingsProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActionCell", for: indexPath)
            cell.textLabel?.text = items[indexPath.section][indexPath.row]
            if indexPath.row == 0 {
                cell.textLabel?.textColor = .red
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
            cell.textLabel?.text = items[indexPath.section][indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                output.didPrivacySettingsCellPressed()
            } else {
                output.didBlacklistSettingsCellPressed()
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                output.didDocumentsCellPressed()
            } else if indexPath.row == 1 {
                output.didFeedback()
            }
        } else {
            if indexPath.row == 0 {
                let alert = Helpers.createWarningAlert(message: "Вы точно хотите удалить профиль?") {
                    self.spinner.setStateSpinner(enable: true, view: self.view)
                    self.output.didDeleteProfile()
                }
                present(alert, animated: true, completion: nil)
            } else {
                AppDelegate.currentDelegate.logOut()
            }
        }
    }
}
