//
//  SettingsProfileInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 26/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol SettingsProfileViewInput: class, Presentable {
    func setupInitialState()
    func showErrorAction()
}
