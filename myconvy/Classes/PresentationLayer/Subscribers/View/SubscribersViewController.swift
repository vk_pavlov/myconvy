//
//  SubscribersViewController.swift
//  myconvy
//
//  Created by Евгений Каштанов on 29/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import RealmSwift

class SubscribersViewController: UIViewController {
    
    var output: SubscribersViewOutput!
    var listUsers: List<String>!
    var numberOfUsers: Int!
    var isMe: Bool!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    
    
}

extension SubscribersViewController: SubscribersViewInput {
    func setUserData(_ users: List<String>) {
        //let realm = try! Realm()
        self.listUsers = users
        self.numberOfUsers = listUsers.count
        //let userMySelf = realm.objects(User.self).first
    }
    
    func setupInitialState() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension SubscribersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if numberOfUsers != 0 {
            return numberOfUsers
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if numberOfUsers != 0 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
//            cell.configure(user: listUsers[indexPath.row])
//            return cell
//        } else {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "NoSubscribersCell", for: indexPath)
//            if isMe {
//                cell.textLabel?.text = "К сожалению, у вас пока нет подписчиков. Создавайте интересные события, чтобы привлечь внимание людей!"
//            } else {
//                cell.textLabel?.text = "У пользователя пока нет подписчиков."
//            }
//            return cell
//        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoSubscribersCell")
        return cell!

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if numberOfUsers != 0 {
            return 84
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}
