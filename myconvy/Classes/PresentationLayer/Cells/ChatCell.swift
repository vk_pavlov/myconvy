//
//  ChatCell.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/08/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class ChatCell: TableViewCellBase {

    @IBOutlet weak var avatarCreatorEventImageView: UIImageView!
    @IBOutlet weak var nameOfEventLabel: UILabel!
    @IBOutlet weak var avatarLastResponderImageView: UIImageView!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var dateLastMessageLabel: UILabel!
    @IBOutlet weak var unreadMarkImageView: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        avatarCreatorEventImageView.layer.cornerRadius = avatarCreatorEventImageView.frame.width / 2
        avatarCreatorEventImageView.clipsToBounds = true

        avatarLastResponderImageView.layer.cornerRadius = avatarLastResponderImageView.frame.width / 2
        avatarLastResponderImageView.clipsToBounds = true

        unreadMarkImageView.layer.cornerRadius = unreadMarkImageView.frame.width / 2
        unreadMarkImageView.clipsToBounds = true
        // Initialization code
    }

    func configure(chat: Chat) {
        nameOfEventLabel.text = chat.chatName
        lastMessageLabel.text = chat.lastMessage.kind.get()
        dateLastMessageLabel.text = chat.lastMessage.sentDate.toString(format: .notificationDate)
        guard let avatarLastResponderUrl = URL(string: chat.lastMessage.author.photoProfile) else { return }
        avatarLastResponderImageView.kf.setImage(with: avatarLastResponderUrl)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
