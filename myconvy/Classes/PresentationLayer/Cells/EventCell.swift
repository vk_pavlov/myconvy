//
//  EventCell.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 23/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import CoreLocation

let categoriesImageName = [
    "category_cinema",
    "category_sport",
    "category_art",
    "category_travel",
    "category_dating",
    "category_concert",
    "category_science",
    "category_party",
    "category_walks",
    "category_other"
]

class EventCell: TableViewCellBase {
    
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var dateStartView: DateStartView!
    @IBOutlet weak var responseStatusView: ResponseStatusView!
    @IBOutlet weak var distanceView: DistanceView!
    @IBOutlet weak var titleEventLabel: UILabel!
    @IBOutlet weak var creatorLabel: UILabel!
    
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = 8
            containerView.backgroundColor = .white
            containerView.layer.masksToBounds = false
            containerView.layer.shadowOpacity = 0.15
            containerView.layer.shadowRadius = 6
            containerView.layer.shadowOffset = CGSize(width: -3, height: 7)
            containerView.layer.shadowColor = UIColor.gray.cgColor
            contentView.layer.cornerRadius = 8
        }
    }

    func set(viewModel: EventCellViewModel) {
        titleEventLabel.text = viewModel.title
        creatorLabel.text = viewModel.author
        dateStartView.date = viewModel.startDate
        responseStatusView.set(callerStatus: viewModel.callerStatus, eventStatus: viewModel.eventStatus, isOrders: viewModel.isOrders)
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    distanceView.isHidden = true
                case .authorizedAlways, .authorizedWhenInUse:
                    distanceView.isHidden = false
                @unknown default:
                break
            }
        }
        distanceView.distance = viewModel.distance
        let imageName = categoriesImageName[viewModel.cagegory]
        categoryImageView.image = UIImage(named: imageName)!
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        categoryImageView.layer.cornerRadius = categoryImageView.frame.height / 2
    }
    
}
