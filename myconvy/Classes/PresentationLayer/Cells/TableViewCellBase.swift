//
//  TableViewCellBase.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 11/05/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class TableViewCellBase: UITableViewCell {

    class var identifier: String {
        return String(describing: self)
    }

    class var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
