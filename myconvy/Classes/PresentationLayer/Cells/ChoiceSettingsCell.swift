//
//  ChoiceSettingsCell.swift
//  myconvy
//
//  Created by Евгений Каштанов on 03/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class ChoiceSettingsCell: TableViewCellBase {

    @IBOutlet private weak var choiceImageView: UIImageView!
    @IBOutlet private weak var cellLabel: UILabel!
    
    var isChoosen: Bool = false {
        didSet {
            isChoosen ? (choiceImageView.image = UIImage(named: "Choosen")) : (choiceImageView.image = UIImage(named: "NotChoosen"))
        }
    }
    
    var cellText: String? {
        get {
            return cellLabel.text
        }
        set {
            cellLabel.text = newValue
        }
    }
    
}
