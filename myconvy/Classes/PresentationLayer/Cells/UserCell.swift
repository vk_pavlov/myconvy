//
//  UserCell.swift
//  myconvy
//
//  Created by Евгений Каштанов on 22/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol UserCellDelegate: class {
    func userCellAddUserToEvent(_ cell: UserCell)
    func userCellRejectUser(_ cell: UserCell)
}

class UserCell: TableViewCellBase {
    
    weak var delegate: UserCellDelegate?

    @IBOutlet weak var avatarImageView: AvatarBezierView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var agePlusCityLabel: UILabel!
    @IBOutlet weak var addUserButton: UIButton!
    @IBOutlet weak var rejectUserButton: UIButton!
    
    var isHiddenButtons: Bool {
        get {
            return addUserButton.isHidden
        }
        set {
            addUserButton.isHidden = newValue
            rejectUserButton.isHidden = newValue
        }
    }
    
    func set(viewModel: UserCellViewModel) {
        avatarImageView.set(imageUrl: viewModel.imageUrl, online: viewModel.online)
        nameLabel.text = viewModel.name
        agePlusCityLabel.text = viewModel.agePlusCity
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.translatesAutoresizingMaskIntoConstraints = false
        avatarImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        avatarImageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        addUserButton.setImage(UIImage(named: "plus_icon")!, for: .normal)
        rejectUserButton.setImage(UIImage(named: "minus_icon")!, for: .normal)
    }
    
    @IBAction func rejectButtonPressed(_ sender: UIButton) {
        delegate?.userCellRejectUser(self)
    }
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        delegate?.userCellAddUserToEvent(self)
    }
}

