//
//  ContactInfoTableViewCell.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 14/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol ContactInfoTableViewCellDelegate: class {
    func contactInfoTableViewCell(_ cell: ContactInfoTableViewCell, didChangedTextFieldContent content: String?)
}

class ContactInfoTableViewCell: UITableViewCell {
    
    weak var delegate: ContactInfoTableViewCellDelegate?
    
    static let reuseId = "ContactInfoTableViewCell"
    
     private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue", size: 14)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        return label
    }()
    
    private let textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.borderStyle = .none
        textField.contentVerticalAlignment = .bottom
        textField.contentHorizontalAlignment = .trailing
        textField.font = UIFont(name: "HelveticaNeue", size: 14)
        textField.textColor = UIColor.black.withAlphaComponent(0.5)
        //textField.attributedPlaceholder =
        textField.textAlignment = .right
        return textField
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        contentView.addSubview(textField)
        contentView.addSubview(label)
        
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        textField.delegate = self
        
        label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        
        textField.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        textField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
        textField.leadingAnchor.constraint(equalTo: label.leadingAnchor, constant: 16).isActive = true
    }
    
    func set(viewModel: ContactInfoViewModel) {
        label.text = viewModel.title
        textField.placeholder = viewModel.placeholder
        textField.text = viewModel.value
    }
    
    func set(viewModel: InfoAboutUserViewModel) {
        label.text = viewModel.key
        textField.text = viewModel.value
        textField.isEnabled = false
    }
    
    @objc private func textFieldDidChange() {
        delegate?.contactInfoTableViewCell(self, didChangedTextFieldContent: textField.text)
    }
    
}

extension ContactInfoTableViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 25
    }
}
