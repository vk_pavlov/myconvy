//
//  InfoAboutUserModuleInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 22/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol InfoAboutUserModuleInput {
    var viewController: UIViewController { get }
    func present(from viewController: UIViewController, userId: Int)
}
