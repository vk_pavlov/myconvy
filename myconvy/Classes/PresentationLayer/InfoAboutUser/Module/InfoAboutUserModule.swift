//
//  InfoAboutUserModule.swift
//  myconvy
//
//  Created by Евгений Каштанов on 22/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class InfoAboutUserModule {
    class func create() -> InfoAboutUserModuleInput {
        let storyboard = UIStoryboard(name: "InfoAboutUser", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "InfoAboutUserViewController") as! InfoAboutUserViewController
        let presenter = InfoAboutUserPresenter()
        let interactor = InfoAboutUserInteractor()
        let router = InfoAboutUserRouter()
        
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router
        
        interactor.output = presenter
        interactor.userDataFetcher = NetworkUserDataFetcher()
        return presenter
    }
}
