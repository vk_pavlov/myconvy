//
//  InfoAboutUserInteractor.swift
//  myconvy
//
//  Created by Евгений Каштанов on 22/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

class InfoAboutUserInteractor {
    weak var output: InfoAboutUserInteractorOutput!
    var userDataFetcher: UserDataFetcher!
}

extension InfoAboutUserInteractor: InfoAboutUserInteractorInput {
    func getUserById(_ userId: Int) {
        userDataFetcher.getUserById(userId: userId) { [weak self] (userResponse, errorResponce) in
            if let userResponse = userResponse {
                self?.output.didReceiveUser(userResponse.userInfo)
            }
        }
    }
}
