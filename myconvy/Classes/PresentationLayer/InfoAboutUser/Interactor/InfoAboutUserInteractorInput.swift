//
//  InfoAboutUserInteractorInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 22/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


protocol InfoAboutUserInteractorInput {
    func getUserById(_ userId: Int)
}
