//
//  InfoAboutUserRouterInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 22/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol InfoAboutUserRouterInput {
    func transitionOnVk(vkId: String, vc: UIViewController)
    func transitionOnInst(instLink: String, vc: UIViewController)
}
