//
//  InfoAboutUserRouter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 22/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class InfoAboutUserRouter {
    
}

extension InfoAboutUserRouter: InfoAboutUserRouterInput {
    func transitionOnVk(vkId: String, vc: UIViewController) {
        Helpers.openLinkInOtherApp(link: vkId, vc: vc, scheme: .vk)
    }

    func transitionOnInst(instLink: String, vc: UIViewController) {
        Helpers.openLinkInOtherApp(link: instLink, vc: vc, scheme: .inst)
    }
    
}
