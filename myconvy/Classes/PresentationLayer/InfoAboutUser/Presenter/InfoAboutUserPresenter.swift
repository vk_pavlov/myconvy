//
//  InfoAboutUserPresenter.swift
//  myconvy
//
//  Created by Евгений Каштанов on 22/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class InfoAboutUserPresenter {
    weak var view: InfoAboutUserViewInput!
    var interactor: InfoAboutUserInteractorInput!
    var router: InfoAboutUserRouterInput!
    var userId: Int!
    
    private func infoAboutUserViewModel(from: UserTwo) -> [[InfoAboutUserViewModel]]{
        var result = [[InfoAboutUserViewModel]]()
        
        // Основная информация
        
        var mainInfo = [InfoAboutUserViewModel]()
        let name = InfoAboutUserViewModel(key: "Имя", value: from.name)
        let lastName = InfoAboutUserViewModel(key: "Фамилия", value: from.lastname)
        mainInfo += [name, lastName]
        if let birthdate = from.birthdate {
            let date = Date(timeIntervalSince1970: birthdate)
            let birthdateViewModel = InfoAboutUserViewModel(key: "Дата рождения", value: date.toString(format: .userBirthDateFormat))
            mainInfo.append(birthdateViewModel)
        }
        
        if let city = from.city, !city.isEmpty {
            let cityViewModel = InfoAboutUserViewModel(key: "Город", value: city)
            mainInfo.append(cityViewModel)
        }
        
        if let status = from.status, !status.isEmpty {
            let statusViewModel = InfoAboutUserViewModel(key: "Статус", value: status)
            mainInfo.append(statusViewModel)
        }
        
        if !from.univ.isEmpty {
            let universityViewModel = InfoAboutUserViewModel(key: "Университет", value: from.univ)
            mainInfo.append(universityViewModel)
        }
        
        result.append(mainInfo)
        
        //  Контакты
        var contactsInfo = [InfoAboutUserViewModel]()
        
        if let phone = from.contPhone, phone != "hidden", !phone.isEmpty {
            let phoneViewModel = InfoAboutUserViewModel(key: "Телефон", value: phone)
            contactsInfo.append(phoneViewModel)
        }
        
        if let vk = from.contVk, vk != "hidden", !vk.isEmpty {
            let vkViewModel = InfoAboutUserViewModel(key: "ВКонтакте", value: vk)
            contactsInfo.append(vkViewModel)
        }
        
        if let fb = from.contFb, fb != "hidden", !fb.isEmpty {
            let fbViewModel = InfoAboutUserViewModel(key: "Facebook", value: fb)
            contactsInfo.append(fbViewModel)
        }
        
        if let inst = from.contInst, inst != "hidden", !inst.isEmpty {
            let instViewModel = InfoAboutUserViewModel(key: "Instagram", value: inst)
            contactsInfo.append(instViewModel)
        }
        
        if let email = from.contEmail, email != "hidden", !email.isEmpty {
            let emailViewModel = InfoAboutUserViewModel(key: "Email", value: email)
            contactsInfo.append(emailViewModel)
        }
        
        if !contactsInfo.isEmpty {
            result.append(contactsInfo)
        }
        
        return result
    }
    
}

extension InfoAboutUserPresenter: InfoAboutUserViewOutput {
    func didTransitionOnVk(vkId: String) {
        router.transitionOnVk(vkId: vkId, vc: view.viewController)
    }

    func didTransitionOnInst(instLink: String) {
        router.transitionOnInst(instLink: instLink, vc: view.viewController)
    }
    
    func viewIsReady() {
        interactor.getUserById(userId)
        view.setupInitialState()
    }
}

extension InfoAboutUserPresenter: InfoAboutUserInteractorOutput {
    func didReceiveUser(_ user: UserTwo) {
        let viewModel = infoAboutUserViewModel(from: user)
        view.setViewModel(viewModel)
    }
}

extension InfoAboutUserPresenter: InfoAboutUserModuleInput {
    var viewController: UIViewController {
        return view.viewController
    }
    
    func present(from: UIViewController, userId: Int) {
        self.userId = userId
        view.present(fromViewController: from)
    }
}
