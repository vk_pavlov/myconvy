//
//  InfoAboutUserOutput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 22/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


protocol InfoAboutUserViewOutput {
    func viewIsReady()
    func didTransitionOnVk(vkId: String)
    func didTransitionOnInst(instLink: String)
}
