//
//  InfoAboutUserInput.swift
//  myconvy
//
//  Created by Евгений Каштанов on 22/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol InfoAboutUserViewInput: class, Presentable {
    func setViewModel(_ viewModel: [[InfoAboutUserViewModel]])
    func setupInitialState()
}
