//
//  InfoAboutUserViewController.swift
//  myconvy
//
//  Created by Евгений Каштанов on 22/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class InfoAboutUserViewController: UIViewController {
    
    @IBOutlet weak var infoAboutUserTableView: UITableView!
    
    var output: InfoAboutUserViewOutput!
    private let spinner = Spinner(style: .gray)
    private var infoAboutUserViewModels = [[InfoAboutUserViewModel]]()
    private let copyView = CopyLinkView()
    private var copyLinkViewVerticalOffset: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.setStateSpinner(enable: true, view: self.view)
        output.viewIsReady()
        view.addSubview(copyView)
        copyView.center = view.center
        copyView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        copyLinkViewVerticalOffset = copyView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: -200)
        copyLinkViewVerticalOffset.isActive = true
    }
    
    private var sections: [String] {
        let count = infoAboutUserViewModels.count
        if count == 1 {
            return ["Основная"]
        } else if count == 2 {
            return ["Основная", "Контакты"]
        }
        return [""]
    }

}

extension InfoAboutUserViewController: InfoAboutUserViewInput {
    func setViewModel(_ viewModel: [[InfoAboutUserViewModel]]) {
        spinner.setStateSpinner(enable: false, view: self.view)
        infoAboutUserViewModels = viewModel
        infoAboutUserTableView.reloadData()
    }
    
    func setupInitialState() {
        title = "Информация"
        infoAboutUserTableView.register(ContactInfoTableViewCell.self, forCellReuseIdentifier: ContactInfoTableViewCell.reuseId)
        //navigationController?.navigationBar.tintColor = OrangeTheme.myconvyOrange
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension InfoAboutUserViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return infoAboutUserViewModels.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoAboutUserViewModels[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactInfoTableViewCell.reuseId, for: indexPath) as! ContactInfoTableViewCell
        cell.set(viewModel: infoAboutUserViewModels[indexPath.section][indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = OrangeTheme.myconvyGray
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = infoAboutUserViewModels[indexPath.section][indexPath.row]
        UIPasteboard.general.string = model.value
        view.isUserInteractionEnabled = false
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: [],
            animations: {
                self.copyLinkViewVerticalOffset.constant = 0
                self.view.layoutIfNeeded()
            },
            completion: { _ in
                UIView.animate(
                    withDuration: 0.1,
                    delay: 1,
                    options: [],
                    animations: {
                        self.copyLinkViewVerticalOffset.constant = -200
                        self.view.layoutIfNeeded()
                    },
                    completion: {_ in
                        self.view.isUserInteractionEnabled = true
                    }
                )
                
            }
        )
    }
    
}
