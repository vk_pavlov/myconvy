//
//  TabBarModuleInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol TabBarModuleInput: class {
    func present()
    func present(from: UIViewController)
}
