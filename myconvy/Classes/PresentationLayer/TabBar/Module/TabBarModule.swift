//
//  TabBarModule.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class TabBarModule {
    class func create() -> TabBarModuleInput {
        let router = TabBarRouter()
        let presenter = TabBarPresenter()
        let interactor = TabBarInteractor()
        let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        let networkServices = NetworkService()
        interactor.output = presenter
        interactor.networkService = networkServices
        viewController.output = presenter
        presenter.view = viewController
        presenter.router = router
        presenter.interactor = interactor
        return presenter
    }
}
