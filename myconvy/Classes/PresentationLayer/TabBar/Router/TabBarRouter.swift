//
//  TabBatRouter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class TabBarRouter {
    lazy var feed: FeedModuleInput = FeedModule.create()
    lazy var map: MapModuleInput = MapModule.create()
    lazy var messages: ChatsModuleInput = ChatsModule.create()
    lazy var profile: ProfileModuleInput = ProfileModule.create()
}

extension TabBarRouter: TabBarRouterInput {
    
    func getTabViewControllers() -> [UIViewController] {
        return [
            feed.viewController.wrapToNavigationController(),
            map.viewController.wrapToNavigationController(),
            UIViewController(),
            messages.viewController.wrapToNavigationController(),
            profile.viewController.wrapToNavigationController()
        ]
    }
}
