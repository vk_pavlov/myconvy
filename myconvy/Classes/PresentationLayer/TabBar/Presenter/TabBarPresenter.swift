//
//  TabBarPresenter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit
import Foundation


class TabBarPresenter {
    
    weak var view: TabBarViewInput!
    var interactor: TabBarInteractorInput!
    var router: TabBarRouterInput!
    
    private func insertViewControllers() {
        let viewControllers = router.getTabViewControllers()
        view.viewControllers = viewControllers
    }
}

extension TabBarPresenter: TabBarViewOutput {
    
    func viewIsReady() {
        insertViewControllers()
        view.setupInitialState()
    }
}


extension TabBarPresenter: TabBarInteractorOutput {
    
}

extension TabBarPresenter: TabBarModuleInput {
    func present(from: UIViewController) {
        view.viewController.modalPresentationStyle = .fullScreen
        from.present(view.viewController, animated: true, completion: nil)
    }
    
    
    
    func present() {
        view.present()
    }
    
}
