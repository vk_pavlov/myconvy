//
//  TabBarViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    var output: TabBarViewOutput!
    
    private lazy var createEventButton: UIButton = {
        let button = UIButton()
        button.frame.size = CGSize(width: 70, height: 70)
        button.setImage(UIImage(named: "new_event")!, for: .normal)
        button.addTarget(self, action: #selector(newEventPressed), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        configureTabBarItems()
        configurePositionTabBarItemsImages()
        view.addSubview(createEventButton)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func configureTabBarItems() {
        for tabBarItem in tabBar.items! {
            tabBarItem.title = ""
        }
        tabBar.items![2].isEnabled = false
        selectedIndex = 1
    }
    
    private func configurePositionTabBarItemsImages() {
        guard #available(iOS 13, *) else {
            for tabBarItem in tabBar.items! {
                tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
            }
            return
        }
        
    }

    @objc private func newEventPressed() {
        NewEventModule.create().present(from: self)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        createEventButton.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: tabBar.frame.origin.y + 14)
    }
    
    @objc func selectedChatIsShow() {
        createEventButton.isHidden = true
    }
    
    @objc func selectedChatIsHidden() {
        createEventButton.isHidden = false
    }
    
}

extension TabBarViewController: TabBarViewInput {
    func setupInitialState() {
        tabBar.tintColor = OrangeTheme.myconvyOrange
        tabBar.barTintColor = .white
        
        tabBar.layer.shadowOffset = CGSize.zero
        tabBar.layer.shadowRadius = 2;
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.3;
        
        NotificationCenter.default.addObserver(self, selector: #selector(selectedChatIsShow), name: NSNotification.Name(rawValue: "SelectedChatIsShow"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selectedChatIsHidden), name: NSNotification.Name(rawValue: "SelectedChatIsHidden"), object: nil)
    }
}
