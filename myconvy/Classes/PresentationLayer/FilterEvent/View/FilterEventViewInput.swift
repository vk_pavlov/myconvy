//
//  FilterEventViewInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol FilterEventViewInput: class, Presentable {
    func setupInitialState()
}
