//
//  FilterEventViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class FilterEventViewController: UIViewController {

    var output: FilterEventViewOutput!
    
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    
    private let feedCategoriesCollectionView = CategoriesCollectionView()
    private var defaultCategories = categories
    
    private let sortButton: ButtonWithImage = {
        let button = ButtonWithImage(iconPosition: .right)
        button.setImage(UIImage(named: "arrow_down"), for: .normal)
        button.heightAnchor.constraint(equalToConstant: 25).isActive = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.black.withAlphaComponent(0.25), for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        button.setTitle("Сортировка", for: .normal)
        button.addTarget(self, action: #selector(sortButtonPressed), for: .touchUpInside)
        button.contentHorizontalAlignment = .left
        return button
    }()
    
    private let moreInfoButtonView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return view
    }()
    
    private let categoriesTitleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return view
    }()
    
    private let categoriesTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        label.heightAnchor.constraint(equalToConstant: 22).isActive = true
        label.text = "Категории"
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        scrollView.alwaysBounceVertical = true
        
        moreInfoButtonView.addSubview(sortButton)
        sortButton.topAnchor.constraint(equalTo: moreInfoButtonView.topAnchor).isActive = true
        sortButton.leadingAnchor.constraint(equalTo: moreInfoButtonView.leadingAnchor, constant: 16).isActive = true
        
        stackView.addArrangedSubview(moreInfoButtonView)
        
        categoriesTitleView.addSubview(categoriesTitle)
        categoriesTitle.leadingAnchor.constraint(equalTo: categoriesTitleView.leadingAnchor, constant: 16).isActive = true
        categoriesTitle.topAnchor.constraint(equalTo: categoriesTitleView.topAnchor).isActive = true
        
        stackView.addArrangedSubview(categoriesTitleView)
        
        stackView.addArrangedSubview(feedCategoriesCollectionView)
        
        feedCategoriesCollectionView.delegate = self
        feedCategoriesCollectionView.dataSource = self
        
    }
    
    @IBAction func closeButtonPressed(_ sender: UIBarButtonItem) {
        dissmissModal()
    }
    
    @objc private func sortButtonPressed() {
    }

}

extension FilterEventViewController: FilterEventViewInput {
    func setupInitialState() {
        
        let textAttributes = [NSAttributedString.Key.foregroundColor: OrangeTheme.myconvyOrange,
                              NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 18)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        closeButton.tintColor = OrangeTheme.myconvyOrange
        closeButton.setTitleTextAttributes([NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Medium", size: 14)], for: .normal)
        closeButton.setTitleTextAttributes([NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Medium", size: 14)], for: .highlighted)
        
    }
    
}

extension FilterEventViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return defaultCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterCategoriesCollectionViewCell.reuseId, for: indexPath) as! FilterCategoriesCollectionViewCell
        cell.title = defaultCategories[indexPath.row]
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let font = UIFont(name: "HelveticaNeue-Bold", size: 14)
        let category = (defaultCategories[indexPath.row] as NSString).size(withAttributes: [NSAttributedString.Key.font : font!])
        return CGSize(width: max(category.width * 2 - 48, CategoriesCollectionView.Metrics.galleryItemWidth), height: CategoriesCollectionView.Metrics.galleryItemHeight)
    }
    
}

extension FilterEventViewController: FilterCategoriesCollectionViewCellDelegate {
    func filterCategoriesCollectionViewCellDidSelect(_ cell: CategoryWithShadowCollectionViewCell) {
        guard let indexPath = feedCategoriesCollectionView.indexPath(for: cell) else { return }
        self.feedCategoriesCollectionView.performBatchUpdates({
            defaultCategories.remove(at: indexPath.row)
            self.feedCategoriesCollectionView.deleteItems(at: [indexPath])
        }, completion: nil)
    }
}
