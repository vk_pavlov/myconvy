//
//  FilterEventPresenter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class FilterEventPresenter {
    weak var view: FilterEventViewInput!
    weak var output: FilterEventModuleOutput!
    var interactor: FilterEventInteractorInput!
    
}


extension FilterEventPresenter: FilterEventViewOutput {
    func didSelectedMetroStations(_ stations: Set<String>) {
        view.dissmissModal()
    }
    
    func viewIsReady() {
        //interactor.getMetroStations()
        view.setupInitialState()
    }
    
    
}


extension FilterEventPresenter: FilterEventInteractorOutput {
    func didReciveMetroStations(_ statoins: [String]) {
        //view.setMetroStations(stations: statoins)
    }
    
    
}

extension FilterEventPresenter: FilterEventModuleInput {
    func present(from viewController: UIViewController, viewModel: FilterViewModel, output: FilterEventModuleOutput) {
        view.presentNavigationController(fromViewController: viewController)
    }
    
    var viewController: UIViewController {
        return view.viewController
    }
    
    func present(from viewController: UIViewController, output: FilterEventModuleOutput) {
        self.output = output
        view.presentNavigationController(fromViewController: viewController)
    }
    
    
}
