//
//  FilterCategoriesCollectionViewCell.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 18/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

import UIKit

protocol FilterCategoriesCollectionViewCellDelegate: class {
    func filterCategoriesCollectionViewCellDidSelect(_ cell: CategoryWithShadowCollectionViewCell)
}

class FilterCategoriesCollectionViewCell: CategoryWithShadowCollectionViewCell {
    
    override class var reuseId: String {
        return "FilterCategoriesCollectionViewCell"
    }
    
    weak var delegate: FilterCategoriesCollectionViewCellDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        confugure()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        confugure()
    }
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 17).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 17).isActive = true
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 8
        imageView.isUserInteractionEnabled = true
        imageView.image = UIImage(named: "off_icon")!
        return imageView
    }()
    
    private func confugure() {
        addSubview(imageView)
        
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.addTarget(self, action: #selector(deleteCategoryPressed))
        imageView.addGestureRecognizer(tapGestureRecognizer)
    
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageView.centerXAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
    }
    
    @objc private func deleteCategoryPressed() {
        delegate?.filterCategoriesCollectionViewCellDidSelect(self)
    }
    
}
