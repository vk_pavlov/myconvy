//
//  FilterEventModuleInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


protocol FilterEventModuleInput {
    var viewController: UIViewController { get }
    func present(from viewController: UIViewController, viewModel: FilterViewModel, output: FilterEventModuleOutput)
}
