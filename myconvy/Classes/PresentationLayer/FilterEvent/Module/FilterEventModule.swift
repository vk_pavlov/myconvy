//
//  FilterEventModule.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class FilterEventModule {
    class func create() -> FilterEventModuleInput {
        let storyboard = UIStoryboard(name: "FilterEvent", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FilterEventViewController") as! FilterEventViewController
        let presenter = FilterEventPresenter()
        let interactor = FilterEventInteractor()
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        interactor.output = presenter
        return presenter
    }
}
