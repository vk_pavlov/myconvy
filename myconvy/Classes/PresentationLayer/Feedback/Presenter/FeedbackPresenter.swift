//
//  FeedbackPresenter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit


class FeedbackPresenter {

    weak var view: FeedbackViewInput!
    var interactor: FeedbackInteractorInput!
    var router: FeedbackRouterInput!

}

extension FeedbackPresenter: FeedbackModuleInput {
    var viewController: UIViewController {
        return view.viewController
    }

    func present(from viewController: UIViewController) {
        view.present(fromViewController: viewController)
    }


}

extension FeedbackPresenter: FeedbackViewOutput {
    func didSendFeedback(message: String) {
        interactor.sendFeedback(message: message)
    }

    func didTransitionOnVk() {
        router.transitionOnVk(viewController: view.viewController)
    }

    func didTransitionOnInst() {
        router.transitionOnInst(viewController: view.viewController)
    }

    func didTransitionOnEmail() {
        router.transitionOnEmail(viewController: view.viewController)
    }

    func viewIsReady() {
        view.setupInitialState()
    }


}

extension FeedbackPresenter: FeedbackInteractorOutput {
    func didReciveSendFeedback() {
        view.successSendFeedback()
    }


}
