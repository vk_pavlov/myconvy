//
//  FeedbackInteractorInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

protocol FeedbackInteractorInput {
    func sendFeedback(message: String)
}
