//
//  FeedbackModule.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit


class FeedbackModule {
    class func create() -> FeedbackModuleInput {
        let storyboard = UIStoryboard(name: "Feedback", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
        let presenter = FeedbackPresenter()
        let interactor = FeedbackInteractor()
        viewController.output = presenter
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = FeedbackRouter()

        interactor.output = presenter
        interactor.networkService = NetworkService()
        return presenter
    }
}
