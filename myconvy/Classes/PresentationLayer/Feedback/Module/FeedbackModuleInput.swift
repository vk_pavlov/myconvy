//
//  FeedbackModuleInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

protocol FeedbackModuleInput {
    var viewController: UIViewController { get }
    func present(from viewController: UIViewController)
}
