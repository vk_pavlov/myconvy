//
//  FeedbackViewController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {

    @IBOutlet weak var feedbackTextView: UITextView!
    @IBOutlet weak var sendFeedbackButton: UIButton!
    @IBOutlet weak var vkTransitionButton: UIButton!
    @IBOutlet weak var instTransitionButton: UIButton!
    @IBOutlet weak var emailTransitionButton: UIButton!

    var output: FeedbackViewOutput!
    let spinner = Spinner(style: .gray)

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    @IBAction func sendFeedbackPressed(_ sender: UIButton) {
        if !feedbackTextView.text.isEmpty {
            spinner.setStateSpinner(enable: true, view: self.view)
            output.didSendFeedback(message: feedbackTextView.text)
        }
    }

    @IBAction func vkTransitionPressed(_ sender: UIButton) {
        output.didTransitionOnVk()
    }

    @IBAction func instTransitionPressed(_ sender: UIButton) {
        output.didTransitionOnInst()
    }

    @IBAction func emailTransitionPressed(_ sender: UIButton) {
        output.didTransitionOnEmail()
    }
    
    @IBAction func tapOnScreenPressed(_ sender: UITapGestureRecognizer) {
        feedbackTextView.resignFirstResponder()
    }

}

extension FeedbackViewController: FeedbackViewInput {
    func successSendFeedback() {
        spinner.setStateSpinner(enable: false, view: self.view)
        feedbackTextView.text = nil
    }

    func setupInitialState() {
        navigationController?.navigationBar.tintColor = OrangeTheme.myconvyOrange
        feedbackTextView.delegate = self
        feedbackTextView.backgroundColor = .white

        view.backgroundColor = OrangeTheme.mvGray

        navigationItem.title = "Обратная связь"
        feedbackTextView.layer.cornerRadius = 14.0
        feedbackTextView.layer.borderWidth = 1.0
        feedbackTextView.layer.borderColor = UIColor.lightGray.cgColor

        sendFeedbackButton.setTitleColor(OrangeTheme.myconvyOrange, for: .normal)
        sendFeedbackButton.layer.borderWidth = 1.0
        sendFeedbackButton.layer.borderColor = OrangeTheme.myconvyOrange.cgColor
        sendFeedbackButton.layer.cornerRadius = 10.0

    }


}

extension FeedbackViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
