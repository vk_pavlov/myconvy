//
//  FeedbackRouterInput.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

protocol FeedbackRouterInput {
    func transitionOnVk(viewController: UIViewController)
    func transitionOnInst(viewController: UIViewController)
    func transitionOnEmail(viewController: UIViewController)
}
