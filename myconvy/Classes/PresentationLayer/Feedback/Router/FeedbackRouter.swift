//
//  FeedbackRouter.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit


class FeedbackRouter {

}

extension FeedbackRouter: FeedbackRouterInput {
    func transitionOnVk(viewController: UIViewController) {
        Helpers.openLinkInOtherApp(link: Links.vkontakte, vc: viewController, scheme: .vk)
    }

    func transitionOnInst(viewController: UIViewController) {
        Helpers.openLinkInOtherApp(link: Links.instagram, vc: viewController, scheme: .inst)
    }

    func transitionOnEmail(viewController: UIViewController) {
        Helpers.openLinkInOtherApp(link: Links.email, vc: viewController, scheme: .email)
    }


}
