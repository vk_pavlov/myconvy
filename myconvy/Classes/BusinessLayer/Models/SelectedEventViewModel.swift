//
//  SelectedEventViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct SelectedEventViewModel {
    let headerViewModel: HeaderSelectedEventViewModel
    let description: String?
    let participantsCount: Int
    let gender: Int
    let place: Place
    let distance: Double
    let descriptionPlace: String
    let creatorInfo: CreatorInfo
    let conditionsEventViewViewModel: ConditionsEventViewViewModel
}

struct CreatorInfo {
    let userId: Int
    let name: String
    let moreInfo: String
    let avatar: String
    let online: Bool
}

enum RelationButtonState {
    case none
    case eventFinished
    case iAmParticipant
    case waitingApprove
    case iAmCreator
}
