//
//  NewEventViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 12/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct NewEventViewModel {
    var title: String
    var place: Place
    var date: Double
    var category: Int
    var descriptionEvent: String?
    var descriptionPlace: String?
    var sex: Int?
    var partLimit: Int
    var privacy: Int?
}
