//
//  User.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift
import Locksmith


class User: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var online: Bool = true
    @objc dynamic var photoProfile: String = ""
    @objc dynamic var birthDate: Double = 0
    @objc dynamic var status: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var sex: Int = 0
    @objc dynamic var relations: Int = 0
    @objc dynamic var meetsOrders: String = "" //события, на которые хочет пользователь
    @objc dynamic var meetsMade: String = "" // созданные события
    @objc dynamic var meetsAll: String = "" // события с участием пользователя
    @objc dynamic var userFollowers: String = "" // подписчики
    @objc dynamic var usersSubs: String = "" // подписки
    @objc dynamic var usersBanned: String = "" // заблокированные пользователи
    @objc dynamic var notificationCount: Int = 0
    @objc dynamic var email: String = ""
    @objc dynamic var phone: String = ""
    @objc dynamic var phonePrivacy: Int = 0
    @objc dynamic var vkLink: String = ""
    @objc dynamic var fbLink: String = ""
    @objc dynamic var instLink: String = ""
    @objc dynamic var socPrivacy: Int = 0
    @objc dynamic var isNewUser: Bool = true
    @objc dynamic var ages: Int = 0
    var callerStatus: Int = 0
    
    @objc dynamic var fullName: String {
        return "\(name) \(lastName)"
    }
    
    @objc dynamic var namePlusAge: String {
        return "\(name), \(birthDate)"
    }
    
    @objc dynamic var cityPlusAge: String {
        return (!city.isEmpty ? "\(city), " : "Город не указан, ") + String(ages)
    }
    
    var dictSocialNetworksLinks: [String: String] {
        return [
            "vkontakte": vkLink,
            //"телефон": phone,
            "instagram": instLink
            //"facebook": fbLink
        ]
    }
    //let dialogs = List<Dialog>()
    static func getSelfUserWithData(data: JSON) -> User {
        let user = User()
        let result = data["result"]
        let userInfo = data["result"]["user_info"]
        user.id = userInfo["id"].intValue
        user.name = userInfo["name"].stringValue
        user.lastName = userInfo["lastname"].stringValue
        user.online = userInfo["online"].boolValue
        user.photoProfile = userInfo["avatar"].stringValue
        user.birthDate = userInfo["birthdate"].doubleValue
        user.status = userInfo["status"].stringValue
        user.city = userInfo["city"].stringValue
        user.sex = userInfo["sex"].intValue
        user.relations = userInfo["relations"].intValue
        user.meetsOrders = userInfo["meets_orders"].stringValue
        user.meetsMade = userInfo["meets_made"].stringValue
        user.meetsAll = userInfo["meets_all"].stringValue
        user.userFollowers = userInfo["users_follow"].stringValue
        user.usersSubs = userInfo["users_subs"].stringValue
        user.usersBanned = userInfo["users_banned"].stringValue
        user.notificationCount = userInfo["nots_unread"].intValue
        user.email = userInfo["email"].stringValue
        user.phone = userInfo["cont_phone"].stringValue
        user.phonePrivacy = userInfo["phone_privacy"].intValue
        user.vkLink = userInfo["vk_link"].stringValue
        user.fbLink = userInfo["fb_link"].stringValue
        user.instLink = userInfo["inst_link"].stringValue
        user.socPrivacy = userInfo["soc_privacy"].intValue
        user.isNewUser = result["new_user"].boolValue
        user.ages = userInfo["ages"].intValue
        return user
    }

    func getMeetsOrders() -> [String] {
        return User.stringToArray(meetsOrders)
    }
    
    func getMeetsMade() -> [String] {
        return User.stringToArray(meetsMade)
    }
    
    func getMeetsAll() -> [String] {
        return User.stringToArray(meetsAll)
    }
    
    func getUsersFollow() -> [String] {
        return User.stringToArray(userFollowers)
    }
    
    func getUsersSubs() -> [String] {
        return User.stringToArray(usersSubs)
    }
    
    func getUsersBanned() -> [String] {
        return User.stringToArray(usersBanned)
    }

    func isSelfUser() -> Bool {
        let selfUser = CommonData.selfUser()
        return selfUser.id == id
    }
    
    private static func stringToArray(_ data: String) -> [String] {
        if data.isEmpty {
            return [String]()
        }
        var resultArray = data.components(separatedBy: ";")
        resultArray.removeLast()
        return resultArray
    }
    
    func arrayToString(array: [String]) -> String {
        return array.joined(separator: ";") + ";"
    }
    
    static func getAuthorEvent(dataUser: JSON) -> User {
        return getUserWithParams(paramsUser: dataUser)
    }
    
    static func getParticipiantsEvent(participiants: [JSON]) -> List<User> {
        let listParticipiants = List<User>()
        for participiant in participiants {
            let user = getUserWithParams(paramsUser: participiant)
            listParticipiants.append(user)
        }
        return listParticipiants
    }
    
    static func getUser(json: JSON) -> User {
        let userInfo = json["result"]["userinfo"]
        let id = userInfo["id"].intValue
        let selfUser = CommonData.selfUser()
        //let usersBanned = stringToArray(userInfo["users_banned"].stringValue)
        if id == selfUser.id {
            return getSelfUserWithData(data: json)
        }
//        else if usersBanned.contains(selfUser.id) {
//            return getUserBanned(data: json)
//        }
        return getUserFollow(data: json["result"]["userinfo"])
    }
    
    static func getUserBanned(data: JSON) -> User {
        let user = User()
        let userInfo = data["result"]["userinfo"]
        user.id = userInfo["id"].intValue
        user.name = userInfo["name"].stringValue
        user.lastName = userInfo["lastname"].stringValue
        user.city = userInfo["city"].stringValue
        user.online = userInfo["online"].boolValue
        user.photoProfile = userInfo["avatar"].stringValue
        user.sex = userInfo["sex"].intValue
        user.usersBanned = userInfo["users_banned"].stringValue
        return user
    }
    
    static func getUserFollow(data: JSON) -> User {
        let user = User()
        user.id = data["id"].intValue
        user.name = data["name"].stringValue
        user.lastName = data["lastname"].stringValue
        user.city = data["city"].stringValue
        user.online = data["online"].boolValue
        user.photoProfile = data["avatar"].stringValue
        user.birthDate = data["ages"].doubleValue
        user.status = data["status"].stringValue
        user.sex = data["sex"].intValue
        user.relations = data["relations"].intValue
        user.meetsMade = data["meets_made"].stringValue
        user.meetsAll = data["meets_all"].stringValue
        user.userFollowers = data["users_follow"].stringValue
        user.usersSubs = data["users_subs"].stringValue
        user.phone = data["phone"].stringValue
        user.vkLink = data["vk_link"].stringValue
        user.fbLink = data["fb_link"].stringValue
        user.instLink = data["inst_link"].stringValue
        user.callerStatus = data["caller_status"].intValue
        return user
    }
//def parseJson(json, result)
//    for attr in dir(result):
//    if "__" not in att: # this is not privete field:
//    if attr not in json:
//    # error handling return "expected json to have {} attr, but got {}".format(attr, json)
//    else:
//    if type(result.gettatr(attr)) != type(json[attr]):
//    # error handling return "type of class and json attr does not match. Expected {}, got {}".format(type(result.gettatr(attr)), type(json[attr]))
//    else:
//        result.setattr(json[attr])
//    return ""
    
    static func getUsersFromJSON(usersArray: JSON) -> [User] {
        var resultListUsers = [User]()
        if let result = usersArray["result"].dictionary,
            let usersInfo = result["usersinfo"]?.array {
            for objectUser in usersInfo {
                let user = User()
                user.name = objectUser["name"].stringValue
                user.lastName = objectUser["lastname"].stringValue
                user.online = objectUser["online"].boolValue
                user.photoProfile = objectUser["avatar"].stringValue
                user.birthDate = objectUser["ages"].doubleValue
                user.sex = objectUser["sex"].intValue
                user.id = objectUser["id"].intValue
                user.city = objectUser["city"].stringValue
                user.vkLink = objectUser["vk_link"].stringValue
                resultListUsers.append(user)
            }
        }
        return resultListUsers
    }
    
    private static func getUserWithParams(paramsUser: JSON) -> User {
        let authorEvent = User()
        authorEvent.city = paramsUser["city"].stringValue
        authorEvent.sex = paramsUser["sex"].intValue
        authorEvent.name = paramsUser["name"].stringValue
        authorEvent.id = paramsUser["id"].intValue
        authorEvent.lastName = paramsUser["lastname"].stringValue
        authorEvent.online = paramsUser["online"].boolValue
        authorEvent.photoProfile = paramsUser["avatar"].stringValue
        authorEvent.birthDate = Double(paramsUser["ages"].intValue)
        return authorEvent
    }
   
    
    func getSexName() -> String {
        let codeNumber = Int(sex)
        switch codeNumber {
        case 1: return "Женский"
        case 2: return "Мужской"
        default: return "Не указан"
        }
    }
    
    func getRelationsName() -> String {
        let codeNumber = Int(relations)
        switch codeNumber {
        case 1: return "Не женат(не замужем)"
        case 2: return "Есть друг/подруга"
        case 3: return "Помолвлен(а)"
        case 4: return "Женат(замужем)"
        case 5: return "Все сложно"
        case 6: return "В активном поиске"
        case 7: return "Влюблен(а)"
        case 8: return "Гражданский брак"
        default: return "Не указан"
        }
    }
    
    
    func getAge() -> String {
//        let now = Date()
//        if let birthday = Date.stringToDate(birthDate) {
//            let calendar = Calendar.current
//            let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
//            let age = ageComponents.year!
//            return String(age)
//        }
        return ""
    }

    func updateUserParamsRealm() {
        let realm = try! Realm()
        let user = CommonData.selfUser()
        try! realm.write {
            user.name = name
            user.lastName = lastName
            user.sex = sex
            user.status = status
            user.birthDate = birthDate
            user.city = city
            user.relations = relations
            user.vkLink = vkLink
            user.instLink = instLink
            user.fbLink = fbLink
            user.phone = phone
            user.usersSubs = usersSubs
            user.userFollowers = userFollowers
            user.meetsAll = meetsAll
            user.meetsMade = meetsMade
            user.meetsOrders = meetsOrders
            user.photoProfile = photoProfile
        }
    }

    static func getUserFromSocket(data: JSON) -> User {
        let user = User()
        user.id = data["id"].intValue
        user.name = data["name"].stringValue
        user.lastName = data["lastname"].stringValue
        user.city = data["city"].stringValue
        user.online = data["online"].boolValue
        user.photoProfile = data["avatar"].stringValue
        user.birthDate = data["ages"].doubleValue
        user.sex = data["sex"].intValue
        user.vkLink = data["vk_link"].stringValue
        user.callerStatus = data["caller_status"].intValue
        return user
    }
    
}


extension User: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = User()
        copy.id = id
        copy.name = name
        copy.lastName = lastName
        copy.online = online
        copy.photoProfile = photoProfile
        copy.birthDate = birthDate
        copy.status = status
        copy.city = city
        copy.sex = sex
        copy.relations = relations
        copy.meetsOrders = meetsOrders
        copy.meetsMade = meetsMade
        copy.meetsAll = meetsAll
        copy.userFollowers = userFollowers
        copy.usersSubs = usersSubs
        copy.usersBanned = usersBanned
        copy.notificationCount = notificationCount
        copy.email = email
        copy.phone = phone
        copy.phonePrivacy = phonePrivacy
        copy.vkLink = vkLink
        copy.fbLink = fbLink
        copy.instLink = instLink
        copy.socPrivacy = socPrivacy
        copy.isNewUser = isNewUser
        return copy
    }
    
    
}
