//
//  Dialog.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Chat {
    let chatId: String
    let chatName: String
    let eventId: String
    let participants: ListIDs
    let messagesCount: String
    let lastMessage: MyMessage

    init(data: JSON, ourselfId: String) {
        chatId = data["chatid"].stringValue
        chatName = data["chatname"].stringValue
        eventId = data["meet_id"].stringValue
        participants = ListIDs(withString: data["participants"].stringValue)
        messagesCount = data["messages"].stringValue
        lastMessage = MockMessage(data: data["lastmessage"], ourselfId: ourselfId)
    }

}
