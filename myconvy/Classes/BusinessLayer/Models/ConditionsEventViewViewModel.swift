//
//  ConditionsEventViewViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 11/01/2020.
//  Copyright © 2020 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct ConditionsEventViewViewModel {
    let participantsCount: Int
    let participantsGender: Int
    let requestToEventCount: Int?
}
