//
//  CountersStackViewViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct CountersStackViewViewModel {
    let eventsCount: Int
    let subscribersCount: Int // подписчики
    let subscriptionsCount: Int // подписки
}
