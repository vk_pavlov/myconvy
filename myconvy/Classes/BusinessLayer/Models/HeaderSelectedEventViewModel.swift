//
//  HeaderSelectedEventViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct HeaderSelectedEventViewModel {
    let category: Int
    let title: String
    let date: Date
}
