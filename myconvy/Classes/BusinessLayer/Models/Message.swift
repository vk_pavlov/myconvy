//
//  Message.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/08/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation
import MessageKit

extension MessageKind {
    func get() -> String {
        switch self {
        case .text(let text):
            return text
        case .attributedText(let text):
            return text.string
        case .photo(_):
            return ""
        case .video(_):
            return ""
        case .location(_):
            return ""
        case .emoji(_):
            return ""
        case .audio(_):
            return ""
        case .contact(_):
            return ""
        case .custom(_):
            return ""
        }
    }
}

private struct MockLocationItem: LocationItem {

    var location: CLLocation
    var size: CGSize

    init(location: CLLocation) {
        self.location = location
        self.size = CGSize(width: 240, height: 240)
    }
}

private struct MockMediaItem: MediaItem {

    var url: URL?
    var image: UIImage?
    var placeholderImage: UIImage
    var size: CGSize

    init(image: UIImage) {
        self.image = image
        self.size = CGSize(width: 240, height: 240)
        self.placeholderImage = UIImage()
    }
}

struct Sender: SenderType {
    var senderId: String
    var displayName: String
}

protocol MyMessage: MessageType {
    var author: User { get set }
    var chatId: String {get set}
}

internal struct MockMessage: MyMessage {

    let messageId: String
    var chatId: String
    let sender: SenderType
    let sentDate: Date
    let kind: MessageKind
    var author: User

    init(data: JSON, ourselfId: String) {
        self.messageId = data["id"].stringValue
        self.chatId = data["meet_id"].stringValue
        author = User.getUserFromSocket(data: data["author"])
        let attributedText = NSAttributedString(string: data["text"].stringValue,
                                                attributes: [.font: UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular),
                                                             .foregroundColor: ourselfId == String(author.id) ? UIColor.white: UIColor.black])
        kind = .attributedText(attributedText)
        self.sender = Sender(senderId: String(author.id), displayName: author.fullName)
        let dat: TimeInterval = Double(data["date"].stringValue)!
        sentDate = Date(timeIntervalSince1970: dat)
    }

    private init(kind: MessageKind, sender: SenderType, messageId: String, date: Date, ourself: User, chatId: String) {
        self.kind = kind
        self.sender = sender
        self.messageId = messageId
        self.sentDate = date
        self.chatId = chatId
        author = ourself
    }

//    init(text: String, sender: SenderType, messageId: String, date: Date) {
//        self.init(kind: .text(text), sender: sender, messageId: messageId, date: date)
//    }

    init(attributedText: NSAttributedString, sender: SenderType, messageId: String, date: Date, ourself: User, chatId: String) {
        self.init(kind: .attributedText(attributedText), sender: sender, messageId: messageId, date: date, ourself: ourself, chatId: chatId)
    }

//    init(image: UIImage, sender: Sender, messageId: String, date: Date) {
//        let mediaItem = MockMediaItem(image: image)
//        self.init(kind: .photo(mediaItem), sender: sender, messageId: messageId, date: date)
//    }
//
//    init(thumbnail: UIImage, sender: Sender, messageId: String, date: Date) {
//        let mediaItem = MockMediaItem(image: thumbnail)
//        self.init(kind: .video(mediaItem), sender: sender, messageId: messageId, date: date)
//    }
//
//    init(location: CLLocation, sender: Sender, messageId: String, date: Date) {
//        let locationItem = MockLocationItem(location: location)
//        self.init(kind: .location(locationItem), sender: sender, messageId: messageId, date: date)
//    }
//
//    init(emoji: String, sender: Sender, messageId: String, date: Date) {
//        self.init(kind: .emoji(emoji), sender: sender, messageId: messageId, date: date)
//    }
}

