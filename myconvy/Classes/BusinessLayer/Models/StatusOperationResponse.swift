//
//  StatusOperationResponse.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 28/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct StatusOperationResponseWrapped: Decodable {
    let result: SuccessResponse
}

struct SuccessResponse: Decodable {
    var success: Int
}
