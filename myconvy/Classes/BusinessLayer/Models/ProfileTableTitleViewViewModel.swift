//
//  ProfileTableTitleViewViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct ProfileTableTitleViewViewModel {
    let headerUserInfoViewModel: HeaderUserInfoViewModel
    let counterStackViewViewModel: CountersStackViewViewModel
    var isSubscribtion: Bool? = nil
}
