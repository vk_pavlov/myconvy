//
//  SelectedEventResponse.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct SelectedEventResponseWrapped: Decodable {
    let result: SelectedEventResponse
}

struct SelectedEventResponse: Decodable {
    let meetInfo: EventTwo
}
