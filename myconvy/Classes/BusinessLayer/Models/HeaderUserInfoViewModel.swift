//
//  HeaderUserInfoViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


struct HeaderUserInfoViewModel {
    let fullName: String
    let otherInfo: String
    let avatarUrl: String
    let online: Bool
}
