//
//  SingleUserResponse.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 29/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct SingleUserResponseWrapped: Decodable {
    let result: SingleUserResponse
}

struct SingleUserResponse: Decodable {
    let userInfo: UserTwo
}
