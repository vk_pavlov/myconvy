//
//  EventCellViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct EventCellViewModel {
    let id: Int
    let authorId: Int
    let title: String
    let cagegory: Int
    let author: String
    let distance: Double
    let startDate: Date
    let callerStatus: CallerStatus
    let eventStatus: EventStatus
    let isOrders: Bool
}

enum CallerStatus: Int {
    case none
    case participants
    case waitingApprove
    case blocked
}

enum EventStatus: Int {
    case actual
    case limitParticipants
    case inProgress
    case finished
}

