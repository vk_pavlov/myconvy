//
//  EditUserViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 24/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct EditProfileViewModel {
    var avatar: String?
    var name: String
    var lastName: String
    var gender: Int
    var birthdate: Double
    var interest: [Int]
    
    var city: String?
    var university: String?
    var relations: Int?
    var contactInfo: [ContactInfoViewModel]
    var aboutSelf: String?
}

extension EditProfileViewModel: Equatable {
    static func == (lhs: EditProfileViewModel, rhs: EditProfileViewModel) -> Bool {
        return lhs.avatar == rhs.avatar &&
            lhs.name == rhs.name &&
            lhs.lastName == rhs.lastName &&
            lhs.gender == rhs.gender &&
            lhs.birthdate == rhs.birthdate &&
            lhs.interest == rhs.interest &&
            lhs.city == rhs.city &&
            lhs.university == rhs.university &&
            lhs.relations == rhs.relations &&
            lhs.contactInfo == rhs.contactInfo &&
            lhs.aboutSelf == rhs.aboutSelf
    }
    
    
}
