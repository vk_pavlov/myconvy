//
//  ContactInfoViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 14/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct ContactInfoViewModel {
    let title: String
    let placeholder: String
    var value: String?
}

extension ContactInfoViewModel: Equatable {
    static func == (lhs: ContactInfoViewModel, rhs: ContactInfoViewModel) -> Bool {
        return lhs.value == rhs.value
    }
}
