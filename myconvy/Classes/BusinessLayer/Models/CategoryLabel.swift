//
//  Label.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 03/08/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class CategoryLabel: UILabel {

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: -5)
        super.drawText(in: rect.inset(by: insets))
    }
}
