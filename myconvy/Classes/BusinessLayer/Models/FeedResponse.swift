//
//  FeedResponse.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 04/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct ErrorResponseWrapped: Decodable {
    let result: ErrorResponse
}

struct ErrorResponse: Decodable {
    let error: ResultError
}

struct ResultError: Decodable, Error {
    let code: Int
    let description: String
    var problem: String {
        switch code {
        case 110: return "Пользователь удален"
        case 111: return "Событие удалено"
        default: return ""
        }
    }
}

struct FeedResponseWrapped: Decodable {
    let result: FeedResponse
}

struct FeedResponse: Decodable {
    let meetsInfo: [EventTwo]
}

struct EventTwo: Decodable {
    let id: Int
    let title: String
    let place: Place
    let startDate: Double
    let creator: UserTwo
    let partCount: Int
    let partLimit: Int
    let partSex: Int
    let category: Int
    let privacy: Int
    let callerStatus: Int
    let status: Int
    
    let description: String?
    let plDesc: String?
    let chat: String?
    let participants: [Int]?
    let orders: [Int]?
    let deniteds: [Int]?
    
    let ordersCount: Int?
    
    var isOrders: Bool {
        guard ordersCount != nil else { return false }
        return ordersCount != 0
    }
}

struct Place: Decodable {
    var lat: Double
    var lng: Double
}
