//
//  ListIDs.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/08/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct ListIDs {
    var idS: [String]

    init() {
        idS = [String]()
    }

    init(withString: String) {
        if withString.isEmpty {
            idS = [String]()
        }
        var resultArray = withString.components(separatedBy: ",")
        resultArray.removeLast()
        idS = resultArray
    }

    func toString() -> String {
        if idS.isEmpty {
            return ""
        }
        return idS.joined(separator: ";") + ";"
    }

    mutating func addId(id: String) {
        idS.append(id)
    }

    func contains(id: String) -> Bool {
        if let _ = idS.firstIndex(of: id) {
            return true
        }
        return false
    }

    mutating func removeId(id: String) {
        if let index = idS.firstIndex(of: id) {
            idS.remove(at: index)
        }
    }

}
