//
//  InfoAboutUserViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 09/02/2020.
//  Copyright © 2020 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct InfoAboutUserViewModel {
    let key: String
    let value: String
}
