//
//  UserResponse.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 04/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct UserResponseWrapped: Decodable {
    let result: UserResponse
}

struct UserResponse: Decodable {
    let usersInfo: [UserTwo]
}

struct UserTwo: Decodable {
    let id: Int
    let name: String
    let lastname: String
    let online: Bool
    let avatar: String
    let ages: Int?
    let univ: String
    let sex: Int?
    let callerStatus: Int?
    let city: String?
    let status: String?
    let type: Int?
    let birthdate: Double?
    let relations: Int?
    let categories: [Int]?
    
    
    let meets: [Int]?
    let meetsRequests: [Int]?
    let meetsCreated: [Int]?
    let usersFollowers: [Int]?
    let usersFollowing: [Int]?
    let usersBanned: [Int]?
    
    let notsUnread: Int?
    let contVk: String?
    let contPhone: String?
    let contFb: String?
    let contInst: String?
    let contEmail: String?
    let phonePrivacy: Int?
    let socPrivacy: Int?
    let locPrivacy: Int?
    
    
    var namePlusAge: String {
        guard let ages = ages else { return name}
        return "\(name), \(ages)"
    }
    
    var cityPlusAge: String {
        guard
            let city = city,
            let ages = ages else { return "" }
        return "\(city), \(ages)"
    }
    
    var fullName: String {
        return name + " " + lastname
    }
    
    var universityPlusAge: String {
        if univ.isEmpty {
            return ages != nil ? "\(ages!)" : ""
        }
        return ages != nil ? "\(univ), \(ages!)" : "\(univ)"
    }
}
