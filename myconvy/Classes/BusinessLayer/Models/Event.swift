//
//  Event.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON


class Event: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var descriptionEvent: String = ""
    @objc dynamic var startDate: Double = 0
    @objc dynamic var dateEnd: Double = 0
    @objc dynamic var place: String = ""
    @objc dynamic var placeDescription: String = ""
    @objc dynamic var author: User? = nil
    @objc dynamic var metro: String = ""
    @objc dynamic var listParticipants: String = ""
    @objc dynamic var listOrders: String = ""
    @objc dynamic var listDeniteds: String = ""
    @objc dynamic var chatId: String = ""
    @objc dynamic var participantsCount: Int = 0
    @objc dynamic var numParticipantsMax: Int = 0
    @objc dynamic var sexParticipants: Int = 0
    @objc dynamic var status: Int = 0
    @objc dynamic var ordersCount: String = ""
    @objc dynamic var callerStatus: Int = 0
    @objc dynamic var category: Int = 0
    
//    var dateEvent: Date? {
//        let dateFormatter = DateFormatter()
//        let locale = Locale(identifier: "rus")
//        dateFormatter.locale = locale
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let date = dateFormatter.date(from: dateBegin)
//        return date
//    }
    
    static func createEvent(title: String, descriptionEvent: String, dateBegin: String, place: String, placeDescription: String, numParticipantsMax: String, sexParticipants: String) -> Event {
        let event = Event()
        event.title = title
        event.descriptionEvent = descriptionEvent
        //event.dateBegin = dateBegin
        event.place = place
        event.placeDescription = placeDescription
        //event.numParticipantsMax = numParticipantsMax
        //event.sexParticipants = sexParticipants
        return event
    }
    
    
    static func getEventById(event: JSON) -> Event {
        let resultEvent = Event()
        resultEvent.id = event["id"].intValue
        resultEvent.title = event["title"].stringValue
        resultEvent.descriptionEvent = event["description"].stringValue
        resultEvent.startDate = event["start_date"].doubleValue
        resultEvent.dateEnd = event["end_date"].doubleValue
        resultEvent.place = event["place"].stringValue
        resultEvent.placeDescription = event["pl_desc"].stringValue
        resultEvent.author = User.getAuthorEvent(dataUser: event["creator"])
        resultEvent.listParticipants =  event["participants"].stringValue
        resultEvent.participantsCount = event["part_count"].intValue
        resultEvent.numParticipantsMax = event["part_limit"].intValue
        resultEvent.sexParticipants = event["part_sex"].intValue
        resultEvent.status = event["status"].intValue
        resultEvent.listOrders = event["orders"].stringValue
        resultEvent.chatId = event["chat"].stringValue
        resultEvent.callerStatus = event["caller_status"].intValue
        resultEvent.category = event["category"].intValue
        return resultEvent
    }
    
    static func getEvents(eventsArray: JSON) -> [Event] {
        var resultListEvents = [Event]()
        if let result = eventsArray["result"].dictionary,
            let meetsInfo = result["meetsinfo"]?.array {
            for objectEvent in meetsInfo {
                let event = Event()
                event.status = objectEvent["status"].intValue
                event.title = objectEvent["title"].stringValue
                event.place = objectEvent["place"].stringValue
                event.id = objectEvent["id"].intValue
                event.numParticipantsMax = objectEvent["part_limit"].intValue
                event.dateEnd = objectEvent["end_date"].doubleValue
                event.chatId = objectEvent["chat"].stringValue
                event.startDate = objectEvent["start_date"].doubleValue
                event.participantsCount = objectEvent["part_count"].intValue
                event.sexParticipants = objectEvent["part_sex"].intValue
                event.author = User.getAuthorEvent(dataUser: objectEvent["creator"])
                event.ordersCount = objectEvent["orders_count"].stringValue
                event.callerStatus = objectEvent["caller_status"].intValue
                event.category = objectEvent["category"].intValue
                resultListEvents.append(event)
            }
        }
        return resultListEvents
    }
    
//    func getColorForDateEvent() -> UIColor {
//        guard let date = dateEvent else { return .black }
//        var color: UIColor!
//        if Calendar.current.isDateInToday(date) {
//            color = #colorLiteral(red: 0.1137254902, green: 0.6941176471, blue: 0, alpha: 1)
//        } else if Calendar.current.isDateInTomorrow(date) {
//            color = #colorLiteral(red: 0.8745098039, green: 0.4470588235, blue: 0, alpha: 1)
//        } else {
//            color = .black
//        }
//        return color
//    }
    
//    func formattiongDateForSelectedEvent() -> String {
//        guard let date = dateEvent else { return "дата не указана"}
//        let time = date.getDateAsStringWithFormat(dateFormat: "HH:mm")
//        if Calendar.current.isDateInToday(date) {
//            return "Сегодня в \(time)"
//        } else if Calendar.current.isDateInTomorrow(date) {
//            return "Завтра в \(time)"
//        } else {
//            return date.getDateAsStringWithFormat(dateFormat: "dd MMMM в HH:mm")
//        }
//    }
//
//    func formattingDateForCellEvent() -> String {
//        guard let date = dateEvent else { return "дата не указана"}
//        if Calendar.current.isDateInToday(date) {
//            return date.getDateAsStringWithFormat(dateFormat: "HH:mm")
//        } else if Calendar.current.isDateInTomorrow(date) {
//            return "Завтра"
//        } else {
//            return date.getDateAsStringWithFormat(dateFormat: "dd MMM")
//        }
//    }
    

    func getSexName() -> String {
        switch sexParticipants {
        case 1: return "Пол: женский"
        case 2: return "Пол: мужской"
        default: return "Пол: неважно"
        }
    }

    func isMyEvent() -> Bool {
        let selfUser = CommonData.selfUser()
        return author?.id == selfUser.id
    }
    
    func formattingCountPeopleOfEvent() -> String {
        if numParticipantsMax == 0 {
            return "\(participantsCount) \(getDeclensionForCountPeople())"
        }
        return "\(participantsCount) из \(numParticipantsMax) \(getDeclensionForCountPeople())"
    }
    
    private func getDeclensionForCountPeople() -> String {
        if participantsCount == 2 || participantsCount == 3 || participantsCount == 4 {
            return "человека"
        }
        return "человек"
    }
    
//    func getShortDateFinished() -> String {
//        guard let date = dateEvent else { return "неизвестно" }
//        return date.getDateAsStringWithFormat(dateFormat: "dd MMMM yyyy")
//    }

    func getUsersOrders() -> [String] {
        return Event.stringToArray(listOrders)
    }

    private static func stringToArray(_ data: String) -> [String] {
        if data.isEmpty {
            return [String]()
        }
        var resultArray = data.components(separatedBy: ";")
        resultArray.removeLast()
        return resultArray
    }

    func getPlaceDescription() -> NSMutableAttributedString {
        let attrString = NSMutableAttributedString(string: placeDescription)
        if placeDescription == "hidden" {
            let text = "Место станет доступно, когда Вас примут в событие"
            attrString.mutableString.setString(text)
            attrString.addAttribute(.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 0, length: text.count))
            return attrString
        }
        return attrString
    }

}


extension Event: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Event()
        copy.author = author
        copy.id = id
        copy.title = title
        copy.descriptionEvent = descriptionEvent
        copy.startDate = startDate
        copy.dateEnd = dateEnd
        copy.place = place
        copy.placeDescription = placeDescription
        copy.metro = metro
        copy.listParticipants = listParticipants
        copy.listOrders = listOrders
        copy.listDeniteds = listDeniteds
        copy.chatId = chatId
        copy.participantsCount = participantsCount
        copy.numParticipantsMax = numParticipantsMax
        copy.sexParticipants = sexParticipants
        copy.status = status
        return copy
    }



}

