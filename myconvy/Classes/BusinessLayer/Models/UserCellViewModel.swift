//
//  UserCellViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 29/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct UserCellViewModel {
    var id: Int
    var imageUrl: String
    var online: Bool
    var name: String
    var agePlusCity: String
}

extension UserCellViewModel: Equatable {
    
}
