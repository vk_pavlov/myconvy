//
//  FilterViewModel.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct FilterViewModel {
    var sortedBy: Int
    var categories: [Int]
    var gender: Int
    var minAges: Int
    var maxAges: Int
}
 
