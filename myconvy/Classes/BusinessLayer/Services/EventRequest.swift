//
//  EventsRequest.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 09/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//


import Foundation
import Alamofire
import Locksmith


enum EventRequest {
    case createMeet(event: Event)
    case getMeets(user: User, start: Int, place: Int, followingOnly: Bool)
    case getMeetById(eventId: String)
    case getMeetsById(events: String)
    case joinMeetById(eventId: String)
    case getMeetsSubs(start: Int)
    case editMeetByID(event: Event, initialEventId: String)
    //case editMeetPartRel(action: ActionForEditMeetRel, eventId: String, userId: String)
    case leaveMeet(eventId: String)
    case delMeetByID(eventId: String)
    case getMeetsGone
}

extension EventRequest: APIMethodProtocol {
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        return ""
    }
    
    var params: Params? {
        switch self {
        case .createMeet(let event):
            return [
                "method": "createMeet",
                "title": event.title,
                "place": event.place,
                "start_date": event.startDate,
                "end_date": event.startDate,
                "description": event.descriptionEvent,
                "pl_desc": event.placeDescription,
                "part_limit": event.numParticipantsMax,
                "part_sex": event.sexParticipants,
                "category": 0,
                "privacy": 0,
                "add_after_start": 0,
                "notify_followers": 0,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
            // передаем пользователя потому что getMeets можно попросить у другого пользователя а не только у себя
        case .getMeets(let user, let start, let place, let followingOnly):
            return [
                "method": "getMeets",
                "start": start,
                "place": place,
                "search": "",
                "following_only": followingOnly,
                "callerid": user.id,
                "token": CommonData.accesToken()
            ]
        case .getMeetById(let eventId):
            return [
                "method": "getMeetByID",
                "id": eventId,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .getMeetsById(let events):
            return [
                "method": "getMeetsByID",
                "id": events,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .joinMeetById(let eventId):
            return [
                "method": "joinMeetByID",
                "id": eventId,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .getMeetsSubs(let start):
            return [
                "method": "getMeetsSubs",
                "start": start,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .editMeetByID(let event, let initialEventId):
            return [
                "id": initialEventId,
                "method": "editMeetByID",
                "title": event.title,
                "description": event.descriptionEvent,
                "date_begin": event.startDate,
                "date_end": event.startDate,
                "place": event.place,
                "place_desc": event.placeDescription,
                "num_part_max": event.numParticipantsMax,
                "sex_part": event.sexParticipants,
                "add_after_begin": 0,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
//        case .editMeetPartRel(let action, let eventId, let userId):
//            return [
//                "method": "editMeetPartRel",
//                "id": eventId,
//                "action": action.rawValue,
//                "userid": userId,
//                "callerid": CommonData.selfUser().id,
//                "token": CommonData.accesToken()
//            ]
        case .leaveMeet(let eventId):
            return [
                "method": "leaveMeet",
                "id": eventId,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .delMeetByID(let eventId):
            return [
                "method": "delMeetByID",
                "id": eventId,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .getMeetsGone:
            return [
                "method": "getMeetsGone",
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        }
        
    }
}

