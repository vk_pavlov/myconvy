//
//  NetworkUserDataFetcher.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 04/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import SwiftyJSON


/*
 let utf8Text = String(data: data!, encoding: .utf8)
 print("Data: \(String(describing: utf8Text))")
 */

protocol UserDataFetcher {
    func getUserById(userId: Int, response: @escaping (SingleUserResponse?, ResultError?) -> Void)
    func editUser(profileViewModel: EditProfileViewModel, response: @escaping (SuccessResponse?, Error?) -> Void)
    func getUsersById(usersIds: [Int], response: @escaping (UserResponse?, Error?) -> Void)
    func editUserRel(action: RelationAction, userId: Int, response: @escaping (SuccessResponse?, Error?) -> Void)
    func deleteUser(response: @escaping (SuccessResponse?, Error?) -> Void)
    func editUserPrivacy(phonePrivacy: Int, socPrivacy: Int, locPrivacy: Int, response: @escaping (SuccessResponse?, Error?) -> Void)
}

class NetworkUserDataFetcher: NetworkDataFetcher, UserDataFetcher {
    func editUserPrivacy(phonePrivacy: Int, socPrivacy: Int, locPrivacy: Int, response: @escaping (SuccessResponse?, Error?) -> Void) {
        let params: Params = [
            "method": APITwo.editUserPrivacy,
            "phone_privacy": phonePrivacy,
            "soc_privacy": socPrivacy,
            "loc_privacy": locPrivacy
        ]
        
        networkService.request(params: params) { (data, error) in
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: StatusOperationResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    func deleteUser(response: @escaping (SuccessResponse?, Error?) -> Void) {
        let params: Params = [
            "method": APITwo.delUser
        ]
        
        networkService.request(params: params) { (data, error) in
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: StatusOperationResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    
    func editUserRel(action: RelationAction, userId: Int, response: @escaping (SuccessResponse?, Error?) -> Void) {
        let params: Params = [
            "id": userId,
            "action": action.rawValue,
            "method": APITwo.editUserRel
        ]
        
        networkService.request(params: params) { (data, error) in
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: StatusOperationResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    func getUsersById(usersIds: [Int], response: @escaping (UserResponse?, Error?) -> Void) {
        let params: Params = [
            "id": usersIds,
            "method": APITwo.getUsersById
        ]
        networkService.request(params: params) { (data, error) in
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: UserResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    func editUser(profileViewModel: EditProfileViewModel, response: @escaping (SuccessResponse?, Error?) -> Void) {

        let params: Params = [
            "name": profileViewModel.name,
            "method": APITwo.editUser,
            "lastname": profileViewModel.lastName,
            "birthdate": profileViewModel.birthdate,
            "city": profileViewModel.city,
            "univ": profileViewModel.university,
            "categories": profileViewModel.interest,
            "sex": profileViewModel.gender,
            "relations": profileViewModel.relations,
            
            "cont_phone": profileViewModel.contactInfo[0].value,
            "cont_vk": profileViewModel.contactInfo[1].value,
            "cont_fb": profileViewModel.contactInfo[2].value,
            "cont_inst": profileViewModel.contactInfo[3].value,
            "cont_email": profileViewModel.contactInfo[4].value,
    
            "status": profileViewModel.aboutSelf,
        ]
        
        networkService.request(params: params) { (data, error) in
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: StatusOperationResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
        
        
    }
    
    func getUserById(userId: Int, response: @escaping (SingleUserResponse?, ResultError?) -> Void) {
        let params: Params = [
            "id": userId,
            "method": APITwo.getUserById
        ]
        networkService.request(params: params) { (data, error) in
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            if let decoded = self.decodeJSON(type: SingleUserResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }

}
