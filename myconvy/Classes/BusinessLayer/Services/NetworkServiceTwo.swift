//
//  NetworkServiceTwo.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 04/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Alamofire
import Foundation

protocol Networking {
    func request(params: Params, completion: @escaping (Data?, Error?) -> Void)
}

struct Request: URLRequestConvertible {
    
    var requestTimeout: TimeInterval {
        return 20
    }
    
    var headers: [String: String] {
        let headers = [String: String]()
        return headers
    }
    
    var params: Params?
    
    
    
    var method: Alamofire.HTTPMethod = .get
    
    func asURLRequest() throws -> URLRequest {
        let url = try (baseUrlMV).asURL()
        var urlRequest = Alamofire.URLRequest(url: url)
        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        urlRequest.timeoutInterval = requestTimeout
        urlRequest.httpMethod = method.rawValue
          
        for header in headers {
            urlRequest.setValue(header.1, forHTTPHeaderField: header.0)
        }
        
          switch method {
          case .get:
              return try Alamofire.URLEncoding.default.encode(urlRequest, with: params?.safe())
          case .post:
              return try Alamofire.JSONEncoding.default.encode(urlRequest, with: params?.safe())
          default:
              return urlRequest
          }
    }
}

class NetworkServiceTwo: Networking {
    
    func request(params: Params, completion: @escaping (Data?, Error?) -> Void) {
        var allParams = params
        allParams["callerid"] = CommonData.selfUser().id
        allParams["token"] = CommonData.accesToken()
        var request = Request()
        request.params = allParams
        
        Alamofire.request(request).responseData { response in
            switch response.result {
            case .success(let data):
               completion(data, nil)
           case .failure(let error):
               completion(nil, error)
           }
       }
    }
    
}
