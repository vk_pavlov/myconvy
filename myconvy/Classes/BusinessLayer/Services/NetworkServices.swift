//
//  NetworkServices.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 04/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import SwiftyJSON
import RealmSwift
import Locksmith


protocol NetworkServiceProtocol  {
    func getMyconvyToken(token: String, vkId: String, reghash: String, pushToken: String, result: @escaping (Result<User>) -> Void)
    func createMeet(event: Event, result: @escaping (Result<String>) -> Void)
    func getMeets(user: User, start: Int, place: Int, followingOnly: Bool, result: @escaping (Result<[Event]>) -> Void)
    func getMeetById(eventId: String, result: @escaping (Result<Event>) -> Void)
    func editUser(user: User, result: @escaping (Result<Int>) -> Void)
    func getUserById(id: Int, result: @escaping (Result<User>) -> Void)
    func getMeetsById(events: String, result: @escaping (Result<[Event]>) -> Void)
    func getUsersById(idUsers: String, result: @escaping (Result<[User]>) -> Void)
    
    
    func editUserPrivacy(phonePrivacy: String, socPrivacy: String, result: @escaping (Result<Int>) -> Void)
    func editUserRel(action: String, idUser: String, result: @escaping (Result<Int>) -> Void)
    
    func joinMeetById(eventId: String, result: @escaping (Result<Int>) -> Void)

    func editMeetById(event: Event, initialEventId: String, result: @escaping (Result<Int>) -> Void)
    //func editMeetPartRel(action: ActionForEditMeetRel, eventId: String, userId: String, result: @escaping (Result<Int>) -> Void)

    func leaveMeet(eventId: String, result: @escaping (Result<Int>) -> Void)
    

    func delMeetByID(eventId: String, result: @escaping (Result<Int>) -> Void)

    func sendFeedback(message: String, result: @escaping (Result<Int>) -> Void)
    
    func delUser(result: @escaping (Result<Int>) -> Void)

    func sendReport(userId: String?, eventId: String?, message: String, result: @escaping (Result<Int>) -> Void)
    func uploadImage(img: UIImage, result: @escaping (Result<Int>) -> Void)

    func getMeetsGone(result: @escaping (Result<[Event]>) -> Void)

}

// MARK: - NetworkServiceProtocol

class NetworkService: NetworkServiceProtocol {
    func getMeetsGone(result: @escaping (Result<[Event]>) -> Void) {
        let request = API(request: EventRequest.getMeetsGone)
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)

                if let isError = json["result"]["error"].dictionary {
                    let errorCode = isError["code"]!.intValue
                    if let error = NetworkError.Common.init(rawValue: errorCode) {
                        result(.failure(error))
                    }
                } else {
                    let events = Event.getEvents(eventsArray: json)
                    result(.success(events))
                }
            case .failure(let error):
                result(.failure(error))
            }
        }
    }

    func sendReport(userId: String?, eventId: String?, message: String, result: @escaping (Result<Int>) -> Void) {
        let request = API(request: FeedbackRequest.sendReport(userId: userId, eventId: eventId, message: message))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                if let output = json["result"]["success"].int {
                    if output == 1 {
                        result(.success(output))
                    } else {
                        result(.failure(output as! Error))
                    }
                }
            case .failure(let error):
                result(.failure(error))
            }
        }
    }

    
    func delUser(result: @escaping (Result<Int>) -> Void) {
        let request = API(request: UserRequest.delUser())
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                if let output = json["result"]["success"].int {
                    if output == 1 {
                        result(.success(output))
                    } else {
                        result(.failure(output as! Error))
                    }
                }
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
    
    func sendFeedback(message: String, result: @escaping (Result<Int>) -> Void) {
        let request = API(request: FeedbackRequest.sendFeedback(message: message))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                if let output = json["result"]["success"].int {
                    result(.success(output))
                }
            case .failure(let error):
                result(.failure(error))
            }
        }
    }

    func delMeetByID(eventId: String, result: @escaping (Result<Int>) -> Void) {
        let request = API(request: EventRequest.delMeetByID(eventId: eventId))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                if let output = json["result"]["success"].int {
                    result(.success(output))
                }
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
    
    func leaveMeet(eventId: String, result: @escaping (Result<Int>) -> Void) {
        let request = API(request: EventRequest.leaveMeet(eventId: eventId))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                let output = json["result"]["success"].intValue
                result(.success(output))
            case .failure(let error):
                result(.failure(error))
            }
        }
    }

//    func editMeetPartRel(action: ActionForEditMeetRel, eventId: String, userId: String, result: @escaping (Result<Int>) -> Void) {
//        let request = API(request: EventRequest.editMeetPartRel(action: action, eventId: eventId, userId: userId))
//        Alamofire.request(request).responseJSON { response in
//            switch response.result {
//            case .success(let object):
//                let json = JSON(object)
//                let output = json["result"]["success"].intValue
//                result(.success(output))
//            case .failure(let error):
//                result(.failure(error))
//            }
//        }
//    }

    func editMeetById(event: Event, initialEventId: String, result: @escaping (Result<Int>) -> Void) {
        let request = API(request: EventRequest.editMeetByID(event: event, initialEventId: initialEventId))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                let output = json["result"]["success"].intValue
                result(.success(output))
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
    
    func joinMeetById(eventId: String, result: @escaping (Result<Int>) -> Void) {
        let request = API(request: EventRequest.joinMeetById(eventId: eventId))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                if let isError = json["result"]["error"].dictionary {
                    let errorCode = isError["code"]!.intValue
                    if let error = NetworkError.JoinMeetById.init(rawValue: errorCode) {
                        result(.failure(error))
                    }
                } else {
                    result(.success(1))
                }
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
    
    
    func editUserRel(action: String, idUser: String, result: @escaping (Result<Int>) -> Void) {
        let request = API(request: UserRequest.editUserRel(action: action, idUser: idUser))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                if let isError = json["result"]["error"].dictionary {
                    let errorCode = isError["code"]!.intValue
                    if let error = NetworkError.EditUserRel.init(rawValue: errorCode) {
                        result(.failure(error))
                    }
                } else {
                    result(.success(1))
                }
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
    
    func editUserPrivacy(phonePrivacy: String, socPrivacy: String, result: @escaping (Result<Int>) -> Void) {
        let request = API(request: UserRequest.editUserPrivacy(phonePrivacy: phonePrivacy, socPrivacy: socPrivacy))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                let output = json["result"]["success"].intValue
                result(.success(output))
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
    
    
    func getUsersById(idUsers: String, result: @escaping (Result<[User]>) -> Void) {
        let request = API(request: UserRequest.getUsersById(idUsers: idUsers))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let users = User.getUsersFromJSON(usersArray: JSON(object))
                result(.success(users))
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
    
    
    func getMeetsById(events: String, result: @escaping (Result<[Event]>) -> Void) {
        let request = API(request: EventRequest.getMeetsById(events: events))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                let events = Event.getEvents(eventsArray: json)
                result(.success(events))
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
    
    
    func getUserById(id: Int, result: @escaping (Result<User>) -> Void) {
        let request = API(request: UserRequest.getUserById(id: id))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {

            Alamofire.request(request).responseJSON { response in
                switch response.result {
                case .success(let object):
                    let json = JSON(object)


                    if let isError = json["result"]["error"].dictionary {
                        let errorCode = isError["code"]!.intValue
                        if let error = NetworkError.GetUserOrEventById.init(rawValue: errorCode) {
                            result(.failure(error))
                        }
                    } else {
                        let user = User.getUser(json: json)
                        result(.success(user))
                    }

                case .failure(let error):
                    result(.failure(error))
                }
            }
        }
    }
    
    
    func editUser(user: User, result: @escaping (Result<Int>) -> Void) {
        let request = API(request: UserRequest.editUser(user: user))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                let output = json["result"]["success"].intValue
                result(.success(output))
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
    
    
    func getMeetById(eventId: String, result: @escaping (Result<Event>) -> Void) {
        let request = API(request: EventRequest.getMeetById(eventId: eventId))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            Alamofire.request(request).responseJSON { response in
                switch response.result {
                case .success(let object):
                    let json = JSON(object)

                    if let isError = json["result"]["error"].dictionary {
                        let errorCode = isError["code"]!.intValue
                        if let error = NetworkError.GetUserOrEventById.init(rawValue: errorCode) {
                            result(.failure(error))
                        }
                    } else {
                        let selectedEvent = Event.getEventById(event: json["result"]["meetinfo"])
                        result(.success(selectedEvent))
                    }
                case .failure(let error):
                    result(.failure(error))
                }
            }
        }
    }
    
    func getMeets(user: User, start: Int, place: Int, followingOnly: Bool, result: @escaping (Result<[Event]>) -> Void) {
        let request = API(request: EventRequest.getMeets(user: user, start: start, place: place, followingOnly: followingOnly))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let json = JSON(object)
                let events = Event.getEvents(eventsArray: json)
                result(.success(events))
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
    
    
   
    
    func getMyconvyToken(token: String, vkId: String, reghash: String, pushToken: String, result: @escaping (Result<User>) -> Void) {
        let request = API(request: AuthRequest.getMyconvyToken(token: token, vkId: vkId, reghash: reghash, pushToken: pushToken))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let uiRealm = try! Realm()
                let json = JSON(object)
                let user = User.getSelfUserWithData(data: json)
                UserDefaults.standard.set(true, forKey: "isAuthorized")
                do {
                    try Locksmith.updateData(data: ["myconvy_token": json["result"]["token"].stringValue], forUserAccount: "myconvy")
                } catch {
                    print(error)
                }
                
                try! uiRealm.write {
                    uiRealm.add(user)
                }
                result(.success(user))
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
    
    
    func createMeet(event: Event, result: @escaping (Result<String>) -> Void) {
        let request = API(request: EventRequest.createMeet(event: event))
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let object):
                let createdEvent = JSON(object)
                let createdEventId = createdEvent["result"]["meetinfo"]["id"].stringValue
                result(.success(createdEventId))
            case .failure(let error):
                result(.failure(error))
            }
        }
    }

    func uploadImage(img: UIImage, result: @escaping (Result<Int>) -> Void) {

        var image = img
        var data = image.pngData()!

        while data.count > 3000000 {
            data = image.jpeg(.high)!
            image = UIImage(data:data, scale:1.0)!
        }

        upload(multipartFormData: { multipartFormData in
            multipartFormData.append(data, withName: "photo", fileName: "photo.jpg", mimeType: "image/jpeg")
        }, to:  baseUrlMV + "?callerid=\(CommonData.selfUser().id)&method=editUserAvatar&token=\(CommonData.accesToken())", encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let uploadRequest, let streamingFromDisk, let streamFileURL):
                print(uploadRequest)
                print(streamingFromDisk)
                print(streamFileURL ?? "streamFileURL is NIL")

                uploadRequest.validate().responseJSON() { responseJSON in
                    switch responseJSON.result {
                    case .success(let object):
                        let json = JSON(object)
                        if let isError = json["result"]["error"].dictionary {
                            let errorCode = isError["code"]!.intValue
                            if let error = NetworkError.EditUserRel.init(rawValue: errorCode) {
                                result(.failure(error))
                            }
                        } else {
                            result(.success(1))
                        }

                    case .failure(let error):
                        result(.failure(error))
                    }
                }

            case .failure(let error):
                result(.failure(error))
            }
        })
    }
    
    
}


