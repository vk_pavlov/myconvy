//
//  APITwo.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 04/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct APITwo {
    static let sheme = "http"
    static let host = "185.154.52.97"
    static let getMeets = "getMeets"
    static let path = "/core/api.php"
    static let getUserById = "getUserByID"
    static let getMeetByID = "getMeetByID"
    static let createMeet = "createMeet"
    static let editUser = "editUser"
    static let getMeetsByID = "getMeetsByID"
    static let getUsersById = "getUsersByID"
    static let editUserRel = "editUserRel"
    static let joinMeetByID = "joinMeetByID"
    static let editMeetByID = "editMeetByID"
    static let delUser = "delUser"
    static let sendReport = "sendReport"
    static let leaveMeet = "leaveMeet"
    static let delMeetByID = "delMeetByID"
    static let editMeetPartRel = "editMeetPartRel"
    static let editUserPrivacy = "editUserPrivacy"
    static let getMeetsGone = "getMeetsGone"
}
