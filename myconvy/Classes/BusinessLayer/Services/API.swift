//
//  API.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 04/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import Alamofire

typealias Params = [String: Any?]


#if PROD_TARGET
    let basePathSocket = "ws://185.154.52.97:8000/"
    let baseUrlMV =  "https://injoinapp.com/core/api.php"
#else
    let basePathSocket = "ws://185.154.52.97:8001/"
    let baseUrlMV =  "https://injoin.ga/core/api.php"
#endif

let fullPathToSocket = "\(basePathSocket)token=\(CommonData.accesToken())&callerid=\(CommonData.selfUser().id)"

struct API {
    
    let request: APIMethodProtocol
    
    var baseUrl: String {
        return baseUrlMV
    }
    
    var requestTimeout: TimeInterval {
        return 20
    }
    
    var headers: [String: String] {
        let headers = [String: String]()
        return headers
    }
    
    init(request: APIMethodProtocol) {
        self.request = request
    }
}


protocol APIMethodProtocol {
    var method: Alamofire.HTTPMethod { get }
    var path: String { get }
    var params: Params? { get }
}


extension API: URLRequestConvertible {
    
    func asURLRequest() throws -> URLRequest {
        let url = try (baseUrl + request.path).asURL()
        
        var urlRequest = Alamofire.URLRequest(url: url)
        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        urlRequest.timeoutInterval = requestTimeout
        urlRequest.httpMethod = request.method.rawValue
        
        for header in headers {
            urlRequest.setValue(header.1, forHTTPHeaderField: header.0)
        }
        
        switch request.method {
        case .get:
            return try Alamofire.URLEncoding.default.encode(urlRequest, with: request.params?.safe())
        case .post:
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: request.params?.safe())
        default:
            return urlRequest
        }
    }
    
}

// MARK: - Removing all key-value pairs with nil value
extension Dictionary where Key == String, Value == Any? {
    
    func safe() -> [String: Any] {
        var dict = [String: Any]()
        for (key, value) in self {
            if let value = value {
                dict[key] = value
            }
        }
        return dict
    }
    
}
