//
//  AuthRequest.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 04/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import Alamofire


enum AuthRequest {
    case getMyconvyToken(token: String, vkId: String, reghash: String, pushToken: String)
}

extension AuthRequest: APIMethodProtocol {
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        return ""
    }
    
    var params: Params? {
        switch self {
        case .getMyconvyToken(let token, let vkId, let reghash, let pushToken):
            return [
                "method": "authorize2v",
                "vkid": vkId,
                "reghash": reghash,
                "vktoken": token,
                "push_id": pushToken
            ]
        }
        
    }
}
