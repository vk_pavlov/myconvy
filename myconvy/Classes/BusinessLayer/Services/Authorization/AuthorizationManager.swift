//
//  AuthorizationManager.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 29/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

protocol AuthorizationManagerDelegate: class {
    func shouldPresentAuthorizationController(_ controller: UIViewController)
    func accessAuthorizationFinished()
}

protocol AuthServiceDelegate: class {
    func authServiceShouldShow(_ viewController: UIViewController)
    func authServiceSignIn()
    func authServiceDidSignInFail()
}

final class AuthorizationManager {
     
    weak var delegate: AuthorizationManagerDelegate?
    
    var authService: AuthService? {
        didSet {
            authService?.delegate = self
            authService?.signUp()
        }
    }
    
    var paramsForAuthFromMV: [String: String]? {
        return authService?.params
    }
    
    func forceLogout() {
        VKAuthorization().forceLogout()
    }
    
    
}

extension AuthorizationManager: AuthServiceDelegate {
    func authServiceShouldShow(_ viewController: UIViewController) {
        delegate?.shouldPresentAuthorizationController(viewController)
    }
    
    func authServiceSignIn() {
        delegate?.accessAuthorizationFinished()
    }
    
    func authServiceDidSignInFail() {
        
    }
    
    
}

