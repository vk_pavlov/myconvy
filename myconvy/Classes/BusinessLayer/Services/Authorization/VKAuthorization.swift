//
//  VKAuthorization.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 28/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import VK_ios_sdk
import CryptoSwift


final class VKAuthorization: NSObject, AuthService, VKSdkDelegate, VKSdkUIDelegate {
    
    var params: [String : String]? {
        guard
            let token = token,
            let vkId = vkId
            else { return nil }
        let text = "mcnv1" + token
        let reghash = text.sha256()
        return [
            "token": token,
            "vkId": vkId,
            "reghash": reghash
        ]
    }
    
    #if PROD_TARGET
        private let appId = "6743260"
    #else
        private let appId = "6771452"
    #endif
    
    private let vkSdk: VKSdk

    var token: String? {
        return VKSdk.accessToken()?.accessToken
    }

    var vkId: String? {
        return VKSdk.accessToken()?.userId
    }

    weak var delegate: AuthServiceDelegate?
    
    func signUp() {
        let scope = ["offline"]
        VKSdk.wakeUpSession(scope) { [delegate] (state, error) in
            if state == .authorized {
                print("VKauthorizationState.authorized")
                delegate?.authServiceSignIn()
            } else if state == .initialized {
                print("VKauthorizationState.initialized")
                VKSdk.authorize(scope)
            } else {
                print("auth problems, state \(state) error \(String(describing: error))")
                delegate?.authServiceDidSignInFail()
            }
        }
    }
    
    

    override init() {
        vkSdk = VKSdk.initialize(withAppId: appId)
        super.init()
        vkSdk.register(self)
        vkSdk.uiDelegate = self
    }

    func forceLogout() {
        VKSdk.forceLogout()
    }

    // MARK: - VKSdkDelegate

    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        if result.token != nil {
            self.delegate?.authServiceSignIn()
        }
    }

    func vkSdkUserAuthorizationFailed() {
        
    }

    // MARK: - VKSdkUiDelegate

    func vkSdkShouldPresent(_ controller: UIViewController!) {
        if token != nil {
             self.delegate?.authServiceSignIn()
        } else {
            self.delegate?.authServiceShouldShow(controller)
        }
    }

    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
    }


}
