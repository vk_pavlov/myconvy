//
//  NotificationsRequest.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 02/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//


import Foundation
import Alamofire


enum NotificationsRequest {
    case getNotsOld(start: String, onlynew: String, quiet: String)
}

extension NotificationsRequest: APIMethodProtocol {
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        return ""
    }
    
    var params: Params? {
        switch self {
        case .getNotsOld(let start, let onlynew, let quiet):
            return [
                "method": "getNotsOld",
                "start": start,
                "onlynew": onlynew,
                "quiet": quiet,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        }
        
        
    }
}
