//
//  NetworkDataFetcher.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 04/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

protocol EventDataFetcher {
    func getMeets(start: Int, place: Int, search: String, followingOnly: Bool, response: @escaping (FeedResponse?, ResultError?) -> Void)
    func getMeetsGone(start: Int, place: Int, search: String, followingOnly: Bool, response: @escaping (FeedResponse?, ResultError?) -> Void)
    func getMeetByID(eventId: Int, response: @escaping (SelectedEventResponse?, ResultError?) -> Void)
    func createMeet(event: NewEventViewModel, response: @escaping (SelectedEventResponse?, Error?) -> Void)
    func getMeetsByID(eventIds: [Int], response: @escaping (FeedResponse?, Error?) -> Void)
    func joinMeetByID(eventId: Int, response: @escaping (SuccessResponse?, Error?) -> Void)
    func editMeetByID(eventId: Int, eventViewModel: NewEventViewModel, response: @escaping (SuccessResponse?, Error?) -> Void)
    func sendReport(eventId: Int, action: ReportAction, response: @escaping (SuccessResponse?, Error?) -> Void)
    func deleteMeetByID(eventId: Int, response: @escaping (SuccessResponse?, Error?) -> Void)
    func leaveMeet(eventId: Int, response: @escaping (SuccessResponse?, Error?) -> Void)
    func editMeetPartRelation(eventId: Int, userId: Int, action: EditMeetRelAction, response: @escaping (SuccessResponse?, Error?) -> Void)
}

class NetworkEventDataFetcher: NetworkDataFetcher, EventDataFetcher {
    func getMeetsGone(start: Int, place: Int, search: String, followingOnly: Bool, response: @escaping (FeedResponse?, ResultError?) -> Void) {
        let params: Params = [
                   "method": APITwo.getMeetsGone,
                   "start": start,
                   "place": place,
                   "search": search,
                   "following_only": followingOnly
               ]
               
               networkService.request(params: params) { (data, error) in
                   if let error = error {
                        print("Error received requesting data: \(error.localizedDescription)")
                        response(nil, nil)
                   }
                   
                   if let decoded = self.decodeJSON(type: FeedResponseWrapped.self, from: data) {
                       response(decoded.result, nil)
                       return
                   }
                   let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
                   response(nil, decodedError?.result.error)
               }
    }
    
    func editMeetPartRelation(eventId: Int, userId: Int, action: EditMeetRelAction, response: @escaping (SuccessResponse?, Error?) -> Void) {
        let params: Params = [
            "id": eventId,
            "method": APITwo.editMeetPartRel,
            "action": action.rawValue,
            "userid": userId
        ]
        
        networkService.request(params: params) { (data, error) in
            
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: StatusOperationResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    func deleteMeetByID(eventId: Int, response: @escaping (SuccessResponse?, Error?) -> Void) {
        let params: Params = [
            "id": eventId,
            "method": APITwo.delMeetByID,
        ]
        
        networkService.request(params: params) { (data, error) in
            
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: StatusOperationResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    func leaveMeet(eventId: Int, response: @escaping (SuccessResponse?, Error?) -> Void) {
        let params: Params = [
            "id": eventId,
            "method": APITwo.leaveMeet
        ]
        
        networkService.request(params: params) { (data, error) in
            
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: StatusOperationResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    func sendReport(eventId: Int, action: ReportAction, response: @escaping (SuccessResponse?, Error?) -> Void) {
        let params: Params = [
            "meetid": eventId,
            "method": APITwo.sendReport,
            "message": action.rawValue
        ]
        
        networkService.request(params: params) { (data, error) in

            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: StatusOperationResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    func editMeetByID(eventId: Int, eventViewModel: NewEventViewModel, response: @escaping (SuccessResponse?, Error?) -> Void) {
        let params: Params = [
            "id": eventId,
            "method": APITwo.editMeetByID,
            "title": eventViewModel.title,
            "place": "\(eventViewModel.place.lat),\(eventViewModel.place.lng)",
            "start_date": eventViewModel.date,
            "description": eventViewModel.descriptionEvent,
            "pl_desc": eventViewModel.descriptionPlace,
            "part_limit": eventViewModel.partLimit,
            "part_sex": eventViewModel.sex,
            "category": eventViewModel.category,
            "privacy": eventViewModel.privacy
        ]
        
        networkService.request(params: params) { (data, error) in
            
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: StatusOperationResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
        
    }
    
    
    func joinMeetByID(eventId: Int, response: @escaping (SuccessResponse?, Error?) -> Void) {
        let params: Params = [
            "id": eventId,
            "method": APITwo.joinMeetByID
        ]
        
        networkService.request(params: params) { (data, error) in
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: StatusOperationResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    func getMeetsByID(eventIds: [Int], response: @escaping (FeedResponse?, Error?) -> Void) {
        let params: Params = [
            "method": APITwo.getMeetsByID,
            "id": eventIds
        ]
        
        networkService.request(params: params) { (data, error) in
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: FeedResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    func createMeet(event: NewEventViewModel, response: @escaping (SelectedEventResponse?, Error?) -> Void) {
        let params: Params = [
            "method": APITwo.createMeet,
            "title": event.title,
            "place": "\(event.place.lat),\(event.place.lng)",
            "start_date": event.date,
            "description": event.descriptionEvent,
            "pl_desc": event.descriptionPlace,
            "part_limit": event.partLimit,
            "part_sex": event.sex,
            "category": event.category,
            "privacy": event.privacy
        ]
        
        networkService.request(params: params) { (data, error) in
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, error)
                 return
            }
            
            if let decoded = self.decodeJSON(type: SelectedEventResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    func getMeetByID(eventId: Int, response: @escaping (SelectedEventResponse?, ResultError?) -> Void) {
        let params: Params = [
            "method": APITwo.getMeetByID,
            "id": eventId
        ]
        
        networkService.request(params: params) { (data, error) in
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: SelectedEventResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
    
    func getMeets(start: Int, place: Int, search: String, followingOnly: Bool, response: @escaping (FeedResponse?, ResultError?) -> Void) {
        
        let params: Params = [
            "method": APITwo.getMeets,
            "start": start,
            "place": place,
            "search": search,
            "following_only": followingOnly
        ]
        
        networkService.request(params: params) { (data, error) in
            if let error = error {
                 print("Error received requesting data: \(error.localizedDescription)")
                 response(nil, nil)
            }
            
            if let decoded = self.decodeJSON(type: FeedResponseWrapped.self, from: data) {
                response(decoded.result, nil)
                return
            }
            let decodedError = self.decodeJSON(type: ErrorResponseWrapped.self, from: data)
            response(nil, decodedError?.result.error)
        }
    }
    
}
