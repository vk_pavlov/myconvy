//
//  StubsFactory.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/02/2020.
//  Copyright © 2020 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

enum Stub {
    case noInternet((() -> ()))
    case noEvents(UIViewController)
    case noEventsSubscribtions(UIViewController)
    case noSubscribers
    case noSubscriptions
    case noMessages
}

protocol StubView {
    var title: String { get set }
    var subtitle: String? { get set }
    var icon: String { get set }
    var action: (() -> ())? { get set }
    var buttonTitle: String? { get set }
}

struct StubsFactory {
    
    static func makeStub(_ stub: Stub) -> CommonStubView {
        switch stub {
        case .noSubscribers: return CommonStubView(title: "В данный момент у вас нет подписчиков.",
                                                   subtitle: nil,
                                                   icon: "no_subscribers_stub",
                                                   buttonTitle: nil,
                                                   action: nil)
        case .noSubscriptions: return CommonStubView(title: "В данный момент у вас нет подписок.",
                                                     subtitle: nil,
                                                     icon: "no_subscribers_stub",
                                                     buttonTitle: nil,
                                                     action: nil)
        case .noInternet(let action): return CommonStubView(title: "Интернет соединение отсутствует.",
                                                subtitle: "Попробуйте обновить страницу",
                                                icon: "no_internet_stub",
                                                buttonTitle: "Обновить",
                                                action: action)

        case .noEvents(let viewController): return CommonStubView(title: "В данный момент событий нет.",
                                                                  subtitle: "К сожалению, на данный момент событий нет. Будь первым, создай свое!",
                                                                  icon: "no_events_stub",
                                                                  buttonTitle: "Создать событие") {
                                                                      NewEventModule.create().present(from: viewController)
                                                                  }
        case .noEventsSubscribtions(let viewController): return CommonStubView(title: "В данный момент событий нет.",
                                                                               subtitle: "Подписывайтесь на интересных людей, чтобы увидеть их события. Они будут отображаться здесь.",
                                                                               icon: "no_events_stub",
                                                                               buttonTitle: "Создать событие") {
                                                                                   NewEventModule.create().present(from: viewController)
                                                                               }
        case .noMessages: return CommonStubView(title: "В данный момент сообщений нет.",
                                                subtitle: "Добавляйтесь в события, чтобы увидеть чаты по ним. Они появятся здесь, как только ваши отклики будут приняты.",
                                                icon: "no_messages_stub",
                                                buttonTitle: nil,
                                                action: nil)
        }
    }
}
