//
//  UserRequest.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 12/02/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import Alamofire


enum UserRequest {
    case editUser(user: User)
    case getUserById(id: Int)
    case getUsersById(idUsers: String)
    case editUserPrivacy(phonePrivacy: String, socPrivacy: String)
    case editUserRel(action: String, idUser: String)
    case delUser()
    case editUserAvatar(photo: UIImage)
}

extension UserRequest: APIMethodProtocol {
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        return ""
    }
    
    var params: Params? {
        switch self {
        case .editUser(let user):
            return [
                "method": "editUser",
                "callerid": user.id,
                "name": user.name,
                "lastname": user.lastName,
                "birthdate": user.birthDate,
                "city": user.city,
                "sex": user.sex,
                "relations": user.relations,
                "email": user.email,
                "phone": user.phone,
                "vk_link": user.vkLink,
                "fb_link": user.fbLink,
                "inst_link": user.instLink,
                "status": user.status,
                "token": CommonData.accesToken()
            ]
        case .getUserById(let id):
            return [
                "method": "getUserById",
                "id": id,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .getUsersById(let idUsers):
            return [
                "method": "getUsersByID",
                "id": idUsers,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .editUserPrivacy(let phonePrivacy, let socPrivacy):
            return [
                "method": "editUserPrivacy",
                "phone_privacy": phonePrivacy,
                "soc_privacy": socPrivacy,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .editUserRel(let action, let idUser):
            return [
                "method": "editUserRel",
                "id": idUser,
                "action": action,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .delUser:
            return [
                "method": "delUser",
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .editUserAvatar(let photo):
            return [
                "method": "editUserAvatar",
                "photo": photo,
                "token": CommonData.accesToken()
            ]
        }
        
        
    }
}
