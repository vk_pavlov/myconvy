//
//  FeedbackRequest.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

import Foundation
import Alamofire


enum FeedbackRequest {
    case sendFeedback(message: String)
    case sendReport(userId: String?, eventId: String?, message: String)
}

extension FeedbackRequest: APIMethodProtocol {
    var method: HTTPMethod {
        return .get
    }

    var path: String {
        return ""
    }

    var params: Params? {
        switch self {

        case .sendFeedback(let message):
            return [
                "method": "sendFeedback",
                "message": message,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        case .sendReport(let userId, let eventId, let message):
            return [
                "method": "sendReport",
                "userid": userId ?? "",
                "meetid": eventId ?? "",
                "message": message,
                "callerid": CommonData.selfUser().id,
                "token": CommonData.accesToken()
            ]
        }


    }
}
