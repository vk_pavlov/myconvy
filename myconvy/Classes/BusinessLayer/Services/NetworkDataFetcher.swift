//
//  NetworkDataFetcher.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 04/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

class NetworkDataFetcher {

    let networkService: Networking!

    init(networkService: Networking = NetworkServiceTwo()) {
        self.networkService = networkService
    }
    
    func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        guard
            let data = from
        else { return nil }
        do {
            let response = try decoder.decode(T.self, from: data)
            return response
        } catch {
            print("Decoding error - \(error)")
        }
        return nil
    }

}
