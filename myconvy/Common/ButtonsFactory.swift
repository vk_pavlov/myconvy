//
//  ButtonsFactory.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 17/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

struct ButtonsFactory {
    
    static func backButton() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "arrow_left")!, for: .normal)
        return button
    }
    
    
    
    
}
