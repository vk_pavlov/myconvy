//
//  FeedCategoriesCollectionViewCell.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class CategoryWithShadowCollectionViewCell: UICollectionViewCell {
    
    class var reuseId: String {
        return "CategoryWithShadowCollectionViewCell"
    }
    
    let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
        label.textColor = UIColor.black.withAlphaComponent(0.5)
        return label
    }()
    
    var title: String? {
        didSet {
            label.text = title
            invalidateIntrinsicContentSize()
        }
    }
    
    var isSelectedCategory: Bool = false {
        didSet {
            configureCell()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        confugure()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        confugure()
    }
    
    private func confugure() {
        addSubview(label)
        backgroundColor = .white
        translatesAutoresizingMaskIntoConstraints = false
        clipsToBounds = false
        
        label.anchor(top: topAnchor,
                     leading: leadingAnchor,
                     bottom: bottomAnchor,
                     trailing: trailingAnchor,
                     padding: UIEdgeInsets(top: 9, left: 24, bottom: 9, right: 24))
        configureCell()
        
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.cancelsTouchesInView = false
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    private func configureCell() {
        if isSelectedCategory {
            backgroundColor = OrangeTheme.myconvyOrange.withAlphaComponent(0.2)
            layer.shadowRadius = 0
            layer.shadowOpacity = 0
            layer.shadowOffset = CGSize(width: 0, height: 0)
        } else {
            backgroundColor = .white
            layer.shadowRadius = 6
            layer.shadowOpacity = 0.1
            layer.shadowOffset = CGSize(width: 0, height: 3)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
        
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 5 + label.intrinsicContentSize.width + 5, height: CategoriesCollectionView.Metrics.galleryItemHeight)
    }
    
}
