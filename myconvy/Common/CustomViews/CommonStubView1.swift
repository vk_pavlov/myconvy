//
//  CommonStub.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 10/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


//
//enum Stubs {
//    case search
//    case notification
//    case subscribers
//    case subscriptions
//    case profileUser
//    case mySubscribers
//    case mySubscriptions
//    case myCreatedEvent
//    case myResponsesEvent
//
//    struct Stub {
//        let title: String
//        let label: String
//    }
//
//    var values: Stub {
//        switch self {
//        case .search:
//            return Stub(title: "Нет актуальных событий", label: "Создай свое, будь первым(-ой)!")
//        case .notification:
//            return Stub(title: "Нет новых уведомлений за последнюю неделю.", label: "Создавайте интересные события и ждите новые отклики! Они появятся здесь.")
//        case .subscribers:
//            return Stub(title: "Список подписчиков пуст.", label: "Вы можете стать первым подписчиком пользователя.")
//        case .subscriptions:
//            return Stub(title: "Пользователь пока ни на кого не подписан.", label: "")
//        case .profileUser:
//            return Stub(title: "Пользователь пока не создал ни одного события.", label: "")
//        case .mySubscribers:
//            return Stub(title: "Список подписчиков пуст.", label: "Создавайте интересные события, чтобы привлечь к себе внимание!")
//        case .mySubscriptions:
//            return Stub(title: "Вы пока ни на кого не подписаны.", label: "Находите людей, создающих интересные события, в разделе «Поиск»!")
//        case .myCreatedEvent:
//            return Stub(title: "Вы пока не создали ни одного события. Самое время это сделать!", label: "")
//        case .myResponsesEvent:
//            return Stub(title: "Вы пока не откликнулись ни на одно событие.", label: "Находите интересные события в разделе «Поиск» или создавайте свои собственные!")
//        }
//    }
//}
//
//
//
//class CommonStubView: UIView {
//
//    @IBOutlet weak var contentView: UIView!
//    @IBOutlet weak var createEventButton: UIButton!
//    @IBOutlet weak var titleStub: UILabel!
//    @IBOutlet weak var labelStub: UILabel!
//
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//    }
//
//    init(frame: CGRect, stub: Stubs, vc: UIViewController, targetOnButton: Selector?) {
//        super.init(frame: frame)
//        commonInit(stub: stub, vc: vc, targetOnButton: targetOnButton)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//
//    func commonInit(stub: Stubs, vc: UIViewController, targetOnButton: Selector?) {
//        Bundle.main.loadNibNamed("CommonStub", owner: self, options: nil)
//
//        if let target = targetOnButton {
//            createEventButton.isHidden = false
//            createEventButton.addTarget(vc, action: target, for: .touchUpInside)
//        }
//        titleStub.text = stub.values.title
//        labelStub.text = stub.values.label
//
//        contentView.backgroundColor = UIColor.clear
//        createEventButton.setTitleColor(OrangeTheme.myconvyOrange, for: .normal)
//        contentView.fixInView(self)
//    }
//
//}
