//
//  NoInternetConnectionView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 09/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class NoInternetConnectionView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var repeatRequestButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        Bundle.main.loadNibNamed("NoInternetConnection", owner: self, options: nil)
        contentView.backgroundColor = OrangeTheme.mvGray
        repeatRequestButton.setTitleColor(OrangeTheme.myconvyOrange, for: .normal)
        contentView.fixInView(self)
    }
}

