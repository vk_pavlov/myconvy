//
//  CategoriesCollectionView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

protocol CategoriesCollectionViewDelegate: class {
    var isNotSelectedCategory: Bool { get }
}

class CategoriesCollectionView: UICollectionView, UnfieldItems {
    
    weak var someDelegate: CategoriesCollectionViewDelegate?
    
    func highlight() {
        backgroundColor = .red
        UIView.animate(withDuration: 0.5) {
            self.backgroundColor = .white
        }
    }
    
    var isEmpty: Bool {
        guard someDelegate != nil else { return true}
        return someDelegate!.isNotSelectedCategory
    }
    
    enum Metrics {
        static let galleryMinimalLineSpacing: CGFloat = 10
        static let leftDistanceToView: CGFloat = 15
        static let rightDistanceToView: CGFloat = 15
        static let galleryItemWidth: CGFloat = 140
        static let galleryItemHeight: CGFloat = 34
    }

    init() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.estimatedItemSize = CGSize(width: 1, height: 1);
        super.init(frame: .zero, collectionViewLayout: layout)
        
        register(CategoryWithShadowCollectionViewCell.self, forCellWithReuseIdentifier: CategoryWithShadowCollectionViewCell.reuseId)
        register(CategoryCollectionViewCell.self, forCellWithReuseIdentifier: CategoryCollectionViewCell.reuseId)
        register(FilterCategoriesCollectionViewCell.self, forCellWithReuseIdentifier: FilterCategoriesCollectionViewCell.reuseId)
        backgroundColor = .white
        layout.minimumLineSpacing = Metrics.galleryMinimalLineSpacing
        contentInset = UIEdgeInsets(top: 0, left: Metrics.leftDistanceToView, bottom: 0, right: Metrics.rightDistanceToView)
        translatesAutoresizingMaskIntoConstraints = false
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
