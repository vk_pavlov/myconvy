//
//  DistanceView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

class DistanceView: UIView {
    
    var distanceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupConstraints()
    }
    
    private func setupConstraints() {
        backgroundColor = OrangeTheme.myconvyOrange
        layer.cornerRadius = 6
        addSubview(distanceLabel)
        
        distanceLabel.anchor(top: topAnchor,
                           leading: leadingAnchor,
                           bottom: bottomAnchor,
                           trailing: trailingAnchor,
                           padding: UIEdgeInsets(top: 5, left: 16, bottom: 5, right: 16))
    }
    
    var distance: Double? {
        get {
            return Double(distanceLabel.text!)
        }
        set {
            guard let newValue = newValue else { return }
            distanceLabel.text = newValue >= 1000 ? String(format: "~%.1f км", newValue / 1000) : "~\(String(Int(newValue))) м"
        }
    }
}
