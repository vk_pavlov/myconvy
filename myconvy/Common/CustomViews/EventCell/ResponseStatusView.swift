//
//  ResponseStatusView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

class ResponseStatusView: UIView {
    
    private enum Colors {
        static let participants = UIColor.hexStringToUIColor(hex: "6DD400")
        static let waitingApprove = UIColor.hexStringToUIColor(hex: "FCA54B")
        static let finished = UIColor.black.withAlphaComponent(0.5)
        static let orders = UIColor.red
    }
    
    private var statusIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private var statusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupConstraints()
    }
    
    private func setupConstraints() {
        
        addSubview(statusIcon)
        addSubview(statusLabel)
        
        statusLabel.anchor(top: topAnchor,
                           leading: nil,
                           bottom: bottomAnchor,
                           trailing: trailingAnchor,
                           padding: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        
        
        statusIcon.anchor(top: nil,
                          leading: nil,
                          bottom: nil,
                          trailing: statusLabel.leadingAnchor,
                          padding: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 7),
                          size: CGSize(width: 13, height: 13))
        
        statusIcon.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    func set(callerStatus: CallerStatus, eventStatus: EventStatus, isOrders: Bool) {
        
        statusIcon.isHidden = false
        statusLabel.isHidden = false
        statusIcon.backgroundColor = .clear
        
        if isOrders {
            statusIcon.image = nil
            statusIcon.backgroundColor = .red
            statusIcon.layer.cornerRadius = 6.5
            statusLabel.text = "Новые заявки"
            statusLabel.textColor = Colors.orders
            return
        }
        
        if eventStatus == .finished {
            statusIcon.image = UIImage(named: "ic_check_circle")!
            statusLabel.text = "Завершено"
            statusLabel.textColor = Colors.finished
            return
        }
        
        switch callerStatus {
        case .none:
            statusIcon.isHidden = true
            statusLabel.isHidden = true
        case .participants:
            statusIcon.image = UIImage(named: "ic_check_approved")!
            statusLabel.text = "Вы участник"
            statusLabel.textColor = Colors.participants
        case .waitingApprove:
            statusIcon.image = UIImage(named: "ic_watch_later")!
            statusLabel.text = "Отклик отправлен"
            statusLabel.textColor = Colors.waitingApprove
        case .blocked:
            break
        }
    }

}
