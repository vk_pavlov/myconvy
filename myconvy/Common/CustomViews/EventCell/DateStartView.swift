//
//  DateStartView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

class DateStartView: UIView {
    
    private enum Colors {
        static let today = UIColor.hexStringToUIColor(hex: "8ADC32")
        static let tomorrow = UIColor.hexStringToUIColor(hex: "FCA54B")
        static let other = UIColor.hexStringToUIColor(hex: "D8D8D8")
    }
    
    private var dateIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "Unread Badge")!
        return imageView
    }()
    
    private var dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()
    
    var date: Date? {
        didSet {
            updateDateFormat()
            invalidateIntrinsicContentSize()
        }
    }
    
    var dateTodayFormat = "HH:mm"
    var dateOtherFormat = "dd.MM.yyyy"
    var dateTomorrowFormat: String!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupConstraints()
    }
    
    private func setupConstraints() {
        addSubview(dateIcon)
        addSubview(dateLabel)
        
        dateIcon.heightAnchor.constraint(equalToConstant: 8).isActive = true
        dateIcon.widthAnchor.constraint(equalToConstant: 8).isActive = true
        dateIcon.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        dateIcon.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        dateLabel.anchor(top: topAnchor,
                           leading: dateIcon.trailingAnchor,
                           bottom: bottomAnchor,
                           trailing: nil,
                           padding: UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0))
    }
    
    private func updateDateFormat() {
        guard let date = date else { return }
        if Calendar.current.isDateInToday(date) {
            dateLabel.text = date.getDateAsStringWithFormat(dateFormat: dateTodayFormat)
            dateIcon.tintColor = Colors.today
            dateLabel.textColor = Colors.today
        } else if Calendar.current.isDateInTomorrow(date) {
            dateLabel.text = dateTomorrowFormat == nil ? "Завтра" : date.getDateAsStringWithFormat(dateFormat: dateTomorrowFormat)
            dateIcon.tintColor = Colors.tomorrow
            dateLabel.textColor = Colors.tomorrow
        } else {
            dateLabel.text = date.getDateAsStringWithFormat(dateFormat: dateOtherFormat)
            dateIcon.tintColor = Colors.other
            dateLabel.textColor = Colors.other
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: dateIcon.intrinsicContentSize.width + dateLabel.intrinsicContentSize.width + 8, height: dateLabel.intrinsicContentSize.height)
    }

}
