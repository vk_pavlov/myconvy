//
//  HomeView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 10/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class HomeView: UIView {

    @IBOutlet var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("Home", owner: self, options: nil)
        contentView.backgroundColor = UIColor.clear
        contentView.fixInView(self)
    }

}
