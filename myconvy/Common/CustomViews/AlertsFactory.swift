//
//  AlertsFactory.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 19/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

struct AlertsFactory {
    
    static func makeAlertError(error: String) -> UIAlertController {
        let alert = UIAlertController(title: "Ошибка", message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ок", style: .cancel, handler: nil)
        alert.addAction(action)
        return alert
    }
    
    
}
