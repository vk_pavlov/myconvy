//
//  Spinner.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 05/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

class Spinner: UIActivityIndicatorView {

    var container: UIView = UIView()
    var loadingView: UIView = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    override init(style: UIActivityIndicatorView.Style) {
        super.init(style: style)
        setup()
    }

    func setup() {
        
    }

    func setStateSpinner(enable: Bool, view: UIView) {
        if enable {
            showActivityIndicatory(uiView: view)
        } else {
            hideActivityIndicator(uiView: view)
        }

    }

    func hideActivityIndicator(uiView: UIView) {
        self.stopAnimating()
        container.removeFromSuperview()
    }

    func showActivityIndicatory(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = Helpers.UIColorFromHex(rgbValue: 0xffffff, alpha: 0.3)
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = Helpers.UIColorFromHex(rgbValue: 0x444444, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10

        self.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        self.style =
            UIActivityIndicatorView.Style.whiteLarge
        self.center = CGPoint(x: loadingView.frame.size.width / 2, y:
                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(self)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        self.startAnimating()
    }

    
}
