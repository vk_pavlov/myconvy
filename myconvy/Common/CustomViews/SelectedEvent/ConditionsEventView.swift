//
//  ConditionsEventView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 12/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol ConditionsEventViewDelegate: class {
    func conditionsEventViewParticipantsButtonPressed(_ view: ConditionsEventView)
}

final class ConditionsEventView: UIView {
    
    weak var delegate: ConditionsEventViewDelegate?
    
    private let participantsButton: ButtonWithImage = {
        let button = ButtonWithImage(iconPosition: .right)
        button.heightAnchor.constraint(equalToConstant: 34).isActive = true
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.hexStringToUIColor(hex: "#FCA54B"), for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 14)
        button.setImage(UIImage(named: "ic_chevron_right")!, for: .normal)
        button.addTarget(self, action: #selector(participantsPressed), for: .touchUpInside)
        return button
    }()
    
    private let genderButton: UIButton = {
        let button = UIButton()
        button.heightAnchor.constraint(equalToConstant: 34).isActive = true
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button.backgroundColor = UIColor.hexStringToUIColor(hex: "#FFEDDC")
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.hexStringToUIColor(hex: "#FCA54B"), for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 14)
        button.layer.shadowOffset = CGSize(width: 0, height: 4)
        button.layer.shadowOpacity = 6
        button.layer.shadowColor = UIColor.black.withAlphaComponent(0.1).cgColor
        button.layer.cornerRadius = 6
        button.isUserInteractionEnabled = false
        return button
    }()
    
    private let participantsView: UIView = {
           let view = UIView()
           view.translatesAutoresizingMaskIntoConstraints = false
           view.backgroundColor = UIColor.hexStringToUIColor(hex: "#FFEDDC")
           view.layer.shadowOffset = CGSize(width: 0, height: 4)
           view.layer.shadowOpacity = 6
           view.layer.shadowColor = UIColor.black.withAlphaComponent(0.1).cgColor
           view.layer.cornerRadius = 6
           view.heightAnchor.constraint(equalToConstant: 34).isActive = true
           view.widthAnchor.constraint(equalToConstant: 140).isActive = true
           return view
    }()
    
    private let counterLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .red
        label.heightAnchor.constraint(equalToConstant: 17).isActive = true
        label.widthAnchor.constraint(equalToConstant: 17).isActive = true
        label.layer.cornerRadius = 8.5
        label.textAlignment = .center
        label.textColor = .white
        label.clipsToBounds = true
        label.font = UIFont.systemFont(ofSize: 12)
        label.isHidden = true
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    func set(model: ConditionsEventViewViewModel) {
        let title = String.localizedStringWithFormat(NSLocalizedString("selectedEvent participants count", comment: ""), model.participantsCount)
        participantsButton.setTitle(title, for: .normal)
        if let requestToEventCount = model.requestToEventCount, requestToEventCount != 0 {
            counterLabel.isHidden = false
            counterLabel.text = String(requestToEventCount)
        } else {
            counterLabel.isHidden = true
        }
        
        switch model.participantsGender {
        case 0: genderButton.setTitle("Любой пол", for: .normal)
        case 1: genderButton.setTitle("Женский", for: .normal)
        default: genderButton.setTitle("Мужской", for: .normal)
        }
    }
    
    private func configure() {
        addSubview(participantsView)
        participantsView.addSubview(participantsButton)
        participantsView.addSubview(counterLabel)
        addSubview(genderButton)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        heightAnchor.constraint(equalToConstant: 40).isActive = true
        participantsView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        participantsView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        
        counterLabel.leadingAnchor.constraint(equalTo: participantsView.leadingAnchor, constant: 8).isActive = true
        counterLabel.centerYAnchor.constraint(equalTo: participantsView.centerYAnchor).isActive = true
        
        participantsButton.topAnchor.constraint(equalTo: participantsView.topAnchor).isActive = true
        participantsButton.leadingAnchor.constraint(equalTo: counterLabel.trailingAnchor).isActive = true
        
        genderButton.centerYAnchor.constraint(equalTo: participantsButton.centerYAnchor).isActive = true
        genderButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
    }
    
    @objc private func participantsPressed() {
        delegate?.conditionsEventViewParticipantsButtonPressed(self)
    }
    
}
