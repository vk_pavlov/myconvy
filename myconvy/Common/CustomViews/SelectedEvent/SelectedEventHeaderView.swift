//
//  SelectedEventHeaderView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 12/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

fileprivate let sizeCategoryImageView: CGFloat = 70

final class SelectedEventHeaderView: UIView, UIGestureRecognizerDelegate {
    
    private let categoryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.heightAnchor.constraint(equalToConstant: sizeCategoryImageView).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: sizeCategoryImageView).isActive = true
        imageView.layer.cornerRadius = sizeCategoryImageView / 2
        return imageView
    }()

    private let categotyLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue", size: 13)
        label.textColor = OrangeTheme.myconvyOrange
        label.textAlignment = .center
        label.backgroundColor = UIColor.hexStringToUIColor(hex: "#FFEDDC")
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        return label
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 19)
        label.textColor = OrangeTheme.myconvyOrange
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private let dateStartView = DateStartView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    private func configure() {
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(categoryImageView)
        addSubview(categotyLabel)
        addSubview(titleLabel)
        addSubview(dateStartView)
        
        dateStartView.dateOtherFormat = "dd MMMM в HH:mm"
        dateStartView.dateTodayFormat = "Сегодня в HH:mm"
        dateStartView.dateTomorrowFormat = "Завтра в HH:mm"
        
        dateStartView.translatesAutoresizingMaskIntoConstraints = false
        
        heightAnchor.constraint(equalToConstant: 160).isActive = true
        
        categoryImageView.topAnchor.constraint(equalTo: topAnchor, constant: 3).isActive = true
        categoryImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        categotyLabel.topAnchor.constraint(equalTo: categoryImageView.bottomAnchor, constant: -11).isActive = true
        categotyLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true

        titleLabel.topAnchor.constraint(equalTo: categotyLabel.bottomAnchor, constant: 8).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
        dateStartView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10).isActive = true
        dateStartView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
    
    func set(model: HeaderSelectedEventViewModel) {
        categoryImageView.image = UIImage(named: categoriesImageName[model.category])
        categotyLabel.text = categories[model.category]
        titleLabel.text = model.title
        dateStartView.date = model.date
    }
    
}
