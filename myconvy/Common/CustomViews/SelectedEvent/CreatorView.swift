//
//  CreatorView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 12/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol CreatorViewDelegate: class {
    func creatorViewCreatorDidPressed(_ view: CreatorView, creatorId: Int)
}

final class CreatorView: UIView {
    
    weak var delegate: CreatorViewDelegate?
    
    private let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        label.text = "Автор"
        return label
    }()
    
    private let container: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 8
        view.layer.shadowOffset = CGSize(width: -3, height: 4)
        view.layer.shadowOpacity = 1
        view.layer.shadowRadius = 7
        view.layer.shadowColor = UIColor.hexStringToUIColor(hex: "EBEAEA").cgColor
        view.heightAnchor.constraint(equalToConstant: 70).isActive = true
        view.backgroundColor = .white
        return view
    }()
    
    private let avatarView: AvatarBezierView = {
        let view = AvatarBezierView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        view.heightAnchor.constraint(equalToConstant: 60).isActive = true
        view.widthAnchor.constraint(equalToConstant: 60).isActive = true
        view.layer.cornerRadius = 30
        return view
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        label.textColor = UIColor.black.withAlphaComponent(0.85)
        return label
    }()
    
    private let moreInfoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue", size: 14)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        return label
    }()
    
    private let rightArrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 17).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 11).isActive = true
        imageView.image = UIImage(named: "arrow_right")
        return imageView
    }()
    
    private var creatorId: Int!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    func set(model: CreatorInfo) {
        creatorId = model.userId
        avatarView.set(imageUrl: model.avatar, online: model.online)
        nameLabel.text = model.name
        moreInfoLabel.text = model.moreInfo
    }
    
    private func configure() {
        addSubview(title)
        addSubview(container)
        container.addSubview(avatarView)
        container.addSubview(nameLabel)
        container.addSubview(moreInfoLabel)
        container.addSubview(rightArrowImageView)
        
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.addTarget(self, action: #selector(creatorDidPressed))
        addGestureRecognizer(tapGestureRecognizer)
        
        heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        title.topAnchor.constraint(equalTo: topAnchor).isActive = true
        title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        
        container.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 9).isActive = true
        container.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        container.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
        avatarView.topAnchor.constraint(equalTo: container.topAnchor, constant: 5).isActive = true
        avatarView.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        
        nameLabel.topAnchor.constraint(equalTo: container.topAnchor, constant: 13).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: avatarView.trailingAnchor, constant: 13).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: rightArrowImageView.leadingAnchor, constant: -16).isActive = true
        
        rightArrowImageView.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true
        rightArrowImageView.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        moreInfoLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 9).isActive = true
        moreInfoLabel.leadingAnchor.constraint(equalTo: avatarView.trailingAnchor, constant: 13).isActive = true
        moreInfoLabel.trailingAnchor.constraint(equalTo: rightArrowImageView.leadingAnchor, constant: -16).isActive = true
    }
    
    @objc private func creatorDidPressed() {
        delegate?.creatorViewCreatorDidPressed(self, creatorId: creatorId)
    }
    
}
