//
//  CommonStubView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 16/02/2020.
//  Copyright © 2020 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class CommonStubView: UIView {
    
    var title: String
    
    var subtitle: String?
    
    var icon: String
    
    var action: (() -> ())?
    
    var buttonTitle: String?
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: icon)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.layer.cornerRadius = 50
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont(name: "HelveticaNeue", size: 17)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = title
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont(name: "HelveticaNeue", size: 13)
        label.textColor = UIColor.black.withAlphaComponent(0.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = subtitle
        return label
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(buttonTitle, for: .normal)
        button.addTarget(self, action: #selector(actionButtonPressed), for: .touchUpInside)
        return button
    }()
    
    init(title: String, subtitle: String?, icon: String, buttonTitle: String?, action: (() -> ())?) {
        self.title = title
        self.subtitle = subtitle
        self.icon = icon
        self.action = action
        self.buttonTitle = buttonTitle
        super.init(frame: .zero)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        addSubview(imageView)
        addSubview(titleLabel)
        translatesAutoresizingMaskIntoConstraints = false
        
        imageView.topAnchor.constraint(equalTo: topAnchor, constant: 70).isActive = true
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 25).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
        if subtitle != nil {
            addSubview(subtitleLabel)
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 15).isActive = true
            subtitleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
            subtitleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        }
        if action != nil {
            addSubview(actionButton)
            subtitle != nil ? (actionButton.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: 50).isActive = true) : (actionButton.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 50).isActive = true)
            actionButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        }
        
    }
    
    @objc private func actionButtonPressed() {
        action?()
    }
    
}
