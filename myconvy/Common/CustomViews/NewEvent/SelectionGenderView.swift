//
//  SelectionGenderView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 09/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class SelectionGenderView: UIView {
    
    private let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        label.text = "Пол"
        return label
    }()
    
    private let manButton: ButtonWithImage = {
        let button = ButtonWithImage(iconPosition: .left)
        button.heightAnchor.constraint(equalToConstant: 16).isActive = true
        button.widthAnchor.constraint(equalToConstant: 110).isActive = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.black.withAlphaComponent(0.5), for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 14)
        button.setTitle("Мужской", for: .normal)
        button.setImage(UIImage(named: "ring")!, for: .normal)
        button.addTarget(self, action: #selector(manButtonPressed), for: .touchUpInside)
        button.contentHorizontalAlignment = .leading
        return button
    }()
    
    private let womanButton: ButtonWithImage = {
         let button = ButtonWithImage(iconPosition: .left)
         button.heightAnchor.constraint(equalToConstant: 16).isActive = true
         button.widthAnchor.constraint(equalToConstant: 110).isActive = true
         button.translatesAutoresizingMaskIntoConstraints = false
         button.setTitleColor(UIColor.black.withAlphaComponent(0.5), for: .normal)
         button.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 14)
         button.setTitle("Женский", for: .normal)
         button.setImage(UIImage(named: "ring")!, for: .normal)
         button.addTarget(self, action: #selector(womanButtonPressed), for: .touchUpInside)
        button.contentHorizontalAlignment = .leading
         return button
    }()
    
    private var isSelectedMan: Bool = false {
        didSet {
            manButton.setImage(isSelectedMan ? UIImage(named: "Oval") : UIImage(named: "ring"), for: .normal)
        }
    }
    
    private var isSelectedWoman: Bool = false {
        didSet {
            womanButton.setImage(isSelectedWoman ? UIImage(named: "Oval") : UIImage(named: "ring"), for: .normal)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    private var anyGender: Bool {
        return isSelectedMan && isSelectedWoman
    }
    
    private var genderNotSelected: Bool {
        return !isSelectedMan && !isSelectedWoman
    }
    
    private var onlyMan: Bool {
        return isSelectedMan && !isSelectedWoman
    }
    
    var selectedGender: Int? {
        get {
            if anyGender {
                return 0
            } else if onlyMan {
                return 2
            } else if genderNotSelected{
                return nil
            } else {
                return 1
            }
        }
        set {
            if newValue == 1 {
                isSelectedWoman = true
                isSelectedMan = false
            } else if newValue == 2 {
                isSelectedMan = true
                isSelectedWoman = false
            } else {
                isSelectedWoman = true
                isSelectedMan = true
            }
        }
    }
    
    var singleGenderMode = true
    
    private func configure() {
        addSubview(title)
        addSubview(manButton)
        addSubview(womanButton)
        
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        title.topAnchor.constraint(equalTo: topAnchor).isActive = true
        title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        
        manButton.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 9).isActive = true
        manButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        manButton.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        womanButton.leadingAnchor.constraint(equalTo: manButton.trailingAnchor, constant: 50).isActive = true
        womanButton.centerYAnchor.constraint(equalTo: manButton.centerYAnchor).isActive = true
    }
    
    @objc private func manButtonPressed() {
        isSelectedMan = !isSelectedMan
        if singleGenderMode {
            isSelectedWoman = false
        }
    }
    
    @objc private func womanButtonPressed() {
        isSelectedWoman = !isSelectedWoman
        if singleGenderMode {
            isSelectedMan = false
        }
    }
}
