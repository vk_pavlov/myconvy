//
//  CategoriesSelectedView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class CategoriesSelectionView: UIView {
    
    private var allCategories = Array.init(repeating: false, count: categories.count)
    
    private let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        label.heightAnchor.constraint(equalToConstant: 22).isActive = true
        label.text = "Категории"
        return label
    }()
    
    private let categoriesCarousel = CategoriesCollectionView()
    private var selectedCellsIndexPath = [IndexPath]()
    
    var selectedCategories: [Int] {
        get {
            var indexes = [Int]()
            for index in allCategories.indices {
                if allCategories[index] {
                    indexes.append(index)
                }
            }
            return indexes
        }
        set {
            for index in newValue {
                allCategories[index] = true
            }
        }
    }
    
    var isSingleModeSelectionCell = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        configure()
    }
    
    init(title: String) {
        self.title.text = title
        super.init(frame: .zero)
        configure()
    }
    
    var isCellWithShadow = false
    
    private func configure() {
        translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(title)
        addSubview(categoriesCarousel)
        categoriesCarousel.dataSource = self
        categoriesCarousel.delegate = self
        
        title.topAnchor.constraint(equalTo: topAnchor).isActive = true
        title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        
        categoriesCarousel.anchor(top: title.bottomAnchor,
                                  leading: leadingAnchor,
                                  bottom: bottomAnchor,
                                  trailing: trailingAnchor,
                                  padding: UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0))
    }
    
    private func resetAllCells() {
//        allCategories = Array.init(repeating: false, count: categories.count)
//        for row in 0..<categoriesCarousel.numberOfItems(inSection: 0) {
//            let indexPath = IndexPath(row: row, section: 0)
//            if isCellWithShadow {
//                let cell = categoriesCarousel.cellForItem(at: indexPath) as! CategoryWithShadowCollectionViewCell
//                cell.isSelectedCategory = false
//            } else {
//                let cell = categoriesCarousel.cellForItem(at: indexPath) as! CategoryCollectionViewCell
//                cell.isSelectedCategory = false
//            }
//        }
    }
    
}

extension CategoriesSelectionView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isCellWithShadow {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryWithShadowCollectionViewCell.reuseId, for: indexPath) as! CategoryWithShadowCollectionViewCell
            cell.title = categories[indexPath.row]
            cell.isSelectedCategory = allCategories[indexPath.row]
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.reuseId, for: indexPath) as! CategoryCollectionViewCell
        cell.title = categories[indexPath.row]
        cell.isSelectedCategory = allCategories[indexPath.row]
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isCellWithShadow {
            let cell = collectionView.cellForItem(at: indexPath) as! CategoryWithShadowCollectionViewCell
            allCategories[indexPath.row] = !allCategories[indexPath.row]
            cell.isSelectedCategory = allCategories[indexPath.row]
            return
        }
        let cell = collectionView.cellForItem(at: indexPath) as! CategoryCollectionViewCell
        allCategories[indexPath.row] = !allCategories[indexPath.row]
        cell.isSelectedCategory = allCategories[indexPath.row]
    }
}
