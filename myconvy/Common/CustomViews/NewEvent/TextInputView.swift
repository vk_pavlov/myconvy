//
//  TextInputView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 07/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class TextInputView: UIView, UnfieldItems {
    
    func highlight() {
        separator.backgroundColor = .red
        UIView.animate(withDuration: 0.5) {
            self.separator.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        }
    }
    
    var isEmpty: Bool {
        return textField.text!.isEmpty
    }
    
    private let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.heightAnchor.constraint(equalToConstant: 22).isActive = true
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        return label
    }()
    
    let textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.borderStyle = .none
        textField.contentVerticalAlignment = .bottom
        textField.font = UIFont(name: "HelveticaNeue", size: 14)
        textField.textColor = UIColor.black.withAlphaComponent(0.5)
        textField.heightAnchor.constraint(equalToConstant: 22).isActive = true
        return textField
    }()
    
    var text: String? {
        get {
            return textField.text
        }
        set {
            textField.text = newValue
        }
    }
    
    private let separator = SeparatorView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    init(title: String) {
        super.init(frame: .zero)
        self.title.text = title
        configure()
    }
    
    private func configure() {
        addSubview(title)
        addSubview(textField)
        addSubview(separator)
        
        textField.delegate = self
        
        translatesAutoresizingMaskIntoConstraints = false
        
        title.topAnchor.constraint(equalTo: topAnchor).isActive = true
        title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        
        textField.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 9).isActive = true
        textField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        textField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
        separator.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 5).isActive = true
        separator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        separator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        separator.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}

extension TextInputView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 40
    }
}
