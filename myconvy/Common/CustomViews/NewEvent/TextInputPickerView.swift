//
//  PrivacyPicker.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 09/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol TextInputPickerViewDelegate: class {
    func numberOfRows() -> Int
    func titleForRow(titleForRow row: Int) -> String?
}

class TextInputPickerView: TextInputView {
    
    weak var delegate: TextInputPickerViewDelegate?
    
    private lazy var toolbar: UIToolbar = {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(doneDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Отменить", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.sizeToFit()
        toolbar.tintColor = .white
        toolbar.barTintColor = UIColor.hexStringToUIColor(hex: "CFCFCF")
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        return toolbar
    }()

    private lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.backgroundColor = .white
        pickerView.dataSource = self
        pickerView.delegate = self
        return pickerView
    }()
    
    private var lastRelation: Int = 0
    
    var relation: Int {
        get {
            return pickerView.selectedRow(inComponent: 0)
        }
        set {
            lastRelation = newValue
            pickerView.selectRow(newValue, inComponent: 0, animated: true)
            textField.text = delegate?.titleForRow(titleForRow: newValue)
        }
    }
    
    override init(title: String) {
        super.init(title: title)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        configure()
    }
    
    private func configure() {
        textField.inputAccessoryView = toolbar
        textField.inputView = pickerView
    }
    
    @objc private func doneDatePicker() {
        lastRelation = pickerView.selectedRow(inComponent: 0)
        textField.text = delegate?.titleForRow(titleForRow: lastRelation)
        endEditing(true)
    }
    
    @objc private func cancelDatePicker() {
        pickerView.selectRow(lastRelation, inComponent: 0, animated: true)
        textField.text = delegate?.titleForRow(titleForRow: lastRelation)
        endEditing(true)
    }
}

extension TextInputPickerView: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return delegate?.numberOfRows() ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return delegate?.titleForRow(titleForRow: row)
    }
    
}
