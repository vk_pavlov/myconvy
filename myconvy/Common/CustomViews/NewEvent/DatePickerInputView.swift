//
//  DatePickerInputView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class DatePickerInputView: TextInputView {
    
    private lazy var toolbar: UIToolbar = {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(doneDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Отменить", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.sizeToFit()
        toolbar.tintColor = .white
        toolbar.barTintColor = UIColor.hexStringToUIColor(hex: "CFCFCF")
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        return toolbar
    }()

    private lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.backgroundColor = .white
        datePicker.locale = Locale(identifier: "rus")
        return datePicker
    }()
    
    private var lastDate = Date()
    
    var datePickerMode: UIDatePicker.Mode = .date {
        didSet {
            datePicker.datePickerMode = datePickerMode
            if datePickerMode == .date {
                datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -16, to: Date())
            } else if datePickerMode == .dateAndTime {
                datePicker.minimumDate = Calendar.current.date(byAdding: .minute, value: 0, to: Date())
            }
        }
    }
    
    var isEmptyDate: Bool {
        return textField.text!.isEmpty
    }
    
    var date: Double {
        get {
            return datePicker.date.timeIntervalSince1970
        }
        set {
            lastDate = Date(timeIntervalSince1970: newValue)
            datePicker.date = Date(timeIntervalSince1970: newValue)
            textField.text = lastDate.getDateAsStringWithFormat(dateFormat: "dd.MM.yyyy")
        }
    }
    
    override init(title: String) {
        super.init(title: title)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        configure()
    }
    
    private func configure() {
        textField.inputAccessoryView = toolbar
        textField.inputView = datePicker
    }
    
    @objc private func doneDatePicker() {
        lastDate = datePicker.date
        textField.text = datePicker.date.getDateAsStringWithFormat(dateFormat: "dd.MM.yyyy")
        endEditing(true)
    }
    
    @objc private func cancelDatePicker() {
        datePicker.date = lastDate
        textField.text = lastDate.getDateAsStringWithFormat(dateFormat: "dd.MM.yyyy")
        endEditing(true)
    }
}
