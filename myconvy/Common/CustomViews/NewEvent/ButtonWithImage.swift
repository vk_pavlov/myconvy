//
//  ButtonWithImage.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

enum IconPosition {
    case left
    case right
}

class ButtonWithImage: UIButton {
    
    var iconPosition: IconPosition
    
    init(iconPosition: IconPosition) {
        self.iconPosition = iconPosition
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //https://stackoverflow.com/questions/4564621/aligning-text-and-image-on-uibutton-with-imageedgeinsets-and-titleedgeinsets
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            let insetAmount: CGFloat = 5
            if iconPosition == .right {
                semanticContentAttribute = .forceRightToLeft
                imageEdgeInsets = UIEdgeInsets(top: 3, left: insetAmount, bottom: 0, right: -insetAmount)
                titleEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
                contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
            } else {
                imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
                titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
                contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
            }
        }
    }
}
