//
//  MoreInfoView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class MoreInfoView: UIView {
    
    private let descriptionEventView = DescriptionView(title: "Описание", placeholder: "Хочу сходить в свободное время...")
    private let descriptionPlaceView = DescriptionView(title: "Описание места", placeholder: "Встречаемся у метро...")
    private let selectionGenderView = SelectionGenderView()
    private let slider = SliderPeople()
    private let privacyPicker = TextInputPickerView(title: "Видимость")
    
    private let pickerData = ["Для всех", "Для зарегистрированных", "Только по ссылке", "Для подписчиков"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        configure()
    }
    
    func hiddenDescriptionEvent() {
        descriptionEventView.hidden()
    }
    
    func hiddenDescriptionPlace() {
        descriptionPlaceView.hidden()
    }
    
    var descriptionEvent: String? {
        get {
            return descriptionEventView.text
        }
        set {
            descriptionEventView.text = newValue
        }
    }
    
    var descriptionPlace: String? {
        get {
            return descriptionPlaceView.text
        }
        set {
            descriptionPlaceView.text = newValue
        }
    }
    
    var selectionGender: Int? {
        get {
            return selectionGenderView.selectedGender
        }
        set {
            selectionGenderView.selectedGender = newValue
        }
    }
    
    var partLimit: Int {
        get {
            return slider.count
        }
        set {
            slider.count = newValue
        }
    }
    
    var privacy: Int? {
        get {
            return privacyPicker.text!.isEmpty ? nil : pickerData.firstIndex(of: privacyPicker.text!)!
        }
        set {
            privacyPicker.relation = newValue!
        }
    }
    
    private func configure() {
        
        addSubview(descriptionEventView)
        addSubview(descriptionPlaceView)
        addSubview(selectionGenderView)
        addSubview(slider)
        addSubview(privacyPicker)
        
        privacyPicker.delegate = self
        privacyPicker.relation = 0
        selectionGenderView.singleGenderMode = false
        selectionGenderView.selectedGender = 0
        descriptionEventView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        descriptionEventView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        descriptionEventView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        descriptionPlaceView.topAnchor.constraint(equalTo: descriptionEventView.bottomAnchor, constant: 14).isActive = true
        descriptionPlaceView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        descriptionPlaceView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        selectionGenderView.topAnchor.constraint(equalTo: descriptionPlaceView.bottomAnchor, constant: 14).isActive = true
        selectionGenderView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        selectionGenderView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        slider.topAnchor.constraint(equalTo: selectionGenderView.bottomAnchor, constant: 14).isActive = true
        slider.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        slider.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        privacyPicker.topAnchor.constraint(equalTo: slider.bottomAnchor, constant: 14).isActive = true
        privacyPicker.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        privacyPicker.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        privacyPicker.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    
    }
    
}

extension MoreInfoView: TextInputPickerViewDelegate {
    func numberOfRows() -> Int {
        return pickerData.count
    }
    
    func titleForRow(titleForRow row: Int) -> String? {
        return pickerData[row]
    }
}
