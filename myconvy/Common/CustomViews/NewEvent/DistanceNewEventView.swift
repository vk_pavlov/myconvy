//
//  DistanceNewEventView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit


class DistanceNewEventView: DistanceView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        configure()
    }
    
    private func configure() {
        backgroundColor = UIColor.black.withAlphaComponent(0.25)
        layer.cornerRadius = 6
        distanceLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
    }
}
