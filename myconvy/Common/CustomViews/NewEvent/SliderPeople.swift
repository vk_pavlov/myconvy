//
//  SliderPeople.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 09/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

class SliderPeople: UIView {
    
    private let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        label.text = "Кол-во людей"
        return label
    }()
    
    private let slider: UISlider = {
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.maximumValue = 10.0
        slider.minimumValue = 2.0
        slider.value = 10
        slider.minimumTrackTintColor = OrangeTheme.myconvyOrange
        slider.maximumTrackTintColor = OrangeTheme.myconvyOrange
        slider.setThumbImage(UIImage(named: "Oval"), for: .normal)
        slider.addTarget(self, action: #selector(countPeopleChanged), for: .valueChanged)
        return slider
    }()
    
    private let countPeople: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = OrangeTheme.myconvyOrange
        label.text = "Неважно"
        return label
    }()
    
    private var isChanagedCount = false
    
    var count: Int {
        get {
            return isChanagedCount ? Int(slider.value) : 0
        }
        set {
            if newValue == 0 {
                slider.value = 10
                countPeople.text = "Неважно"
            } else {
                slider.value = Float(newValue)
                countPeople.text = String(newValue)
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    private func configure() {
        addSubview(title)
        addSubview(slider)
        addSubview(countPeople)
        
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        title.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 17).isActive = true
        
        slider.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 9).isActive = true
        slider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        slider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
        countPeople.leadingAnchor.constraint(equalTo: title.trailingAnchor, constant: 7).isActive = true
        countPeople.centerYAnchor.constraint(equalTo: title.centerYAnchor).isActive = true
    }
    
    @objc private func countPeopleChanged(_ sender: UISlider) {
        let count = Int(sender.value)
        count == 10 ? (countPeople.text = "Неважно") : (countPeople.text = String(count))
        isChanagedCount = true
    }
}
