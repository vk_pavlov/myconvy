//
//  MapsPositionView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol MapsPositionViewDelegate: class {
    func mapsPositionViewSelectedAddressPressed(_ view: MapsPositionView)
    var isNoSelectedAddress: Bool { get }
}

class MapsPositionView: UIView, UnfieldItems {
    func highlight() {
        selectPointButton.backgroundColor = .red
        UIView.animate(withDuration: 0.5) {
            self.selectPointButton.backgroundColor = UIColor.hexStringToUIColor(hex: "#FFEDDC")
        }
    }
    
    var isEmpty: Bool {
        guard delegate != nil else { return true }
        return delegate!.isNoSelectedAddress
    }
    
    weak var delegate: MapsPositionViewDelegate?
    
    private let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        label.text = "Место"
        return label
    }()
    
    private let selectPointButton: ButtonWithImage = {
        let button = ButtonWithImage(iconPosition: .right)
        button.heightAnchor.constraint(equalToConstant: 34).isActive = true
        button.widthAnchor.constraint(equalToConstant: 170).isActive = true
        button.backgroundColor = UIColor.hexStringToUIColor(hex: "#FFEDDC")
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.hexStringToUIColor(hex: "#FCA54B"), for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 14)
        button.setImage(UIImage(named: "ic_chevron_right")!, for: .normal)
        button.layer.shadowOffset = CGSize(width: 0, height: 4)
        button.layer.shadowOpacity = 6
        button.layer.shadowColor = UIColor.black.withAlphaComponent(0.1).cgColor
        button.layer.cornerRadius = 6
        button.addTarget(self, action: #selector(selectionPointPressed), for: .touchUpInside)
        return button
    }()
    
    private let distanceView: DistanceNewEventView = {
        let view = DistanceNewEventView()
        view.heightAnchor.constraint(equalToConstant: 25).isActive = true
        view.widthAnchor.constraint(equalToConstant: 90).isActive = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var isHiddenDistanceView: Bool {
        get {
            return distanceView.isHidden
        }
        set {
            distanceView.isHidden = newValue
        }
    }
    
    var distance: Double? {
        get {
            return distanceView.distance
        }
        set {
            distanceView.distance = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    init(buttonTitle: String) {
        super.init(frame: .zero)
        selectPointButton.setTitle(buttonTitle, for: .normal)
        configure()
    }
    
    private func configure() {
        addSubview(title)
        addSubview(selectPointButton)
        addSubview(distanceView)
        
        let tapGestureRecognizer = UITapGestureRecognizer()
        selectPointButton.addGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer.addTarget(self, action: #selector(selectionPointPressed))
        
        translatesAutoresizingMaskIntoConstraints = false
        
        title.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 17).isActive = true
        
        selectPointButton.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 10).isActive = true
        selectPointButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 17).isActive = true
        selectPointButton.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true

        distanceView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -17).isActive = true
        distanceView.centerYAnchor.constraint(equalTo: selectPointButton.centerYAnchor).isActive = true
    }
    
    @objc private func selectionPointPressed() {
        delegate?.mapsPositionViewSelectedAddressPressed(self)
    }
}

