//
//  DescriptionView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 09/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//


import UIKit
import KMPlaceholderTextView

class DescriptionView: UIView {
    
    private let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        label.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return label
    }()
    
    private let textView: KMPlaceholderTextView = {
        let textView = KMPlaceholderTextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.font = UIFont(name: "HelveticaNeue", size: 14)
        textView.placeholderFont = UIFont(name: "HelveticaNeue", size: 14)
        textView.textColor = UIColor.black.withAlphaComponent(0.25)
        textView.placeholderColor = UIColor.black.withAlphaComponent(0.25)
        textView.backgroundColor = UIColor.black.withAlphaComponent(0.08)
        textView.layer.cornerRadius = 8
        textView.clipsToBounds = true
        return textView
    }()
    
    private var titleBottomConstraint: NSLayoutConstraint!
    
    func hidden() {
        isHidden = true
    }
    
    var text: String? {
        set {
            if newValue == "hidden" {
                textView.text = "Место станет доступно как только Вас примут в событие."
            } else {
                let result = newValue?.filter { !$0.isNewline }
                textView.text = result
            }
            textView.sizeToFit()
            textView.invalidateIntrinsicContentSize()
            invalidateIntrinsicContentSize()
        }
        get {
            return textView.text!.isEmpty ? nil : textView.text!
        }
    }
    
    var isClearBackgroundTextView: Bool = false {
        didSet {
            if isClearBackgroundTextView {
                textView.backgroundColor = .white
                titleBottomConstraint.constant = -3
                invalidateIntrinsicContentSize()
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    init(title: String, placeholder: String) {
        super.init(frame: .zero)
        self.title.text = title
        self.textView.placeholder = placeholder
        configure()
    }
    
    private func configure() {
        addSubview(title)
        addSubview(textView)
        
        textView.delegate = self
        
        translatesAutoresizingMaskIntoConstraints = false
        
        title.topAnchor.constraint(equalTo: topAnchor).isActive = true
        title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        
        titleBottomConstraint = textView.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 9)
        titleBottomConstraint.isActive = true
        textView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        textView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: frame.width, height: title.intrinsicContentSize.height + titleBottomConstraint.constant + textView.intrinsicContentSize.height)
    }

}

extension DescriptionView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= 140
    }
}
