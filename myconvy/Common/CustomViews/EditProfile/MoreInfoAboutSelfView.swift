//
//  MoreInfoAboutSelfView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 14/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

protocol MoreInfoAboutSelfViewDelegate: class {
    
}

class MoreInfoAboutSelfView: UIView {
    
    weak var delegate: MoreInfoAboutSelfViewDelegate?
    
    private lazy var contacts = [ContactInfoViewModel]()
    
    private let headerView = UIView()
    
    private let cityTextView = TextInputView(title: "Город")
    private let universityTextView = TextInputView(title: "ВУЗ")
    private let relationPickerView = TextInputPickerView(title: "Семейное положение")
    
    private let pickerData = [
        "Не указан",
        "Не женат(не замужем)",
        "Есть друг/подруга",
        "Помолвлен(а)",
        "Женат(замужем)",
        "Все сложно",
        "В активном поиске",
        "Влюблен(а)",
        "Гражданский брак"
    ]
    
    private let contactsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        label.heightAnchor.constraint(equalToConstant: 22).isActive = true
        label.text = "Контакты"
        return label
    }()
    
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.register(ContactInfoTableViewCell.self, forCellReuseIdentifier: ContactInfoTableViewCell.reuseId)
        table.dataSource = self
        table.delegate = self
        table.isScrollEnabled = false
        table.allowsSelection = false
        return table
    }()
    
    private let aboutMeView = DescriptionView(title: "О себе", placeholder: "Расскажите о себе")
   
    
    var city: String? {
        get {
            return cityTextView.text
        }
        set {
            cityTextView.text = newValue
        }
        
    }

    var university: String? {
        get {
            return universityTextView.text
        }
        set {
            universityTextView.text = newValue
        }
    }

    var relation: Int {
        get {
            return relationPickerView.relation
        }
        set {
            relationPickerView.relation = newValue
        }
    }
    
    var contactInformation: [ContactInfoViewModel] {
        set {
            contacts = newValue
            tableView.reloadData()
        }
        get {
            return contacts
        }
    }

    var aboutMe: String? {
        get {
            return aboutMeView.text
        }
        set {
            aboutMeView.text = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        configure()
    }

    private func configure() {
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(tableView)
        addSubview(aboutMeView)
        
        tableView.tableFooterView = UIView(frame: .zero)
        relationPickerView.delegate = self
        
        headerView.addSubview(cityTextView)
        headerView.addSubview(universityTextView)
        headerView.addSubview(contactsLabel)
        headerView.addSubview(relationPickerView)
        
        cityTextView.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        cityTextView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor).isActive = true
        cityTextView.trailingAnchor.constraint(equalTo: headerView.trailingAnchor).isActive = true

        universityTextView.topAnchor.constraint(equalTo: cityTextView.bottomAnchor, constant: 9).isActive = true
        universityTextView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor).isActive = true
        universityTextView.trailingAnchor.constraint(equalTo: headerView.trailingAnchor).isActive = true

        relationPickerView.topAnchor.constraint(equalTo: universityTextView.bottomAnchor, constant: 9).isActive = true
        relationPickerView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor).isActive = true
        relationPickerView.trailingAnchor.constraint(equalTo: headerView.trailingAnchor).isActive = true

        contactsLabel.topAnchor.constraint(equalTo: relationPickerView.bottomAnchor, constant: 9).isActive = true
        contactsLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16).isActive = true
        
        tableView.topAnchor.constraint(equalTo: topAnchor, constant: 14).isActive = true
        tableView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        tableView.heightAnchor.constraint(equalToConstant: 470).isActive = true

        aboutMeView.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 9).isActive = true
        aboutMeView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        aboutMeView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        aboutMeView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
}

extension MoreInfoAboutSelfView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactInfoTableViewCell.reuseId, for: indexPath) as! ContactInfoTableViewCell
        cell.set(viewModel: contacts[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 235
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
}

extension MoreInfoAboutSelfView: TextInputPickerViewDelegate {
    func numberOfRows() -> Int {
        return pickerData.count
    }
    
    func titleForRow(titleForRow row: Int) -> String? {
        return pickerData[row]
    }
}

extension MoreInfoAboutSelfView: ContactInfoTableViewCellDelegate {
    func contactInfoTableViewCell(_ cell: ContactInfoTableViewCell, didChangedTextFieldContent content: String?) {
        if let row = tableView.indexPath(for: cell)?.row {
            contacts[row].value = content
        }
    }
}
