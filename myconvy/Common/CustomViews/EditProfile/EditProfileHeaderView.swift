//
//  EditProfileHeaderView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 13/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol EditProfileHeaderViewDelegate: class {
    func editProfileHeaderViewBackButtonPressed(_ view: EditProfileHeaderView)
    func editProfileHeaderViewDidAvatarPressed(_ view: EditProfileHeaderView)
}

final class EditProfileHeaderView: UIView, UIGestureRecognizerDelegate {
    
    weak var delegate: EditProfileHeaderViewDelegate?
    
    private let avatarImageView: WebImageView = {
        let imageView = WebImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        imageView.image = UIImage(named: "empty_avatar")
        imageView.isUserInteractionEnabled = true
        imageView.clipsToBounds = true
        return imageView
    }()
    
    var avatar: String? {
        get {
            return avatarImageView.currentUrlString
        }
        set {
            guard newValue != nil else { return }
            avatarImageView.set(imageUrl: newValue)
        }
    }
    
    var image: UIImage? {
        get {
            return avatarImageView.image
        }
        set {
            avatarImageView.image = newValue
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    private func configure() {
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: 80).isActive = true
        addSubview(avatarImageView)
        
        avatarImageView.topAnchor.constraint(equalTo: topAnchor, constant: 19).isActive = true
        avatarImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        let avatarViewTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleavatarImageViewTap(_:)))
        avatarViewTapRecognizer.delegate = self
        avatarImageView.addGestureRecognizer(avatarViewTapRecognizer)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
    }
    
    @objc private func handleavatarImageViewTap(_ sender: UITapGestureRecognizer?) {
        delegate?.editProfileHeaderViewDidAvatarPressed(self)
    }
    
}
