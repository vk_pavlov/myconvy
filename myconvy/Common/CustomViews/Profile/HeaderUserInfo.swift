//
//  HeaderUserInfo.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

protocol HeaderUserInfoDelegate: class {
    func headerUserInfoMoreInfoButtonPressed(_ view: HeaderUserInfo)
    func headerUserInfoAvatarImageViewPressed(_ view: HeaderUserInfo, imageView: UIImageView)
}

final class HeaderUserInfo: UIView, UIGestureRecognizerDelegate {
    
    weak var delegate: HeaderUserInfoDelegate?
    
    private let fullNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 21)
        label.textColor = UIColor.hexStringToUIColor(hex: "#FE901D")
        label.textAlignment = .center
        return label
    }()
    
    private let moreInfoView = MoreInfoAboutUserView ()
    
    private let avatarView: AvatarBezierView = {
        let view = AvatarBezierView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        view.heightAnchor.constraint(equalToConstant: 70).isActive = true
        view.widthAnchor.constraint(equalToConstant: 70).isActive = true
        return view
    }()
    
    var subtitleImage: UIImage? {
        set {
            moreInfoView.iconImage = newValue
        }
        get {
            return moreInfoView.iconImage
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    private func configure() {
        addSubview(fullNameLabel)
        addSubview(avatarView)
        addSubview(moreInfoView)
        
        moreInfoView.delegate = self
        
        avatarView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        avatarView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        let avatarViewTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleavatarImageViewTap(_:)))
        avatarViewTapRecognizer.delegate = self
        avatarView.addGestureRecognizer(avatarViewTapRecognizer)
        avatarView.isUserInteractionEnabled = true

        fullNameLabel.topAnchor.constraint(equalTo: avatarView.bottomAnchor, constant: 13).isActive = true
        fullNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        fullNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
        moreInfoView.topAnchor.constraint(equalTo: fullNameLabel.bottomAnchor, constant: 10).isActive = true
        moreInfoView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
    
    func set(model: HeaderUserInfoViewModel) {
        fullNameLabel.text = model.fullName
        moreInfoView.text = model.otherInfo
        avatarView.set(imageUrl: model.avatarUrl, online: model.online)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarView.layer.cornerRadius = avatarView.frame.width / 2
    }
    
    @objc func handleavatarImageViewTap(_ sender: UITapGestureRecognizer?) {
        delegate?.headerUserInfoAvatarImageViewPressed(self, imageView: avatarView.avatarImageView)
    }
    
}

extension HeaderUserInfo: MoreInfoAboutUserViewDelegate {
    func moreInfoAboutUserViewInfoButtonPressed(_ view: MoreInfoAboutUserView) {
        delegate?.headerUserInfoMoreInfoButtonPressed(self)
    }
}


