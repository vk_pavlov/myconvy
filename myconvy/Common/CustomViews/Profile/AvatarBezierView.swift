//
//  AvatarBezierView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

final class AvatarBezierView: UIView {
    
    private let imageView: WebImageView = {
        let imageView = WebImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.backgroundColor = .white
        return imageView
    }()
    
    private lazy var onlineIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.hexStringToUIColor(hex: "8ADC32")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.borderWidth = 3
        return imageView
    }()
    
    var avatarImageView: UIImageView {
        return imageView
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        configure()
    }
    
    private func configure() {
        addSubview(imageView)
        addSubview(onlineIconImageView)
        
        backgroundColor = .clear
        imageView.anchor(top: topAnchor,
                         leading: leadingAnchor,
                         bottom: bottomAnchor,
                         trailing: trailingAnchor,
                         padding: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5))
        
        onlineIconImageView.anchor(top: topAnchor,
                                   leading: nil,
                                   bottom: nil,
                                   trailing: trailingAnchor,
                                   padding: UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0),
                                   size: CGSize(width: 21, height: 21))
        
        layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
        layer.shadowRadius = 8
        layer.shadowOffset = .zero
        layer.shadowOpacity = 0.4
    }
    
    func set(imageUrl: String, online: Bool) {
        imageView.set(imageUrl: imageUrl)
        onlineIconImageView.isHidden = !online
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.layer.cornerRadius = imageView.frame.width / 2
        onlineIconImageView.layer.cornerRadius = onlineIconImageView.frame.width / 2
        
    }
    
    override func draw(_ rect: CGRect) {
        let pathRect = CGRect(x: 1,
                              y: 1,
                              width: rect.width - 2,
                              height: rect.height - 2)
        let path = UIBezierPath(ovalIn: pathRect)
            
        let color = OrangeTheme.myconvyOrange
        path.lineWidth = 2
        color.setStroke()
        path.stroke(with: .normal, alpha: 1)
        
        layer.shadowPath = path.cgPath
        clipsToBounds = false
    }
}
