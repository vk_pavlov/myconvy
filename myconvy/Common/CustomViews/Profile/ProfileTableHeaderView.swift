//
//  ProfileTableHeaderView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit
import TOSegmentedControl

@objc protocol ProfileTableHeaderViewDelegate: class {
    
    // RelationsButton and Segmented Control
    @objc optional func profileTableHeaderViewDidChangeSegmentItem(_ view: ProfileTableHeaderView, at index: Int)
    @objc optional func profileTableHeaderViewSubscribe(_ view: ProfileTableHeaderView)
    @objc optional func profileTableHeaderViewUnsubscribe(_ view: ProfileTableHeaderView)
    
    // HeaderUserInfoDelegate
    func profileTableHeaderViewMoreInfoButtonPressed(_ view: ProfileTableHeaderView)
    func profileTableHeaderViewAvatarImageViewPressed(_ view: ProfileTableHeaderView, imageView: UIImageView)
    
    // CountersStackViewDelegate
    func profileTableHeaderViewDidEventsPressed(_ view: ProfileTableHeaderView)
    func profileTableHeaderViewDidSubscribersPressed(_ view: ProfileTableHeaderView)
    func profileTableHeaderViewDidSubscriptionsPressed(_ view: ProfileTableHeaderView)
}

final class ProfileTableHeaderView: UIView {
    
    private let headerUserInfo = HeaderUserInfo()
    private let countersStackView = CountersStackView()
    
    weak var delegate: ProfileTableHeaderViewDelegate?
    
    private lazy var relationButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 24)
        button.addTarget(self, action: #selector(handleRelationButtonTap), for: .touchUpInside)
        return button
    }()
    
    private lazy var segmentedControl: SegmentedControl = {
        let items = ["Мои события", "Мои отклики"]
        let segmentedControl = SegmentedControl()
        segmentedControl.items = items
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.segmentTappedHandler = { [weak self] (index, reverced) in
            guard let self = self else { return }
            self.delegate?.profileTableHeaderViewDidChangeSegmentItem?(self, at: index)
        }
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        return segmentedControl
    }()
    
    private var isSubscribtion: Bool? {
        didSet {
            updateAppearanceButton()
        }
    }
    
    var isSelf: Bool = false {
        didSet {
            if isSelf {
                addSegmentedControlEvents()
            } else {
                addRelationButton()
            }
        }
    }
    
    var selectedSegmentIndex: Int {
        return segmentedControl.selectedSegmentIndex
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 15
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private func configure() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        addSubview(stackView)
        stackView.fillSuperview()
        stackView.addArrangedSubview(headerUserInfo)
        stackView.addArrangedSubview(countersStackView)
        headerUserInfo.delegate = self
        countersStackView.delegate = self
        headerUserInfo.heightAnchor.constraint(equalToConstant: 160).isActive = true
        countersStackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func set(model: ProfileTableTitleViewViewModel) {
        headerUserInfo.set(model: model.headerUserInfoViewModel)
        countersStackView.set(viewModel: model.counterStackViewViewModel)
        
        model.isSubscribtion == nil ? (headerUserInfo.subtitleImage = UIImage(named: "edit")) : (headerUserInfo.subtitleImage = UIImage(named: "ic_info"))
        
        if isSelf {
            return
        }
        
        isSubscribtion = model.isSubscribtion
    }
    
    private func addRelationButton() {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(relationButton)
        relationButton.fillSuperview(padding: UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 16))
        stackView.addArrangedSubview(view)
        view.heightAnchor.constraint(equalToConstant: 70).isActive = true
    }
    
    private func updateAppearanceButton() {
        guard let isSubscribtion = isSubscribtion  else { return }
        if isSubscribtion {
            relationButton.setTitle("Вы подписаны", for: .normal)
            relationButton.layer.borderWidth = 2.0
            relationButton.setTitleColor(OrangeTheme.myconvyOrange, for: .normal)
            relationButton.layer.borderColor = OrangeTheme.myconvyOrange.cgColor
            relationButton.backgroundColor = .white
        } else {
            relationButton.setTitle("Подписаться", for: .normal)
            relationButton.setTitleColor(.white, for: .normal)
            relationButton.backgroundColor = OrangeTheme.myconvyOrange
        }
    }
    
    private func addSegmentedControlEvents() {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(segmentedControl)
        segmentedControl.fillSuperview(padding: UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 16))
        stackView.addArrangedSubview(view)
        view.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    @objc private func handleRelationButtonTap() {
        relationButton.isEnabled = false
        isSubscribtion! ? delegate?.profileTableHeaderViewUnsubscribe?(self) : delegate?.profileTableHeaderViewSubscribe?(self)
    }
    
    func subscribe() {
        isSubscribtion = true
        countersStackView.subscribe()
        relationButton.isEnabled = true
    }
    
    func unsubscribe() {
        isSubscribtion = false
        countersStackView.unsubscribe()
        relationButton.isEnabled = true
    }
    
}

// MARK: - HeaderUserInfoDelegate

extension ProfileTableHeaderView: HeaderUserInfoDelegate {
    
    func headerUserInfoMoreInfoButtonPressed(_ view: HeaderUserInfo) {
        delegate?.profileTableHeaderViewMoreInfoButtonPressed(self)
    }
    
    func headerUserInfoAvatarImageViewPressed(_ view: HeaderUserInfo, imageView: UIImageView) {
        delegate?.profileTableHeaderViewAvatarImageViewPressed(self, imageView: imageView)
    }
}

// MARK: - CountersStackViewDelegate

extension ProfileTableHeaderView: CountersStackViewDelegate {
    func countersStackViewDidEventsPressed(_ view: CountersStackView) {
        delegate?.profileTableHeaderViewDidEventsPressed(self)
    }
    
    func countersStackViewDidSubscribersPressed(_ view: CountersStackView) {
        delegate?.profileTableHeaderViewDidSubscribersPressed(self)
    }
    
    func countersStackViewDidSubscriptionsPressed(_ view: CountersStackView) {
        delegate?.profileTableHeaderViewDidSubscriptionsPressed(self)
    }
}
