//
//  CounterItem.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

final class CounterItem: UIView {
    
    private let countLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.textColor = UIColor.black.withAlphaComponent(0.5)
        label.textAlignment = .center
        return label
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue", size: 14)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        label.textAlignment = .center
        return label
    }()
    
    var count: Int = 0 {
        didSet {
            countLabel.text = String(count)
        }
    }
    
    init(title: String) {
        super.init(frame: .zero)
        titleLabel.text = title
        titleLabel.sizeToFit()
        configure()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    private func configure() {
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(countLabel)
        addSubview(titleLabel)
        
        countLabel.anchor(top: topAnchor,
                          leading: leadingAnchor,
                          bottom: nil,
                          trailing: trailingAnchor)
        countLabel.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        titleLabel.anchor(top: countLabel.bottomAnchor,
                          leading: leadingAnchor,
                          bottom: nil,
                          trailing: trailingAnchor,
                          padding: UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0))
        titleLabel.heightAnchor.constraint(equalToConstant: 22).isActive = true
    }
    
}
