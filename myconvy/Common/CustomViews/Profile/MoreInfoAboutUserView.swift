//
//  MoreInfoAboutUserView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 27/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol MoreInfoAboutUserViewDelegate: class {
    func moreInfoAboutUserViewInfoButtonPressed(_ view: MoreInfoAboutUserView)
}

final class MoreInfoAboutUserView: UIView {
    
    weak var delegate: MoreInfoAboutUserViewDelegate?
    
    private let otherInfoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "HelveticaNeue", size: 14)
        label.textColor = UIColor.black.withAlphaComponent(0.25)
        label.textAlignment = .center
        return label
    }()
       
    private let moreInfoButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(moreInfoButtonPressed), for: .touchUpInside)
        button.tintColor = OrangeTheme.myconvyGray
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        button.heightAnchor.constraint(equalToConstant: 27).isActive = true
        button.widthAnchor.constraint(equalToConstant: 27).isActive = true
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        configure()
    }
    
    var text: String? {
        get {
            return otherInfoLabel.text
        }
        set {
            otherInfoLabel.text = newValue
            invalidateIntrinsicContentSize()
        }
    }
    
    var iconImage: UIImage? {
        set {
            moreInfoButton.setImage(newValue, for: .normal)
        }
        get {
            moreInfoButton.image(for: .normal)
        }
    }
    
    private func configure() {
        addSubview(otherInfoLabel)
        addSubview(moreInfoButton)
        translatesAutoresizingMaskIntoConstraints = false
        
        otherInfoLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        otherInfoLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        
        moreInfoButton.centerYAnchor.constraint(equalTo: otherInfoLabel.centerYAnchor).isActive = true
        moreInfoButton.leadingAnchor.constraint(lessThanOrEqualTo: otherInfoLabel.trailingAnchor, constant: 8).isActive = true
        moreInfoButton.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: otherInfoLabel.intrinsicContentSize.width + moreInfoButton.intrinsicContentSize.width + 15, height: moreInfoButton.intrinsicContentSize.height)
    }
    
    @objc private func moreInfoButtonPressed() {
        delegate?.moreInfoAboutUserViewInfoButtonPressed(self)
    }
}
