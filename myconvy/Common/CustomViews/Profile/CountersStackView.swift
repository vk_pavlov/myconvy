//
//  CountersStackView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/11/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

protocol CountersStackViewDelegate: class {
    func countersStackViewDidEventsPressed(_ view: CountersStackView)
    func countersStackViewDidSubscribersPressed(_ view: CountersStackView)
    func countersStackViewDidSubscriptionsPressed(_ view: CountersStackView)
}

final class CountersStackView: UIView {
    
    weak var delegate: CountersStackViewDelegate?
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        return stackView
    }()
    
    private let eventsCounter = CounterItem(title: "События")
    private let subscribersCounter = CounterItem(title: "Подписчики")
    private let subscriptionsCounter = CounterItem(title: "Подписки")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    private func configure() {
        addSubview(stackView)
        isUserInteractionEnabled = true
        translatesAutoresizingMaskIntoConstraints = false
        stackView.fillSuperview(padding: UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16))
        
        stackView.addArrangedSubview(eventsCounter)
        stackView.addArrangedSubview(subscribersCounter)
        stackView.addArrangedSubview(subscriptionsCounter)
        
        let eventsTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleEventsTap(_:)))
        eventsTapRecognizer.delegate = self
        eventsCounter.addGestureRecognizer(eventsTapRecognizer)
        
        let subscribersTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleSubscribersTap(_:)))
        subscribersTapRecognizer.delegate = self
        subscribersCounter.addGestureRecognizer(subscribersTapRecognizer)
        
        let subscriptionsTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleSubscriptionsTap(_:)))
        subscriptionsTapRecognizer.delegate = self
        subscriptionsCounter.addGestureRecognizer(subscriptionsTapRecognizer)
        
    }
    
    func set(viewModel: CountersStackViewViewModel) {
        eventsCounter.count = viewModel.eventsCount
        subscribersCounter.count = viewModel.subscribersCount
        subscriptionsCounter.count = viewModel.subscriptionsCount
    }
    
    func subscribe() {
        subscribersCounter.count += 1
    }
    
    func unsubscribe() {
        subscribersCounter.count -= 1
    }
    
}

extension CountersStackView: UIGestureRecognizerDelegate {
    
    @objc func handleEventsTap(_ sender: UITapGestureRecognizer?) {
        delegate?.countersStackViewDidEventsPressed(self)
    }
    
    @objc func handleSubscribersTap(_ sender: UITapGestureRecognizer?) {
        delegate?.countersStackViewDidSubscribersPressed(self)
    }
    
    @objc func handleSubscriptionsTap(_ sender: UITapGestureRecognizer?) {
        delegate?.countersStackViewDidSubscriptionsPressed(self)
    }
    
}



