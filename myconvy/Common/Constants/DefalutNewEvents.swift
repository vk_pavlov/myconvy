//
//  DefalutNewEvents.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/01/2020.
//  Copyright © 2020 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

let futureDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())!

let defaultCategories: [[NewEventViewModel]] = [
    // Прогулки
    [
        NewEventViewModel(title: "Все на прогулку в ПГ! 👋",
                          place: Place(lat: 55.731497, lng: 37.603742),
                          date: futureDate.timeIntervalSince1970,
                          category: 8,
                          descriptionEvent: "Предлагаю собрать компанию и погулять в Парке Горького, одном из лучших мест Москвы! 👀",
                          descriptionPlace: "Встречаемся у главного входа в парк, возле арки.",
                          sex: 0,
                          partLimit: 0,
                          privacy: 0),
        NewEventViewModel(title: "Кто со мной гулять? 🏃‍♀️🏃‍♂️",
                          place: Place(lat: 55.757818, lng: 37.616472),
                          date: futureDate.timeIntervalSince1970,
                          category: 8,
                          descriptionEvent: "Красная площадь - идеальное место для прогулки! Предлагаю вместе со мной полюбоваться красотами Кремля.",
                          descriptionPlace: "Встречаемся на выходе со станции метро «Охотный ряд",
                          sex: 0,
                          partLimit: 0,
                          privacy: 0),
        NewEventViewModel(title: "Ребята, гуляем! 🙌",
                          place: Place(lat: 55.822945, lng: 37.640000),
                          date: futureDate.timeIntervalSince1970,
                          category: 8,
                          descriptionEvent: "Погуляем на ВДНХ? Там всегда интересно. Кто со мной? 😉",
                          descriptionPlace: "Встречаемся у памятника покорителям космоса (ракета).",
                          sex: 0,
                          partLimit: 0,
                          privacy: 0)
    ],
    // Знакомства
    [
        NewEventViewModel(title: "Кто хочет встретиться, пообщаться? 🙃",
                          place: Place(lat: 55.764164, lng: 37.592791),
                          date: futureDate.timeIntervalSince1970,
                          category: 4,
                          descriptionEvent: "Можем засесть в одной из уютных кафешек на Патриарших прудах и пообщаться в непринужденной обстановке 👍",
                          descriptionPlace: "Патриаршие - хорошее место для общения 😉",
                          sex: 0,
                          partLimit: 0,
                          privacy: 0),
        NewEventViewModel(title: "Давайте общаться! 😎",
                          place: Place(lat: 55.747283, lng: 37.581707),
                          date: futureDate.timeIntervalSince1970,
                          category: 4,
                          descriptionEvent: "Как вариант, предлагаю совершить прогулку по старому Арбату. Там обычно весело!",
                          descriptionPlace: "Встретимся у Макдональдса на выходе из метро Смоленская и двинемся гулять по Арбату 🏃‍♂️🏃‍♀️",
                          sex: 0,
                          partLimit: 0,
                          privacy: 0),
        NewEventViewModel(title: "Кто любит новые знакомства? 🤗",
                          place: Place(lat: 55.789226, lng: 37.679891),
                          date: futureDate.timeIntervalSince1970,
                          category: 4,
                          descriptionEvent: "Можно погулять в Сокольниках. Там бывает довольно мило. Пообщаемся! 😃Подробности встречи обсудим в чате 👌",
                          descriptionPlace: "Встречаемся на выходе метро Сокольники, а там уже выдвинемся в сторону парка.",
                          sex: 0,
                          partLimit: 0,
                          privacy: 0)
    ],
    // Спорт
    [
        NewEventViewModel(title: "Го растрясывать жирок на пробежку 💪",
                          place: Place(lat: 55.731484, lng: 37.603679),
                          date: futureDate.timeIntervalSince1970,
                          category: 1,
                          descriptionEvent: "Физкульт-привет всем бегунам! 👋В одиночку бегать скучно, давайте объединяться!",
                          descriptionPlace: "Встретимся на входе в парк и побежим по набережной. Подробности в чате",
                          sex: 0,
                          partLimit: 0,
                          privacy: 0),
        NewEventViewModel(title: "Собираю компанию для пробежки 🏃‍♂️",
                          place: Place(lat: 55.702285, lng: 37.529386),
                          date: futureDate.timeIntervalSince1970,
                          category: 1,
                          descriptionEvent: "Хочу устроить коллективный совместный забег по околоМГУшным паркам. Студенты и все все все, выползайте из нор и пошли оздоровляться вместе 🤪",
                          descriptionPlace: "Встретимся у главного здания МГУ, а там уже решим насчет конкретного маршрута.",
                          sex: 0,
                          partLimit: 0,
                          privacy: 0)
    ]

]
