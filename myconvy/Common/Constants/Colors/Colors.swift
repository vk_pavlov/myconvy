//
//  File.swift
//  myconvy
//
//  Created by Евгений Каштанов on 08/11/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

struct OrangeTheme {
    static let myconvyOrange = UIColor.hexStringToUIColor(hex: "#FCA54B")
    static let myconvyRedNotification = UIColor(red: 239.0 / 255.0, green: 0.0 / 255.0, blue: 59.0 / 255.0, alpha: 1.0)
    static let myconvyYellowTranslucent = UIColor(red: 248.0 / 255.0, green: 231.0 / 255.0, blue: 28.0 / 255.0, alpha: 0.1)
    static let myconvyGray = UIColor.black.withAlphaComponent(0.25)
    static let mvGray = UIColor(red: 235/255, green: 237/255, blue: 241/255, alpha: 1)
}
