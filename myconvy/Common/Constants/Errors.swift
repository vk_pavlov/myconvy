//
//  Errors.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 18/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

enum NetworkError {
    enum JoinMeetById: Int, Error {
        case youHaveAlreadyJoinedThisMeet = 301
        case youAreDeniedThisMeet = 302
        case tooMuchParticipants = 303
        case tooLateToJoin = 304
        case genderIncorrect = 305
        case unknownError = 399
    }
    enum Common: Int, Error {
        case tokenIsIncorrect = 104
    }

    enum EditUserRel: Int, Error {
        case userBannedYouOrWasBannedFromYou = 201
        case youAreNotSubscribedOnThisUser = 202
        case youDidNotBanThisUser = 203
        case unknownError = 299
    }

    enum EditMeetById: Int, Error {
        case youCannotEditThisMeet = 306
    }

    enum EditMeetPartRel: Int, Error {
        case youCannotEditThisMeet = 307
        case userAreAlreadyParticipantOrAreNotInvolved = 308
        case userAreAlreadyBlockedOrAreNotInvolved = 309
        case unknownError = 399
    }

    enum DelMeetById: Int, Error {
        case youCannotDeleteThisMeet = 310
    }

    enum LeaveMeet: Int, Error {
        case youAreCreator = 311
        case youAreNotAparticipantOrOrderman = 312
    }

    enum SendReport: Int, Error {
        case sendMeetIdOrUserId = 901
    }

    enum GetUserOrEventById: Int, Error {
        case eventNil = 110
        case userNil = 111
    }

}

// MARK: - CustomStringConvertible

extension NetworkError.JoinMeetById: CustomStringConvertible {
    var description: String {
        switch self {
        case .genderIncorrect: return "Событие не для твоего пола, извини"
        case .youHaveAlreadyJoinedThisMeet: return "Ты уже присоединился к этому событию"
        case .youAreDeniedThisMeet: return "Тебя исключили из этого события или отказали в заявкe"
        case .tooMuchParticipants: return "Достигнут лимит участников события"
        case .tooLateToJoin: return "Событие уже началось, слишком поздно присоединился"
        case .unknownError: return "Неизвестная ошибка"
        }

    }
}

extension NetworkError.Common: CustomStringConvertible {
    var description: String {
        switch self {
        case .tokenIsIncorrect: return "Токен некорректен"
        }

    }
}

extension NetworkError.EditUserRel: CustomStringConvertible {
    var description: String {
        switch self {
        case .userBannedYouOrWasBannedFromYou: return "Попытка подписаться на пользователя, который забанен тобой или забанил тебя"
        case .youAreNotSubscribedOnThisUser: return "Попытка отписаться от пользователя, на которого не был подписан"
        case .youDidNotBanThisUser: return "Попытка разбанить незабаненного пользователя"
        case .unknownError: return "Неизвестная ошибка"
        }
    }
}

extension NetworkError.EditMeetById: CustomStringConvertible {
    var description: String {
        switch self {
        case .youCannotEditThisMeet: return "Попытка редактировать чужое событие"
        }
    }
}

extension NetworkError.EditMeetPartRel: CustomStringConvertible {
    var description: String {
        switch self {
        case .youCannotEditThisMeet: return "Попытка редактировать участников чужого события"
        case .userAreAlreadyParticipantOrAreNotInvolved: return "Попытка принять заявку на событие от участника или вообще левого чела"
        case .userAreAlreadyBlockedOrAreNotInvolved: return "Попытка заблокировать уже заблокированного или вообще левого пользователя"
        case .unknownError: return "Неизвестная ошибка"
        }
    }
}

extension NetworkError.DelMeetById: CustomStringConvertible {
    var description: String {
        switch self {
        case .youCannotDeleteThisMeet: return "Попытка удалить чужое событие"
        }
    }
}

extension NetworkError.LeaveMeet: CustomStringConvertible {
    var description: String {
        switch self {
        case .youAreCreator: return "Попытка покинуть событие, когда ты создатель (нельзя)"
        case .youAreNotAparticipantOrOrderman: return "Попытка покинуть событие не участнику и не подавшего заявку"
        }
    }
}

extension NetworkError.SendReport: CustomStringConvertible {
    var description: String {
        switch self {
        case .sendMeetIdOrUserId: return "Попытка отправить жалобу без цели (пользователя или события)"
        }
    }
}
