//
//  Schemes.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation


enum AppScheme: String {
    case vk = "vk://"
    case inst = "instagram://"
    case email = "mailto://"
}
