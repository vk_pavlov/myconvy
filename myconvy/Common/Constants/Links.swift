//
//  Links.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 08/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

struct Links {
    static let privacyPolicy = "https://injoinapp.com/privacy/"
    static let termsOfUse = "https://injoinapp.com/terms/"
    static let vkontakte = "myconvy"
    static let instagram = "myconvy"
    static let email = "contact@myconvy.com"
}

struct DefaultLinksShared {
    static let user = "https://injoinapp.com/go.php?user="
    static let event = "https://injoinapp.com/go.php?event="
}
