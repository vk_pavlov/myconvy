//
//  Categories.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 19/12/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

let categories = [
    "Кино",
    "Спорт",
    "Искусство",
    "Путешествия",
    "Знакомства",
    "Концерты",
    "Образование",
    "Тусовки",
    "Прогулки",
    "Другое"
]

let desires = [
    "Хочу гулять",
    "Хочу познакомиться",
    "Хочу заняться спортом"
]
