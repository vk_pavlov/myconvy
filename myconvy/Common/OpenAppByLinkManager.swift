//
//  OpenAppByLinkManager.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 22/07/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

enum ExternalLink: String {
    case user
    case event
}

class OpenAppByLinkManager {

    var fromVC: UIViewController!

    let selfUser = CommonData.selfUser()

    func presentViewControllerIfNeeded(typeLink: ExternalLink, value: String) {

        fromVC = UIApplication.getTopViewController()

        switch typeLink {
        case .user:
            presentSelectedUser(id: value)
        case .event:
            presentSelectedEvent(id: value)
        }

    }

    private func presentSelectedUser(id: String) {
        if selfUser.id == Int(id) {
            ProfileModule.create().present(from: fromVC)
        } else {
            UserProfileModule.create().present(from: fromVC, userId: Int(id)!)
        }
    }

    private func presentSelectedEvent(id: String) {
        SelectedEventModule.create().present(from: fromVC, eventId: Int(id)!)
    }
}
