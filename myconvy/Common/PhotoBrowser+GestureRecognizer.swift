//
//  PhotoBrowser+GestureRecognizer.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 17/07/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

protocol PhotoBrowserDelegate: class {
    func presentImageFullScreen()
    func dismisImage()

}

class PhotoBrowserAnimator {

    private var presenter: UIView!
    private var pinchGestureRecognizer: UIPinchGestureRecognizer!
    var pinchGestureAnchorScale: CGFloat?

    weak var delegate: PhotoBrowserDelegate?

    let maxScale: CGFloat = 4.0
    let minScale: CGFloat = 1.0
    var imageViewScale: CGFloat = 1.0

    var newImageView: UIImageView!

    init(presenter: UIView) {
        self.presenter = presenter
    }

    func presentImage(image: UIImageView) {

        newImageView = UIImageView(image: image.image)
        newImageView.frame = CGRect(x: presenter.center.x, y: presenter.center.y, width: 0, height: 0)
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture(_ :)))
        let myPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(myPanAction(recognizer:)))
        myPanGestureRecognizer.minimumNumberOfTouches = 1
        myPanGestureRecognizer.maximumNumberOfTouches = 1
        newImageView.addGestureRecognizer(pinchGestureRecognizer)
        newImageView.addGestureRecognizer(tap)
        newImageView.addGestureRecognizer(myPanGestureRecognizer)


        presenter.addSubview(newImageView)
        //        self.navigationController?.isNavigationBarHidden = true
        //        self.tabBarController?.tabBar.isHidden = true
        let toFrame = CGRect(x: 0, y: 0, width: presenter.frame.width, height: presenter.frame.height)
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            options: [],
            animations: {
                self.delegate?.presentImageFullScreen()
        },
            completion: { t in
                UIView.animate(
                    withDuration: 0.3,
                    delay: 0,
                    options: [],
                    animations: {
                        self.newImageView.frame = toFrame
                },
                    completion: nil)
        })



    }

    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: [],
            animations: {
                sender.view!.alpha = 0.0
        }, completion: { (true) in
            sender.view?.removeFromSuperview()
        })
        delegate?.dismisImage()

    }

    @objc func handlePinchGesture(_ gestureRecognizer: UIPinchGestureRecognizer) {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            let pinchScale: CGFloat = gestureRecognizer.scale
            if imageViewScale * pinchScale < maxScale && imageViewScale * pinchScale > minScale {
                imageViewScale *= pinchScale
                newImageView.transform = (newImageView.transform.scaledBy(x: pinchScale, y: pinchScale))
            }
            gestureRecognizer.scale = 1.0
        }
    }

    @objc func myPanAction(recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.newImageView)
        if let myView = recognizer.view {
            myView.center = CGPoint(x: myView.center.x + translation.x, y: myView.center.y + translation.y)
        }
        recognizer.setTranslation(CGPoint(x: 0, y: 0), in: self.newImageView)
    }
}


