//
//  CommonData.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 15/12/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import RealmSwift
import Locksmith

class CommonData {
    
    private init() {}
    
    static let shared = CommonData()
    
    static func selfUser() -> User {
        let realm = try! Realm()
        return realm.objects(User.self).first!
    }
    
    static func accesToken() -> String {
        let dataMyconvy = Locksmith.loadDataForUserAccount(userAccount: "myconvy")
        let token = dataMyconvy!["myconvy_token"] as! String
        return token
    }
    
}
