//
//  Presentable.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 30/10/2018.
//  Copyright © 2018 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

protocol Presentable {
    var viewController: UIViewController { get }
    func present()
    func present(fromViewController viewController: UIViewController)
    func presentModal(fromViewController viewController: UIViewController)
    func dissmissModal()
    func dissmiss()
    func presentNavigationController(fromViewController viewController: UIViewController)
    func presentViewController(viewController: UIViewController)
}

extension Presentable where Self: UIViewController {
    var viewController: UIViewController {
        return self
    }
    
    func presentViewController(viewController: UIViewController) {
        present(viewController, animated: true, completion: nil)
    }
    
    func present() {
        AppDelegate.currentWindow.rootViewController = viewController
    }
    
    func present(fromViewController viewController: UIViewController) {
        viewController.navigationController?.pushViewController(self, animated: true)
    }
    
    func presentModal(fromViewController viewController: UIViewController) {
        viewController.present(self, animated: true, completion: nil)
    }
    
    func dissmissModal() {
        dismiss(animated: true, completion: nil)
    }
    
    func presentNavigationController(fromViewController viewController: UIViewController) {
        let navController = UINavigationController(rootViewController: self)
        viewController.present(navController, animated: true, completion: nil)
    }

    func dissmiss() {
        let _ = navigationController?.popViewController(animated: true)
    }
}
