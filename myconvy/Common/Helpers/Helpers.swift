//
//  Helpers.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 28/03/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

class Helpers {

    static func getBirthDateInFormat(initialBirthDate: String, resultDateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        let initialDateFormat = "yyyy-mm-dd"
        dateFormatter.dateFormat = initialDateFormat
        let initialDate = dateFormatter.date(from: initialBirthDate)
        dateFormatter.dateFormat = resultDateFormat
        return dateFormatter.string(from: initialDate ?? Date()) 
    }

    static func stringToArray(_ data: String) -> [String] {
        if data.isEmpty {
            return [String]()
        }
        var resultArray = data.components(separatedBy: ";")
        resultArray.removeLast()
        return resultArray
    }

    static func createAlertWithError(error: String) -> UIAlertController {
        let alert = UIAlertController(title: "Ошибка", message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ок", style: .cancel, handler: nil)
        alert.addAction(action)
        return alert
    }

    static func arrayToString(array: [String]) -> String {
        let res = array.joined(separator: ";") + ";"
        return res.count == 1 ? "" : res
    }

    static func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }


//    static func createErrorAlert(message: String) -> UIAlertController {
//
//    }

    static func createWarningAlert(message: String, completion: @escaping () -> ()) -> UIAlertController {
        let alertViewController = UIAlertController(title: "Внимание", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Да", style: .default) { (UIAlertAction) in
            completion()
        }
        let alertCancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alertViewController.addAction(action)
        alertViewController.addAction(alertCancel)
        return alertViewController
    }

    static func openLinkInOtherApp(link: String, vc: UIViewController, scheme: AppScheme) {
        let schemeApp = scheme.rawValue
        if let _ = URL(string: schemeApp) {
            var domen = ""
            if scheme == .vk {
                domen = "vk.com/"
            } else if scheme == .inst {
                domen = "user?username="
            }
            let appSchemeUrl = URL(string: (schemeApp + domen + link).trimmingCharacters(in: .whitespaces))
            if UIApplication.shared.canOpenURL(appSchemeUrl! as URL) {
                UIApplication.shared.open(appSchemeUrl!, options: [:], completionHandler: nil)
            } else {
                let alert = UIAlertController(title: "Ошибка", message: "", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Ок", style: .default, handler: nil)
                alert.addAction(cancelAction)
                vc.present(alert, animated: true, completion: nil)
            }
        }
    }
}
