//
//  NSMutableAttributedString.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/03/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            
            self.addAttribute(.link, value: linkURL, range: foundRange)
            
            return true
        }
        return false
    }

    func addString(add: String) {
        let mutableString = NSMutableAttributedString(string: " \"\(add)\"")
        let myRange = NSRange(location: 1, length: add.count + 2)
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 13)
        ]
        mutableString.addAttributes(attributes, range: myRange)
        self.append(mutableString)
    }
}
