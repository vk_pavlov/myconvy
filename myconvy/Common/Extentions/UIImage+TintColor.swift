//
//  UIImage+TintColor.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 21/01/2020.
//  Copyright © 2020 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

extension UIImage {
    func imageWithColor(_ color: UIColor, newSize: CGSize) -> UIImage? {
        var image: UIImage? = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(newSize, false, scale)
        color.set()
        image?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
