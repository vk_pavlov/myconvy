//
//  UIView.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 10/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

extension UIView
{
    func fixInView(_ container: UIView!) -> Void {
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
    
    func removeFromView(view: UIView, result: (() -> ())?) {
        if self.isDescendant(of: view) {
            self.removeFromSuperview()
            if let res = result {
                res()
            }
        }
    }
    
    func addToView(view: UIView, result: (() -> ())?) {
        if !self.isDescendant(of: view) {
            view.addSubview(self)
            if let res = result {
                res()
            }
        }

    }
}
