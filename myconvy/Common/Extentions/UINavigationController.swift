//
//  UINavigationController.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 11/04/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

extension UINavigationController {

    func previousViewController() -> UIViewController? {
        
        let lenght = self.viewControllers.count
        
        let previousViewController: UIViewController? = lenght >= 2 ? self.viewControllers[lenght-2] : nil
        
        return previousViewController
    }
}
