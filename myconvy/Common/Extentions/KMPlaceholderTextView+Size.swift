//
//  KMPlaceholderTextView+Size.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 01/02/2020.
//  Copyright © 2020 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import KMPlaceholderTextView

extension KMPlaceholderTextView {
    open override var intrinsicContentSize: CGSize {
        return CGSize(width: max(frame.width, 50), height: max(frame.height, 40))
    }
}
