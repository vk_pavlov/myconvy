//
//  UISearchBar.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 28/02/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit

extension UISearchBar {
    
    func setTextFieldColor() {
        
        if let textfield = value(forKey: "searchField") as? UITextField {
            //textfield.textColor = // Set text color
            if let backgroundview = textfield.subviews.first {
                
                // Background color
                backgroundview.backgroundColor = .white
                // Rounded corner
                //backgroundview.layer.borderWidth = 1.0
                //backgroundview.layer.borderColor = UIColor.myconvyOrange.cgColor
                backgroundview.layer.cornerRadius = 10;
                backgroundview.clipsToBounds = true;
                
            }
        }
    }
    
    
}
