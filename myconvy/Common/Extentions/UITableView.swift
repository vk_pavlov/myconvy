//
//  File.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 20/02/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import UIKit

extension UITableView {
    func updateHeaderViewHeight() {
        guard let headerView = self.tableHeaderView else {
            return
        }
        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            self.tableHeaderView = headerView
            self.layoutIfNeeded()
        }
    }
}
