//
//  Date.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 10/02/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation

enum FormatForDate: String {
    case commonDateFormat = "yyyy-MM-dd HH:mm:ss"
    case eventCellDateFormat = ""
    case userBirthDateFormat = "dd.mm.yyyy"
    case notificationDate = "EEEEE HH:mm"
}


extension Date {
    static func stringToDate(_ stringDate: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.date(from: stringDate)
        return result 
    }
    
    func getDateAsStringWithFormat(dateFormat: String) -> String {
        let dateFormatterPrint = DateFormatter()
        let locale = Locale(identifier: "rus")
        dateFormatterPrint.locale = locale
        dateFormatterPrint.dateFormat = dateFormat
        return dateFormatterPrint.string(from: self)
    }

    func toString(format: FormatForDate) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = format.rawValue
        return dateFormatter.string(from: self)
    }
    
}
