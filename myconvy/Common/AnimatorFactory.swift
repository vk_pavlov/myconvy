//
//  AnimatorFactory.swift
//  myconvy
//
//  Created by Vyacheslav Pavlov on 31/03/2019.
//  Copyright © 2019 Vyacheslav Pavlov. All rights reserved.
//

import Foundation
import UIKit


enum AnimatorFactory {
    static func makeMoveUpWithBounce(rowHeight: CGFloat, duration: TimeInterval, delayFactor: Double) -> Animation {
        return { cell, indexPath, tableView in
            cell.transform = CGAffineTransform(translationX: 0, y: rowHeight)

            UIView.animate(
                withDuration: duration,
                delay: delayFactor * Double(indexPath.row),
                usingSpringWithDamping: 0.4,
                initialSpringVelocity: 0.1,
                options: [.curveEaseInOut],
                animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0)
            })
        }
    }
}
